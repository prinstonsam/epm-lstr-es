import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt.Keys._
import sbt._

object Settings {
  val baseName = "student-management"
  val baseOrganization = "com.epam"
  val baseVersion = "0.1.0"

  val commonSettings = Seq(
    organization := baseOrganization,
    version := baseVersion,
    isSnapshot := true,
    scalaVersion := "2.11.8",
    scalacOptions := Seq(
      "-target:jvm-1.8",
      "-Xlint",
      "-unchecked",
      "-deprecation",
      "-feature",
      "-Ywarn-unused-import",
      "-Yno-adapted-args",
      "-Ywarn-numeric-widen",
      "-Ywarn-value-discard",
      "-Xcheckinit",
      "-encoding", "UTF-8",
//      "-Xfatal-warnings",
      "-Yinline-warnings",
//      "-Ywarn-dead-code",
      "-Xfuture"
    ),
    logBuffered := false,
    parallelExecution in Test := false,
    testOptions in Test <+= name[Task[TestOption]] {
      n => inlineTask(Tests.Argument(TestFrameworks.ScalaTest, "-f", n + "-test.txt", "-o"))
    },
    dependencyOverrides += "org.webjars" % "jquery" % versions.jQuery
  )

  object versions {
    val config = "1.3.0"

    val slf4j = "1.6.4"
    val logback = "1.1.7"

    val akka = "2.4.9"

    // upickle
    val upickle = "0.4.1"

    // DB
    val slick = "3.2.0-M1"
    val postgresql = "9.1-901.jdbc4"

    val async = "0.9.6-RC2"

    // test
    val scalactic = "3.0.0"
    val scalatest = "3.0.0"
    val postgresqlEmbedded = "1.15"
    val flyway = "4.0.3"
    val mockito = "2.0.106-beta"
    val macwire = "2.2.3"

    //client
    val scalaDom = "0.9.1"
    val scalajsReact = "0.11.1"
    val scalaCSS = "0.4.1"
    val log4js = "1.4.13-1"
    val diode = "1.0.0"
    val uTest = "0.4.3"
    val `scalajs-react-components` = "0.5.0"

    val react = "15.3.0"
    val jQuery = "2.2.4"
    val scalaJsJQuery = "0.9.0"
    val bootstrap = "3.3.7-1"
    val chartjs = "2.1.3"

    val fontAwesome = "4.6.3"

    val moment = "2.12.0"

    val `scalajs-java-time` = "0.2.0"
    val `scala-java-time` = "2.0.0-M2"
    val `scala-java-locales` = "0.3.0-cldr29"
    val `scala-js-momentjs` = "0.1.5"

    val paradise = "2.1.0"
  }

  object dependencies {
    val jvmTest = libraryDependencies ++= Seq(
      "org.scalactic" %% "scalactic" % versions.scalactic % "test",
      "org.scalatest" %% "scalatest" % versions.scalatest % "test"
    )

    val mockito = libraryDependencies += "org.mockito" % "mockito-core" % versions.mockito % "test"

    val dbTest = libraryDependencies ++= Seq(
      "ru.yandex.qatools.embed" % "postgresql-embedded" % versions.postgresqlEmbedded % "test",
      "org.flywaydb" % "flyway-core" % versions.flyway % "test"
    )

    val db = libraryDependencies ++= Seq(
      "com.typesafe.slick" %% "slick" % versions.slick,
      "postgresql" % "postgresql" % versions.postgresql
    )

    val slf4j = libraryDependencies ++= Seq(
      "org.slf4j" % "slf4j-api" % versions.slf4j,
      "ch.qos.logback" % "logback-classic" % versions.logback,
      "ch.qos.logback" % "logback-core" % versions.logback
    )

    val akkaHttp = libraryDependencies += "com.typesafe.akka" %% "akka-http-experimental" % versions.akka

    val upickle = libraryDependencies += "com.lihaoyi" %%% "upickle" % versions.upickle

    val akkaHttpTest = libraryDependencies += "com.typesafe.akka" %% "akka-http-testkit" % versions.akka

    val akkaTest = libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % versions.akka

    val akka = libraryDependencies += "com.typesafe.akka" %% "akka-actor" % versions.akka
    val akkaTyped = libraryDependencies += "com.typesafe.akka" %% "akka-typed-experimental" % versions.akka

    val macwire = libraryDependencies ++= Seq(
      "com.softwaremill.macwire" %% "macros" % versions.macwire,
      "com.softwaremill.macwire" %% "util" % versions.macwire
    )

    val async = libraryDependencies += "org.scala-lang.modules" %% "scala-async" % versions.async

    val config = libraryDependencies += "com.typesafe" % "config" % versions.config

    val assets = libraryDependencies ++= Seq(
      "org.webjars" % "font-awesome" % versions.fontAwesome,
      "org.webjars" % "bootstrap" % versions.bootstrap % Provided
    )

    val scalajs = libraryDependencies ++= Seq(
      "com.github.japgolly.scalajs-react" %%% "core" % versions.scalajsReact,
      "com.github.japgolly.scalajs-react" %%% "extra" % versions.scalajsReact,
      "com.github.japgolly.scalacss" %%% "ext-react" % versions.scalaCSS,
      "me.chrons" %%% "diode" % versions.diode,
      "me.chrons" %%% "diode-react" % versions.diode,
      "org.scala-js" %%% "scalajs-dom" % versions.scalaDom,
      "be.doeraene" %%% "scalajs-jquery" % versions.scalaJsJQuery,
      "com.lihaoyi" %%% "utest" % versions.uTest % Test,
      "io.github.widok" %%% "scala-js-momentjs" % versions.`scala-js-momentjs`
    )

    val js = jsDependencies ++= Seq(
      "org.webjars.bower" % "react" % versions.react / "react-with-addons.js" minified "react-with-addons.min.js" commonJSName "React",
      "org.webjars.bower" % "react" % versions.react / "react-dom.js" minified "react-dom.min.js" dependsOn "react-with-addons.js" commonJSName "ReactDOM",
      "org.webjars" % "jquery" % versions.jQuery / "jquery.js" minified "jquery.min.js",
      "org.webjars" % "bootstrap" % versions.bootstrap / "bootstrap.js" minified "bootstrap.min.js" dependsOn "jquery.js",
      "org.webjars" % "chartjs" % versions.chartjs / "Chart.js" minified "Chart.min.js",
      "org.webjars" % "log4javascript" % versions.log4js / "js/log4javascript_uncompressed.js" minified "js/log4javascript.js",
      "org.webjars.npm" % "moment" % versions.moment / s"${versions.moment}/moment.js" minified "min/moment.min.js"
    )

    val embededPg = libraryDependencies ++= Seq(
      "ru.yandex.qatools.embed" % "postgresql-embedded" % versions.postgresqlEmbedded,
      "org.flywaydb" % "flyway-core" % versions.flyway
    )

    val `scalajs-java-time` = libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-java-time" % versions.`scalajs-java-time`,
      "io.github.soc" %%% "scala-java-time" % versions.`scala-java-time`,
      "com.github.cquiroz" %%% "scala-java-locales" % versions.`scala-java-locales`
    )

    val macroPlugin = addCompilerPlugin("org.scalamacros" % "paradise" % versions.paradise cross CrossVersion.full)
  }

}
