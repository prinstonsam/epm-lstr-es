package routes

import java.time.Instant

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import dto.{Feedback, Student, StudentIteration, StudentData, StudentWithData}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols._
import com.epam.education.management.utils.AkkaHttpPickler._
import service.protocols.{FeedbackProtocol, StudentProtocol}

class StudentRoutesTest extends WordSpec with Matchers with ScalatestRouteTest with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("StudentRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }

  lazy val studentProbe          = TestProbe("student")(testKit.system)
  lazy val studentWithDataProbe  = TestProbe("studentWithData")(testKit.system)
  lazy val studentDataProbe      = TestProbe("studentData")(testKit.system)
  lazy val studentIterationProbe = TestProbe("studentIteration")(testKit.system)
  lazy val feedbackProbe         = TestProbe("feedback")(testKit.system)
  lazy val reportProbe           = TestProbe("report")(testKit.system)

  lazy val studentRoutes =
    new StudentRoutes(
      ActorRef(studentProbe.ref),
      ActorRef(studentWithDataProbe.ref),
      ActorRef(studentDataProbe.ref),
      ActorRef(studentIterationProbe.ref),
      ActorRef(feedbackProbe.ref),
      ActorRef(reportProbe.ref),
      routeConfig
    ).routes

  "The StudentRoutes" should {
    "create new StudentWithData for POST request to /student" in {
      val student         = Student(Some(1), "John", "last", "mail")
      val studentData     = StudentData(1, 0, 1, Instant.now(), None)
      val studentWithData = StudentWithData(student, studentData)
      studentWithDataProbe.setAutoPilot(singleMessagePilot {
        case StudentWithDataProtocol.Create(`studentWithData`, sender) => sender ! studentWithData
      })

      Post("/student", studentWithData) ~> studentRoutes ~> check {
        responseAs[StudentWithData] shouldEqual studentWithData
      }
    }

    "retrieve all StudentsWithData for GET request to /student?date" in {
      val date = Instant.now()
      val studentWithDataSeq =
        StudentWithData(Student(Some(1), "John", "last", "mail"), StudentData(1, 1 - 1, 1, Instant.now(), None)) ::
        StudentWithData(Student(Some(2), "Jon", "last", "mail"), StudentData(2, 1, -1, Instant.now(), None)) ::
        Nil
      studentWithDataProbe.setAutoPilot(singleMessagePilot {
        case StudentWithDataProtocol.FindAll(Some(`date`), sender) => sender ! studentWithDataSeq
      })
        StudentWithData(Student(Some(1), "John", "last", "mail"),
          StudentData(1, 1, 1, date, None)) ::
          StudentWithData(Student(Some(2), "Jon", "last", "mail"),
            StudentData(2, 1, -1, date, None)) ::
          Nil
      studentWithDataProbe.setAutoPilot(singleMessagePilot {
        case StudentWithDataProtocol.FindAll(Some(`date`), sender) => sender ! studentWithDataSeq
      })

      Get(s"/student?date=$date") ~> studentRoutes ~> check {
        responseAs[Seq[StudentWithData]] shouldEqual studentWithDataSeq
      }
    }

    "retrieve all StudentsWithData for GET request to /student without date" in {
      val date = Instant.now()
      val studentWithDataSeq =
        StudentWithData(
          Student(Some(1), "John", "last", "mail"),
          StudentData(1, 1 - 1, 1, Instant.now(), None)) ::
        StudentWithData(
          Student(Some(2), "Jon", "last", "mail"),
          StudentData(2, 1, -1, Instant.now(), None)) ::
        Nil
        studentWithDataProbe.setAutoPilot(singleMessagePilot {
          case StudentWithDataProtocol.FindAll(None, sender) => sender ! studentWithDataSeq
        })
        Get("/student?date") ~> studentRoutes ~> check {
          responseAs[Seq[StudentWithData]] shouldEqual studentWithDataSeq
        }
      }

    "retrieve specific existing StudentWithData for GET request to /student/id?date" in {
      val date = Instant.now()
      val student = Student(Some(1), "John", "last", "mail")
      val studentData = StudentData(1,1-1,1,Instant.now(), None)
      val studentWithData = StudentWithData(student, studentData)
      studentWithDataProbe.setAutoPilot(singleMessagePilot {
        case StudentWithDataProtocol
        .GetByStudentId(1, Some(`date`), sender) => sender ! Some(studentWithData)
      })

      Get(s"/student/1?date=$date") ~> studentRoutes ~> check {
        responseAs[StudentWithData] shouldEqual studentWithData
      }
    }
    "retrieve specific existing StudentWithData for GET request to /student/id" in {
      val student         = Student(Some(1), "John", "last", "mail")
      val studentData     = StudentData(1, 1 - 1, 1, Instant.now(), None)
      val studentWithData = StudentWithData(student, studentData)
      studentWithDataProbe.setAutoPilot(singleMessagePilot {
        case StudentWithDataProtocol.GetByStudentId(1, None, sender) => sender ! Some(studentWithData)
      })

      Get("/student/1") ~> studentRoutes ~> check {
        responseAs[StudentWithData] shouldEqual studentWithData
      }
    }

    "return NotFound for unknown studentId for GET request to /student/id" in {
      studentWithDataProbe.setAutoPilot(singleMessagePilot {
        case StudentWithDataProtocol.GetByStudentId(666, None, sender) => sender ! None
      })

      Get("/student/666") ~> Route.seal(studentRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific student for PUT request to /student/id" in {
      val updExistingStudentId = 2
      val updExistingStudent   = Student(Some(updExistingStudentId), "John", "last", "mail")
      studentProbe.setAutoPilot(singleMessagePilot {
        case StudentProtocol.Update(`updExistingStudent`, sender) =>
          sender ! Some(updExistingStudent)
      })

      Put(s"/student/$updExistingStudentId", updExistingStudent) ~> studentRoutes ~> check {
        responseAs[Student] shouldEqual updExistingStudent
      }
    }

    "return NotFound for unknown studentId for PUT request to /student/id" in {
      val updNotExistingStudentId = 666
      val updNotExistingStudent   = Student(Some(updNotExistingStudentId), "404", "last", "mail")
      studentProbe.setAutoPilot(singleMessagePilot {
        case StudentProtocol.Update(`updNotExistingStudent`, sender) => sender ! None
      })

      Put(s"/student/$updNotExistingStudentId", updNotExistingStudent) ~> Route.seal(studentRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete specific student for DELETE request to /student/id" in {
      val delExistingStudentId = 3
      val delExistingStudent   = Student(Some(delExistingStudentId), "deleted", "last", "mail")
      studentProbe.setAutoPilot(singleMessagePilot {
        case StudentProtocol.Delete(`delExistingStudentId`, sender) =>
          sender ! Some(delExistingStudent)
      })

      Delete(s"/student/$delExistingStudentId") ~> studentRoutes ~> check {
        responseAs[Student] shouldEqual delExistingStudent
      }
    }

    "return NotFound for unknown studentId for DELETE request to /student/id" in {
      val delNotExistingStudentId = 666
      studentProbe.setAutoPilot(singleMessagePilot {
        case StudentProtocol.Delete(`delNotExistingStudentId`, sender) => sender ! None
      })

      Delete(s"/student/$delNotExistingStudentId") ~> Route.seal(studentRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "return collection of StudentData for GET request to /student/id/data" in {
      val endDate = Instant.now().plusSeconds(10000)
      val studentHistory = Seq(
        StudentData(1,studentTypeId = 1,startDate = Instant.now(), endDate = Some(endDate)),
        StudentData(1,studentTypeId = 2,startDate = endDate, endDate = None)
      )
      studentDataProbe.setAutoPilot(singleMessagePilot {
        case StudentDataProtocol.FindById(1, sender) => sender ! studentHistory
      })
      Get("/student/1/data") ~> studentRoutes ~> check {
        responseAs[Seq[StudentData]] shouldEqual studentHistory
      }
    }

    "create new StudentData for POST request to /student/id/data" in {
      val studentData = StudentData(1, studentTypeId = 1, startDate = Instant.now(), endDate = None)
      studentDataProbe.setAutoPilot(singleMessagePilot {
        case StudentDataProtocol.Create(`studentData`, sender) => sender ! studentData
      })
      Post("/student/1/data", studentData) ~> studentRoutes ~> check {
        responseAs[StudentData] shouldEqual studentData
      }
    }

    "assign specific student to iteration for POST request to /student/id/iteration" in {
      val studentId        = 99
      val studentIteration = StudentIteration(1, studentId, 1)

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.Create(`studentIteration`, sender) => sender ! studentIteration
      })

      Post(s"/student/$studentId/iteration", studentIteration) ~> studentRoutes ~> check {
        responseAs[StudentIteration] shouldEqual studentIteration
      }
    }

    "retrieve all studentIterations for GET request to /student/id/iteration" in {
      val studentId           = 33
      val studentIterationSeq = StudentIteration(1, 1, 1) :: StudentIteration(2, 2, 2) :: Nil

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.FindByStudentId(`studentId`, sender) => sender ! studentIterationSeq
      })

      Get(s"/student/$studentId/iteration") ~> studentRoutes ~> check {
        responseAs[Seq[StudentIteration]] shouldEqual studentIterationSeq
      }
    }

    "retrieve specific existing studentIteration for GET request to /student/id/iteration/id" in {
      val studentId                     = 55
      val getExistingStudentIterationId = 23
      val getExistingStudentIteration   = StudentIteration(getExistingStudentIterationId, 1, 1)

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.GetById(`getExistingStudentIterationId`, `studentId`, sender) =>
          sender ! Some(getExistingStudentIteration)
      })

      Get(s"/student/$studentId/iteration/$getExistingStudentIterationId") ~> studentRoutes ~> check {
        responseAs[StudentIteration] shouldEqual getExistingStudentIteration
      }
    }

    "return NotFound for unknown studentIteratonId for GET request to /student/id/iteration/id" in {
      val studentId                        = 55
      val getNotExistingStudentIterationId = 666

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.GetById(`getNotExistingStudentIterationId`, `studentId`, sender) => sender ! None
      })

      Get(s"/student/$studentId/iteration/$getNotExistingStudentIterationId") ~> Route.seal(studentRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific studentIteration for PUT request to /student/id/iteration/id" in {
      val studentId                     = 69
      val updExistingStudentIterationId = 96
      val updExistingStudentIteration   = StudentIteration(updExistingStudentIterationId, 1, 1)

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.Update(`updExistingStudentIteration`, sender) =>
          sender ! Some(updExistingStudentIteration)
      })

      Put(s"/student/$studentId/iteration/$updExistingStudentIterationId", updExistingStudentIteration) ~>
        studentRoutes ~> check {
        responseAs[StudentIteration] shouldEqual updExistingStudentIteration
      }
    }

    "return NotFound for unknown studentIterationId for PUT request to /student/id/iteration/id" in {
      val studentId                        = 69
      val updNotExistingStudentIterationId = 666
      val updNotExistingStudentIteration   = StudentIteration(updNotExistingStudentIterationId, 1, 1)

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.Update(`updNotExistingStudentIteration`, sender) => sender ! None
      })

      Put(s"/student/$studentId/iteration/$updNotExistingStudentIterationId", updNotExistingStudentIteration) ~> Route
        .seal(studentRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "unassign specific student from specific existing iterationId for DELETE request to /student/id/iteration/id" in {
      val studentId                   = 34
      val delExistingIterationId      = 50
      val delExistingStudentIteration = StudentIteration(delExistingIterationId, 1, 1)

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.Delete(delNotExistingStudentIterationId, `studentId`, sender) =>
          sender ! Some(delExistingStudentIteration)
      })

      Delete(s"/student/$studentId/iteration/$delExistingIterationId") ~> studentRoutes ~> check {
        responseAs[StudentIteration] shouldEqual delExistingStudentIteration
      }
    }

    "return NotFound for specific student from unknown iterationId for DELETE request to /student/id/iteration/id" in {
      val studentId                        = 34
      val delNotExistingStudentIterationId = 666

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.Delete(`delNotExistingStudentIterationId`, `studentId`, sender) => sender ! None
      })

      Delete(s"/student/$studentId/iteration/$delNotExistingStudentIterationId") ~> Route.seal(studentRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "create feedback to specific student on specific iteration for POST request " +
      "to /student/id/iteration/id/feedback" in {
      val studentId     = 77
      val iterationId   = 23
      val feedback      = Feedback(Some(0), 1, 1, 1, "Good!")
      val newFeedbackId = 11
      val newFeedback   = feedback.copy(id = Some(newFeedbackId))
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Create(`feedback`, sender) => sender ! newFeedback
      })

      Post(s"/student/$studentId/iteration/$iterationId/feedback", feedback) ~> studentRoutes ~> check {
        responseAs[Feedback] shouldEqual newFeedback
      }
    }

    "retrieve all feedback for specific student on specific iteration for GET request" +
      "to /student/id/iterationid/id/feedback" in {
      val studentId   = 77
      val iterationId = 23
      val feedbackSeq = Feedback(Some(1), 1, 1, 1, "1") :: Feedback(Some(2), 2, 2, 2, "2") :: Nil

      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.FindByStudentIdIterationId(`studentId`, `iterationId`, sender) => sender ! feedbackSeq
      })

      Get(s"/student/$studentId/iteration/$iterationId/feedback") ~> studentRoutes ~> check {
        responseAs[Seq[Feedback]] shouldEqual feedbackSeq
      }
    }

    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> studentRoutes ~> check {
        handled shouldBe false
      }

      Get("/student/asd") ~> studentRoutes ~> check {
        handled shouldBe false
      }

      Put("/student/iteration/ggg") ~> studentRoutes ~> check {
        handled shouldBe false
      }

      Delete("/stUdent") ~> studentRoutes ~> check {
        handled shouldBe false
      }
    }
  }
}
