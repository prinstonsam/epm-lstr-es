package routes

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import com.epam.education.management.utils.AkkaHttpPickler._
import dto.ResultType
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.ResultTypeProtocol


class ResultTypeRoutesTest
  extends WordSpec
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("ResultTypeRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }
  lazy val resultTypeProbe = TestProbe("resultType")(testKit.system)

  lazy val resultTypeRoutes =
    new ResultTypeRoutes(ActorRef(resultTypeProbe.ref), routeConfig).routes

  "The ResultTypeRoutes" should {
    "create new resultType for POST request to /resulttype" in {
      val newResultTypeId = 67
      val resultType = ResultType(None, "John")
      val newResultType = resultType.copy(id = Some(newResultTypeId))
      resultTypeProbe.setAutoPilot(singleMessagePilot {
        case ResultTypeProtocol.Create(`resultType`, sender) => sender ! newResultType
      })

      Post("/resulttype", resultType) ~> resultTypeRoutes ~> check {
        responseAs[ResultType] shouldEqual newResultType
      }
    }

    "retrieve all resultTypes for GET request to /resulttype" in {
      val resultTypeSeq = ResultType(Some(1), "Result") :: ResultType(Some(2), "Developer") :: Nil
      resultTypeProbe.setAutoPilot(singleMessagePilot {
        case ResultTypeProtocol.FindAll(sender) => sender ! resultTypeSeq
      })

      Get("/resulttype") ~> resultTypeRoutes ~> check {
        responseAs[Seq[ResultType]] shouldEqual resultTypeSeq
      }

    }

    "retrieve specific existing resultType for GET request to /resulttype/id" in {
      val getExistingResultTypeId = 1
      val getExistingResultType = ResultType(Some(getExistingResultTypeId), "Result")
      resultTypeProbe.setAutoPilot(singleMessagePilot {
        case ResultTypeProtocol.GetById(`getExistingResultTypeId`, sender) =>
          sender ! Some(getExistingResultType)
      })

      Get(s"/resulttype/$getExistingResultTypeId") ~> resultTypeRoutes ~> check {
        responseAs[ResultType] shouldEqual getExistingResultType
      }
    }

    "return NotFound for unknown resultTypeId for GET request to /resulttype/id" in {
      val getNotExistingResultTypeId = 1
      resultTypeProbe.setAutoPilot(singleMessagePilot {
        case ResultTypeProtocol.GetById(`getNotExistingResultTypeId`, sender) => sender ! None
      })

      Get(s"/resulttype/$getNotExistingResultTypeId") ~> Route.seal(resultTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific resultType for PUT request to /resulttype/id" in {
      val updExistingResultTypeId = 2
      val updExistingResultType = ResultType(Some(updExistingResultTypeId), "Developer")
      resultTypeProbe.setAutoPilot(singleMessagePilot {
        case ResultTypeProtocol.Update(`updExistingResultType`, sender) =>
          sender ! Some(updExistingResultType)
      })

      Put(s"/resulttype/$updExistingResultTypeId", updExistingResultType) ~> resultTypeRoutes ~> check {
        responseAs[ResultType] shouldEqual updExistingResultType
      }
    }

    "return NotFound for unknown resultTypeId for PUT request to /resulttype/id" in {
      val updNotExistingResultTypeId = 666
      val updNotExistingResultType = ResultType(Some(updNotExistingResultTypeId), "NotExist")
      resultTypeProbe.setAutoPilot(singleMessagePilot {
        case ResultTypeProtocol.Update(`updNotExistingResultType`, sender) => sender ! None
      })

      Put(s"/resulttype/$updNotExistingResultTypeId", updNotExistingResultType) ~> Route.seal(resultTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete specific resultType for DELETE request to /resulttype/id" in {
      val delExistingResultTypeId = 3
      val delExistingResultType = ResultType(Some(delExistingResultTypeId), "deleted")
      resultTypeProbe.setAutoPilot(singleMessagePilot {
        case ResultTypeProtocol.Delete(`delExistingResultTypeId`, sender) => sender ! Some(delExistingResultType)
      })

      Delete(s"/resulttype/$delExistingResultTypeId") ~> resultTypeRoutes ~> check {
        responseAs[ResultType] shouldEqual delExistingResultType
      }
    }

    "return NotFound for unknown resultTypeId for DELETE request to /resulttype/id" in {
      val delNotExistingResultTypeId = 3
      resultTypeProbe.setAutoPilot(singleMessagePilot {
        case ResultTypeProtocol.Delete(`delNotExistingResultTypeId`, sender) => sender ! None
      })

      Delete(s"/resulttype/$delNotExistingResultTypeId") ~> Route.seal(resultTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> resultTypeRoutes ~> check {
        handled shouldBe false
      }

      Get("/resulttype/asd") ~> resultTypeRoutes ~> check {
        handled shouldBe false
      }

      Put("/resulttype/iteration/ggg") ~> resultTypeRoutes ~> check {
        handled shouldBe false
      }

      Delete("/resulttype") ~> resultTypeRoutes ~> check {
        handled shouldBe false
      }
    }
  }
}
