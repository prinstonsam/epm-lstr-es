package routes

import java.time.ZonedDateTime

import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import dto.StudentIteration
import com.epam.education.management.utils.AkkaHttpPickler._
import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import service.protocols.StudentIterationProtocol

class StudentIterationRoutesTest extends WordSpec with Matchers with ScalatestRouteTest with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("StudentIterationRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }

  lazy val studentIterationProbe = TestProbe("studentIteration")(testKit.system)

  lazy val studentIterationRoutes = new StudentIterationRoutes(ActorRef(studentIterationProbe.ref), routeConfig).routes

  "The StudentIterationRoutes" should {

    val startDate = ZonedDateTime.now().minusYears(1).toInstant
    val endDate = ZonedDateTime.now().plusYears(1).toInstant

    "create new studentIteration for POST request to /studentIteration" in {
      val newStudentIterationId = 67
      val studentIteration = StudentIteration(1,1,1)
      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.Create(`studentIteration`, sender) => sender ! studentIteration
      })

      Post("/studentIteration", studentIteration) ~> studentIterationRoutes ~> check {
        responseAs[StudentIteration] shouldEqual studentIteration
      }
    }

    "retrieve all studentIterations for GET request to /studentIteration" in {
      val studentIterationSeq = StudentIteration(1,1,1) ::
        StudentIteration(2,2,2) ::
        Nil

      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.FindAll(sender) => sender ! studentIterationSeq
      })

      Get("/studentIteration") ~> studentIterationRoutes ~> check {
        responseAs[Seq[StudentIteration]] shouldEqual studentIterationSeq
      }
    }
  }
}