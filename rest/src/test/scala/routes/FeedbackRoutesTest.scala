package routes

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestProbe}
import com.epam.education.management.utils.AkkaHttpPickler._
import akka.typed.ActorRef
import dto.Feedback
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.FeedbackProtocol

class FeedbackRoutesTest extends WordSpec with Matchers with ScalatestRouteTest with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("FeedbackRotesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }

  lazy val feedbackProbe = TestProbe("feedback")(testKit.system)

  lazy val feedbackRoutes = new FeedbackRoutes(ActorRef(feedbackProbe.ref), routeConfig).routes

  val feedbackSeq = Feedback(Some(1), 1, 1, 1, "feedback1") :: Feedback(Some(2), 2, 2, 2, "feedback2") :: Nil

  "The FeedbackRoutes" should {

    "create new feedback for POST request to /feedback" in {
      val feedbackId = 67
      val feedback      = Feedback(None, 1, 1, 1, "feedback")
      val newFeedback   = feedback.copy(id = Some(feedbackId))
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Create(`feedback`, sender) => sender ! newFeedback
      })
      Post("/feedback", feedback) ~> feedbackRoutes ~> check {
        responseAs[Feedback] shouldEqual newFeedback
      }
    }

    "retrieve all feedbacks for GET request to /feedback" in {
      val feedbackSeq = Feedback(Some(1), 1, 1, 1, "feedback1") :: Feedback(Some(2), 2, 2, 2, "feedback1") :: Nil
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.FindAll(sender) => sender ! feedbackSeq
      })
      Get("/feedback") ~> feedbackRoutes ~> check {
        responseAs[Seq[Feedback]] shouldEqual feedbackSeq
      }
    }

    "retrieve specific existing feedback for GET request to /feedback/id" in {
      val feedbackId       = 1
      val existingFeedback = Feedback(None, 1, 1, 1, "feedback")
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.GetById(`feedbackId`, sender) => sender ! Some(existingFeedback)
      })
      Get(s"/feedback/$feedbackId") ~> feedbackRoutes ~> check {
        responseAs[Feedback] shouldEqual existingFeedback
      }
    }

    "return NotFound for unknown feedbackId on GET request to /feedback/id" in {
      val getNotExistingFeedbackId = 3
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.GetById(`getNotExistingFeedbackId`, sender) => sender ! None
      })
      Get(s"/feedback/$getNotExistingFeedbackId") ~> Route.seal(feedbackRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific feedback for PUT request to /feedback/id" in {
      val feedbackId = 2
      val feedback   = Feedback(Some(feedbackId), 1, 1, 1, "feedback")
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Update(`feedback`, sender) => sender ! Some(feedback)
      })
      Put(s"/feedback/$feedbackId", feedback) ~> feedbackRoutes ~> check {
        responseAs[Feedback] shouldEqual feedback
      }
    }

    "return NotFound for unknown feedbackId for PUT request to /feedback/id" in {
      val feedbackId = 666
      val feedback   = Feedback(Some(feedbackId), 1, 1, 1, "feedback")
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Update(`feedback`, sender) => sender ! None
      })
      Put(s"/feedback/$feedbackId", feedback) ~> Route.seal(feedbackRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete specific feedback for DELETE request to /feedback/id" in {
      val feedbackId = 3
      val feedback   = Feedback(Some(feedbackId), 1, 1, 1, "feedback")
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Delete(`feedbackId`, sender) => sender ! Some(feedback)
      })
      Delete(s"/feedback/$feedbackId") ~> feedbackRoutes ~> check {
        responseAs[Feedback] shouldEqual feedback
      }
    }

    "return NotFound for unknown feedbackId for DELETE request to /feedback/id" in {
      val feedbackId = 666
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Delete(`feedbackId`, sender) => sender ! None
      })
      Delete(s"/feedback/$feedbackId") ~> Route.seal(feedbackRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> feedbackRoutes ~> check {
        handled shouldBe false
      }

      Get("/feedback/1/uyiy") ~> feedbackRoutes ~> check {
        handled shouldBe false
      }

      Put("/feedback/feedback/ggg") ~> feedbackRoutes ~> check {
        handled shouldBe false
      }

      Delete("/feeDBacK") ~> feedbackRoutes ~> check {
        handled shouldBe false
      }
    }
  }
}
