package routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import dto.User
import com.epam.education.management.utils.AkkaHttpPickler._
import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import service.protocols.UserProtocol

class UserRoutesTest extends WordSpec with Matchers with ScalatestRouteTest with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("UserRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }

  lazy val userProbe = TestProbe("user")(testKit.system)

  lazy val userRoutes = new UserRoutes(ActorRef(userProbe.ref), routeConfig).routes

  "The UserRoutes" should {

    "create new user for POST request to /user" in {
      val newUserId = 67
      val user = User(None, "John", "1", "2", "3")
      val newUser = user.copy(id = Some(newUserId))
      userProbe.setAutoPilot(singleMessagePilot {
        case UserProtocol.Create(`user`, sender) => sender ! newUser
      })

      Post("/user", user) ~> userRoutes ~> check {
        responseAs[User] shouldEqual newUser
      }
    }

    "retrieve all users for GET request to /user" in {
      val userSeq = User(Some(1), "Doe", "1", "2", "3") :: User(Some(2), "Galt", "1", "2", "3") :: Nil
      userProbe.setAutoPilot(singleMessagePilot {
        case UserProtocol.FindAll(sender) => sender ! userSeq
      })

      Get("/user") ~> userRoutes ~> check {
        responseAs[Seq[User]] shouldEqual userSeq
      }
    }

    "retrieve specific existing user for GET request to /user/id" in {
      val getExistingUserId = 1
      val existingUser = User(Some(getExistingUserId), "Doe", "1", "2", "3")
      userProbe.setAutoPilot(singleMessagePilot {
        case UserProtocol.GetById(`getExistingUserId`, sender) => sender ! Some(existingUser)
      })

      Get(s"/user/$getExistingUserId") ~> userRoutes ~> check {
        responseAs[User] shouldEqual existingUser
      }
    }

    "return NotFound for unknown userId on GET request to /user/id" in {
      val getNotExistingUserId = 3
      userProbe.setAutoPilot(singleMessagePilot {
        case UserProtocol.GetById(`getNotExistingUserId`, sender) => sender ! None
      })

      Get(s"/user/$getNotExistingUserId") ~> Route.seal(userRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific user for PUT request to /user/id" in {
      val updExistingUserId = 2
      val updExistingUser = User(Some(updExistingUserId), "Galt", "1", "2", "3")
      userProbe.setAutoPilot(singleMessagePilot {
        case UserProtocol.Update(`updExistingUser`, sender) => sender ! Some(updExistingUser)
      })

      Put(s"/user/$updExistingUserId", updExistingUser) ~> userRoutes ~> check {
        responseAs[User] shouldEqual updExistingUser
      }
    }

    "return NotFound for unknown userId for PUT request to /user/id" in {
      val updNotExistingUserId = 666
      val notExistingUser = User (Some(updNotExistingUserId), "666", "1", "2", "3")
      userProbe.setAutoPilot(singleMessagePilot {
        case UserProtocol.Update(`notExistingUser`, sender) => sender ! None
      })

      Put(s"/user/$updNotExistingUserId", notExistingUser) ~> Route.seal(userRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete specific user for DELETE request to /user/id" in {
      val delExistingUserId = 3
      val delExistingUser = User(Some(delExistingUserId), "deleted", "1", "2", "3")
      userProbe.setAutoPilot(singleMessagePilot {
        case UserProtocol.Delete(`delExistingUserId`, sender) => sender ! Some(delExistingUser)
      })

      Delete(s"/user/$delExistingUserId") ~> userRoutes ~> check {
        responseAs[User] shouldEqual delExistingUser
      }
    }

    "return NotFound for unknown userId for DELETE request to /user/id" in {
      val delNotExistingUserId = 666
      userProbe.setAutoPilot(singleMessagePilot {
        case UserProtocol.Delete(`delNotExistingUserId`, sender) => sender ! None
      })

      Delete(s"/user/$delNotExistingUserId") ~> Route.seal(userRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> userRoutes ~> check {
        handled shouldBe false
      }

      Get("/user/1/uyiy") ~> userRoutes ~> check {
        handled shouldBe false
      }

      Put("/user/iteration/ggg") ~> userRoutes ~> check {
        handled shouldBe false
      }

      Delete("/uSer") ~> userRoutes ~> check {
        handled shouldBe false
      }
    }
  }
}
