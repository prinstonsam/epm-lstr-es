package routes

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import dto.EmploymentType
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import com.epam.education.management.utils.AkkaHttpPickler._
import service.protocols.EmploymentTypeProtocol

class EmploymentTypeRoutesTest
  extends WordSpec
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("EmploymentTypeRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }

  lazy val employmentTypeProbe = TestProbe("employment")(testKit.system)

  lazy val employmentTypeRoutes = new EmploymentTypeRoutes(ActorRef(employmentTypeProbe.ref), routeConfig).routes

  "The service" should {
    "create new employmentType for POST request to /employmenttype" in {
      val newEmploymentTypeId = 67
      val employmentType = EmploymentType(Some(0), "John")
      val newEmploymentType = employmentType.copy(Some(newEmploymentTypeId))
      employmentTypeProbe.setAutoPilot(singleMessagePilot {
        case EmploymentTypeProtocol.Create(`employmentType`, sender) => sender ! newEmploymentType
      })

      Post("/employmenttype", employmentType) ~> employmentTypeRoutes ~> check {
        responseAs[EmploymentType] shouldEqual newEmploymentType
      }
    }

    "retrieve all employmentTypes for GET request to /employmenttype" in {
      val employmentTypeSeq =
        EmploymentType(Some(1), "Employment") ::
          EmploymentType(Some(2), "Developer") ::
          Nil
      employmentTypeProbe.setAutoPilot(singleMessagePilot {
        case EmploymentTypeProtocol.FindAll(sender) => sender ! employmentTypeSeq
      })

      Get("/employmenttype") ~> employmentTypeRoutes ~> check {
        responseAs[Seq[EmploymentType]] shouldEqual employmentTypeSeq
      }

    }

    "retrieve specific employmentType for GET request to /employmenttype/id returrn ET" in {
      val getEmploymentTypeId = 1
      val getEmploymentType = EmploymentType(Some(getEmploymentTypeId), "Employment")
      employmentTypeProbe.setAutoPilot(singleMessagePilot {
        case EmploymentTypeProtocol.GetById(`getEmploymentTypeId`, sender) =>
          sender ! Some(getEmploymentType)
      })

      Get(s"/employmenttype/$getEmploymentTypeId") ~> employmentTypeRoutes ~> check {
        responseAs[EmploymentType] shouldEqual getEmploymentType
      }
    }

    "retrieve specific employmentType for GET request to /employmenttype/id return None" in {
      val getEmploymentTypeId = 1
      val getEmploymentType = EmploymentType(Some(getEmploymentTypeId), "Employment")
      employmentTypeProbe.setAutoPilot(singleMessagePilot {
        case EmploymentTypeProtocol.GetById(`getEmploymentTypeId`, sender) => sender ! None
      })

      Get(s"/employmenttype/$getEmploymentTypeId") ~> Route.seal(employmentTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific employmentType for PUT request to /employmenttype/id return ET" in {
      val updEmploymentTypeId = 2
      val updEmploymentType = EmploymentType(Some(updEmploymentTypeId), "Developer")
      employmentTypeProbe.setAutoPilot(singleMessagePilot {
        case EmploymentTypeProtocol.Update(`updEmploymentType`, sender) =>
          sender ! Some(updEmploymentType)
      })

      Put(s"/employmenttype/$updEmploymentTypeId", updEmploymentType) ~> employmentTypeRoutes ~> check {
        responseAs[EmploymentType] shouldEqual updEmploymentType
      }
    }
    "update specific employmentType for PUT request to /employmenttype/id return None" in {
      val updEmploymentTypeId = 2
      val updEmploymentType = EmploymentType(Some(updEmploymentTypeId), "Developer")
      employmentTypeProbe.setAutoPilot(singleMessagePilot {
        case EmploymentTypeProtocol.Update(`updEmploymentType`, sender) => sender ! None
      })
      Put(s"/employmenttype/$updEmploymentTypeId", updEmploymentType) ~> Route.seal(employmentTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete specific employmentType for DELETE request to /employmenttype/id return Int" in {
      val employmentType = EmploymentType(Some(1), "eT")
      employmentTypeProbe.setAutoPilot(singleMessagePilot {
        case EmploymentTypeProtocol.Delete(1, sender) => sender ! Some(employmentType)
      })

      Delete(s"/employmenttype/1") ~> employmentTypeRoutes ~> check {
        responseAs[EmploymentType] shouldEqual employmentType
      }
    }

    "delete specific employmentType for DELETE request to /employmenttype/id return None" in {
      val delEmploymentTypeId = 3
      employmentTypeProbe.setAutoPilot(singleMessagePilot {
        case EmploymentTypeProtocol.Delete(`delEmploymentTypeId`, sender) => sender ! None
      })

      Delete(s"/employmenttype/$delEmploymentTypeId") ~> Route.seal(employmentTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> employmentTypeRoutes ~> check {
        handled shouldBe false
      }

      Get("/employmenttype/asd") ~> employmentTypeRoutes ~> check {
        handled shouldBe false
      }

      Put("/employmenttype/iteration/ggg") ~> employmentTypeRoutes ~> check {
        handled shouldBe false
      }

      Delete("/employmenttype") ~> employmentTypeRoutes ~> check {
        handled shouldBe false
      }
    }
  }
}
