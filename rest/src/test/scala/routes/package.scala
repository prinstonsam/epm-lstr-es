import akka.testkit.TestActor
import akka.testkit.TestActor.AutoPilot
import akka.util.Timeout
import config.RouteConfig

package object routes {
  val routeConfig: RouteConfig = new RouteConfig {
    import scala.concurrent.duration._
    override implicit def routeTimeout: Timeout = Timeout(5.seconds)
  }

  def singleMessagePilot(receive: PartialFunction[Any, Any]) = new AutoPilot{
    override def run(sender: akka.actor.ActorRef, msg: Any): AutoPilot = {
      receive.lift(msg).orNull
      TestActor.NoAutoPilot
    }
  }
}
