package routes

import java.time.ZonedDateTime

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import dto.Iteration
import com.epam.education.management.utils.AkkaHttpPickler._
import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import service.protocols.IterationProtocol

class IterationRoutesTest extends WordSpec with Matchers with ScalatestRouteTest with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("IterationRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }

  lazy val iterationProbe = TestProbe("iteration")(testKit.system)

  lazy val iterationRoutes = new IterationRoutes(ActorRef(iterationProbe.ref), routeConfig).routes

  "The IterationRoutes" should {

    val startDate = ZonedDateTime.now().minusYears(1).toInstant
    val endDate = ZonedDateTime.now().plusYears(1).toInstant

    "create new iteration for POST request to /iteration" in {
      val newIterationId = 67
      val iteration = Iteration(None, 1, 1, startDate, endDate)
      val newIteration = iteration.copy(id = Some(newIterationId))
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Create(`iteration`, sender) => sender ! newIteration
      })

      Post("/iteration", iteration) ~> iterationRoutes ~> check {
        responseAs[Iteration] shouldEqual newIteration
      }
    }

    "retrieve all iterations for GET request to /iteration" in {
      val iterationSeq = Iteration(Some(1), 1, 1, startDate, endDate) ::
                         Iteration(Some(2), 2, 2, startDate, endDate) ::
                         Nil

      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.FindAll(None, None, sender) => sender ! iterationSeq
      })

      Get("/iteration") ~> iterationRoutes ~> check {
        responseAs[Seq[Iteration]] shouldEqual iterationSeq
      }
    }

    "retrieve all iterations for GET request with dates parameters to /iteration" in {
      val iterationSeq = Iteration(Some(1), 1, 1, startDate, endDate) ::
                         Iteration(Some(2), 2, 2, startDate, endDate) ::
                         Nil

      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.FindAll(Some(`startDate`), Some(`endDate`), sender) => sender ! iterationSeq
      })

      Get(s"/iteration?start=$startDate&end=$endDate") ~> iterationRoutes ~> check {
        responseAs[Seq[Iteration]] shouldEqual iterationSeq
      }
    }

    "retrieve specific existing iteration for GET request to /iteration/id" in {
      val getExistingIterationId = 1
      val existingIteration = Iteration(Some(getExistingIterationId), 2, 2, startDate, endDate)
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.GetById(`getExistingIterationId`, sender) => sender ! Some(existingIteration)
      })

      Get(s"/iteration/$getExistingIterationId") ~> iterationRoutes ~> check {
        responseAs[Iteration] shouldEqual existingIteration
      }
    }

    "return NotFound for unknown iterationId on GET request to /iteration/id" in {
      val getNotExistingIterationId = 3
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.GetById(`getNotExistingIterationId`, sender) => sender ! None
      })

      Get(s"/iteration/$getNotExistingIterationId") ~> Route.seal(iterationRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific iteration for PUT request to /iteration/id" in {
      val updExistingIterationId = 2
      val updExistingIteration = Iteration(Some(updExistingIterationId), 1, 1, startDate, endDate)
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Update(`updExistingIteration`, sender) => sender ! Some(updExistingIteration)
      })

      Put(s"/iteration/$updExistingIterationId", updExistingIteration) ~> iterationRoutes ~> check {
        responseAs[Iteration] shouldEqual updExistingIteration
      }
    }

    "return NotFound for unknown iterationId for PUT request to /iteration/id" in {
      val updNotExistingIterationId = 666
      val notExistingIteration = Iteration (Some(updNotExistingIterationId), 1, 1, startDate, endDate)
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Update(`notExistingIteration`, sender) => sender ! None
      })

      Put(s"/iteration/$updNotExistingIterationId", notExistingIteration) ~> Route.seal(iterationRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete specific iteration for DELETE request to /iteration/id" in {
      val delExistingIterationId = 3
      val delExistingIteration = Iteration(Some(delExistingIterationId), 1, 1, startDate, endDate)
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Delete(`delExistingIterationId`, sender) => sender ! Some(delExistingIteration)
      })

      Delete(s"/iteration/$delExistingIterationId") ~> iterationRoutes ~> check {
        responseAs[Iteration] shouldEqual delExistingIteration
      }
    }

    "return NotFound for unknown iterationId for DELETE request to /iteration/id" in {
      val delNotExistingIterationId = 666
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Delete(`delNotExistingIterationId`, sender) => sender ! None
      })

      Delete(s"/iteration/$delNotExistingIterationId") ~> Route.seal(iterationRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> iterationRoutes ~> check {
        handled shouldBe false
      }

      Get("/iteration/1/uyiy") ~> iterationRoutes ~> check {
        handled shouldBe false
      }

      Put("/iteration/iteration/ggg") ~> iterationRoutes ~> check {
        handled shouldBe false
      }

      Delete("/itEration") ~> iterationRoutes ~> check {
        handled shouldBe false
      }
    }
  }
}