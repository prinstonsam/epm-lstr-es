package routes

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import dto.StudentType
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import com.epam.education.management.utils.AkkaHttpPickler._
import service.protocols.StudentTypeProtocol


class StudentTypeRoutesTest
  extends WordSpec
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("StudentTypeRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }
  lazy val studentTypeProbe = TestProbe("studentType")(testKit.system)

  lazy val studentTypeRoutes =
    new StudentTypeRoutes(ActorRef(studentTypeProbe.ref), routeConfig).routes

  "The StudentTypeRoutes" should {
    "create new studentType for POST request to /studenttype" in {
      val newStudentTypeId = 67
      val studentType = StudentType(None, "John")
      val newStudentType = studentType.copy(id = Some(newStudentTypeId))
      studentTypeProbe.setAutoPilot(singleMessagePilot {
        case StudentTypeProtocol.Create(`studentType`, sender) => sender ! newStudentType
      })

      Post("/studenttype", studentType) ~> studentTypeRoutes ~> check {
        responseAs[StudentType] shouldEqual newStudentType
      }
    }

    "retrieve all studentTypes for GET request to /studenttype" in {
      val studentTypeSeq = StudentType(Some(1), "Student") :: StudentType(Some(2), "Developer") :: Nil
      studentTypeProbe.setAutoPilot(singleMessagePilot {
        case StudentTypeProtocol.FindAll(sender) => sender ! studentTypeSeq
      })

      Get("/studenttype") ~> studentTypeRoutes ~> check {
        responseAs[Seq[StudentType]] shouldEqual studentTypeSeq
      }

    }

    "retrieve specific existing studentType for GET request to /studenttype/id" in {
      val getExistingStudentTypeId = 1
      val getExistingStudentType = StudentType(Some(getExistingStudentTypeId), "Student")
      studentTypeProbe.setAutoPilot(singleMessagePilot {
        case StudentTypeProtocol.GetById(`getExistingStudentTypeId`, sender) =>
          sender ! Some(getExistingStudentType)
      })

      Get(s"/studenttype/$getExistingStudentTypeId") ~> studentTypeRoutes ~> check {
        responseAs[StudentType] shouldEqual getExistingStudentType
      }
    }

    "return NotFound for unknown studentTypeId for GET request to /studenttype/id" in {
      val getNotExistingStudentTypeId = 1
      studentTypeProbe.setAutoPilot(singleMessagePilot {
        case StudentTypeProtocol.GetById(`getNotExistingStudentTypeId`, sender) => sender ! None
      })

      Get(s"/studenttype/$getNotExistingStudentTypeId") ~> Route.seal(studentTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific studentType for PUT request to /studenttype/id" in {
      val updExistingStudentTypeId = 2
      val updExistingStudentType = StudentType(Some(updExistingStudentTypeId), "Developer")
      studentTypeProbe.setAutoPilot(singleMessagePilot {
        case StudentTypeProtocol.Update(`updExistingStudentType`, sender) =>
          sender ! Some(updExistingStudentType)
      })

      Put(s"/studenttype/$updExistingStudentTypeId", updExistingStudentType) ~> studentTypeRoutes ~> check {
        responseAs[StudentType] shouldEqual updExistingStudentType
      }
    }

    "return NotFound for unknown studentTypeId for PUT request to /studenttype/id" in {
      val updNotExistingStudentTypeId = 666
      val updNotExistingStudentType = StudentType(Some(updNotExistingStudentTypeId), "NotExist")
      studentTypeProbe.setAutoPilot(singleMessagePilot {
        case StudentTypeProtocol.Update(`updNotExistingStudentType`, sender) => sender ! None
      })

      Put(s"/studenttype/$updNotExistingStudentTypeId", updNotExistingStudentType) ~> Route.seal(studentTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete specific studentType for DELETE request to /studenttype/id" in {
      val delExistingStudentTypeId = 3
      val delExistingStudentType = StudentType(Some(delExistingStudentTypeId), "deleted")
      studentTypeProbe.setAutoPilot(singleMessagePilot {
        case StudentTypeProtocol.Delete(`delExistingStudentTypeId`, sender) => sender ! Some(delExistingStudentType)
      })

      Delete(s"/studenttype/$delExistingStudentTypeId") ~> studentTypeRoutes ~> check {
        responseAs[StudentType] shouldEqual delExistingStudentType
      }
    }

    "return NotFound for unknown studentTypeId for DELETE request to /studenttype/id" in {
      val delNotExistingStudentTypeId = 3
      studentTypeProbe.setAutoPilot(singleMessagePilot {
        case StudentTypeProtocol.Delete(`delNotExistingStudentTypeId`, sender) => sender ! None
      })

      Delete(s"/studenttype/$delNotExistingStudentTypeId") ~> Route.seal(studentTypeRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> studentTypeRoutes ~> check {
        handled shouldBe false
      }

      Get("/studenttype/asd") ~> studentTypeRoutes ~> check {
        handled shouldBe false
      }

      Put("/studenttype/iteration/ggg") ~> studentTypeRoutes ~> check {
        handled shouldBe false
      }

      Delete("/studenttype") ~> studentTypeRoutes ~> check {
        handled shouldBe false
      }
    }
  }
}
