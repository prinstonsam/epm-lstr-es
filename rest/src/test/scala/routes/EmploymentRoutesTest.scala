package routes

import java.time.{Instant, ZonedDateTime}

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import dto.{Employment, Iteration, Student, StudentIteration}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols._
import com.epam.education.management.utils.AkkaHttpPickler._
import service.protocols.{EmploymentProtocol, IterationProtocol}

class EmploymentRoutesTest
    extends WordSpec
    with BeforeAndAfter
    with BeforeAndAfterAll
    with Matchers
    with ScalatestRouteTest {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("EmploymentRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }

  val employment1                = Employment(Some(1), "Pavel", 1, 1)
  val employment2                = Employment(Some(2), "Semen", 1, 1)
  val employmentsList            = List(employment1, employment2)
  val iteration1                 = Iteration(Some(1), 1, 1, ZonedDateTime.now().toInstant, ZonedDateTime.now().toInstant)
  val iteration2                 = Iteration(Some(2), 2, 2, ZonedDateTime.now().toInstant, ZonedDateTime.now().toInstant)
  val iterationsList             = List(iteration1, iteration2)
  val student1                   = Student(Some(1), "Student1", "last", "mail")
  val student2                   = Student(Some(2), "Student1", "last", "mail")
  val studentsList               = List(student1, student2)
  val studentIteration1          = StudentIteration(1, 1, 1)
  val studentIteration2          = StudentIteration(1, 2, 1)
  val studentIterations          = Seq(studentIteration1, studentIteration2)
  lazy val employmentProbe       = TestProbe("employment")(testKit.system)
  lazy val iterationProbe        = TestProbe("iteration")(testKit.system)
  lazy val studentWithDataProbe  = TestProbe("studentWithData")(testKit.system)
  lazy val studentProbe          = TestProbe("student")(testKit.system)
  lazy val studentIterationProbe = TestProbe("student")(testKit.system)
  lazy val employmentRoute =
    new EmploymentRoutes(
      ActorRef(employmentProbe.ref),
      ActorRef(iterationProbe.ref),
      ActorRef(studentWithDataProbe.ref),
      ActorRef(studentProbe.ref),
      ActorRef(studentIterationProbe.ref),
      routeConfig)

  lazy val routes = employmentRoute.routes

  "The service" should {

    "post /employment" in {
      val newEmployment: Employment = Employment(Some(-1), "a", 1, 1)
      employmentProbe.setAutoPilot(singleMessagePilot {
        case EmploymentProtocol.Create(`newEmployment`, sender) =>
          sender ! newEmployment.copy(id = Some(1))
      })
      Post("/employment", newEmployment) ~> routes ~> check {
        responseAs[Employment] shouldEqual newEmployment.copy(id = Some(1))
      }
    }
    "get /employment" in {
      employmentProbe.setAutoPilot(singleMessagePilot {
        case EmploymentProtocol.FindAll(sender) => sender ! employmentsList
      })
      Get("/employment") ~> routes ~> check {
        responseAs[List[Employment]] shouldEqual employmentsList
      }
    }
    "get /employment/id return Employment" in {
      val employmentId = 1
      employmentProbe.setAutoPilot(singleMessagePilot {
        case EmploymentProtocol.GetById(1, sender) => sender ! Some(employmentsList(employmentId - 1))
      })
      Get(s"/employment/$employmentId") ~> routes ~> check {
        responseAs[Employment] shouldEqual employment1
      }
    }
    "get /employment/id return None" in {
      val employmentId = 1
      employmentProbe.setAutoPilot(singleMessagePilot {
        case EmploymentProtocol.GetById(1, sender) => sender ! None
      })
      Get(s"/employment/$employmentId") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "put /employment/id return Employment" in {
      val newEmployment: Employment = Employment(Some(-1), "a", 1, 1)
      employmentProbe.setAutoPilot(singleMessagePilot {
        case EmploymentProtocol.Update(`newEmployment`, sender) =>
          sender ! Some(newEmployment.copy(id = Some(1)))
      })
      Put("/employment/1", newEmployment) ~> routes ~> check {
        responseAs[Employment] shouldEqual newEmployment.copy(id = Some(1))
      }
    }
    "put /employment/id return None" in {
      val newEmployment: Employment = Employment(Some(-1), "a", 1, 1)
      employmentProbe.setAutoPilot(singleMessagePilot {
        case EmploymentProtocol.Update(`newEmployment`, sender) => sender ! None
      })
      Put("/employment/1", newEmployment) ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "delete /employment/id return Employment" in {
      employmentProbe.setAutoPilot(singleMessagePilot {
        case EmploymentProtocol.Delete(1, sender) => sender ! Some(employment1)
      })
      Delete("/employment/1") ~> routes ~> check {
        responseAs[Employment] shouldEqual employment1
      }
    }
    "delete /employment/id return None" in {
      employmentProbe.setAutoPilot(singleMessagePilot {
        case EmploymentProtocol.Delete(1, sender) => sender ! None
      })
      Delete("/employment/1") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "post /employment/id/iteration" in {
      val newIteration: Iteration =
        Iteration(Some(-1), 1, 1, ZonedDateTime.now().toInstant, ZonedDateTime.now().toInstant)
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Create(`newIteration`, sender) =>
          sender ! newIteration.copy(id = Some(1))
      })
      Post("/employment/1/iteration", newIteration) ~> routes ~> check {
        responseAs[Iteration] shouldEqual newIteration.copy(id = Some(1))
      }
    }

    val startDate = Instant.now()
    "get /employment/id/iteration/date" in {
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.FindByEmploymentId(1, Some(`startDate`), None, sender) => sender ! iterationsList
      })
      Get(s"/employment/1/iteration?start=$startDate") ~> routes ~> check {
        responseAs[List[Iteration]] shouldEqual iterationsList
      }
    }
    "get /employment/id/iteration" in {
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.FindByEmploymentId(1, None, None, sender) => sender ! iterationsList
      })
      Get(s"/employment/1/iteration") ~> routes ~> check {
        responseAs[List[Iteration]] shouldEqual iterationsList
      }
    }
    "get /employment/id/iteration/id return Iteration" in {
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.GetById(1, sender) => sender ! Some(iterationsList.head)
      })
      Get("/employment/1/iteration/1") ~> routes ~> check {
        responseAs[Iteration] shouldEqual iteration1
      }
    }
    "get /employment/id/iteration/id return None" in {
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.GetById(1, sender) => sender ! None
      })
      Get("/employment/1/iteration/1") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "put /employment/id/iteration/id return Iteration" in {
      val newIteration: Iteration =
        Iteration(Some(-1), 1, 1, ZonedDateTime.now().toInstant, ZonedDateTime.now().toInstant)
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Update(`newIteration`, sender) =>
          sender ! Some(newIteration.copy(id = Some(1)))
      })
      Put("/employment/1/iteration/1", newIteration) ~> routes ~> check {
        responseAs[Iteration] shouldEqual newIteration.copy(id = Some(1))
      }
    }
    "put /employment/id/iteration/id return None" in {
      val newIteration: Iteration =
        Iteration(Some(-1), 1, 1, ZonedDateTime.now().toInstant, ZonedDateTime.now().toInstant)
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Update(`newIteration`, sender) => sender ! None
      })
      Put("/employment/1/iteration/1", newIteration) ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete /employment/id/iteration/id return Int" in {
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Delete(1, sender) => sender ! Some(iteration1)
      })
      Delete("/employment/1/iteration/1") ~> routes ~> check {
        responseAs[Iteration] shouldEqual iteration1
      }
    }
    "delete /employment/id/iteration/id return None" in {
      iterationProbe.setAutoPilot(singleMessagePilot {
        case IterationProtocol.Delete(1, sender) => sender ! None
      })
      Delete("/employment/1/iteration/1") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "get /employment/id/iteration/id/student/" in {
      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.FindByIterationId(1, sender) => sender ! studentIterations
      })
      Get("/employment/1/iteration/1/student") ~> routes ~> check {
        responseAs[Seq[StudentIteration]] shouldEqual studentIterations
      }
    }
    "delete /employment/id/iteration/id/student/id return StudentIeration" in {
      val employmentId     = 1
      val iterationId      = 1
      val studentId        = 1
      val studentIteration = StudentIteration(studentId, iterationId, 1)
      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.Delete(`studentId`, `iterationId`, sender) => sender ! Some(studentIteration)
      })
      Delete(s"/employment/$employmentId/iteration/$iterationId/student/$studentId") ~> routes ~> check {
        responseAs[StudentIteration] shouldEqual studentIteration
      }
    }
    "delete /employment/id/iteration/id/student/id return None" in {
      val employmentId = 1
      val iterationId  = 1
      val studentId    = 1
      studentIterationProbe.setAutoPilot(singleMessagePilot {
        case StudentIterationProtocol.Delete(`studentId`, `iterationId`, sender) => sender ! None
      })
      Delete(s"/employment/$employmentId/iteration/$iterationId/student/$studentId") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> routes ~> check {
        handled shouldBe false
      }

      Get("/employment/asd") ~> routes ~> check {
        handled shouldBe false
      }

      Put("/employment/iteration/ggg") ~> routes ~> check {
        handled shouldBe false
      }

      Delete("/stUdent") ~> routes ~> check {
        handled shouldBe false
      }
    }
  }
}
