package routes

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import com.epam.education.management.utils.AkkaHttpPickler._
import dto.Skill
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.SkillProtocol


class SkillRoutesTest
  extends WordSpec
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("SkillRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }
  lazy val skillProbe = TestProbe("skill")(testKit.system)

  lazy val skillRoutes =
    new SkillRoutes(ActorRef(skillProbe.ref), routeConfig).routes

  "The SkillRoutes" should {
    "create new skill for POST request to /skill" in {
      val newSkillId = 67
      val skill = Skill(None, "John")
      val newSkill = skill.copy(id = Some(newSkillId))
      skillProbe.setAutoPilot(singleMessagePilot {
        case SkillProtocol.Create(`skill`, sender) => sender ! newSkill
      })

      Post("/skill", skill) ~> skillRoutes ~> check {
        responseAs[Skill] shouldEqual newSkill
      }
    }

    "retrieve all skills for GET request to /skill" in {
      val skillSeq = Skill(Some(1), "Student") :: Skill(Some(2), "Developer") :: Nil
      skillProbe.setAutoPilot(singleMessagePilot {
        case SkillProtocol.FindAll(sender) => sender ! skillSeq
      })

      Get("/skill") ~> skillRoutes ~> check {
        responseAs[Seq[Skill]] shouldEqual skillSeq
      }

    }

    "retrieve specific existing skill for GET request to /skill/id" in {
      val getExistingSkillId = 1
      val getExistingSkill = Skill(Some(getExistingSkillId), "Student")
      skillProbe.setAutoPilot(singleMessagePilot {
        case SkillProtocol.GetById(`getExistingSkillId`, sender) =>
          sender ! Some(getExistingSkill)
      })

      Get(s"/skill/$getExistingSkillId") ~> skillRoutes ~> check {
        responseAs[Skill] shouldEqual getExistingSkill
      }
    }

    "return NotFound for unknown skillId for GET request to /skill/id" in {
      val getNotExistingSkillId = 1
      skillProbe.setAutoPilot(singleMessagePilot {
        case SkillProtocol.GetById(`getNotExistingSkillId`, sender) => sender ! None
      })

      Get(s"/skill/$getNotExistingSkillId") ~> Route.seal(skillRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "update specific skill for PUT request to /skill/id" in {
      val updExistingSkillId = 2
      val updExistingSkill = Skill(Some(updExistingSkillId), "Developer")
      skillProbe.setAutoPilot(singleMessagePilot {
        case SkillProtocol.Update(`updExistingSkill`, sender) =>
          sender ! Some(updExistingSkill)
      })

      Put(s"/skill/$updExistingSkillId", updExistingSkill) ~> skillRoutes ~> check {
        responseAs[Skill] shouldEqual updExistingSkill
      }
    }

    "return NotFound for unknown skillId for PUT request to /skill/id" in {
      val updNotExistingSkillId = 666
      val updNotExistingSkill = Skill(Some(updNotExistingSkillId), "NotExist")
      skillProbe.setAutoPilot(singleMessagePilot {
        case SkillProtocol.Update(`updNotExistingSkill`, sender) => sender ! None
      })

      Put(s"/skill/$updNotExistingSkillId", updNotExistingSkill) ~> Route.seal(skillRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "delete specific skill for DELETE request to /skill/id" in {
      val delExistingSkillId = 3
      val delExistingSkill = Skill(Some(delExistingSkillId), "deleted")
      skillProbe.setAutoPilot(singleMessagePilot {
        case SkillProtocol.Delete(`delExistingSkillId`, sender) => sender ! Some(delExistingSkill)
      })

      Delete(s"/skill/$delExistingSkillId") ~> skillRoutes ~> check {
        responseAs[Skill] shouldEqual delExistingSkill
      }
    }

    "return NotFound for unknown skillId for DELETE request to /skill/id" in {
      val delNotExistingSkillId = 3
      skillProbe.setAutoPilot(singleMessagePilot {
        case SkillProtocol.Delete(`delNotExistingSkillId`, sender) => sender ! None
      })

      Delete(s"/skill/$delNotExistingSkillId") ~> Route.seal(skillRoutes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> skillRoutes ~> check {
        handled shouldBe false
      }

      Get("/skill/asd") ~> skillRoutes ~> check {
        handled shouldBe false
      }

      Put("/skill/iteration/ggg") ~> skillRoutes ~> check {
        handled shouldBe false
      }

      Delete("/skill") ~> skillRoutes ~> check {
        handled shouldBe false
      }
    }
  }
}
