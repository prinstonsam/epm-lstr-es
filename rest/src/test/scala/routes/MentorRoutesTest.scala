package routes

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.testkit.{TestKit, TestProbe}
import akka.typed.ActorRef
import dto.{Feedback, Mentor}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, Matchers, WordSpec}
import com.epam.education.management.utils.AkkaHttpPickler._
import service.protocols.{FeedbackProtocol, MentorProtocol}

class MentorRoutesTest
    extends WordSpec
    with BeforeAndAfter
    with Matchers
    with BeforeAndAfterAll
    with ScalatestRouteTest {

  var testKit: TestKit = _

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    testKit = new TestKit(ActorSystem("EmploymentRoutesTest"))
  }

  override protected def afterAll(): Unit = {
    testKit.shutdown()
    super.afterAll()
  }

  val i             = 1
  val mentor1       = Mentor(Some(1), "skill", "Pavel", "last", "mail")
  val mentor2       = Mentor(Some(2), "skill", "Semen", "last", "mail")
  val mentorsList   = List(mentor1, mentor2)
  val feedback1     = Feedback(Some(1), 1, 1, 1, "Feedback1")
  val feedback2     = Feedback(Some(2), 2, 2, 2, "Feedback1")
  val feedbacksList = List(feedback1, feedback2)

  lazy val mentorProbe   = TestProbe("mentor")(testKit.system)
  lazy val feedbackProbe = TestProbe("feedback")(testKit.system)

  lazy val mentorRoute =
    new MentorRoutes(
      ActorRef(mentorProbe.ref),
      ActorRef(feedbackProbe.ref),
      routeConfig
    )

  lazy val routes = mentorRoute.routes

  "The service" should {

    "post /mentor" in {
      val newMentor: Mentor = Mentor(Some(-1), "skill", "a", "last", "mail")
      mentorProbe.setAutoPilot(singleMessagePilot {
        case MentorProtocol.Create(`newMentor`, sender) =>
          sender ! newMentor.copy(id = Some(1))
      })
      Post("/mentor", newMentor) ~> routes ~> check {
        responseAs[Mentor] shouldEqual newMentor.copy(id = Some(1))
      }
    }
    "get /mentor" in {
      mentorProbe.setAutoPilot(singleMessagePilot {
        case MentorProtocol.FindAll(sender) => sender ! mentorsList
      })
      Get("/mentor") ~> routes ~> check {
        responseAs[List[Mentor]] shouldEqual mentorsList
      }
    }
    "get /mentor/id return Mentor" in {
      mentorProbe.setAutoPilot(singleMessagePilot {
        case MentorProtocol.GetById(1, sender) => sender ! Some(mentorsList(i - 1))
      })
      Get("/mentor/1") ~> routes ~> check {
        responseAs[Mentor] shouldEqual mentor1
      }
    }
    "get /mentor/id return None" in {
      mentorProbe.setAutoPilot(singleMessagePilot {
        case MentorProtocol.GetById(1, sender) => sender ! None
      })
      Get("/mentor/1") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "put /mentor/id return Mentor" in {
      val newMentor: Mentor = Mentor(Some(-1), "skill", "a", "last", "mail")
      mentorProbe.setAutoPilot(singleMessagePilot {
        case MentorProtocol.Update(`newMentor`, sender) =>
          sender ! Some(newMentor.copy(id = Some(1)))
      })
      Put("/mentor/1", newMentor) ~> routes ~> check {
        responseAs[Mentor] shouldEqual newMentor.copy(id = Some(1))
      }
    }
    "put /mentor/id return None" in {
      val newMentor: Mentor = Mentor(Some(-1), "skill", "a", "last", "mail")
      mentorProbe.setAutoPilot(singleMessagePilot {
        case MentorProtocol.Update(`newMentor`, sender) => sender ! None
      })
      Put("/mentor/1", newMentor) ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "delete /mentor/id return Mentor" in {
      mentorProbe.setAutoPilot(singleMessagePilot {
        case MentorProtocol.Delete(1, sender) => sender ! Some(mentor1)
      })
      Delete("/mentor/1") ~> routes ~> check {
        responseAs[Mentor] shouldEqual mentor1
      }
    }
    "delete /mentor/id return None" in {
      mentorProbe.setAutoPilot(singleMessagePilot {
        case MentorProtocol.Delete(1, sender) => sender ! None
      })
      Delete("/mentor/1") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "get /mentor/id/feedback" in {
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.FindByMentorId(1, sender) => sender ! feedbacksList
      })
      Get("/mentor/1/feedback") ~> routes ~> check {
        responseAs[List[Feedback]] shouldEqual feedbacksList
      }
    }
    "get /mentor/id/feedback/id" in {
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.GetById(1, sender) => sender ! Some(feedback1)
      })
      Get("/mentor/1/feedback/1") ~> routes ~> check {
        responseAs[Feedback] shouldEqual feedback1
      }
    }
    "get /mentor/id/feedback/id return StatusCode" in {
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.GetById(1, sender) => sender ! None
      })
      Get("/mentor/1/feedback/1") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "put /mentor/id/feedback/id return Feedback" in {
      val newFeedback: Feedback = Feedback(Some(-1), 1, 1, 1, "Feedback1")
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Update(`newFeedback`, sender) =>
          sender ! Some(newFeedback.copy(id = Some(1)))
      })
      Put("/mentor/1/feedback/1", newFeedback) ~> routes ~> check {
        responseAs[Feedback] shouldEqual newFeedback.copy(id = Some(1))
      }
    }
    "put /mentor/id/feedback/id return None" in {
      val newFeedback: Feedback = Feedback(Some(-1), 1, 1, 1, "Feedback1")
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Update(`newFeedback`, sender) => sender ! None
      })
      Put("/mentor/1/feedback/1", newFeedback) ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "delete /mentor/id/feedback/id return Int" in {
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Delete(1, sender) => sender ! Some(feedback1)
      })
      Delete("/mentor/1/feedback/1") ~> routes ~> check {
        responseAs[Feedback] shouldEqual feedback1
      }
    }
    "delete /mentor/id/feedback/id return None" in {
      feedbackProbe.setAutoPilot(singleMessagePilot {
        case FeedbackProtocol.Delete(1, sender) => sender ! None
      })
      Delete("/mentor/1/feedback/1") ~> Route.seal(routes) ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
    "leave any type of requests to other pathes unhandled" in {
      Post("/qwe/asd") ~> routes ~> check {
        handled shouldBe false
      }

      Get("/mentor/asd") ~> routes ~> check {
        handled shouldBe false
      }

      Put("/mentor/feedback/ggg") ~> routes ~> check {
        handled shouldBe false
      }
    }
  }
}
