package config

import akka.util.Timeout

trait RouteConfig {
  implicit def routeTimeout: Timeout
}
