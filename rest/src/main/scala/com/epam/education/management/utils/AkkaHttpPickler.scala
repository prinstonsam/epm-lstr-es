package com.epam.education.management.utils

import java.time.Instant
import java.time.format.DateTimeParseException

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.MediaTypes._
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import upickle.{Js, json}

object AkkaHttpPickler extends CustomPickler {

  implicit def upickleUnmarshaller[A](implicit reader: Reader[A]): FromEntityUnmarshaller[A] =
    Unmarshaller.byteStringUnmarshaller
      .forContentTypes(`application/json`)
      .mapWithCharset((data, charset) => readJs[A](json.read(data.decodeString(charset.nioCharset.name))))

  implicit def upickleMarshaller[A](implicit writer: Writer[A],
                                    printer: Js.Value => String = json.write(_, 0)): ToEntityMarshaller[A] = {
    Marshaller.StringMarshaller.wrap(`application/json`)(printer).compose(writeJs[A])
  }

  implicit val instantStringUnmarshaler: Unmarshaller[String, Instant] =
    Unmarshaller.strict(str =>
      try {
        Instant.parse(str)
      } catch {
        case e: DateTimeParseException =>
          throw
            if (str.isEmpty) Unmarshaller.NoContentException
            else new IllegalArgumentException(s"'$str' is not a valid Instant value", e)
      }
    )
}
