package routes

import akka.http.scaladsl.server.Directives._
import akka.typed.ActorRef
import akka.typed.AskPattern._
import config.RouteConfig
import dto._
import service.protocols._
import com.epam.education.management.utils.AkkaHttpPickler._

class MentorRoutes(mentorService: ActorRef[MentorProtocol],
                   feedbackService: ActorRef[FeedbackProtocol],
                   config: RouteConfig) {

  import config._

  def feedbackRoute(mentorId: Int) = {
    import FeedbackProtocol._
    pathPrefix("feedback") {
      pathEnd {
        get {
          complete {
            feedbackService ? FindByMentorId.m(mentorId)
          }
        }
      } ~
      path(IntNumber) { feedbackId =>
        get {
          complete {
            feedbackService ? GetById.m(feedbackId)
          }
        } ~
        (put & entity(as[Feedback])) { feedback =>
          complete {
            feedbackService ? Update.m(feedback)
          }
        } ~
        delete {
          complete {
            feedbackService ? Delete.m(feedbackId)
          }
        }
      }
    }
  }

  val routes = rejectEmptyResponse {
    import MentorProtocol._
    pathPrefix("mentor") {
      pathEnd {
        (post & entity(as[Mentor])) { mentor =>
          complete {
            mentorService ? Create.m(mentor)
          }
        } ~
        get {
          complete {
            mentorService ? FindAll
          }
        }
      } ~
      pathPrefix(IntNumber) { mentorId =>
        pathEnd {
          get {
            complete {
              mentorService ? GetById.m(mentorId)
            }
          } ~
          (put & entity(as[Mentor])) { mentor =>
            complete {
              mentorService ? Update.m(mentor)
            }
          } ~
          delete {
            complete {
              mentorService ? Delete.m(mentorId)
            }
          }
        } ~
        feedbackRoute(mentorId)
      }
    }
  }
}
