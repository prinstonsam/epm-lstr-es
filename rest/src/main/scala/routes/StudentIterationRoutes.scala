package routes

import akka.http.scaladsl.server.Directives._
import akka.typed.ActorRef
import akka.typed.AskPattern._
import config.RouteConfig
import dto.StudentIteration
import service.protocols.StudentIterationProtocol
import com.epam.education.management.utils.AkkaHttpPickler._

class StudentIterationRoutes(studentIterationService: ActorRef[StudentIterationProtocol], config: RouteConfig) {

  import config._

  val routes =
    (pathPrefix("studentIteration") & rejectEmptyResponse) {
      import StudentIterationProtocol._
      pathEnd {
        (post & entity(as[StudentIteration])) { studentIteration =>
          complete {
            studentIterationService ? Create.m(studentIteration)
          }
        } ~ // TODO use single `parameters` group
      get {
        complete {
          studentIterationService ? FindAll
        }
      }
      }
    }
}
