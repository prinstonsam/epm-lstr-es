package routes

import akka.http.scaladsl.server.Directives._
import akka.typed.ActorRef
import akka.typed.AskPattern._
import com.epam.education.management.utils.AkkaHttpPickler._
import config.RouteConfig
import dto.Skill
import service.protocols.SkillProtocol

class SkillRoutes(skillService: ActorRef[SkillProtocol], config: RouteConfig) {

  import config._

  val routes =
    (pathPrefix("skill") & rejectEmptyResponse) {
      import SkillProtocol._
      pathEnd {
        (post & entity(as[Skill])) { skill =>
          complete {
            skillService ? Create.m(skill)
          }
        } ~
        get {
          complete {
            skillService ? FindAll
          }
        }
      } ~
      path(IntNumber) { skillId =>
        get {
          complete {
            skillService ? GetById.m(skillId)
          }
        } ~
        (put & entity(as[Skill])) { skill =>
          complete {
            skillService ? Update.m(skill)
          }
        } ~
        delete {
          complete {
            skillService ? Delete.m(skillId)
          }
        }
      }
    }
}