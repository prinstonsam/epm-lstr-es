package routes

import akka.http.scaladsl.server.Directives._
import akka.typed.ActorRef
import akka.typed.AskPattern._
import config.RouteConfig
import dto.User
import service.protocols.UserProtocol
import com.epam.education.management.utils.AkkaHttpPickler._

class UserRoutes(userService: ActorRef[UserProtocol], config: RouteConfig) {

  import config._

  val routes =
    (pathPrefix("user") & rejectEmptyResponse) {
      import UserProtocol._
      pathEnd {
        (post & entity(as[User])) { user =>
          complete {
            userService ? Create.m(user)
          }
        } ~
        get {
          complete {
            userService ? FindAll
          }
        }
      } ~
      path(IntNumber) { userId =>
        get {
          complete {
            userService ? GetById.m(userId)
          }
        } ~
        (put & entity(as[User])) { user =>
          complete {
            userService ? Update.m(user)
          }
        } ~
        delete {
          complete {
            userService ? Delete.m(userId)
          }
        }
      }
    }
}
