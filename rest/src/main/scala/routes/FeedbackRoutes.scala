package routes

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.IntNumber
import akka.typed.ActorRef
import akka.typed.AskPattern._
import config.RouteConfig
import dto.Feedback
import service.protocols.FeedbackProtocol
import com.epam.education.management.utils.AkkaHttpPickler._

class FeedbackRoutes(feedbackService: ActorRef[FeedbackProtocol], config: RouteConfig) {

  import config._

  val routes =
    (pathPrefix("feedback") & rejectEmptyResponse) {
      import FeedbackProtocol._
      pathEnd {
        (post & entity(as[Feedback])) { feedback =>
          complete {
            feedbackService ? Create.m(feedback)
          }
        } ~
          get {
            complete(
              feedbackService ? FindAll
            )
          }
      } ~
        path(IntNumber) { feedbackId =>
          get {
            complete {
              feedbackService ? GetById.m(feedbackId)
            }
          } ~
            (put & entity(as[Feedback])) { feedback =>
              complete {
                feedbackService ? Update.m(feedback)
              }
            } ~
            delete {
              complete {
                feedbackService ? Delete.m(feedbackId)
              }
            }
        }
    }
}
