package routes

import akka.typed.ActorRef
import akka.http.scaladsl.server.Directives._
import com.epam.education.management.utils.AkkaHttpPickler._
import akka.typed.AskPattern._
import config.RouteConfig
import dto._
import service.protocols._
import java.time.Instant

class StudentRoutes(
    studentService: ActorRef[StudentProtocol],
    studentWithDataService: ActorRef[StudentWithDataProtocol],
    studentDataService: ActorRef[StudentDataProtocol],
    studentIterationService: ActorRef[StudentIterationProtocol],
    feedbackService: ActorRef[FeedbackProtocol],
    reportService: ActorRef[StudentReportProtocol],
    config: RouteConfig
) {

  import config._

  val routes =
    (pathPrefix("student") & rejectEmptyResponse) {
      import StudentWithDataProtocol._
      import StudentProtocol._
      pathEnd {
        (post & entity(as[StudentWithData])) { studentWithData =>
          complete {
            studentWithDataService ? StudentWithDataProtocol.Create.m(studentWithData)
          }
        } ~
        (get & parameters('date.as[Instant].?)) { (date: Option[Instant]) =>
          complete {
            studentWithDataService ? StudentWithDataProtocol.FindAll.m(date)
          }
        }
      } ~
      pathPrefix(IntNumber) { studentId =>
        pathEnd {
          (get & parameters('date.as[Instant].?)) { (date: Option[Instant]) =>
            complete {
              studentWithDataService ? GetByStudentId.m(studentId, date)
            }
          } ~
          (put & entity(as[Student])) { student =>
            complete {
              studentService ? Update.m(student)
            }
          } ~
          delete {
            complete {
              studentService ? Delete.m(studentId)
            }
          }
        } ~
        iterationRoutes(studentId) ~
        studentDataRoutes(studentId)
      }
    }

  def studentDataRoutes(studentId: Int) = {
    pathPrefix("data") {
      import StudentDataProtocol._
      pathEnd {
        get {
          complete(studentDataService ? FindById.m(studentId))
        } ~
        (post & entity(as[StudentData])) { studentData: StudentData =>
          complete {
            studentDataService ? Create.m(studentData)
          }
        }
      }
      //TODO update
      /*(put & entity(as[StudentData])) { iteration =>
        complete {
          studentDataService ? (Update.apply _).curried(iteration)
        }
      }*/
    }
  }

  def iterationRoutes(studentId: Int) = {
    pathPrefix("iteration") {
      import StudentIterationProtocol._
      pathEnd {
        (post & entity(as[StudentIteration])) { studentIteration =>
          complete {
            studentIterationService ? Create.m(studentIteration)
          }
        } ~
        get {
          complete {
            studentIterationService ? FindByStudentId.m(studentId)
          }
        }
      } ~
      pathPrefix(IntNumber) { iterationId =>
        pathEnd {
          get {
            complete {
              studentIterationService ? GetById.m(iterationId, studentId)
            }
          } ~
          (put & entity(as[StudentIteration])) { studentIteration =>
            complete {
              studentIterationService ? Update.m(studentIteration)
            }
          } ~
          delete {
            complete {
              studentIterationService ? Delete.m(iterationId, studentId)
            }
          }
        } ~
        feedbackRoutes(studentId, iterationId)
      }
    }
  }

  def feedbackRoutes(studentId: Int, iterationId: Int) = {
    path("feedback") {
      import FeedbackProtocol._
      (post & entity(as[Feedback])) { feedback =>
        complete {
          feedbackService ? Create.m(feedback)
        }
      } ~
      get {
        complete {
          feedbackService ? FindByStudentIdIterationId.m(studentId, iterationId)
        }
      }
    }
  }

  def reportRoutes(studentId: Int, startDate: Option[Instant], endDate: Option[Instant]) = {
    path("report") {
      import StudentReportProtocol._
      get {
        complete {
          reportService ? GetStudentReport.m(studentId, startDate, endDate)
        }
      }
    }
  }
}
