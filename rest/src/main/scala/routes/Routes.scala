package routes

import akka.http.scaladsl.server.Directives._

class Routes(employmentRoutes: EmploymentRoutes,
             employmentTypeRoutes: EmploymentTypeRoutes,
             mentorRoutes: MentorRoutes,
             studentRoutes: StudentRoutes,
             studentTypeRoutes: StudentTypeRoutes,
             userRoutes: UserRoutes,
             iterationRoutes: IterationRoutes,
             feedbackRoutes: FeedbackRoutes,
             studentIterationRoutes: StudentIterationRoutes,
             resultTypeRoutes: ResultTypeRoutes,
             skillRoutes: SkillRoutes) {

  val routes =
    employmentRoutes.routes ~
      employmentTypeRoutes.routes ~
      mentorRoutes.routes ~
      studentRoutes.routes ~
      studentTypeRoutes.routes ~
      userRoutes.routes ~
      iterationRoutes.routes ~
      resultTypeRoutes.routes ~
      feedbackRoutes.routes ~
      studentIterationRoutes.routes ~
      skillRoutes.routes
}
