package routes

import java.time.Instant
import akka.http.scaladsl.server.Directives._
import akka.typed.ActorRef
import akka.typed.AskPattern._
import config.RouteConfig
import dto.Iteration
import service.protocols.IterationProtocol
import com.epam.education.management.utils.AkkaHttpPickler._

class IterationRoutes(iterationService: ActorRef[IterationProtocol], config: RouteConfig) {

  import config._

  val routes =
    (pathPrefix("iteration") & rejectEmptyResponse) {
      import IterationProtocol._
      pathEnd {
        (post & entity(as[Iteration])) { iteration =>
          complete {
            iterationService ? Create.m(iteration)
          }
        } ~ // TODO use single `parameters` group
        (get & parameters('start.as[Instant].?) & parameters('end.as[Instant].?)) {
          (startDate: Option[Instant], endDate: Option[Instant]) =>
            complete {
              iterationService ? FindAll.m(startDate, endDate)
            }
        }
      } ~
      path(IntNumber) { iterationId =>
        get {
          complete {
            iterationService ? GetById.m(iterationId)
          }
        } ~
        (put & entity(as[Iteration])) { iteration =>
          complete {
            iterationService ? Update.m(iteration)
          }
        } ~
        delete {
          complete {
            iterationService ? Delete.m(iterationId)
          }
        }
      }
    }
}
