package routes

import java.time.Instant

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.IntNumber
import akka.typed.ActorRef
import akka.typed.AskPattern._
import config.RouteConfig
import dto.{Employment, Iteration}
import com.epam.education.management.utils.AkkaHttpPickler._
import service.protocols._

class EmploymentRoutes(employmentService: ActorRef[EmploymentProtocol],
                       iterationService: ActorRef[IterationProtocol],
                       studentWithDataService: ActorRef[StudentWithDataProtocol],
                       studentService: ActorRef[StudentProtocol],
                       studentIterationService: ActorRef[StudentIterationProtocol],
                       config: RouteConfig) {

  import config._

  def iterationRoute(employmentId: Int) = {
    import IterationProtocol._
    pathPrefix("iteration") {
      pathEnd {
        (post & entity(as[Iteration])) { iteration =>
          complete {
            iterationService ? Create.m(iteration)
          }
        } ~ // TODO use single `parameters` group
        (get & parameters('start.as[Instant].?) & parameters('end.as[Instant].?)) {
          (startDate: Option[Instant], endDate: Option[Instant]) =>
            complete {
              iterationService ? FindByEmploymentId.m(employmentId, startDate, endDate)
            }
        }
      } ~
      pathPrefix(IntNumber) { iterationId =>
        pathEnd {
          get {
            complete {
              iterationService ? GetById.m(iterationId)
            }
          } ~
          (put & entity(as[Iteration])) { iteration =>
            complete {
              iterationService ? Update.m(iteration)
            }
          } ~
          delete {
            complete {
              iterationService ? Delete.m(iterationId)
            }
          }
        } ~
        studentRoute(employmentId, iterationId)
      }
    }
  }

  def studentRoute(employmentId: Int, iterationId: Int) = {
    import StudentIterationProtocol._
    pathPrefix("student") {
      pathEnd {
        get {
          complete {
            studentIterationService ? FindByIterationId.m(iterationId)
          }
        }
      } ~
      path(IntNumber) { studentId =>
        delete {
          complete {
            studentIterationService ? Delete.m(studentId, iterationId)
          }
        }
      }
    }
  }

  val routes = rejectEmptyResponse {
    import EmploymentProtocol._
    pathPrefix("employment") {
      pathEnd {
        (post & entity(as[Employment])) { employment =>
          complete {
            employmentService ? Create.m(employment)
          }
        } ~
        get {
          complete(employmentService ? FindAll)
        }
      } ~
      pathPrefix(IntNumber) { employmentId =>
        pathEnd {
          get {
            complete {
              employmentService ? GetById.m(employmentId)
            }
          } ~
          (put & entity(as[Employment])) { employment =>
            complete {
              employmentService ? Update.m(employment)
            }
          } ~
          delete {
            complete {
              employmentService ? Delete.m(employmentId)
            }
          }
        } ~
        iterationRoute(employmentId)
      }
    }
  }
}
