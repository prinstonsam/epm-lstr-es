package routes

import akka.http.scaladsl.server.Directives._
import akka.typed.ActorRef
import akka.typed.AskPattern._
import config.RouteConfig
import dto.StudentType
import service.protocols.StudentTypeProtocol
import com.epam.education.management.utils.AkkaHttpPickler._

class StudentTypeRoutes(studentTypeService: ActorRef[StudentTypeProtocol], config: RouteConfig) {

  import config._

  val routes =
    (pathPrefix("studenttype") & rejectEmptyResponse) {
      import StudentTypeProtocol._
      pathEnd {
        (post & entity(as[StudentType])) { studentType =>
          complete {
            studentTypeService ? Create.m(studentType)
          }
        } ~
        get {
          complete {
            studentTypeService ? FindAll
          }
        }
      } ~
      path(IntNumber) { studentTypeId =>
        get {
          complete {
            studentTypeService ? GetById.m(studentTypeId)
          }
        } ~
        (put & entity(as[StudentType])) { studentType =>
          complete {
            studentTypeService ? Update.m(studentType)
          }
        } ~
        delete {
          complete {
            studentTypeService ? Delete.m(studentTypeId)
          }
        }
      }
    }
}