package routes

import akka.http.scaladsl.server.Directives._
import akka.typed.ActorRef
import akka.typed.AskPattern._
import com.epam.education.management.utils.AkkaHttpPickler._
import config.RouteConfig
import dto.ResultType
import service.protocols.ResultTypeProtocol

class ResultTypeRoutes(resultTypeService: ActorRef[ResultTypeProtocol], config: RouteConfig) {

  import config._

  val routes =
    (pathPrefix("resulttype") & rejectEmptyResponse) {
      import ResultTypeProtocol._
      pathEnd {
        (post & entity(as[ResultType])) { resultType =>
          complete {
            resultTypeService ? Create.m(resultType)
          }
        } ~
        get {
          complete {
            resultTypeService ? FindAll
          }
        }
      } ~
      path(IntNumber) { resultTypeId =>
        get {
          complete {
            resultTypeService ? GetById.m(resultTypeId)
          }
        } ~
        (put & entity(as[ResultType])) { resultType =>
          complete {
            resultTypeService ? Update.m(resultType)
          }
        } ~
        delete {
          complete {
            resultTypeService ? Delete.m(resultTypeId)
          }
        }
      }
    }
}