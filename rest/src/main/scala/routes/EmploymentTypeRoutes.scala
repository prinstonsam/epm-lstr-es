package routes

import akka.http.scaladsl.server.Directives._
import akka.typed.ActorRef
import dto.EmploymentType
import com.epam.education.management.utils.AkkaHttpPickler._
import akka.typed.AskPattern._
import config.RouteConfig
import service.protocols._

class EmploymentTypeRoutes(employmentTypeService: ActorRef[EmploymentTypeProtocol], config: RouteConfig) {

  import config._

  val routes = rejectEmptyResponse {
    import EmploymentTypeProtocol._
    pathPrefix("employmenttype") {
      pathEnd {
        (post & entity(as[EmploymentType])) { employmentType =>
          complete {
            employmentTypeService ? Create.m(employmentType)
          }
        } ~
        get {
          complete(employmentTypeService ? FindAll)
        }
      } ~
      pathPrefix(IntNumber) { employmentTypeId =>
        pathEnd {
          get {
            complete {
              employmentTypeService ? GetById.m(employmentTypeId)
            }
          } ~
          (put & entity(as[EmploymentType])) { employmentType =>
            complete {
              employmentTypeService ? Update.m(employmentType)
            }
          } ~
          delete {
            complete {
              employmentTypeService ? Delete.m(employmentTypeId)
            }
          }
        }
      }
    }
  }
}
