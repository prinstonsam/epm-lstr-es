package com.epam.education.management.utils

import java.time.Instant

import upickle.Js

class CustomPickler extends upickle.AttributeTagged {
  override implicit def OptionW[T: Writer]: Writer[Option[T]] = Writer {
    case None => Js.Null
    case Some(s) => implicitly[Writer[T]].write(s)
  }

  override implicit def OptionR[T: Reader]: Reader[Option[T]] = Reader {
    case Js.Null => None
    case v: Js.Value => Some(implicitly[Reader[T]].read.apply(v))
  }

  case class InstantDto(epochSecond: Long, nano: Int)

  private val InstantDtoW = implicitly[Writer[InstantDto]]
  private val InstantDtoR = implicitly[Reader[InstantDto]]

  implicit val InstantW: Writer[Instant] = Writer { instant =>
    InstantDtoW.write(InstantDto(instant.getEpochSecond, instant.getNano))
  }

  implicit val InstantR: Reader[Instant] = Reader {
    InstantDtoR.read.andThen(dto => Instant.ofEpochSecond(dto.epochSecond, dto.nano.toLong))
  }
}

object CustomPickler {
  val Import = new CustomPickler
}
