Jira Epam: https://jirapct.epam.com/jira/secure/RapidBoard.jspa?rapidView=40057&view=detail&selectedIssue=EPMLSTRES-6

Run this command before fist compilation and after each change in `serviceInterfaces`:
```sh
sbt serviceInterfaces/publishLocal
```

Use this command to start Web GUI:
```scala
sbt clean serviceInterfaces/publishLocal ~launch/re-start
```
