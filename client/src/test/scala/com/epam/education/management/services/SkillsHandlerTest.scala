package com.epam.education.management.services

import com.epam.education.management.services.SkillsHandler._
import diode.ActionResult.{ModelUpdate, ModelUpdateEffect}
import diode.RootModelRW
import diode.data._
import dto.Skill
import utest._

object SkillsHandlerTest extends TestSuite {

  override def tests = TestSuite {
    "SkillsHandler" - {
      val skill1: Skill = Skill(Some(1), "type1")
      val skill2: Skill = Skill(Some(2), "type2")
      val skill3: Skill = Skill(Some(3), "type3")
      val skill4: Skill = Skill(Some(4), "type4")

      val skills = Seq(skill1, skill2, skill3, skill4)

      val newSkills = Seq(Skill(Some(10), "type10"), Skill(Some(11), "type11"))

      def build = new SkillsHandler(new RootModelRW(Ready(Skills(skills))))

      "UpdateSkills(Empty -> Empty)" - {
        val h      = new SkillsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateSkills())
        assertMatch(result) {
          case ModelUpdateEffect(Pending(_), _) =>
        }
      }

      "UpdateSkills(Ready -> Empty)" - {
        val h      = build
        val result = h.handle(UpdateSkills())
        assertMatch(result) {
          case ModelUpdateEffect(PendingStale(Skills(`skills`), _), _) =>
        }
      }

      "UpdateSkills(Empty -> Ready)" - {
        val h      = new SkillsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateSkills(Ready(Skills(newSkills))))
        assertMatch(result) {
          case ModelUpdate(Ready(Skills(`newSkills`))) =>
        }
      }

      "UpdateSkills(Ready -> Ready)" - {
        val h      = build
        val result = h.handle(UpdateSkills(Ready(Skills(newSkills))))
        assertMatch(result) {
          case ModelUpdate(Ready(Skills(`newSkills`))) =>
        }
      }

      'UpdateSkill - {
        val h                     = build
        val updatedSkill = Skill(Some(3), "newType3")
        val result                = h.handle(UpdateSkill(updatedSkill))
        val resultSeq =
          Seq(skill1, skill2, updatedSkill, skill4)
        assertMatch(result) {
          case ModelUpdateEffect(
              Ready(
              Skills(`resultSeq`)
              ),
              _) =>
        }
      }

      'CreateSkill - {
        val h                 = build
        val newSkill = Skill(Some(667), "newType667")
        val result            = h.handle(CreateSkill(newSkill))
        val resultSeq =
          Seq(skill1, skill2, skill3, skill4, newSkill)
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Skills(`resultSeq`)), _) =>
        }
      }

      'DeleteSkill - {
        val h         = build
        val result    = h.handle(DeleteSkill(skill3))
        val resultSeq = Seq(skill1, skill2, skill4)
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Skills(`resultSeq`)), _) =>
        }
      }
    }
  }
}
