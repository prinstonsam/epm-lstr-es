package com.epam.education.management.services

import com.epam.education.management.services.EmploymentTypesHandler._
import diode.ActionResult.{ModelUpdate, ModelUpdateEffect}
import diode.RootModelRW
import diode.data._
import dto.EmploymentType
import utest._

object EmploymentTypesHandlerTest extends TestSuite {

  override def tests = TestSuite {
    "EmploymentTypesHandler" - {
      val employmentType1: EmploymentType = EmploymentType(Some(1), "type1")
      val employmentType2: EmploymentType = EmploymentType(Some(2), "type2")
      val employmentType3: EmploymentType = EmploymentType(Some(3), "type3")
      val employmentType4: EmploymentType = EmploymentType(Some(4), "type4")

      val employmentTypes = Seq(employmentType1, employmentType2, employmentType3, employmentType4)

      val newEmploymentTypes = Seq(EmploymentType(Some(10), "type10"), EmploymentType(Some(11), "type11"))

      def build = new EmploymentTypesHandler(new RootModelRW(Ready(EmploymentTypes(employmentTypes))))

      "UpdateEmploymentTypes(Empty -> Empty)" - {
        val h      = new EmploymentTypesHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateEmploymentTypes())
        assertMatch(result) {
          case ModelUpdateEffect(Pending(_), _) =>
        }
      }

      "UpdateEmploymentTypes(Ready -> Empty)" - {
        val h      = build
        val result = h.handle(UpdateEmploymentTypes())
        assertMatch(result) {
          case ModelUpdateEffect(PendingStale(EmploymentTypes(`employmentTypes`), _), _) =>
        }
      }

      "UpdateEmploymentTypes(Empty -> Ready)" - {
        val h      = new EmploymentTypesHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateEmploymentTypes(Ready(EmploymentTypes(newEmploymentTypes))))
        assertMatch(result) {
          case ModelUpdate(Ready(EmploymentTypes(`newEmploymentTypes`))) =>
        }
      }

      "UpdateEmploymentTypes(Ready -> Ready)" - {
        val h      = build
        val result = h.handle(UpdateEmploymentTypes(Ready(EmploymentTypes(newEmploymentTypes))))
        assertMatch(result) {
          case ModelUpdate(Ready(EmploymentTypes(`newEmploymentTypes`))) =>
        }
      }

      'UpdateEmploymentType - {
        val h                     = build
        val updatedEmploymentType = EmploymentType(Some(3), "newType3")
        val result                = h.handle(UpdateEmploymentType(updatedEmploymentType))
        val resultSeq =
          Seq(employmentType1, employmentType2, updatedEmploymentType, employmentType4)
        assertMatch(result) {
          case ModelUpdateEffect(
              Ready(
              EmploymentTypes(`resultSeq`)
              ),
              _) =>
        }
      }

      'CreateEmploymentType - {
        val h                 = build
        val newEmploymentType = EmploymentType(Some(667), "newType667")
        val result            = h.handle(CreateEmploymentType(newEmploymentType))
        val resultSeq =
          Seq(employmentType1, employmentType2, employmentType3, employmentType4, newEmploymentType)
        assertMatch(result) {
          case ModelUpdateEffect(Ready(EmploymentTypes(`resultSeq`)), _) =>
        }
      }

      'DeleteEmploymentType - {
        val h         = build
        val result    = h.handle(DeleteEmploymentType(employmentType3))
        val resultSeq = Seq(employmentType1, employmentType2, employmentType4)
        assertMatch(result) {
          case ModelUpdateEffect(Ready(EmploymentTypes(`resultSeq`)), _) =>
        }
      }
    }
  }
}
