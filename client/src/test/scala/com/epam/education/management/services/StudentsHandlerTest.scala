package com.epam.education.management.services

import java.time.Instant

import com.epam.education.management.services.StudentsHandler._
import diode.ActionResult.{ModelUpdate, ModelUpdateEffect}
import diode.RootModelRW
import diode.data._
import dto.{Student, StudentData, StudentWithData}
import utest._

object StudentsHandlerTest extends TestSuite {
  override def tests = TestSuite {
    "StudentsHandler" - {
      val studentWithData1: StudentWithData = StudentWithData(
        Student(Some(1), "firstName1", "lastName1", "email1"),
        StudentData(1, 1, 1, Instant.now(), None))
      val studentWithData2: StudentWithData = StudentWithData(
        Student(Some(2), "firstName2", "lastName2", "email2"),
        StudentData(2, 1, 1, Instant.now(), None))
      val studentWithData3: StudentWithData = StudentWithData(
        Student(Some(3), "firstName3", "lastName3", "email3"),
        StudentData(3, 1, 1, Instant.now(), None))
      val studentWithData4: StudentWithData = StudentWithData(
        Student(Some(4), "firstName4", "lastName4", "email4"),
        StudentData(4, 1, 1, Instant.now(), None))

      val studentWithData = Seq(
        studentWithData1,
        studentWithData2,
        studentWithData3,
        studentWithData4
      )

      val newStudentWithData = Seq(
        StudentWithData(
          Student(Some(10), "firstName10", "lastName10", "email10"),
          StudentData(10, 1, 1, Instant.now(), None)),
        StudentWithData(
          Student(Some(11), "firstName11", "lastName11", "email11"),
          StudentData(11, 1, 1, Instant.now(), None))
      )

      def build = new StudentsHandler(new RootModelRW(Ready(StudentsWithData(studentWithData))))

      "UpdateStudents(Empty -> Empty)" - {
        val h      = new StudentsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateStudents())
        assertMatch(result) {
          case ModelUpdateEffect(Pending(_), _) =>
        }
      }

      "UpdateStudents(Ready -> Empty)" - {
        val h      = build
        val result = h.handle(UpdateStudents())
        assertMatch(result) {
          case ModelUpdateEffect(PendingStale(StudentsWithData(`studentWithData`), _), _) =>
        }
      }

      "UpdateStudents(Empty -> Ready)" - {
        val h      = new StudentsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateStudents(Ready(StudentsWithData(newStudentWithData))))
        assertMatch(result) {
          case ModelUpdate(Ready(StudentsWithData(`newStudentWithData`))) =>
        }
      }

      "UpdateStudents(Ready -> Ready)" - {
        val h      = build
        val result = h.handle(UpdateStudents(Ready(StudentsWithData(newStudentWithData))))
        assertMatch(result) {
          case ModelUpdate(Ready(StudentsWithData(`newStudentWithData`))) =>
        }
      }

      'UpdateStudentWithData - {
        val h = build
        val updatedStudentWithData = StudentWithData(
          Student(Some(3), "newFirstName", "newLastName", "newEmail"),
          StudentData(3, 1, 1, Instant.now(), None))
        val result = h.handle(UpdateStudent(updatedStudentWithData))
        assertMatch(result) {
          case ModelUpdateEffect(
              Ready(
              StudentsWithData(
              Seq(`studentWithData1`, `studentWithData2`, `updatedStudentWithData`, `studentWithData4`))),
              _) =>
        }
      }

      'CreateStudentWithData - {
        val h = build
        val newStudentWithData = StudentWithData(
          Student(Some(667), "newFirstName", "newLastName", "newEmail"),
          StudentData(667, 1, 1, Instant.now(), None))
        val result = h.handle(CreateStudentWithData(newStudentWithData))
        assertMatch(result) {
          case ModelUpdateEffect(
              Ready(
              StudentsWithData(
              Seq(
              `studentWithData1`,
              `studentWithData2`,
              `studentWithData3`,
              `studentWithData4`,
              `newStudentWithData`))),
              _) =>
        }
      }

      'DeleteStudentWithData - {
        val h      = build
        val result = h.handle(DeleteStudent(studentWithData3))
        assertMatch(result) {
          case ModelUpdateEffect(
              Ready(StudentsWithData(Seq(`studentWithData1`, `studentWithData2`, `studentWithData4`))),
              _) =>
        }
      }
    }
  }
}
