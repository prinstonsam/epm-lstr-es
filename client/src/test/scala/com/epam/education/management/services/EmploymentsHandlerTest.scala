package com.epam.education.management.services

import com.epam.education.management.services.EmploymentsHandler._
import diode.ActionResult.{ModelUpdate, ModelUpdateEffect}
import diode.RootModelRW
import diode.data._
import dto.Employment
import utest._

object EmploymentsHandlerTest extends TestSuite{
  override def tests = TestSuite {
    "EmploymentsHandler" - {
      val employment1: Employment = Employment(Some(1), "firstName1", 1, 1)
      val employment2: Employment = Employment(Some(2), "firstName2", 1, 1)
      val employment3: Employment = Employment(Some(3), "firstName3", 1, 1)
      val employment4: Employment = Employment(Some(4), "firstName4", 1, 1)

      val employments = Seq(
       employment1,
       employment2,
       employment3,
       employment4
      )

      val newEmployments = Seq(
        Employment(Some(10), "firstName10", 1, 1),
        Employment(Some(11), "firstName11", 1, 1)
      )

      def build = new EmploymentsHandler(new RootModelRW(Ready(Employments(employments))))

      "UpdateEmployments(Empty -> Empty)" - {
        val h = new EmploymentsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateEmployments())
        assertMatch(result) {
          case ModelUpdateEffect(Pending(_), _) =>
        }
      }

      "UpdateEmployments(Ready -> Empty)" - {
        val h = build
        val result = h.handle(UpdateEmployments())
        assertMatch(result) {
          case ModelUpdateEffect(PendingStale(Employments(`employments`), _), _) =>
        }
      }

      "UpdateEmployments(Empty -> Ready)" - {
        val h = new EmploymentsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateEmployments(Ready(Employments(newEmployments))))
        assertMatch(result) {
          case ModelUpdate(Ready(Employments(`newEmployments`))) =>
        }
      }

      "UpdateEmployments(Ready -> Ready)" - {
        val h = build
        val result = h.handle(UpdateEmployments(Ready(Employments(newEmployments))))
        assertMatch(result) {
          case ModelUpdate(Ready(Employments(`newEmployments`))) =>
        }
      }

      'UpdateEmployment - {
        val h = build
        val updatedEmployment = Employment(Some(3), "newFirstName", 1, 1)
        val result = h.handle(UpdateEmployment(updatedEmployment))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Employments(Seq(`employment1`, `employment2`, `updatedEmployment`, `employment4`))), _) =>
        }
      }

      'CreateEmployment - {
        val h = build
        val newEmployment = Employment(Some(667), "newFirstName", 1, 1)
        val result = h.handle(CreateEmployment(newEmployment))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Employments(Seq(`employment1`, `employment2`, `employment3`, `employment4`, `newEmployment`))), _) =>
        }
      }

      'DeleteEmployment - {
        val h = build
        val result = h.handle(DeleteEmployment(employment3))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Employments(Seq(`employment1`, `employment2`, `employment4`))), _) =>
        }
      }
    }
  }
}