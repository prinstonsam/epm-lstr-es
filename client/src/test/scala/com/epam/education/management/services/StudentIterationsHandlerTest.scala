package com.epam.education.management.services

import com.epam.education.management.services.StudentIterationsHandler._
import diode.ActionResult.{ModelUpdate, ModelUpdateEffect}
import diode.RootModelRW
import diode.data._
import dto.StudentIteration
import utest._

object StudentIterationsHandlerTest extends TestSuite {
  override def tests = TestSuite {
    "StudentIterationsHandler" - {
      val studentIteration1: StudentIteration = StudentIteration(1,1,1)
      val studentIteration2: StudentIteration = StudentIteration(1,2,1)
      val studentIteration3: StudentIteration = StudentIteration(2,1,1)
      val studentIteration4: StudentIteration = StudentIteration(2,2,2)

      val studentIterations = Seq(
        studentIteration1,
        studentIteration2,
        studentIteration3,
        studentIteration4
      )

      val newStudentIterations = Seq(
        StudentIteration(2,1,2),
        StudentIteration(1,1,2)
      )

      def build = new StudentIterationsHandler(new RootModelRW(Ready(StudentIterations(studentIterations))))

      "UpdateStudentIterations(Empty -> Empty)" - {
        val h = new StudentIterationsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateStudentIterations())
        assertMatch(result) {
          case ModelUpdateEffect(Pending(_), _) =>
        }
      }

      "UpdateStudentIterations(Ready -> Empty)" - {
        val h = build
        val result = h.handle(UpdateStudentIterations())
        assertMatch(result) {
          case ModelUpdateEffect(PendingStale(StudentIterations(`studentIterations`), _), _) =>
        }
      }

      "UpdateStudentIterations(Empty -> Ready)" - {
        val h = new StudentIterationsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateStudentIterations(Ready(StudentIterations(newStudentIterations))))
        assertMatch(result) {
          case ModelUpdate(Ready(StudentIterations(`newStudentIterations`))) =>
        }
      }

      "UpdateStudentIterations(Ready -> Ready)" - {
        val h = build
        val result = h.handle(UpdateStudentIterations(Ready(StudentIterations(newStudentIterations))))
        assertMatch(result) {
          case ModelUpdate(Ready(StudentIterations(`newStudentIterations`))) =>
        }
      }

      'UpdateStudentIteration - {
        val h = build
        val updatedStudentIteration = StudentIteration(2,1,1)
        val result = h.handle(UpdateStudentIteration(updatedStudentIteration))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(StudentIterations(Seq(`studentIteration1`, `studentIteration2`, `updatedStudentIteration`, `studentIteration4`))), _) =>
        }
      }

      'CreateStudentIteration - {
        val h = build
        val newStudentIteration = StudentIteration(4,4,4)
        val result = h.handle(CreateStudentIteration(newStudentIteration))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(StudentIterations(Seq(`studentIteration1`, `studentIteration2`, `studentIteration3`, `studentIteration4`, `newStudentIteration`))), _) =>
        }
      }

      'DeleteStudentIteration - {
        val h = build
        val result = h.handle(DeleteStudentIteration(studentIteration3))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(StudentIterations(Seq(`studentIteration1`, `studentIteration2`, `studentIteration4`))), _) =>
        }
      }
    }
  }
}
