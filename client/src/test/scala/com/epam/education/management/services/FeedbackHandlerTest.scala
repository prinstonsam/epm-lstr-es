package com.epam.education.management.services

import com.epam.education.management.services.FeedbackHandler._
import diode.ActionResult.{ModelUpdate, ModelUpdateEffect}
import diode.RootModelRW
import diode.data.{Empty, Pending, PendingStale, Ready}
import dto.Feedback
import utest._

object FeedbackHandlerTest extends TestSuite {

  override def tests = TestSuite {
    "FeedbackHandler" - {
      val feedback1 = Feedback(Some(1), 1, 1, 1, "feedback1")
      val feedback2 = Feedback(Some(2), 2, 2, 2, "feedback2")
      val feedback3 = Feedback(Some(3), 3, 3, 3, "feedback3")
      val feedback4 = Feedback(Some(4), 3, 3, 3, "feedback4")

      val feedbacks = Seq(feedback1, feedback2, feedback3, feedback4)

      val newFeedbacks = Seq(Feedback(Some(10), 2, 2, 2, "feedback10"), Feedback(Some(11), 1, 1, 1, "feedback11"))

      def build = new FeedbackHandler(new RootModelRW(Ready(Feedbacks(feedbacks))))

      "UpdateFeedbacks(Empty -> Empty)" - {
        val h      = new FeedbackHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateFeedbacks())
        assertMatch(result) {
          case ModelUpdateEffect(Pending(_), _) =>
        }

        "UpdateFeedbacks(Ready -> Empty)" - {
          val h      = build
          val result = h.handle(UpdateFeedbacks())
          assertMatch(result) {
            case ModelUpdateEffect(PendingStale(Feedbacks(`feedbacks`), _), _) =>
          }
        }

        "UpdateFeedbacks(Empty -> Ready)" - {
          val h      = new FeedbackHandler(new RootModelRW(Empty))
          val result = h.handle(UpdateFeedbacks(Ready(Feedbacks(newFeedbacks))))
          assertMatch(result) {
            case ModelUpdate(Ready(Feedbacks(`newFeedbacks`))) =>
          }
        }

        "UpdateFeedbacks(Ready -> Ready)" - {
          val h      = build
          val result = h.handle(UpdateFeedbacks(Ready(Feedbacks(newFeedbacks))))
          assertMatch(result) {
            case ModelUpdate(Ready(Feedbacks(`newFeedbacks`))) =>
          }
        }

        "UpdateFeedbacks" - {
          val h               = build
          val updatedFeedback = Feedback(Some(3), 1, 1, 1, "Updated feedback")
          val result          = h.handle(UpdateFeedback(updatedFeedback))
          assertMatch(result) {
            case ModelUpdateEffect(
                Ready(Feedbacks(Seq(`feedback1`, `feedback2`, `updatedFeedback`, `feedback4`))),
                _) =>
          }
        }

        "CreateFeedback" - {
          val h           = build
          val newFeedback = Feedback(Some(20), 1, 1, 1, "CreatedFeedback")
          val result      = h.handle(CreateFeedback(newFeedback))
          assertMatch(result) {
            case ModelUpdateEffect(
                Ready(Feedbacks(Seq(`feedback1`, `feedback2`, `feedback3`, `feedback4`, `newFeedback`))),
                _) =>
          }
        }

        "DeleteFeedback" - {
          val h      = build
          val result = h.handle(DeleteFeedback(feedback4))
          assertMatch(result) {
            case ModelUpdateEffect(Ready(Feedbacks(Seq(`feedback1`, `feedback2`, `feedback3`))), _) =>
          }
        }
      }
    }
  }
}
