package com.epam.education.management.services

import com.epam.education.management.services.UsersHandler._
import diode.ActionResult.{ModelUpdate, ModelUpdateEffect}
import diode.RootModelRW
import diode.data._
import dto.User
import utest._

object UsersHandlerTest extends TestSuite {
  override def tests = TestSuite {
    "UsersHandler" - {
      val user1: User = User(Some(1), "firstName1", "lastName1", "email1","password1")
      val user2: User = User(Some(2), "firstName2", "lastName2", "email2","password2")
      val user3: User = User(Some(3), "firstName3", "lastName3", "email3","password3")
      val user4: User = User(Some(4), "firstName4", "lastName4", "email4","password4")

      val users = Seq(
        user1,
        user2,
        user3,
        user4
      )

      val newUsers = Seq(
        User(Some(10), "firstName10", "lastName10", "email10","password10"),
        User(Some(11), "firstName11", "lastName11", "email11","password11")
      )

      def build = new UsersHandler(new RootModelRW(Ready(Users(users))))

      "UpdateUsers(Empty -> Empty)" - {
        val h = new UsersHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateUsers())
        assertMatch(result) {
          case ModelUpdateEffect(Pending(_), _) =>
        }
      }

      "UpdateUsers(Ready -> Empty)" - {
        val h = build
        val result = h.handle(UpdateUsers())
        assertMatch(result) {
          case ModelUpdateEffect(PendingStale(Users(`users`), _), _) =>
        }
      }

      "UpdateUsers(Empty -> Ready)" - {
        val h = new UsersHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateUsers(Ready(Users(newUsers))))
        assertMatch(result) {
          case ModelUpdate(Ready(Users(`newUsers`))) =>
        }
      }

      "UpdateUsers(Ready -> Ready)" - {
        val h = build
        val result = h.handle(UpdateUsers(Ready(Users(newUsers))))
        assertMatch(result) {
          case ModelUpdate(Ready(Users(`newUsers`))) =>
        }
      }

      'UpdateUser - {
        val h = build
        val updatedUser = User(Some(3), "newFirstName", "newLastName", "newEmail","newPassword")
        val result = h.handle(UpdateUser(updatedUser))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Users(Seq(`user1`, `user2`, `updatedUser`, `user4`))), _) =>
        }
      }

      'CreateUser - {
        val h = build
        val newUser = User(Some(667), "newFirstName", "newLastName", "newEmail","newPassword")
        val result = h.handle(CreateUser(newUser))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Users(Seq(`user1`, `user2`, `user3`, `user4`, `newUser`))), _) =>
        }
      }

      'DeleteUser - {
        val h = build
        val result = h.handle(DeleteUser(user3))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Users(Seq(`user1`, `user2`, `user4`))), _) =>
        }
      }
    }
  }
}
