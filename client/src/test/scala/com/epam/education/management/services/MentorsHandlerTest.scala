package com.epam.education.management.services

import com.epam.education.management.services.MentorsHandler._
import diode.ActionResult.{ModelUpdate, ModelUpdateEffect}
import diode.RootModelRW
import diode.data._
import dto.Mentor
import utest._

object MentorsHandlerTest extends TestSuite {
  override def tests = TestSuite {
    "MentorsHandler" - {
      val mentor1: Mentor = Mentor(Some(1),"skill1", "fierstName1", "lastName1", "emsil1")
      val mentor2: Mentor = Mentor(Some(2),"skill2", "fierstName2", "lastName2", "emsil2")
      val mentor3: Mentor = Mentor(Some(3),"skill3", "fierstName3", "lastName3", "emsil3")
      val mentor4: Mentor = Mentor(Some(4),"skill4", "fierstName4", "lastName4", "emsil4")

      val mentors = Seq(
        mentor1,
        mentor2,
        mentor3,
        mentor4
      )

      val newMentors = Seq(
        Mentor(Some(10),"skill10", "fierstName10", "lastName10", "emsil10"),
        Mentor(Some(11),"skill11", "fierstName11", "lastName11", "emsil11")
      )

      def build = new MentorsHandler(new RootModelRW(Ready(Mentors(mentors))))

      "UpdateMentors(Empty -> Empty)" - {
        val h = new MentorsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateMentors())
        assertMatch(result) {
          case ModelUpdateEffect(Pending(_), _) =>
        }
      }

      "UpdateMentors(Ready -> Empty)" - {
        val h = build
        val result = h.handle(UpdateMentors())
        assertMatch(result) {
          case ModelUpdateEffect(PendingStale(Mentors(`mentors`), _), _) =>
        }
      }

      "UpdateMentors(Empty -> Ready)" - {
        val h = new MentorsHandler(new RootModelRW(Empty))
        val result = h.handle(UpdateMentors(Ready(Mentors(newMentors))))
        assertMatch(result) {
          case ModelUpdate(Ready(Mentors(`newMentors`))) =>
        }
      }

      "UpdateMentors(Ready -> Ready)" - {
        val h = build
        val result = h.handle(UpdateMentors(Ready(Mentors(newMentors))))
        assertMatch(result) {
          case ModelUpdate(Ready(Mentors(`newMentors`))) =>
        }
      }

      'UpdateMentor - {
        val h = build
        val updatedMentor = Mentor(Some(3),"newSkill", "newFierstName", "newLastName", "newEmsil")
        val result = h.handle(UpdateMentor(updatedMentor))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Mentors(Seq(`mentor1`, `mentor2`, `updatedMentor`, `mentor4`))), _) =>
        }
      }

      'CreateMentor - {
        val h = build
        val newMentor = Mentor(Some(667),"newSkill", "newFierstName", "newLastName", "newEmsil")
        val result = h.handle(CreateMentor(newMentor))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Mentors(Seq(`mentor1`, `mentor2`, `mentor3`, `mentor4`, `newMentor`))), _) =>
        }
      }

      'DeleteMentor - {
        val h = build
        val result = h.handle(DeleteMentor(mentor3))
        assertMatch(result) {
          case ModelUpdateEffect(Ready(Mentors(Seq(`mentor1`, `mentor2`, `mentor4`))), _) =>
        }
      }
    }
  }
}
