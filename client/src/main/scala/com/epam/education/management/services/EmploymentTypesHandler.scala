package com.epam.education.management.services

import com.epam.education.management.services.EmploymentTypesHandler.EmploymentTypes
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.EmploymentType

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

class EmploymentTypesHandler[M](modelRW: ModelRW[M, Pot[EmploymentTypes]]) extends ActionHandler(modelRW) {

  import EmploymentTypesHandler._

  override def handle = {
    case a: EmploymentTypeAction => handleStrict(a)
  }

  def handleStrict(action: EmploymentTypeAction) = action match {
    case FetchEmploymentTypesIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateEmploymentTypes()))
      else noChange
    case action: UpdateEmploymentTypes =>
      val updateF =
        action.effect(AjaxClient.employmentType.findAll())(EmploymentTypes)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateEmploymentType(item) =>
      // make a local update and inform server
      updated(
        value.map(_.updated(item)),
        Effect(AjaxClient.employmentType.update(item).map(_ => UpdateEmploymentTypes())))
    case CreateEmploymentType(item) =>
      // make a local update and inform server
      updated(
        value.map(_.updated(item)),
        Effect(AjaxClient.employmentType.create(item).map(_ => UpdateEmploymentTypes())))
    case DeleteEmploymentType(item) =>
      // make a local update and inform server
      updated(
        value.map(_.remove(item)),
        Effect(AjaxClient.employmentType.delete(item.id.get).map(_ => UpdateEmploymentTypes())))
  }
}

object EmploymentTypesHandler {

  case class EmploymentTypes(items: Seq[EmploymentType]) {
    def updated(item: EmploymentType) = {
      items.indexWhere(_.id == item.id) match {
        case -1  => EmploymentTypes(items :+ item)
        case idx => EmploymentTypes(items.updated(idx, item))
      }
    }

    def remove(item: EmploymentType) = EmploymentTypes(items.filterNot(_ == item))
  }

  sealed trait EmploymentTypeAction extends Action

  case object FetchEmploymentTypesIfEmpty extends EmploymentTypeAction

  case class UpdateEmploymentTypes(potResult: Pot[EmploymentTypes] = Empty)
      extends PotAction[EmploymentTypes, UpdateEmploymentTypes]
      with EmploymentTypeAction {
    override def next(newResult: Pot[EmploymentTypes]): UpdateEmploymentTypes = UpdateEmploymentTypes(newResult)
  }

  case class UpdateEmploymentType(item: EmploymentType) extends EmploymentTypeAction

  case class CreateEmploymentType(item: EmploymentType) extends EmploymentTypeAction

  case class DeleteEmploymentType(item: EmploymentType) extends EmploymentTypeAction

}
