package com.epam.education.management.modules

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{GlobalStyles, Icon, MentorList}
import com.epam.education.management.logger.log
import com.epam.education.management.services.MentorsHandler._
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto.Mentor
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object MentorsModule {

  case class Props(proxy: ModelProxy[Pot[Mentors]])

  case class State(selectedItem: Option[Mentor] = None, showMentorForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the mentors, which will cause MentorsStore to fetch mentors from the server
      Callback.when(props.proxy().isEmpty)(props.proxy.dispatch(UpdateMentors()))

    def editMentor(item: Option[Mentor]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showMentorForm = true))

    def mentorEdited(item: Mentor, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Mentor editing cancelled")
      } else {
        val action =
          if (item.id.getOrElse(-1) > 0) UpdateMentor(item)
          else CreateMentor(item)
        Callback.log(s"Mentor edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showMentorForm = false))
    }

    def render(p: Props, s: State) =
      Panel(
        Panel.Props("Mentors list"),
        <.div(
          Button(Button.Props(editMentor(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render { case (mentors) =>
          MentorList(
            mentors.items,
            item => editMentor(Some(item)),
            item => p.proxy.dispatch(DeleteMentor(item)))
        },
          // if the dialog is open, add it to the panel
          if (s.showMentorForm) MentorForm(MentorForm.Props(s.selectedItem, mentorEdited))
          else // otherwise add an empty placeholder
            Seq.empty[ReactElement]

      )
  }
  val component = ReactComponentB[Props]("Mentors")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[Mentors]]) = component(Props(proxy))
}

object MentorForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
    item: Option[Mentor],
    submitHandler: (Mentor, Boolean) => Callback
  )

  case class State(item: Mentor, cancelled: Boolean = true)

  class Backend(t: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      t.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.cancelled)

    def updateField(f: (Mentor, String) => Mentor)(e: ReactEventI) = {
      val text = e.target.value
      t.modState(s => s.copy(item = f(s.item, text)))
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.item.id.getOrElse(-1) <= 0) "adding" else "editing"} a mentor or two")
      val headerText = if (s.item.id.getOrElse(-1) <= 0) "Add new mentor" else "Edit mentor"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          <.label(^.`for` := "id", "Id"),
          <.input.text(bss.formControl, ^.id := "id", ^.value := s.item.id, ^.disabled := true)),
        <.div(bss.formGroup,
          <.label(^.`for` := "primarySkills", "Primary Skills"),
          <.input.text(bss.formControl, ^.id := "primarySkills", ^.value := s.item.primarySkills,
            ^.placeholder := "write primary skills", ^.onChange ==> updateField((s, t) => s.copy(primarySkills = t)))),
        <.div(bss.formGroup,
          <.label(^.`for` := "firstName", "Given Name"),
          <.input.text(bss.formControl, ^.id := "firstName", ^.value := s.item.firstName,
            ^.placeholder := "write given name", ^.onChange ==> updateField((s, t) => s.copy(firstName = t)))),
        <.div(bss.formGroup,
          <.label(^.`for` := "lastName", "Family Name"),
          <.input.text(bss.formControl, ^.id := "lastName", ^.value := s.item.lastName,
            ^.placeholder := "write family name", ^.onChange ==> updateField((s, t) => s.copy(lastName = t)))),
        <.div(bss.formGroup,
          <.label(^.`for` := "email", "Email"),
          <.input.text(bss.formControl, ^.id := "email", ^.value := s.item.email,
            ^.placeholder := "write email", ^.onChange ==> updateField((s, t) => s.copy(email = t))))
      )
    }
  }

  val component = ReactComponentB[Props]("MentorForm")
    .initialState_P(p => State(p.item.getOrElse(Mentor(primarySkills="", firstName = "", lastName = "", email = ""))))
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}