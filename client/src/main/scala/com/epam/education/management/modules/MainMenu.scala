package com.epam.education.management.modules

import com.epam.education.management.SMCMain._
import com.epam.education.management.components.Icon.Icon
import com.epam.education.management.components.{GlobalStyles, Icon}
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object MainMenu {
  // shorthand for styles
  @inline private def bss = GlobalStyles.bootstrapStyles

  case class Props(router: RouterCtl[Loc], currentLoc: Loc)

  private case class MenuItem(idx: Int, label: (Props) => ReactNode, icon: Icon, location: Loc)

  private val menuItems = Seq(
    MenuItem(1, _ => "Dashboard", Icon.dashboard, DashboardLoc),
    MenuItem(2, _ => "Students", Icon.check, StudentsLoc),
    MenuItem(3, _ => "Mentors", Icon.check, MentorsLoc),
    MenuItem(4, _ => "Users", Icon.check, UsersLoc),
    MenuItem(5, _ => "Feedbacks", Icon.check, FeedbackLoc),
    MenuItem(6, _ => "Iterations", Icon.check, IterationsLoc),
    MenuItem(7, _ => "Employments", Icon.check, EmploymentsLoc),
    MenuItem(8, _ => "Student iterations", Icon.check, StudentIterationsLoc),
    MenuItem(9, _ => "Dictionaries", Icon.check, DictionaryLoc),
    MenuItem(10, _ => "Skill", Icon.check, SkillsLoc)
  )

  private class Backend($: BackendScope[Props, Unit]) {
    def render(props: Props) = {
      <.ul(bss.navbar)(
        // build a list of menu items
        for (item <- menuItems) yield {
          <.li(
            ^.key := item.idx,
            (props.currentLoc == item.location) ?= (^.className := "active"),
            props.router.link(item.location)(item.icon, " ", item.label(props)))
        }
      )
    }
  }

  private val component = ReactComponentB[Props]("MainMenu").renderBackend[Backend].build

  def apply(ctl: RouterCtl[Loc], currentLoc: Loc): ReactElement =
    component(Props(ctl, currentLoc))
}
