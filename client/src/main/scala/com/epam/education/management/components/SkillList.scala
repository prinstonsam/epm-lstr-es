package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto.Skill
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object SkillList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[Skill],
      editItem: Skill => Callback,
      deleteItem: Skill => Callback
  )

  private val component =
    ReactComponentB[Props]("SkillList").render_P { p =>
      def renderItem(item: Skill) = {
        <.tr(
          <.td(item.id),
          <.td(item.title),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete")
            )
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Title"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[Skill], editItem: Skill => Callback, deleteItem: Skill => Callback) =
    component(Props(items, editItem, deleteItem))

}
