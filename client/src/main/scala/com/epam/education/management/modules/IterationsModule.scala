package com.epam.education.management.modules

import java.time.Instant

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{GlobalStyles, Icon, IterationList}
import com.epam.education.management.logger.log
import com.epam.education.management.services.IterationsHandler._
import com.epam.education.management.services.MentorsHandler.{FetchMentorsIfEmpty, Mentors}
import com.epam.education.management.services.EmploymentsHandler.{Employments, FetchEmploymentsIfEmpty}
import com.epam.education.management.utils.Instants._
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto.{Employment, Iteration, Mentor}
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scala.collection.breakOut
import scalacss.ScalaCssReact._

object IterationsModule {

  case class Props(proxy: ModelProxy[Pot[(Iterations, Map[Int, Mentor], Map[Int, Employment])]])

  case class State(selectedItem: Option[Iteration] = None, showIterationForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the iterations, which will cause IterationsStore to fetch iterations from the server
      Callback.when(props.proxy().isEmpty)(
        props.proxy.dispatch(UpdateIterations()) >>
        props.proxy.dispatch(FetchEmploymentsIfEmpty) >>
        props.proxy.dispatch(FetchMentorsIfEmpty)
      )

    def editIteration(item: Option[Iteration]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showIterationForm = true))

    def iterationEdited(item: Iteration, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Iteration editing cancelled")
      } else {
        val action =
          if (item.id.getOrElse(-1) > 0) UpdateIteration(item)
          else CreateIteration(item)
        Callback.log(s"Iteration edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showIterationForm = false))
    }

    def render(p: Props, s: State) = {
      Panel(
        Panel.Props("Iterations list"),
        <.div(
          Button(Button.Props(editIteration(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render { case (iterations, mentors, employments) =>
          IterationList(
            iterations.items,
            mentors,
            employments,
            item => editIteration(Some(item)),
            item => p.proxy.dispatch(DeleteIteration(item)))
        },
        p.proxy().render { case (_, mentors, employments) =>
          // if the dialog is open, add it to the panel
          if (s.showIterationForm) IterationForm(IterationForm.Props(s.selectedItem, mentors, employments, iterationEdited))
          else // otherwise add an empty placeholder
            Seq.empty[ReactElement]
        }
      )
    }
  }

  val component = ReactComponentB[Props]("Iterations")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */

  def apply(proxy: ModelProxy[Pot[(Iterations, Mentors, Employments)]]) = {

    def mentorsToMap(ms: Seq[Mentor]): Map[Int, Mentor] =
      ms.collect { case m@Mentor(Some(id), _, _, _, _) => id -> m }(breakOut)
    def employmentsToMap(ms: Seq[Employment]): Map[Int, Employment] =
      ms.collect { case e@Employment(Some(id), _, _, _) => id -> e }(breakOut)
    component(Props(proxy.zoom(x => x.map { case (iterations, mentors, employments) =>
      (iterations, mentorsToMap(mentors.items), employmentsToMap(employments.items))
    })))
  }
}

object IterationForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
                    item: Option[Iteration],
                    mentors: Map[Int, Mentor],
                    employments: Map[Int, Employment],
                    submitHandler: (Iteration, Boolean) => Callback
                  )

  case class State(item: Iteration, cancelled: Boolean = true)

  class Backend($: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      $.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.cancelled)

    def updateField(f: (Iteration, String) => Iteration)(e: ReactEventI) = {
      val text = e.target.value
      $.modState(s => s.copy(item = f(s.item, text)))
    }


    def updateDate(f: (Iteration, Instant) => Iteration)(e: ReactEventI) =  {
      e.target.value.parseDate match {
        case Some(dt) => ($.modState(s => s.copy(item = f(s.item, dt))))
        case None => Callback.log("Can't parse startDate Instant: " + e.target.value)
      }
    }

    def updateIntField(f: (Iteration, Int) => Iteration)(e: ReactEventI) = {
      val num = e.target.value
      try {
        val intNum = num.toInt
        $.modState(s => s.copy(item = f(s.item, intNum)))
      } catch {
        case _: NumberFormatException => Callback.warn("Incorrect Int: " + num)
      }
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.item.id.getOrElse(-1) <= 0) "adding" else "editing"} a iteration or two")
      val headerText = if (s.item.id.getOrElse(-1) <= 0) "Add new iteration" else "Edit iteration"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          <.label(^.`for` := "id", "Id"),
          <.input.text(bss.formControl, ^.id := "id", ^.value := s.item.id, ^.disabled := true)),
        <.div(bss.formGroup,
          <.label(^.`for` := "employment", "Employments"),
          <.select(bss.formControl, ^.id := "employment", ^.value := s.item.employmentId.toString,
            ^.onChange ==> updateIntField((s, i) => s.copy(employmentId = i)),
            for ((id, employment) <- p.employments) yield {
              <.option(^.value := employment.id, employment.title)
            }
          )
        ),
        <.div(bss.formGroup,
          <.label(^.`for` := "mentors", "Mentors"),
          <.select(bss.formControl, ^.id := "mentors", ^.value := s.item.mentorId.toString,
            ^.onChange ==> updateIntField((s, i) => s.copy(mentorId = i)),
            for ((id, mentor) <- p.mentors) yield {
              <.option(^.value := mentor.id, mentor.firstName+" "+mentor.lastName)
            }
          )
        ),
        <.div(bss.formGroup,
          <.label(^.`for` := "startDate", "Start date"),
          <.input.date(bss.formControl, ^.id := "startDate", ^.placeholder := datePattern,
            ^.value := s.item.startDate.localDateFormat, ^.onChange ==> updateDate((s, i) => s.copy(startDate = i)))),
        <.div(bss.formGroup,
          <.label(^.`for` := "endDate", "End date"),
          <.input.date(bss.formControl, ^.id := "endDate", ^.placeholder := datePattern,
            ^.value := s.item.endDate.localDateFormat, ^.onChange ==> updateDate((s, i) => s.copy(endDate = i))))
      )
    }
  }

  val component = ReactComponentB[Props]("IterationForm")
    .initialState_P(p => State(p.item.getOrElse(Iteration(employmentId = 0, mentorId = 0, startDate = Instant.now(), endDate = Instant.now()))))
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}

