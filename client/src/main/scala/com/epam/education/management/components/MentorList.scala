package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto.Mentor
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object MentorList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[Mentor],
      editItem: Mentor => Callback,
      deleteItem: Mentor => Callback
  )

  private val component =
    ReactComponentB[Props]("MentorList").render_P { p =>
      def renderItem(item: Mentor) = {
        <.tr(
          <.td(item.id),
          <.td(item.primarySkills),
          <.td(item.firstName),
          <.td(item.lastName),
          <.td(item.email),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete")
            )
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Primary Skills"),
            <.th("Given Name"),
            <.th("Family Name"),
            <.th("Email"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[Mentor], editItem: Mentor => Callback, deleteItem: Mentor => Callback) =
    component(Props(items, editItem, deleteItem))

}
