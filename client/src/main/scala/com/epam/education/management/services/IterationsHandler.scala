package com.epam.education.management.services

import com.epam.education.management.services.IterationsHandler.Iterations
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.Iteration

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

class IterationsHandler[M](modelRW: ModelRW[M, Pot[Iterations]]) extends ActionHandler(modelRW) {

  import IterationsHandler._

  override def handle = {
    case a: IterationAction => handleStrict(a)
  }

  def handleStrict(action: IterationAction) = action match {
    case FetchIterationsIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateIterations()))
      else noChange
    case action: UpdateIterations =>
      val updateF = action.effect(AjaxClient.iteration.findAll())(Iterations)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateIteration(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.iteration.update(item).map(_ => UpdateIterations())))
    case CreateIteration(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.iteration.create(item).map(_ => UpdateIterations())))
    case DeleteIteration(item) =>
      // make a local update and inform server
      updated(value.map(_.remove(item)), Effect(AjaxClient.iteration.delete(item.id.get).map(_ => UpdateIterations())))
  }
}

object IterationsHandler {

  case class Iterations(items: Seq[Iteration]) {
    def updated(item: Iteration) = {
      items.indexWhere(_.id == item.id) match {
        case -1  => Iterations(items :+ item)
        case idx => Iterations(items.updated(idx, item))
      }
    }

    def remove(item: Iteration) = Iterations(items.filterNot(_ == item))
  }

  sealed trait IterationAction extends Action

  case object FetchIterationsIfEmpty extends IterationAction

  case class UpdateIterations(potResult: Pot[Iterations] = Empty)
      extends PotAction[Iterations, UpdateIterations]
      with IterationAction {
    override def next(newResult: Pot[Iterations]): UpdateIterations = UpdateIterations(newResult)
  }

  case class UpdateIteration(item: Iteration) extends IterationAction

  case class CreateIteration(item: Iteration) extends IterationAction

  case class DeleteIteration(item: Iteration) extends IterationAction

}
