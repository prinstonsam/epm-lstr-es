package com.epam.education.management

import com.epam.education.management.components.GlobalStyles
import com.epam.education.management.logger.log
import com.epam.education.management.modules._
import com.epam.education.management.services.EmploymentTypesHandler.FetchEmploymentTypesIfEmpty
import com.epam.education.management.services.ResultTypesHandler.FetchResultTypesIfEmpty
import com.epam.education.management.services.SMCCircuit
import com.epam.education.management.services.StudentTypesHandler.FetchStudentTypesIfEmpty
import japgolly.scalajs.react.ReactDOM
import japgolly.scalajs.react.extra.router.{BaseUrl, Redirect, Resolution, Router, RouterConfigDsl, RouterCtl}
import japgolly.scalajs.react.vdom.prefix_<^._
import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import scalacss.Defaults._
import scalacss.ScalaCssReact._

/**
  * Created by Semen_Popugaev on 2016-08-26.
  */
@JSExport("SMCMain")
object SMCMain extends js.JSApp {

  // Define the locations (pages) used in this application
  sealed trait Loc

  case object DashboardLoc extends Loc

  case object StudentsLoc extends Loc

  case object EmploymentsLoc extends Loc

  case object MentorsLoc extends Loc

  case object IterationsLoc extends Loc

  case object FeedbackLoc extends Loc

  case object UsersLoc extends Loc

  case object SkillsLoc extends Loc

  case object StudentIterationsLoc extends Loc

  case object DictionaryLoc extends Loc

  // configure the router
  val routerConfig = RouterConfigDsl[Loc].buildConfig { dsl =>
    import dsl._

    val studentsWrapper = SMCCircuit.connect(root => root.students -> root.studentTypes)

    val mentorsWrapper = SMCCircuit.connect(_.mentors)

    val usersWrapper = SMCCircuit.connect(_.users)

    val employmentsWrapper = SMCCircuit.connect { root =>
      for {
        es <- root.employments
        ets <- root.employmentTypes
      } yield (es, ets)
    }

    val iterationsWrapper = SMCCircuit.connect { root =>
      for {
        ms <- root.mentors
        is <- root.iterations
        es <- root.employments
      } yield (is, ms, es)
    }

    val feedbackWrapper = SMCCircuit.connect { root =>
      for {
        fd <- root.feedbacks
        ms <- root.mentors
        is <- root.iterations
        st <- root.students.students
        es <- root.employments
      } yield (fd, ms, is, st, es)
    }

    val skillsWrapper = SMCCircuit.connect(_.skills)

    val studentIterationsWrapper = SMCCircuit.connect { root =>
      for {
        sis <- root.studentIterations
        sts <- root.students.students
        its <- root.iterations
        ems <- root.employments
        rts <- root.resultTypes
      } yield (sis, sts, its, ems, rts)

    }

    // wrap/connect components to the circuit
    (
      staticRoute(root, DashboardLoc) ~>
        renderR(ctl => SMCCircuit.wrap(_.motd)(proxy => Dashboard(ctl, proxy))) |
      staticRoute("#students", StudentsLoc) ~>
        renderR(ctl => studentsWrapper(StudentsModule(_))) |
      staticRoute("#employments", EmploymentsLoc) ~>
        renderR(ctl => employmentsWrapper(EmploymentsModule(_))) |
      staticRoute("#iterations", IterationsLoc) ~>
        renderR(ctl => iterationsWrapper(IterationsModule(_))) |
      staticRoute("#mentors", MentorsLoc) ~>
        renderR(ctl => mentorsWrapper(MentorsModule(_))) |
      staticRoute("#users", UsersLoc) ~>
        renderR(ctl => usersWrapper(UsersModule(_))) |
      staticRoute("#feedbacks", FeedbackLoc) ~>
        renderR(ctl => feedbackWrapper(FeedbackModule(_))) |
      staticRoute("#student_iterations", StudentIterationsLoc) ~>
        renderR(ctl => studentIterationsWrapper(StudentIterationsModule(_))) |
      staticRoute("#dicts", DictionaryLoc) ~>
        render(DictionaryModule()) |
      staticRoute("#skill", SkillsLoc) ~>
        renderR(ctl => skillsWrapper(SkillsModule(_)))
      ).notFound(redirectToPage(DashboardLoc)(Redirect.Replace))
  }.renderWith(layout)

  // base layout for all pages
  def layout(c: RouterCtl[Loc], r: Resolution[Loc]) = {
    <.div(
      // here we use plain Bootstrap class names as these are specific to the top level layout defined here
      <.nav(
        ^.className := "navbar navbar-inverse navbar-fixed-top",
        <.div(
          ^.className := "container",
          <.div(^.className := "navbar-header", <.span(^.className := "navbar-brand", "Student management")),
          <.div(^.className := "collapse navbar-collapse", MainMenu(c, r.page)))),
      // currently active module is shown in this container
      <.div(^.className := "container", r.render())
    )
  }

  @JSExport
  override def main(): Unit = {
    log.warn("Student management system starting")
    // send log messages also to the server
    // log.enableServerLogging("/logging")
    // log.info("This message goes to server as well")

    // create stylesheet
    GlobalStyles.addToDocument()
    // create the router
    val router = Router(BaseUrl.until_#, routerConfig)
    // tell React to render the router in the document body
    val _ = ReactDOM.render(router(), dom.document.getElementById("root"))

    SMCCircuit.dispatch(FetchStudentTypesIfEmpty)
    SMCCircuit.dispatch(FetchResultTypesIfEmpty)
    SMCCircuit.dispatch(FetchEmploymentTypesIfEmpty)
  }
}
