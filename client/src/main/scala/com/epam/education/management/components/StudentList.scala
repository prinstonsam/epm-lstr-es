package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import diode.data.Pot
import dto.{StudentType, StudentWithData}
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scala.collection.breakOut
import scalacss.ScalaCssReact._
import diode.react.ReactPot._

object StudentList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[StudentWithData],
      studentTypes: Pot[Map[Int, StudentType]],
      editItem: StudentWithData => Callback,
      deleteItem: StudentWithData => Callback
  )

  private val component =
    ReactComponentB[Props]("StudentList").render_P { p =>
      def getType(id: Int) = {
        <.span(
          p.studentTypes.renderFailed(ex => s"Error loading: $ex"),
          p.studentTypes.renderPending(_ > 500, _ => "Loading..."),
          p.studentTypes.render { types =>
            val res: String = types.get(id).map(_.title).getOrElse("Error!")
            res
          }
        )
      }
      def renderItem(item: StudentWithData) = {
        <.tr(
          <.td(item.student.id),
          <.td(item.student.firstName),
          <.td(item.student.lastName),
          <.td(item.student.email),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete")
            )
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Given Name"),
            <.th("Family Name"),
            <.th("Email"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[StudentWithData],
            studentTypes: Pot[Seq[StudentType]],
            editItem: StudentWithData => Callback,
            deleteItem: StudentWithData => Callback) =
    component(Props(
      items,
      studentTypes.map(_.collect{ case t @ StudentType(Some(id), _) => id -> t }(breakOut)),
      editItem,
      deleteItem))

}
