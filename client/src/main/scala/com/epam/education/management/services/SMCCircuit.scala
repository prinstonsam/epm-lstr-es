package com.epam.education.management.services

import com.epam.education.management.services.EmploymentTypesHandler.EmploymentTypes
import com.epam.education.management.services.EmploymentsHandler.Employments
import com.epam.education.management.services.FeedbackHandler.Feedbacks
import com.epam.education.management.services.IterationsHandler.Iterations
import com.epam.education.management.services.MentorsHandler.Mentors
import com.epam.education.management.services.ResultTypesHandler.ResultTypes
import com.epam.education.management.services.SelectedStudentHandler.SelectedStudent
import com.epam.education.management.services.SkillsHandler.Skills
import com.epam.education.management.services.StudentIterationsHandler.StudentIterations
import com.epam.education.management.services.StudentTypesHandler.StudentTypes
import com.epam.education.management.services.StudentsHandler.StudentsWithData
import com.epam.education.management.services.UsersHandler.Users
import diode._
import diode.data.{Empty, Pot}
import diode.react.ReactConnector

// The base model of our application
case class StudentsModel(students: Pot[StudentsWithData], selectedStudent: SelectedStudent)

case class RootModel(
    students: StudentsModel,
    studentTypes: Pot[StudentTypes],
    resultTypes: Pot[ResultTypes],
    employments: Pot[Employments],
    mentors: Pot[Mentors],
    iterations: Pot[Iterations],
    users: Pot[Users],
    feedbacks: Pot[Feedbacks],
    employmentTypes: Pot[EmploymentTypes],
    studentIterations: Pot[StudentIterations],
    skills: Pot[Skills],
    motd: Pot[String]
)

object SMCCircuit extends Circuit[RootModel] with ReactConnector[RootModel] {
  override protected def initialModel =
    RootModel(
      StudentsModel(Empty, SelectedStudent(None, Empty)),
      Empty,
      Empty,
      Empty,
      Empty,
      Empty,
      Empty,
      Empty,
      Empty,
      Empty,
      Empty,
      Empty)

  val zoomStudentsModel   = zoomRW(_.students)((m, v) => m.copy(students = v))
  val zoomSelectedStudent = zoomStudentsModel.zoomRW(_.selectedStudent)((m, v) => m.copy(selectedStudent = v))

  override protected def actionHandler = composeHandlers(
    new StudentsHandler(zoomStudentsModel.zoomRW(_.students)((m, v) => m.copy(students = v))),
    new SelectedStudentHandler(zoomSelectedStudent),
    new SelectedStudentHistoryHandler(zoomSelectedStudent.zoomRW(_.history)((m, v) => m.copy(history = v))),
    new MentorsHandler(zoomRW(_.mentors)((m, v) => m.copy(mentors = v))),
    new IterationsHandler(zoomRW(_.iterations)((m, v) => m.copy(iterations = v))),
    new StudentTypesHandler(zoomRW(_.studentTypes)((m, v) => m.copy(studentTypes = v))),
    new ResultTypesHandler(zoomRW(_.resultTypes)((m, v) => m.copy(resultTypes = v))),
    new UsersHandler(zoomRW(_.users)((m, v) => m.copy(users = v))),
    new MotdHandler(zoomRW(_.motd)((m, v) => m.copy(motd = v))),
    new FeedbackHandler(zoomRW(_.feedbacks)((m, v) => m.copy(feedbacks = v))),
    new EmploymentTypesHandler(zoomRW(_.employmentTypes)((m, v) => m.copy(employmentTypes = v))),
    new StudentIterationsHandler(zoomRW(_.studentIterations)((m, v) => m.copy(studentIterations = v))),
    new EmploymentsHandler(zoomRW(_.employments)((m, v) => m.copy(employments = v))),
    new SkillsHandler(zoomRW(_.skills)((m, v) => m.copy(skills = v)))
  )
}
