package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto._
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object StudentIterationList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[StudentIteration],
      students: Map[Int, StudentWithData],
      iterations: Map[Int, Iteration],
      employments: Map[Int, Employment],
      resultTypes: Map[Int, ResultType],
      editItem: StudentIteration => Callback,
      deleteItem: StudentIteration => Callback
  )

  private val component =
    ReactComponentB[Props]("StudentIterationList").render_P { p =>
      def getResultType(id: Int) = p.resultTypes.get(id).map(_.title).getOrElse("Error!")
      def getEmployment(id: Int) = p.employments.get(id).map(e => e.title).getOrElse("Error!")
      def getStudent(id: Int) =
        p.students.get(id).map(s => s.student.firstName + " " + s.student.lastName).getOrElse("Error!")
      def getIteration(id: Int) =
        p.iterations
          .get(id)
          .map(i => getEmployment(i.id.get) + ", " + i.startDate + ", " + i.endDate)
          .getOrElse("Error!")
      def renderItem(item: StudentIteration) = {
        <.tr(
          <.td(getStudent(item.studentId)),
          <.td(getIteration(item.iterationId)),
          <.td(getResultType(item.resultTypeId)),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete")
            )
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Student"),
            <.th("Iteration"),
            <.th("ResultType"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(
      items: Seq[StudentIteration],
      students: Map[Int, StudentWithData],
      iterations: Map[Int, Iteration],
      employments: Map[Int, Employment],
      resultTypes: Map[Int, ResultType],
      editItem: StudentIteration => Callback,
      deleteItem: StudentIteration => Callback
  ) = component(Props(items, students, iterations, employments, resultTypes, editItem, deleteItem))
}
