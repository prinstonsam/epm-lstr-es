package com.epam.education.management.utils

import java.time.Instant

import org.threeten.bp

object ParseInstant {
  val InstantR1 = """([0-9]{4})-([0-1][0-9])-([0-3][0-9])T([0-2][0-9]):([0-5][0-9]):([0-5][0-9])\.([0-9]*)Z""".r
  val InstantR2 = """([0-9]{4})-([0-1][0-9])-([0-3][0-9])T([0-2][0-9]):([0-5][0-9]):([0-5][0-9])Z""".r
  val InstantR3 = """([0-9]{4})-([0-1][0-9])-([0-3][0-9])T([0-2][0-9]):([0-5][0-9])Z""".r

  def unapply(s: String): Option[Instant] = s match {
    case InstantR1(y, m, d, h, min, s, mil) =>
      val instant: bp.Instant =
        bp.OffsetDateTime.of(
          y.toInt, m.toInt, d.toInt,
          h.toInt, min.toInt, s.toInt, mil.toInt * 1000000,
          bp.ZoneOffset.ofHours(0))
          .toInstant
      Some(Instant.ofEpochSecond(instant.getEpochSecond, instant.getNano.toLong))
    case InstantR2(y, m, d, h, min, s) =>
      val instant: bp.Instant =
        bp.OffsetDateTime.of(
          y.toInt, m.toInt, d.toInt,
          h.toInt, min.toInt, s.toInt, 0,
          bp.ZoneOffset.ofHours(0))
          .toInstant
      Some(Instant.ofEpochSecond(instant.getEpochSecond, instant.getNano.toLong))
    case InstantR3(y, m, d, h, min) =>
      val instant: bp.Instant =
        bp.OffsetDateTime.of(
          y.toInt, m.toInt, d.toInt,
          h.toInt, min.toInt, 0, 0,
          bp.ZoneOffset.ofHours(0))
          .toInstant
      Some(Instant.ofEpochSecond(instant.getEpochSecond, instant.getNano.toLong))
    case _ => None
  }
}
