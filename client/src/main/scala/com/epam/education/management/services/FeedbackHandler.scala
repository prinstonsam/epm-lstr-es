package com.epam.education.management.services

import com.epam.education.management.services.FeedbackHandler.Feedbacks
import diode._
import diode.data.{Empty, Pot, PotAction}
import dto.Feedback

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

class FeedbackHandler[M](modelRW: ModelRW[M, Pot[Feedbacks]]) extends ActionHandler(modelRW) {
  import FeedbackHandler._

  override def handle = {
    case a: FeedbackAction => handleStrict(a)
  }

  def handleStrict(action: FeedbackAction) = action match {
    case action: UpdateFeedbacks =>
      val updateF = action.effect(AjaxClient.feedback.findAll())(Feedbacks)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateFeedback(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.feedback.update(item).map(_ => UpdateFeedbacks())))
    case CreateFeedback(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.feedback.create(item).map(_ => UpdateFeedbacks())))
    case DeleteFeedback(item) =>
      // make a local update and inform server
      updated(value.map(_.remove(item)), Effect(AjaxClient.feedback.delete(item.id.get).map(_ => UpdateFeedbacks())))
  }
}

object FeedbackHandler {

  case class Feedbacks(items: Seq[Feedback]) {
    def updated(item: Feedback) = {
      items.indexWhere(_.id == item.id) match {
        case -1 => Feedbacks(items :+ item)
        case idx => Feedbacks(items.updated(idx, item))
      }
    }

    def remove(item: Feedback) = Feedbacks(items.filterNot(_ == item))
  }

  sealed trait FeedbackAction extends Action

  case class UpdateFeedbacks(potResult: Pot[Feedbacks] = Empty)
    extends PotAction[Feedbacks, UpdateFeedbacks] with FeedbackAction{
    override def next(newResult: Pot[Feedbacks]): UpdateFeedbacks = UpdateFeedbacks(newResult)
  }

  case class UpdateFeedback(item: Feedback) extends FeedbackAction

  case class CreateFeedback(item: Feedback) extends FeedbackAction

  case class DeleteFeedback(item: Feedback) extends FeedbackAction
}
