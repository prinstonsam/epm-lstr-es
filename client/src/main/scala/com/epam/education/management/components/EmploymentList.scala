package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto.{Employment, EmploymentType}
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object EmploymentList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[Employment],
      employmentTypes: Map[Int, EmploymentType],
      editItem: Employment => Callback,
      deleteItem: Employment => Callback
  )

  private val component =
    ReactComponentB[Props]("EmploymentList").render_P { p =>
      def getEmploymentType(id: Int) = p.employmentTypes.get(id).map(_.title).getOrElse("Error!")
      def renderItem(item: Employment) = {
        <.tr(
          <.td(item.id),
          <.td(item.title),
          <.td(item.duration),
          <.td(getEmploymentType(item.employmentTypeId)),
          <.td(<.div(bss.pullRight)(
            Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
            Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete"))
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Title"),
            <.th("Duration"),
            <.th("Type"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(
      items: Seq[Employment],
      employmentTypes: Map[Int, EmploymentType],
      editItem: Employment => Callback,
      deleteItem: Employment => Callback
  ) = component(Props(items, employmentTypes, editItem, deleteItem))
}
