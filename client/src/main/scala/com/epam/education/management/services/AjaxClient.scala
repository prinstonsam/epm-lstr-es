package com.epam.education.management.services

import com.epam.education.management.utils.CustomPickler.Import._
import dto._
import org.scalajs.dom.ext.Ajax

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

object AjaxClient {
  val baseUrl = ""

  def welcomeMsg(s: String): Future[String] = Future.successful(s"Hello $s")

  object student {
    def delete(studentId: Int): Future[StudentWithData] =
      Ajax.delete(s"$baseUrl/student/$studentId").map(r => read[StudentWithData](r.responseText))

    def update(item: Student): Future[Student] =
      Ajax
        .put(
          s"$baseUrl/student/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Student](r.responseText))

    def create(item: StudentWithData): Future[StudentWithData] =
      Ajax
        .post(
          s"$baseUrl/student",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[StudentWithData](r.responseText))

    def findAll(): Future[Seq[StudentWithData]] =
      Ajax.get(s"$baseUrl/student").map(r => read[Seq[StudentWithData]](r.responseText))

    object history {
      def get(id: Int): Future[Seq[StudentData]] =
        Ajax.get(s"$baseUrl/student/$id/data").map(r => read[Seq[StudentData]](r.responseText))

      def create(item: StudentData): Future[StudentData] =
        Ajax.post(
          s"$baseUrl/student/${item.studentId}/data",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        ).map(r => read[StudentData](r.responseText))
    }
  }

  object studentType {
    def delete(studentTypeId: Int): Future[StudentType] =
      Ajax.delete(s"$baseUrl/studenttype/$studentTypeId").map(r => read[StudentType](r.responseText))

    def update(item: StudentType): Future[StudentType] =
      Ajax
        .put(
          s"$baseUrl/studenttype/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[StudentType](r.responseText))

    def create(item: StudentType): Future[StudentType] =
      Ajax
        .post(
          s"$baseUrl/studenttype",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[StudentType](r.responseText))

    def findAll(): Future[Seq[StudentType]] =
      Ajax.get(s"$baseUrl/studenttype").map(r => read[Seq[StudentType]](r.responseText))
  }

  object skill {
    def delete(skillId: Int): Future[Skill] =
      Ajax.delete(s"$baseUrl/skill/$skillId").map(r => read[Skill](r.responseText))

    def update(item: Skill): Future[Skill] =
      Ajax
        .put(
          s"$baseUrl/skill/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Skill](r.responseText))

    def create(item: Skill): Future[Skill] =
      Ajax
        .post(
          s"$baseUrl/skill",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Skill](r.responseText))

    def findAll(): Future[Seq[Skill]] =
      Ajax.get(s"$baseUrl/skill").map(r => read[Seq[Skill]](r.responseText))
  }
  
  object studentIteration {
    def delete(studentId: Int, iterationId: Int): Future[StudentIteration] =
      Ajax.delete(s"$baseUrl/student/$studentId/iteration/$iterationId")
        .map(r => read[StudentIteration](r.responseText))

    def update(item: StudentIteration): Future[StudentIteration] =
      Ajax.put(
        s"$baseUrl/student/${item.studentId}/iteration/${item.iterationId}",
        data = write(item),
        headers = Map("Content-Type" -> "application/json")
      ).map(r => read[StudentIteration](r.responseText))

    def create(item: StudentIteration): Future[StudentIteration] =
      Ajax.post(
        s"$baseUrl/studentIteration",
        data = write(item),
        headers = Map("Content-Type" -> "application/json")
      ).map(r => read[StudentIteration](r.responseText))

    def findAll(): Future[Seq[StudentIteration]] =
      Ajax.get(s"$baseUrl/studentIteration").map(r => read[Seq[StudentIteration]](r.responseText))
  }

  object resultType {
    def delete(resultTypeId: Int): Future[ResultType] =
      Ajax.delete(s"$baseUrl/resulttype/$resultTypeId").map(r => read[ResultType](r.responseText))

    def update(item: ResultType): Future[ResultType] =
      Ajax.put(
        s"$baseUrl/resulttype/${item.id.get}",
        data = write(item),
        headers = Map("Content-Type" -> "application/json")
      ).map(r => read[ResultType](r.responseText))

    def create(item: ResultType): Future[ResultType] =
      Ajax.post(
        s"$baseUrl/resulttype",
        data = write(item),
        headers = Map("Content-Type" -> "application/json")
      ).map(r => read[ResultType](r.responseText))

    def findAll(): Future[Seq[ResultType]] =
      Ajax.get(s"$baseUrl/resulttype").map(r => read[Seq[ResultType]](r.responseText))
  }

  object employment {
    def delete(employmentId: Int): Future[Employment] =
      Ajax.delete(s"$baseUrl/employment/$employmentId").map(r => read[Employment](r.responseText))

    def update(item: Employment): Future[Employment] =
      Ajax
        .put(
          s"$baseUrl/employment/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Employment](r.responseText))

    def create(item: Employment): Future[Employment] =
      Ajax.post(
        s"$baseUrl/employment",
        data = write(item),
        headers = Map("Content-Type" -> "application/json")
      ).map(r => read[Employment](r.responseText))

    def findAll(): Future[Seq[Employment]] =
      Ajax.get(s"$baseUrl/employment").map(r => read[Seq[Employment]](r.responseText))
  }

  object mentor {
    def delete(mentorId: Int): Future[Mentor] =
      Ajax.delete(s"$baseUrl/mentor/$mentorId").map(r => read[Mentor](r.responseText))

    def update(item: Mentor): Future[Mentor] =
      Ajax
        .put(
          s"$baseUrl/mentor/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Mentor](r.responseText))

    def create(item: Mentor): Future[Mentor] =
      Ajax
        .post(
          s"$baseUrl/mentor",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Mentor](r.responseText))

    def findAll(): Future[Seq[Mentor]] =
      Ajax.get(s"$baseUrl/mentor").map(r => read[Seq[Mentor]](r.responseText))
  }

  object iteration {
    def delete(iterationId: Int): Future[Iteration] =
      Ajax.delete(s"$baseUrl/iteration/$iterationId").map(r => read[Iteration](r.responseText))

    def update(item: Iteration): Future[Iteration] =
      Ajax
        .put(
          s"$baseUrl/iteration/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Iteration](r.responseText))

    def create(item: Iteration): Future[Iteration] =
      Ajax
        .post(
          s"$baseUrl/iteration",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Iteration](r.responseText))

    def findAll(): Future[Seq[Iteration]] =
      Ajax.get(s"$baseUrl/iteration").map(r => read[Seq[Iteration]](r.responseText))
  }

  object user {
    def delete(userId: Int): Future[User] =
      Ajax.delete(s"$baseUrl/user/$userId").map(r => read[User](r.responseText))

    def update(item: User): Future[User] =
      Ajax
        .put(
          s"$baseUrl/user/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[User](r.responseText))

    def create(item: User): Future[User] =
      Ajax
        .post(
          s"$baseUrl/user",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[User](r.responseText))

    def findAll(): Future[Seq[User]] =
      Ajax.get(s"$baseUrl/user").map(r => read[Seq[User]](r.responseText))
  }

  object feedback {
    def delete(feedbackId: Int): Future[Feedback] =
      Ajax.delete(s"$baseUrl/feedback/$feedbackId").map(r => read[Feedback](r.responseText))

    def update(item: Feedback): Future[Feedback] =
      Ajax
        .put(
          s"$baseUrl/feedback/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Feedback](r.responseText))

    def create(item: Feedback): Future[Feedback] =
      Ajax
        .post(
          s"$baseUrl/feedback",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[Feedback](r.responseText))

    def findAll(): Future[Seq[Feedback]] =
      Ajax.get(s"$baseUrl/feedback").map(r => read[Seq[Feedback]](r.responseText))
  }

  object employmentType {
    def delete(employmentTypeId: Int): Future[EmploymentType] =
      Ajax.delete(s"$baseUrl/employmenttype/$employmentTypeId").map(r => read[EmploymentType](r.responseText))

    def update(item: EmploymentType): Future[EmploymentType] =
      Ajax
        .put(
          s"$baseUrl/employmenttype/${item.id.get}",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[EmploymentType](r.responseText))

    def create(item: EmploymentType): Future[EmploymentType] =
      Ajax
        .post(
          s"$baseUrl/employmenttype",
          data = write(item),
          headers = Map("Content-Type" -> "application/json")
        )
        .map(r => read[EmploymentType](r.responseText))

    def findAll(): Future[Seq[EmploymentType]] =
      Ajax.get(s"$baseUrl/employmenttype").map(r => read[Seq[EmploymentType]](r.responseText))
  }
}
