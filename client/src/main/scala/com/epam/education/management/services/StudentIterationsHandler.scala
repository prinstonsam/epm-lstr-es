package com.epam.education.management.services

import com.epam.education.management.services.StudentIterationsHandler.StudentIterations
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.StudentIteration

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Handles actions related to student iterations
  *
  * @param modelRW Reader/Writer to access the model
  */
class StudentIterationsHandler[M](modelRW: ModelRW[M, Pot[StudentIterations]]) extends ActionHandler(modelRW) {

  import StudentIterationsHandler._

  override def handle = {
    case a: StudentIterationAction => handleStrict(a)
  }

  def handleStrict(action: StudentIterationAction) = action match {
    case FetchStudentIterationsIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateStudentIterations()))
      else noChange
    case action: UpdateStudentIterations =>
      val updateF =
        action.effect(AjaxClient.studentIteration.findAll())(StudentIterations)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateStudentIteration(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.studentIteration.update(item).map(_ => UpdateStudentIterations())))
    case CreateStudentIteration(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.studentIteration.create(item).map(_ => UpdateStudentIterations())))
    case DeleteStudentIteration(item) =>
      // make a local update and inform server
      updated(
        value.map(_.remove(item)),
        Effect(AjaxClient.studentIteration.delete(item.studentId, item.iterationId).map(_ => UpdateStudentIterations())))
  }
}

object StudentIterationsHandler {
  case class StudentIterations(items: Seq[StudentIteration]) {
    def updated(item: StudentIteration) = {
      items.indexWhere(x => x.studentId == item.studentId && x.iterationId == item.iterationId) match {
        case -1 => StudentIterations(items :+ item)
        case idx => StudentIterations(items.updated(idx, item))
      }
    }

    def remove(item: StudentIteration) = StudentIterations(items.filterNot(_ == item))
  }

  sealed trait StudentIterationAction extends Action

  case object FetchStudentIterationsIfEmpty extends StudentIterationAction

  case class UpdateStudentIterations(potResult: Pot[StudentIterations] = Empty)
    extends PotAction[StudentIterations, UpdateStudentIterations]
      with StudentIterationAction {
    override def next(newResult: Pot[StudentIterations]): UpdateStudentIterations = UpdateStudentIterations(newResult)
  }

  case class UpdateStudentIteration(item: StudentIteration) extends StudentIterationAction

  case class CreateStudentIteration(item: StudentIteration) extends StudentIterationAction

  case class DeleteStudentIteration(item: StudentIteration) extends StudentIterationAction

}
