package com.epam.education.management.utils

import java.time.Instant

import japgolly.scalajs.react.ReactNode
import org.threeten.bp
import org.widok.moment.{Date, Moment}

import scala.language.implicitConversions

/**
  * Utility methods for Instant
  *
  * @author Simon Popugaev
  */
object Instants {

  val pattern = "YYYY-MM-DDTHH:mm:ss.SSSSZ"
  val presentation = "YYYY-MM-DD HH:mm"
  val datePattern = "YYYY-MM-DD"

  @inline implicit def instantToReactNode(i: Instant)(implicit f: String => ReactNode): ReactNode = i.show

  implicit class InstantHelpers(val i: Instant) extends AnyVal {
    def moment: Date = Moment.utc(i.toString, pattern).local()

    def utcFormat: String = moment.utc().format(pattern)

    def localDateFormat: String = moment.local().format(datePattern)

    def show: String = moment.format(presentation)
  }

  implicit class MomentHelper(val m: Date) extends AnyVal {
    def instant: Instant = {
      val d = m.utc().toDate()

      val inst =
        bp.OffsetDateTime.of(
          d.getUTCFullYear(), d.getUTCMonth() + 1, d.getUTCDate(),
          d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds(), d.getUTCMilliseconds() * 1000000,
          bp.ZoneOffset.ofHours(0))
          .toInstant

      Instant.ofEpochSecond(inst.getEpochSecond, inst.getNano.toLong)
    }
  }

  implicit class InstantParseHelper(val s: String) extends AnyVal {
    def parseDate: Option[Instant] = {
      Some(Moment(s, datePattern, strict = true))
        .filter(_.isValid())
        .map(_.instant)
    }
  }

}
