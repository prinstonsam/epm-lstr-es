package com.epam.education.management.services

import diode.{Action, ActionHandler, Effect, ModelRW}
import diode.data.{Empty, Pot, PotAction}
import dto.{StudentData, StudentWithData}
import SelectedStudentHandler._
import SelectedStudentHistoryHandler._
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue


/**
  * Handles actions related to selected student
  *
  * @param modelRW Reader/Writer to access the model
  */
class SelectedStudentHandler[M](modelRW: ModelRW[M, SelectedStudent]) extends ActionHandler(modelRW) {

  override def handle = {
    case a: SelectedStudentAction => handleStrict(a)
  }

  def handleStrict(action: SelectedStudentAction) = action match {
    case SelectStudent(None) =>
      updated(value.copy(item = None, history = Empty))

    case SelectStudent(Some(item)) =>
      if (item.student.id.getOrElse(-1) > 0) {
        updated(
          value.copy(item = Some(item), history = Empty),
          Effect.action(UpdateStudentHistory(item.studentData.studentId)))
      } else {
        updated(value.copy(item = Some(item), history = Empty))
      }

    case UpdateSelectedStudent(item) =>
      updated(value.copy(item = Some(item)))
  }
}

object SelectedStudentHandler {

  case class SelectedStudent(item: Option[StudentWithData], history: Pot[StudentHistory])

  sealed trait SelectedStudentAction extends Action

  case class SelectStudent(item: Option[StudentWithData]) extends SelectedStudentAction

  case class UpdateSelectedStudent(item: StudentWithData) extends SelectedStudentAction
}

class SelectedStudentHistoryHandler[M](modelRW: ModelRW[M, Pot[StudentHistory]]) extends ActionHandler(modelRW) {

  override def handle = {
    case a: SelectedStudentHistoryAction => handleStrict(a)
  }

  def handleStrict(action: SelectedStudentHistoryAction) = action match {
    case action @ UpdateStudentHistory(studentId, _) =>
      val updateF = action.effect(AjaxClient.student.history.get(studentId))(StudentHistory)
      action.handleWith(this, updateF)(PotAction.handler())
    case CreateStudentData(item) =>
      // make a local update and inform server
      updated(
        value.map(_.updated(item)),
        Effect(AjaxClient.student.history.create(item).map(_ => UpdateStudentHistory(item.studentId))))
  }
}

object SelectedStudentHistoryHandler {
  case class StudentHistory(items: Seq[StudentData]) {
    def updated(item: StudentData) = {
      items.indexWhere(_.studentId == item.studentId) match {
        case -1 => copy(items = items :+ item)
        case idx => copy(items = items.updated(idx, item))
      }
    }

    def remove(item: StudentData) = copy(items = items.filterNot(_ == item))
  }

  sealed trait SelectedStudentHistoryAction extends Action

  case class UpdateStudentHistory(studentId: Int, potResult: Pot[StudentHistory] = Empty)
    extends PotAction[StudentHistory, UpdateStudentHistory] with SelectedStudentHistoryAction {
    override def next(newResult: Pot[StudentHistory]): UpdateStudentHistory = copy(potResult = newResult)
  }

  case class CreateStudentData(item: StudentData) extends SelectedStudentHistoryAction

  // TODO implement
  // case class DeleteStudentData(item: StudentData) extends SelectedStudentHistoryAction
}