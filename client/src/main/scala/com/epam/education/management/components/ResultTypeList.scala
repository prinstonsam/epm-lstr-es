package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto.ResultType
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object ResultTypeList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[ResultType],
      editItem: ResultType => Callback,
      deleteItem: ResultType => Callback
  )

  private val component =
    ReactComponentB[Props]("ResultTypeList").render_P { p =>
      def renderItem(item: ResultType) = {
        <.tr(
          <.td(item.id),
          <.td(item.title),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete")
            )
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Title"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[ResultType], editItem: ResultType => Callback, deleteItem: ResultType => Callback) =
    component(Props(items, editItem, deleteItem))

}
