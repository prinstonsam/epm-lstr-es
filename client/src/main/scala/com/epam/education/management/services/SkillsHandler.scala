package com.epam.education.management.services


import com.epam.education.management.services.SkillsHandler.Skills
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.Skill

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Handles actions related to student types
  *
  * @param modelRW Reader/Writer to access the model
  */
class SkillsHandler[M](modelRW: ModelRW[M, Pot[Skills]]) extends ActionHandler(modelRW) {

  import SkillsHandler._

  override def handle = {
    case a: SkillAction => handleStrict(a)
  }

  def handleStrict(action: SkillAction) = action match {
    case FetchSkillsIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateSkills()))
      else noChange
    case action: UpdateSkills =>
      val updateF =
        action.effect(AjaxClient.skill.findAll())(Skills)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateSkill(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.skill.update(item).map(_ => UpdateSkills())))
    case CreateSkill(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.skill.create(item).map(_ => UpdateSkills())))
    case DeleteSkill(item) =>
      // make a local update and inform server
      updated(
        value.map(_.remove(item)),
        Effect(AjaxClient.skill.delete(item.id.get).map(_ => UpdateSkills())))
  }
}

object SkillsHandler {

  case class Skills(items: Seq[Skill]) {
    def updated(item: Skill) = {
      items.indexWhere(_.id == item.id) match {
        case -1 => copy(items = items :+ item)
        case idx => copy(items = items.updated(idx, item))
      }
    }

    def remove(item: Skill) = copy(items = items.filterNot(_ == item))
  }

  sealed trait SkillAction extends Action

  case object FetchSkillsIfEmpty extends SkillAction

  case class UpdateSkills(potResult: Pot[Skills] = Empty)
    extends PotAction[Skills, UpdateSkills]
      with SkillAction {
    override def next(newResult: Pot[Skills]): UpdateSkills = UpdateSkills(newResult)
  }

  case class UpdateSkill(item: Skill) extends SkillAction

  case class CreateSkill(item: Skill) extends SkillAction

  case class DeleteSkill(item: Skill) extends SkillAction

}
