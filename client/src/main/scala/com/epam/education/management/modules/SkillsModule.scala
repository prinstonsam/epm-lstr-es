package com.epam.education.management.modules

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{GlobalStyles, Icon, SkillList}
import com.epam.education.management.logger.log
import com.epam.education.management.services.SkillsHandler._
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto.Skill
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object SkillsModule {

  case class Props(proxy: ModelProxy[Pot[Skills]])

  case class State(selectedItem: Option[Skill] = None, showSkillForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the students, which will cause StudentsStore to fetch students from the server
      Callback.when(props.proxy().isEmpty)(props.proxy.dispatch(FetchSkillsIfEmpty))

    def editSkill(item: Option[Skill]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showSkillForm = true))

    def skillEdited(item: Skill, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Student type editing cancelled")
      } else {
        val action =
          if (item.id.getOrElse(-1) > 0) UpdateSkill(item)
          else CreateSkill(item)
        Callback.log(s"Student type edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showSkillForm = false))
    }

    def render(p: Props, s: State) =
      Panel(
        Panel.Props("Student types list"),
        <.div(
          Button(Button.Props(editSkill(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render { skills =>
          SkillList(
            skills.items,
            item => editSkill(Some(item)),
            item => p.proxy.dispatch(DeleteSkill(item)))
        },
        // if the dialog is open, add it to the panel
        if (s.showSkillForm) SkillForm(SkillForm.Props(s.selectedItem, skillEdited))
        else // otherwise add an empty placeholder
          Seq.empty[ReactElement]
      )
  }

  val component = ReactComponentB[Props]("Skills")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[Skills]]) = component(Props(proxy))
}

object SkillForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      item: Option[Skill],
      submitHandler: (Skill, Boolean) => Callback
  )

  case class State(item: Skill, cancelled: Boolean = true)

  class Backend(t: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      t.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.cancelled)

    def updateTitle(e: ReactEventI) = {
      val text = e.target.value
      t.modState(s => s.copy(item = s.item.copy(title = text)))
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.item.id.getOrElse(-1) <= 0) "adding" else "editing"} a student type or two")
      val headerText = if (s.item.id.getOrElse(-1) <= 0) "Add new student type" else "Edit student type"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          <.label(^.`for` := "id", "Id"),
          <.input.text(bss.formControl, ^.id := "id", ^.value := s.item.id, ^.disabled := true)),
        <.div(bss.formGroup,
          <.label(^.`for` := "title", "Title"),
          <.input.text(bss.formControl, ^.id := "title", ^.value := s.item.title,
            ^.placeholder := "write title", ^.onChange ==> updateTitle))
      )
    }
  }

  val component = ReactComponentB[Props]("SkillForm")
    .initialState_P(p => State(p.item.getOrElse(Skill(title = ""))))
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}