package com.epam.education.management.modules

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{GlobalStyles, Icon, ResultTypeList}
import com.epam.education.management.logger.log
import com.epam.education.management.services.ResultTypesHandler._
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto.ResultType
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object ResultTypesModule {

  case class Props(proxy: ModelProxy[Pot[ResultTypes]])

  case class State(selectedItem: Option[ResultType] = None, showResultTypeForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the results, which will cause ResultsStore to fetch results from the server
      Callback.when(props.proxy().isEmpty)(props.proxy.dispatch(FetchResultTypesIfEmpty))

    def editResultType(item: Option[ResultType]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showResultTypeForm = true))

    def resultTypeEdited(item: ResultType, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Result type editing cancelled")
      } else {
        val action =
          if (item.id.getOrElse(-1) > 0) UpdateResultType(item)
          else CreateResultType(item)
        Callback.log(s"Result type edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showResultTypeForm = false))
    }

    def render(p: Props, s: State) =
      Panel(
        Panel.Props("Result types list"),
        <.div(
          Button(Button.Props(editResultType(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render { resultTypes =>
          ResultTypeList(
            resultTypes.items,
            item => editResultType(Some(item)),
            item => p.proxy.dispatch(DeleteResultType(item)))
        },
        // if the dialog is open, add it to the panel
        if (s.showResultTypeForm) ResultTypeForm(ResultTypeForm.Props(s.selectedItem, resultTypeEdited))
        else // otherwise add an empty placeholder
          Seq.empty[ReactElement]
      )
  }

  val component = ReactComponentB[Props]("ResultTypes")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[ResultTypes]]) = component(Props(proxy))
}

object ResultTypeForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
                    item: Option[ResultType],
                    submitHandler: (ResultType, Boolean) => Callback
                  )

  case class State(item: ResultType, cancelled: Boolean = true)

  class Backend(t: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      t.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.cancelled)

    def updateTitle(e: ReactEventI) = {
      val text = e.target.value
      t.modState(s => s.copy(item = s.item.copy(title = text)))
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.item.id.getOrElse(-1) <= 0) "adding" else "editing"} a result type or two")
      val headerText = if (s.item.id.getOrElse(-1) <= 0) "Add new result type" else "Edit result type"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          <.label(^.`for` := "id", "Id"),
          <.input.text(bss.formControl, ^.id := "id", ^.value := s.item.id, ^.disabled := true)),
        <.div(bss.formGroup,
          <.label(^.`for` := "title", "Title"),
          <.input.text(bss.formControl, ^.id := "title", ^.value := s.item.title,
            ^.placeholder := "write title", ^.onChange ==> updateTitle))
      )
    }
  }

  val component = ReactComponentB[Props]("ResultTypeForm")
    .initialState_P(p => State(p.item.getOrElse(ResultType(title = ""))))
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}