package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto._
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object FeedbackList {
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[Feedback],
      mentors: Map[Int, Mentor],
      iterations: Map[Int, Iteration],
      students: Map[Int, StudentWithData],
      employments: Map[Int, Employment],
      editItem: Feedback => Callback,
      deleteItem: Feedback => Callback
  )

  private val component =
    ReactComponentB[Props]("FeedbackList").render_P { p =>
      def getMentor(id: Int)     = p.mentors.get(id).map(m => m.firstName + " " + m.lastName).getOrElse("Error!")
      def getEmployment(id: Int) = p.employments.get(id).map(e => e.title).getOrElse("Error!")
      def getIteration(id: Int)  =
        p.iterations
          .get(id)
          .map(i => getEmployment(i.id.get) + ", " + i.startDate + ", " + i.endDate)
          .getOrElse("Error!")
      def getStudent(id: Int) =
        p.students.get(id).map(s => s.student.firstName + " " + s.student.lastName).getOrElse("Error!")
      def renderItem(item: Feedback) = {
        <.tr(
          <.td(item.id),
          <.td(getMentor(item.mentorId)),
          <.td(getIteration(item.iterationId)),
          <.td(getStudent(item.studentId)),
          <.td(item.content),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.pullRight, bss.buttonXS)), "Delete")
            )
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Mentor"),
            <.th("Iteration"),
            <.th("Student"),
            <.th("Content"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[Feedback],
            mentors: Map[Int, Mentor],
            iterations: Map[Int, Iteration],
            students: Map[Int, StudentWithData],
            employments: Map[Int, Employment],
            editItem: Feedback => Callback,
            deleteItem: Feedback => Callback) =
    component(Props(items, mentors, iterations, students, employments, editItem, deleteItem))
}
