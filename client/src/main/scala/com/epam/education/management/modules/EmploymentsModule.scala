package com.epam.education.management.modules

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{EmploymentList, GlobalStyles, Icon}
import com.epam.education.management.logger.log
import com.epam.education.management.services.EmploymentTypesHandler.{EmploymentTypes, FetchEmploymentTypesIfEmpty}
import com.epam.education.management.services.EmploymentsHandler._
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto.{Employment, EmploymentType}
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._
import scala.collection.breakOut

object EmploymentsModule {

  case class Props(proxy: ModelProxy[Pot[(Employments, Map[Int, EmploymentType])]])

  case class State(selectedItem: Option[Employment] = None, showEmploymentForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the employments, which will cause EmploymentsStore to fetch employments from the server
      Callback.when(props.proxy().isEmpty)(
        props.proxy.dispatch(FetchEmploymentsIfEmpty) >>
        props.proxy.dispatch(FetchEmploymentTypesIfEmpty)
      )

    def editEmployment(item: Option[Employment]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showEmploymentForm = true))

    def employmentEdited(item: Employment, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Employment editing cancelled")
      } else {
        val action =
          if (item.id.getOrElse(-1) > 0) UpdateEmployment(item)
          else CreateEmployment(item)
        Callback.log(s"Employment edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showEmploymentForm = false))
    }

    def render(p: Props, s: State) = {
      Panel(
        Panel.Props("Employments list"),
        <.div(
          Button(Button.Props(editEmployment(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render { case (employments, employmentTypes) =>
          EmploymentList(
            employments.items,
            employmentTypes,
            item => editEmployment(Some(item)),
            item => p.proxy.dispatch(DeleteEmployment(item)))
        },
        p.proxy().render { case (_, employmentTypes) =>
          // if the dialog is open, add it to the panel
          if (s.showEmploymentForm) EmploymentForm(EmploymentForm.Props(s.selectedItem, employmentTypes, employmentEdited))
          else // otherwise add an empty placeholder
            Seq.empty[ReactElement]
        }
      )
    }
  }

  // create the React component for To Do management
  val component = ReactComponentB[Props]("Employments")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[(Employments, EmploymentTypes)]]) = {
    def employmentTypesToMap(ets: Seq[EmploymentType]): Map[Int, EmploymentType] =
      ets.collect { case et@EmploymentType(Some(id), _) => id -> et }(breakOut)

    component(Props(proxy.zoom(x => x.map { case (employments, employmentTypes) =>
      (employments, employmentTypesToMap(employmentTypes.items))
    })))
  }
}

object EmploymentForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      item: Option[Employment],
      employmentTypes: Map[Int, EmploymentType],
      submitHandler: (Employment, Boolean) => Callback
  )

  case class State(item: Employment, cancelled: Boolean = true)

  class Backend(t: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      t.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.cancelled)

    def updateField(f: (Employment, String) => Employment)(e: ReactEventI) = {
      val text = e.target.value
      t.modState(s => s.copy(item = f(s.item, text)))
    }
    def updateIntField(f: (Employment, Int) => Employment)(e: ReactEventI): Callback = {
      try {
        val value: Int = e.target.value.toInt
        t.modState(s => s.copy(item = f(s.item, value)))
      } catch {
        case _: NumberFormatException => Callback.empty
      }
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.item.id.getOrElse(-1) <= 0) "adding" else "editing"} an employment or two")
      val headerText = if (s.item.id.getOrElse(-1) <= 0) "Add new employment" else "Edit employment"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          <.label(^.`for` := "id", "Id"),
          <.input.text(bss.formControl, ^.id := "id", ^.value := s.item.id, ^.disabled := true)),
        <.div(bss.formGroup,
          <.label(^.`for` := "title", "Title"),
          <.input.text(bss.formControl, ^.id := "title", ^.value := s.item.title,
            ^.placeholder := "write title", ^.onChange ==> updateField((s, t) => s.copy(title = t)))),
        <.div(bss.formGroup,
          <.label(^.`for` := "duration", "Duration"),
          <.input.number(bss.formControl, ^.id := "duration", ^.step := 1, ^.value := s.item.duration,
            ^.placeholder := "write duration", ^.onChange ==> updateIntField((s, t) => s.copy(duration = t)))),
        <.div(bss.formGroup,
          <.label(^.`for` := "employmentType", "EmploymentTypes"),
          <.select(bss.formControl, ^.id := "employmentType", ^.value := s.item.employmentTypeId.toString,
            ^.onChange ==> updateIntField((s, i) => s.copy(employmentTypeId = i)),
            for ((id, employmentType) <- p.employmentTypes) yield {
              <.option(^.value := employmentType.id, employmentType.title)
            }
          )
        )
      )
    }
  }

  val component = ReactComponentB[Props]("EmploymentForm")
    .initialState_P(p => State(p.item.getOrElse(Employment(None, title = "", duration = 0 , employmentTypeId = 0 ))))
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}