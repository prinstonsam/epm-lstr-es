package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto.User
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object UserList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[User],
      editItem: User => Callback,
      deleteItem: User => Callback
  )

  private val component =
    ReactComponentB[Props]("UserList").render_P { p =>
      def renderItem(item: User) = {
        <.tr(
          <.td(item.id),
          <.td(item.firstName),
          <.td(item.lastName),
          <.td(item.email),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete")
            )
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Given Name"),
            <.th("Family Name"),
            <.th("Email"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[User], editItem: User => Callback, deleteItem: User => Callback) =
    component(Props(items, editItem, deleteItem))

}
