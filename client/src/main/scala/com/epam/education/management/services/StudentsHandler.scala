package com.epam.education.management.services

import com.epam.education.management.services.StudentsHandler.StudentsWithData
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.{StudentData, StudentWithData}

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Handles actions related to students
  *
  * @param modelRW Reader/Writer to access the model
  */
class StudentsHandler[M](modelRW: ModelRW[M, Pot[StudentsWithData]]) extends ActionHandler(modelRW) {

  import StudentsHandler._

  override def handle = {
    case a: StudentAction => handleStrict(a)
  }

  def handleStrict(action: StudentAction) = action match {
    case FetchStudentsIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateStudents()))
      else noChange
    case action: UpdateStudents =>
      val updateF = action.effect(AjaxClient.student.findAll())(StudentsWithData)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateStudent(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.student.update(item.student).map(_ => UpdateStudents())))
    case CreateStudentWithData(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.student.create(item).map(_ => UpdateStudents())))
    case DeleteStudent(item) =>
      // make a local update and inform server
      updated(
        value.map(_.remove(item)),
        Effect(AjaxClient.student.delete(item.student.id.get).map(_ => UpdateStudents())))
    case CreateStudentData(item) =>
      AjaxClient.student.history.create(item)
      noChange
  }
}

object StudentsHandler {

  case class StudentsWithData(items: Seq[StudentWithData]) {
    def updated(item: StudentWithData) = {
      items.indexWhere(_.student.id == item.student.id) match {
        case -1  => copy(items = items :+ item)
        case idx => copy(items = items.updated(idx, item))
      }
    }

    def remove(item: StudentWithData) = copy(items = items.filterNot(_ == item))
  }

  sealed trait StudentAction extends Action

  case object FetchStudentsIfEmpty extends StudentAction

  case class UpdateStudents(potResult: Pot[StudentsWithData] = Empty)
      extends PotAction[StudentsWithData, UpdateStudents]
      with StudentAction {
    override def next(newResult: Pot[StudentsWithData]): UpdateStudents = UpdateStudents(newResult)
  }

  case class UpdateStudent(item: StudentWithData) extends StudentAction

  case class CreateStudentWithData(item: StudentWithData) extends StudentAction

  case class DeleteStudent(item: StudentWithData) extends StudentAction

  case class CreateStudentData(item: StudentData) extends StudentAction

}
