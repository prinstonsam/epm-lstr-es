package com.epam.education.management.services

import com.epam.education.management.services.UsersHandler.Users
import diode._
import diode.data.{Empty, Pot, PotAction}
import dto.User

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

class UsersHandler[M](modelRW: ModelRW[M, Pot[Users]]) extends ActionHandler(modelRW) {

  import UsersHandler._

  override def handle = {
    case a: UserAction => handleStrict(a)
  }

  def handleStrict(action: UserAction) = action match {
    case action: UpdateUsers =>
      val updateF = action.effect(AjaxClient.user.findAll())(Users)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateUser(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.user.update(item).map(_ => UpdateUsers())))
    case CreateUser(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.user.create(item).map(_ => UpdateUsers())))
    case DeleteUser(item) =>
      // make a local update and inform server
      updated(value.map(_.remove(item)), Effect(AjaxClient.user.delete(item.id.get).map(_ => UpdateUsers())))
  }
}

object UsersHandler {

  case class Users(items: Seq[User]) {
    def updated(item: User) = {
      items.indexWhere(_.id == item.id) match {
        case -1 => Users(items :+ item)
        case idx => Users(items.updated(idx, item))
      }
    }

    def remove(item: User) = Users(items.filterNot(_ == item))
  }

  sealed trait UserAction extends Action

  case class UpdateUsers(potResult: Pot[Users] = Empty)
    extends PotAction[Users, UpdateUsers] with UserAction{
    override def next(newResult: Pot[Users]): UpdateUsers = UpdateUsers(newResult)
  }

  case class UpdateUser(item: User) extends UserAction

  case class CreateUser(item: User) extends UserAction

  case class DeleteUser(item: User) extends UserAction

}
