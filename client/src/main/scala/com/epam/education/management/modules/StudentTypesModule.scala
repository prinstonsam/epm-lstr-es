package com.epam.education.management.modules

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{GlobalStyles, Icon, StudentTypeList}
import com.epam.education.management.logger.log
import com.epam.education.management.services.StudentTypesHandler._
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto.StudentType
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object StudentTypesModule {

  case class Props(proxy: ModelProxy[Pot[StudentTypes]])

  case class State(selectedItem: Option[StudentType] = None, showStudentTypeForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the students, which will cause StudentsStore to fetch students from the server
      Callback.when(props.proxy().isEmpty)(props.proxy.dispatch(FetchStudentTypesIfEmpty))

    def editStudentType(item: Option[StudentType]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showStudentTypeForm = true))

    def studentTypeEdited(item: StudentType, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Student type editing cancelled")
      } else {
        val action =
          if (item.id.getOrElse(-1) > 0) UpdateStudentType(item)
          else CreateStudentType(item)
        Callback.log(s"Student type edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showStudentTypeForm = false))
    }

    def render(p: Props, s: State) =
      Panel(
        Panel.Props("Student types list"),
        <.div(
          Button(Button.Props(editStudentType(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render { studentTypes =>
          StudentTypeList(
            studentTypes.items,
            item => editStudentType(Some(item)),
            item => p.proxy.dispatch(DeleteStudentType(item)))
        },
        // if the dialog is open, add it to the panel
        if (s.showStudentTypeForm) StudentTypeForm(StudentTypeForm.Props(s.selectedItem, studentTypeEdited))
        else // otherwise add an empty placeholder
          Seq.empty[ReactElement]
      )
  }

  val component = ReactComponentB[Props]("StudentTypes")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[StudentTypes]]) = component(Props(proxy))
}

object StudentTypeForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      item: Option[StudentType],
      submitHandler: (StudentType, Boolean) => Callback
  )

  case class State(item: StudentType, cancelled: Boolean = true)

  class Backend(t: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      t.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.cancelled)

    def updateTitle(e: ReactEventI) = {
      val text = e.target.value
      t.modState(s => s.copy(item = s.item.copy(title = text)))
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.item.id.getOrElse(-1) <= 0) "adding" else "editing"} a student type or two")
      val headerText = if (s.item.id.getOrElse(-1) <= 0) "Add new student type" else "Edit student type"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          <.label(^.`for` := "id", "Id"),
          <.input.text(bss.formControl, ^.id := "id", ^.value := s.item.id, ^.disabled := true)),
        <.div(bss.formGroup,
          <.label(^.`for` := "title", "Title"),
          <.input.text(bss.formControl, ^.id := "title", ^.value := s.item.title,
            ^.placeholder := "write title", ^.onChange ==> updateTitle))
      )
    }
  }

  val component = ReactComponentB[Props]("StudentTypeForm")
    .initialState_P(p => State(p.item.getOrElse(StudentType(title = ""))))
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}