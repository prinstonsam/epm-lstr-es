package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto.{Employment, Iteration, Mentor}
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._
import scalacss.ScalaCssReact._
import com.epam.education.management.utils.Instants._

object IterationList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[Iteration],
      mentors: Map[Int,Mentor],
      employments: Map[Int,Employment],
      editItem: Iteration => Callback,
      deleteItem: Iteration => Callback
  )

  private val component =
    ReactComponentB[Props]("IterationList").render_P { p =>
      def getMentor(id: Int) = p.mentors.get(id).map(m=> m.firstName+" "+m.lastName).getOrElse("Error!")
      def getEmployment(id: Int) = p.employments.get(id).map(_.title).getOrElse("Error!")
      def renderItem(item: Iteration) = {
        <.tr(
          <.td(item.id),
          <.td(getEmployment(item.employmentId)),
          <.td(getMentor(item.mentorId)),
          <.td(item.startDate),
          <.td(item.endDate),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete"))
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Employment"),
            <.th("Mentor"),
            <.th("Date From"),
            <.th("Date To"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[Iteration],
            mentors: Map[Int, Mentor],
            employments: Map[Int,Employment],
            editItem: Iteration => Callback,
            deleteItem: Iteration => Callback) =
    component(Props(items, mentors, employments, editItem, deleteItem))

}
