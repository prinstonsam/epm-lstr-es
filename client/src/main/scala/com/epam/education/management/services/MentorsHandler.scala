package com.epam.education.management.services

import com.epam.education.management.services.MentorsHandler.Mentors
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.Mentor

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

class MentorsHandler[M](modelRW: ModelRW[M, Pot[Mentors]]) extends ActionHandler(modelRW) {

  import MentorsHandler._

  override def handle = {
    case a: MentorAction => handleStrict(a)
  }

  def handleStrict(action: MentorAction) = action match {
    case FetchMentorsIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateMentors()))
      else noChange
    case action: UpdateMentors =>
      val updateF = action.effect(AjaxClient.mentor.findAll())(Mentors)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateMentor(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.mentor.update(item).map(_ => UpdateMentors())))
    case CreateMentor(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.mentor.create(item).map(_ => UpdateMentors())))
    case DeleteMentor(item) =>
      // make a local update and inform server
      updated(value.map(_.remove(item)), Effect(AjaxClient.mentor.delete(item.id.get).map(_ => UpdateMentors())))
  }
}

object MentorsHandler {

  case class Mentors(items: Seq[Mentor]) {
    def updated(item: Mentor) = {
      items.indexWhere(_.id == item.id) match {
        case -1 => Mentors(items :+ item)
        case idx => Mentors(items.updated(idx, item))
      }
    }

    def remove(item: Mentor) = Mentors(items.filterNot(_ == item))
  }

  sealed trait MentorAction extends Action

  case object FetchMentorsIfEmpty extends MentorAction

  case class UpdateMentors(potResult: Pot[Mentors] = Empty)
    extends PotAction[Mentors, UpdateMentors] with MentorAction{
    override def next(newResult: Pot[Mentors]): UpdateMentors = UpdateMentors(newResult)
  }

  case class UpdateMentor(item: Mentor) extends MentorAction

  case class CreateMentor(item: Mentor) extends MentorAction

  case class DeleteMentor(item: Mentor) extends MentorAction

}
