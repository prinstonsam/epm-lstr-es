package com.epam.education.management.services

import com.epam.education.management.services.MotdHandler._
import diode._
import diode.data.{Empty, Pot, PotAction}
import diode.util.RunAfterJS

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Handles actions related to the Motd
  *
  * @param modelRW Reader/Writer to access the model
  */
class MotdHandler[M](modelRW: ModelRW[M, Pot[String]]) extends ActionHandler(modelRW) {
  implicit val runner = new RunAfterJS

  override def handle = {
    case action: UpdateMotd =>
      val updateF = action.effect(AjaxClient.welcomeMsg("User X"))(identity)
      action.handleWith(this, updateF)(PotAction.handler())
  }
}

case object MotdHandler {

  // Actions
  case class UpdateMotd(potResult: Pot[String] = Empty) extends PotAction[String, UpdateMotd] {
    override def next(value: Pot[String]) = UpdateMotd(value)
  }

}
