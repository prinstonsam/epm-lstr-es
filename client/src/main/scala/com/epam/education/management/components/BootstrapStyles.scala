package com.epam.education.management.components

import scalacss.Defaults._
import scalacss.mutable
import Bootstrap.CommonStyle._

class BootstrapStyles(implicit r: mutable.Register) extends StyleSheet.Inline()(r) {

  import dsl._

  val csDomain      = Domain.ofValues(default, primary, success, info, warning, danger)
  val contextDomain = Domain.ofValues(success, info, warning, danger)

  def commonStyle[A](domain: Domain[A], base: String) =
    styleF(domain)(opt => styleS(addClassNames(base, s"$base-$opt")))

  def styleWrap(classNames: String*) = style(addClassNames(classNames: _*))

  val buttonOpt    = commonStyle(csDomain, "btn")
  val button       = buttonOpt(default)
  val panelOpt     = commonStyle(csDomain, "panel")
  val panel        = panelOpt(default)
  val labelOpt     = commonStyle(csDomain, "label")
  val label        = labelOpt(default)
  val alert        = commonStyle(contextDomain, "alert")
  val panelHeading = styleWrap("panel-heading")
  val panelBody    = styleWrap("panel-body")

  // wrap styles in a namespace, assign to val to prevent lazy initialization
  object modal {
    val modal   = styleWrap("modal")
    val fade    = styleWrap("fade")
    val dialog  = styleWrap("modal-dialog")
    val `modal-lg`  = styleWrap("modal-lg")
    val `modal-wide` = style(width(80.%%))
    val content = styleWrap("modal-content")
    val header  = styleWrap("modal-header")
    val body    = styleWrap("modal-body")
    val footer  = styleWrap("modal-footer")
  }

  val _modal = modal

  object listGroup {
    val listGroup = styleWrap("list-group")
    val item      = styleWrap("list-group-item")
    val itemOpt   = commonStyle(contextDomain, "list-group-item")
  }

  val _listGroup = listGroup
  val pullRight  = styleWrap("pull-right")
  val buttonXS   = styleWrap("btn-xs")
  val close      = styleWrap("close")

  val labelAsBadge = style(addClassName("label-as-badge"), borderRadius(1.em))

  val navbar = styleWrap("nav", "navbar-nav")

  val formGroup   = styleWrap("form-group")
  val formControl = styleWrap("form-control")

  val row   = styleWrap("row")

  val col = new Col

  class Col(implicit r: mutable.Register) extends StyleSheet.Inline()(r) {
    val `xs-1` = styleWrap("col-xs-1")
    val `sm-1` = styleWrap("col-sm-1")
    val `md-1` = styleWrap("col-md-1")
    val `lg-1` = styleWrap("col-lg-1")
    val `xs-2` = styleWrap("col-xs-2")
    val `sm-2` = styleWrap("col-sm-2")
    val `md-2` = styleWrap("col-md-2")
    val `lg-2` = styleWrap("col-lg-2")
    val `xs-3` = styleWrap("col-xs-3")
    val `sm-3` = styleWrap("col-sm-3")
    val `md-3` = styleWrap("col-md-3")
    val `lg-3` = styleWrap("col-lg-3")
    val `xs-4` = styleWrap("col-xs-4")
    val `sm-4` = styleWrap("col-sm-4")
    val `md-4` = styleWrap("col-md-4")
    val `lg-4` = styleWrap("col-lg-4")
    val `xs-5` = styleWrap("col-xs-5")
    val `sm-5` = styleWrap("col-sm-5")
    val `md-5` = styleWrap("col-md-5")
    val `lg-5` = styleWrap("col-lg-5")
    val `xs-6` = styleWrap("col-xs-6")
    val `sm-6` = styleWrap("col-sm-6")
    val `md-6` = styleWrap("col-md-6")
    val `lg-6` = styleWrap("col-lg-6")
    val `xs-7` = styleWrap("col-xs-7")
    val `sm-7` = styleWrap("col-sm-7")
    val `md-7` = styleWrap("col-md-7")
    val `lg-7` = styleWrap("col-lg-7")
    val `xs-8` = styleWrap("col-xs-8")
    val `sm-8` = styleWrap("col-sm-8")
    val `md-8` = styleWrap("col-md-8")
    val `lg-8` = styleWrap("col-lg-8")
    val `xs-9` = styleWrap("col-xs-9")
    val `sm-9` = styleWrap("col-sm-9")
    val `md-9` = styleWrap("col-md-9")
    val `lg-9` = styleWrap("col-lg-9")
    val `xs-10` = styleWrap("col-xs-10")
    val `sm-10` = styleWrap("col-sm-10")
    val `md-10` = styleWrap("col-md-10")
    val `lg-10` = styleWrap("col-lg-10")
    val `xs-11` = styleWrap("col-xs-11")
    val `sm-11` = styleWrap("col-sm-11")
    val `md-11` = styleWrap("col-md-11")
    val `lg-11` = styleWrap("col-lg-11")
    val `xs-12` = styleWrap("col-xs-12")
    val `sm-12` = styleWrap("col-sm-12")
    val `md-12` = styleWrap("col-md-12")
    val `lg-12` = styleWrap("col-lg-12")
  }

  object TableModifications extends Enumeration {
    val striped, bordered, hover, condensed, responsive = Value
  }

  val table = new Table

  class Table(implicit r: mutable.Register) extends StyleSheet.Inline()(r) {
    import TableModifications._
    val tableDomain = Domain.ofValues(striped, bordered, hover, condensed, responsive)

    private val tableMod = styleF(tableDomain)(opt => styleS(addClassName(s"table-$opt")))

    def tableMods(mods: TableModifications.Value*) =
      mods.map(tableMod).foldLeft(styleWrap("table")) { _ + _ }

    val table = tableMods()

    val tableContextDomain = Domain.ofValues(active, success, info, warning, danger)

    val context = styleF(tableContextDomain)(opt => styleS(addClassName(s"$opt")))
  }

}
