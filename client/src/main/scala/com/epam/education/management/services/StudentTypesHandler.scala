package com.epam.education.management.services

import com.epam.education.management.services.StudentTypesHandler.StudentTypes
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.StudentType

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Handles actions related to student types
  *
  * @param modelRW Reader/Writer to access the model
  */
class StudentTypesHandler[M](modelRW: ModelRW[M, Pot[StudentTypes]]) extends ActionHandler(modelRW) {

  import StudentTypesHandler._

  override def handle = {
    case a: StudentTypeAction => handleStrict(a)
  }

  def handleStrict(action: StudentTypeAction) = action match {
    case FetchStudentTypesIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateStudentTypes()))
      else noChange
    case action: UpdateStudentTypes =>
      val updateF =
        action.effect(AjaxClient.studentType.findAll())(StudentTypes)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateStudentType(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.studentType.update(item).map(_ => UpdateStudentTypes())))
    case CreateStudentType(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.studentType.create(item).map(_ => UpdateStudentTypes())))
    case DeleteStudentType(item) =>
      // make a local update and inform server
      updated(
        value.map(_.remove(item)),
        Effect(AjaxClient.studentType.delete(item.id.get).map(_ => UpdateStudentTypes())))
  }
}

object StudentTypesHandler {

  case class StudentTypes(items: Seq[StudentType]) {
    def updated(item: StudentType) = {
      items.indexWhere(_.id == item.id) match {
        case -1 => copy(items = items :+ item)
        case idx => copy(items = items.updated(idx, item))
      }
    }

    def remove(item: StudentType) = copy(items = items.filterNot(_ == item))
  }

  sealed trait StudentTypeAction extends Action

  case object FetchStudentTypesIfEmpty extends StudentTypeAction

  case class UpdateStudentTypes(potResult: Pot[StudentTypes] = Empty)
    extends PotAction[StudentTypes, UpdateStudentTypes]
      with StudentTypeAction {
    override def next(newResult: Pot[StudentTypes]): UpdateStudentTypes = UpdateStudentTypes(newResult)
  }

  case class UpdateStudentType(item: StudentType) extends StudentTypeAction

  case class CreateStudentType(item: StudentType) extends StudentTypeAction

  case class DeleteStudentType(item: StudentType) extends StudentTypeAction

}
