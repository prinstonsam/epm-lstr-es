package com.epam.education.management.modules

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{GlobalStyles, Icon, StudentIterationList}
import com.epam.education.management.logger.log
import com.epam.education.management.services.EmploymentsHandler.{Employments, FetchEmploymentsIfEmpty}
import com.epam.education.management.services.IterationsHandler.{FetchIterationsIfEmpty, Iterations}
import com.epam.education.management.services.ResultTypesHandler.{FetchResultTypesIfEmpty, ResultTypes}
import com.epam.education.management.services.StudentIterationsHandler._
import com.epam.education.management.services.StudentsHandler.{FetchStudentsIfEmpty, StudentsWithData}
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto._
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scala.collection.breakOut
import scalacss.ScalaCssReact._

object StudentIterationsModule {

  case class Props(proxy: ModelProxy[
    Pot[(
      StudentIterations,
      Map[Int, StudentWithData],
      Map[Int, Iteration],
      Map[Int, Employment],
      Map[Int, ResultType]
      )]])

  case class State(selectedItem: Option[StudentIteration] = None, showStudentIterationForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the students, which will cause StudentsStore to fetch students from the server
      Callback.when(props.proxy().isEmpty)(
        props.proxy.dispatch(FetchStudentIterationsIfEmpty) >>
        props.proxy.dispatch(FetchEmploymentsIfEmpty) >>
        props.proxy.dispatch(FetchResultTypesIfEmpty) >>
        props.proxy.dispatch(FetchStudentsIfEmpty) >>
        props.proxy.dispatch(FetchIterationsIfEmpty)
      )

    def editStudentIteration(item: Option[StudentIteration]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showStudentIterationForm = true))

    def studentIterationEdited(item: StudentIteration, isNew: Boolean, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Student iteration editing cancelled")
      } else {
        val action =
          if (isNew) UpdateStudentIteration(item)
          else CreateStudentIteration(item)
        Callback.log(s"Student iteration edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showStudentIterationForm = false))
    }

    def render(p: Props, s: State) = {
      Panel(
        Panel.Props("Student iterations list"),
        <.div(
          Button(Button.Props(editStudentIteration(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render {
          case (studentIterations, students, iterations, employments, resultTypes) =>
          StudentIterationList(
            studentIterations.items,
            students,
            iterations,
            employments,
            resultTypes,
            item => editStudentIteration(Some(item)),
            item => p.proxy.dispatch(DeleteStudentIteration(item)))
        },
        p.proxy().render {
          case (_, students, iterations, employments, resultTypes) =>
            if (s.showStudentIterationForm)
              StudentIterationForm(
                StudentIterationForm.Props(
                  s.selectedItem,
                  students,
                  iterations,
                  employments,
                  resultTypes,
                  studentIterationEdited))
            else // otherwise add an empty placeholder
              Seq.empty[ReactElement]
        }
      )
    }
  }

  val component = ReactComponentB[Props]("StudentIterations")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[(StudentIterations, StudentsWithData, Iterations, Employments, ResultTypes)]])  = {
    def resultTypesToMap(rts: Seq[ResultType]): Map[Int, ResultType] =
      rts.collect { case rt@ResultType(Some(id), _) => id -> rt }(breakOut)
    def iterationsToMap(its: Seq[Iteration]): Map[Int, Iteration] =
      its.collect { case it@Iteration(Some(id), _, _, _, _) => id -> it }(breakOut)
    def studentsToMap(sts: Seq[StudentWithData]): Map[Int, StudentWithData] =
      sts.collect { case st@StudentWithData(s, _) => s.id.get -> st }(breakOut)
    def employmentsToMap(ems: Seq[Employment]): Map[Int, Employment] =
      ems.collect { case em@Employment(Some(id), _, _, _) => id -> em }(breakOut)
    component(Props(proxy.zoom(x =>
      x.map {
        case (studentIterations, students, iterations, employments, resultTypes) =>
          (studentIterations,
           studentsToMap(students.items),
           iterationsToMap(iterations.items),
           employmentsToMap(employments.items),
           resultTypesToMap(resultTypes.items))
      })))
  }
}

object StudentIterationForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      item: Option[StudentIteration],
      students: Map[Int, StudentWithData],
      iterations: Map[Int, Iteration],
      employments: Map[Int, Employment],
      resultTypes: Map[Int, ResultType],
      submitHandler: (StudentIteration, Boolean, Boolean) => Callback
  )

  case class State(item: StudentIteration, isNew: Boolean, cancelled: Boolean = true)

  class Backend(t: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      t.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.isNew, state.cancelled)

    def updateIntField(f: (StudentIteration, Int) => StudentIteration)(e: ReactEventI): Callback = {
      try {
        val value: Int = e.target.value.toInt
        t.modState(s => s.copy(item = f(s.item, value)))
      } catch {
        case _: NumberFormatException => Callback.empty
      }
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.isNew) "adding" else "editing"} a student iteration or two")
      val headerText = if (s.isNew) "Add new student iteration" else "Edit student iteration"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          <.label(^.`for` := "student", "StudentTypes"),
          <.select(bss.formControl, ^.id := "student", ^.value := s.item.studentId.toString,
            ^.onChange ==> updateIntField((si, i) => si.copy(studentId = i)),
            for ((id, student) <- p.students) yield {
              <.option(^.value := student.student.id, student.student.firstName + " " + student.student.lastName)
            }
          )
        ),
        <.div(bss.formGroup,
          <.label(^.`for` := "iteration", "Iterations"),
          <.select(bss.formControl, ^.id := "iteration", ^.value := s.item.iterationId.toString,
            ^.onChange ==> updateIntField((si, i) => si.copy(iterationId = i)),
            for ((id, iteration) <- p.iterations) yield {
              val emp = p.employments.get(iteration.id.get).map(e => e.title).getOrElse("Error!")
              <.option(^.value := iteration.id, emp + ", " + iteration.startDate + ", " + iteration.endDate)
            }
          )
        ),
        <.div(bss.formGroup,
          <.label(^.`for` := "result", ^.step := 1, "ResultType"),
          <.select(bss.formControl, ^.id := "resultType", ^.value := s.item.resultTypeId.toString,
            ^.onChange ==> updateIntField((si, i) => si.copy(resultTypeId = i)),
            for ((id, resultType) <- p.resultTypes) yield {
              <.option(^.value := resultType.id, resultType.title)
            }
          )
        )
      )
    }
  }

  val component = ReactComponentB[Props]("StudentIterationForm")
    .initialState_P { p =>
      State(
        p.item.getOrElse(StudentIteration(studentId = 0, iterationId = 0, resultTypeId = 0)),
        isNew = p.item.isEmpty
      )
    }
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}