package com.epam.education.management.services
import com.epam.education.management.services.ResultTypesHandler.ResultTypes
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.ResultType

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Handles actions related to result types
  *
  * @param modelRW Reader/Writer to access the model
  */
class ResultTypesHandler[M](modelRW: ModelRW[M, Pot[ResultTypes]]) extends ActionHandler(modelRW) {

  import ResultTypesHandler._

  override def handle = {
    case a: ResultTypeAction => handleStrict(a)
  }

  def handleStrict(action: ResultTypeAction) = action match {
    case FetchResultTypesIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateResultTypes()))
      else noChange
    case action: UpdateResultTypes =>
      val updateF =
        action.effect(AjaxClient.resultType.findAll())(ResultTypes)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateResultType(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.resultType.update(item).map(_ => UpdateResultTypes())))
    case CreateResultType(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.resultType.create(item).map(_ => UpdateResultTypes())))
    case DeleteResultType(item) =>
      // make a local update and inform server
      updated(
        value.map(_.remove(item)),
        Effect(AjaxClient.resultType.delete(item.id.get).map(_ => UpdateResultTypes())))
  }
}

object ResultTypesHandler {

  case class ResultTypes(items: Seq[ResultType]) {
    def updated(item: ResultType) = {
      items.indexWhere(_.id == item.id) match {
        case -1  => ResultTypes(items :+ item)
        case idx => ResultTypes(items.updated(idx, item))
      }
    }

    def remove(item: ResultType) = ResultTypes(items.filterNot(_ == item))
  }

  sealed trait ResultTypeAction extends Action

  case object FetchResultTypesIfEmpty extends ResultTypeAction

  case class UpdateResultTypes(potResult: Pot[ResultTypes] = Empty)
      extends PotAction[ResultTypes, UpdateResultTypes]
      with ResultTypeAction {
    override def next(newResult: Pot[ResultTypes]): UpdateResultTypes = UpdateResultTypes(newResult)
  }

  case class UpdateResultType(item: ResultType) extends ResultTypeAction

  case class CreateResultType(item: ResultType) extends ResultTypeAction

  case class DeleteResultType(item: ResultType) extends ResultTypeAction

}
