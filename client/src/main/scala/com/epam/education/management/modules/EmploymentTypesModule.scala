package com.epam.education.management.modules

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{GlobalStyles, Icon, EmploymentTypeList}
import com.epam.education.management.logger.log
import com.epam.education.management.services.EmploymentTypesHandler._
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto.EmploymentType
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object EmploymentTypesModule {

  case class Props(proxy: ModelProxy[Pot[EmploymentTypes]])

  case class State(selectedItem: Option[EmploymentType] = None, showEmploymentTypeForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the EmploymentTypes, which will cause EmploymentTypes Store to fetch EmploymentTypes from the server
      Callback.when(props.proxy().isEmpty)(props.proxy.dispatch(FetchEmploymentTypesIfEmpty))

    def editEmploymentType(item: Option[EmploymentType]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showEmploymentTypeForm = true))

    def employmentTypeEdited(item: EmploymentType, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Employment type editing cancelled")
      } else {
        val action =
          if (item.id.getOrElse(-1) > 0) UpdateEmploymentType(item)
          else CreateEmploymentType(item)
        Callback.log(s"Employment type edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showEmploymentTypeForm = false))
    }

    def render(p: Props, s: State) =
      Panel(
        Panel.Props("Employment types list"),
        <.div(
          Button(Button.Props(editEmploymentType(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render { employmentTypes =>
          EmploymentTypeList(
            employmentTypes.items,
            item => editEmploymentType(Some(item)),
            item => p.proxy.dispatch(DeleteEmploymentType(item)))
        },
        // if the dialog is open, add it to the panel
        if (s.showEmploymentTypeForm) EmploymentTypeForm(EmploymentTypeForm.Props(s.selectedItem, employmentTypeEdited))
        else // otherwise add an empty placeholder
          Seq.empty[ReactElement]
      )
  }

  // create the React component for To Do management
  val component = ReactComponentB[Props]("EmploymentTypes")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[EmploymentTypes]]) = component(Props(proxy))
}

object EmploymentTypeForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      item: Option[EmploymentType],
      submitHandler: (EmploymentType, Boolean) => Callback
  )

  case class State(item: EmploymentType, cancelled: Boolean = true)

  class Backend(t: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      t.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.cancelled)

    def updateTitle(e: ReactEventI) = {
      val text = e.target.value
      t.modState(s => s.copy(item = s.item.copy(title = text)))
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.item.id.getOrElse(-1) <= 0) "adding" else "editing"} an employment type or two")
      val headerText = if (s.item.id.getOrElse(-1) <= 0) "Add new employment type" else "Edit employment type"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          <.label(^.`for` := "id", "Id"),
          <.input.text(bss.formControl, ^.id := "id", ^.value := s.item.id, ^.disabled := true)),
        <.div(bss.formGroup,
          <.label(^.`for` := "title", "Title"),
          <.input.text(bss.formControl, ^.id := "title", ^.value := s.item.title,
            ^.placeholder := "write title", ^.onChange ==> updateTitle))
      )
    }
  }

  val component = ReactComponentB[Props]("EmploymentTypeForm")
    .initialState_P(p => State(p.item.getOrElse(EmploymentType(title = ""))))
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}