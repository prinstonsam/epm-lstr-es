package com.epam.education.management.components

import dto.{StudentData, StudentType}
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._
import Bootstrap.CommonStyle.active

import scalacss.ScalaCssReact._
import com.epam.education.management.utils.Instants._

object StudentDataList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[StudentData],
      selected: Option[StudentData],
      types: Map[Int, StudentType]
  )

  private val component =
    ReactComponentB[Props]("StudentDataList").render_P { p =>
      def getType(typeId: Int) = {
        p.types.get(typeId).map(_.title)
      }

      def renderItem(item: StudentData) = {
        val style =
          p.selected
            .filter(s => s.version == item.version && s.studentId == item.studentId)
            .map(_ => bss.table.context(active))
        <.tr(style)(
          <.td(item.studentId),
          <.td(_react_fragReactNode(item.version)),
          <.td(getType(item.studentTypeId)),
          <.td(item.startDate),
          <.td(item.endDate)
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Student id"),
            <.th("Version"),
            <.th("Type"),
            <.th("Start date"),
            <.th("End date")
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[StudentData], selected: Option[StudentData], types: Map[Int, StudentType]) =
    component(Props(items, selected, types))

}
