package com.epam.education.management.components

import com.epam.education.management.components.Bootstrap.Button
import dto.EmploymentType
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object EmploymentTypeList {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
      items: Seq[EmploymentType],
      editItem: EmploymentType => Callback,
      deleteItem: EmploymentType => Callback
  )

  private val component =
    ReactComponentB[Props]("EmploymentTypeList").render_P { p =>
      def renderItem(item: EmploymentType) = {
        <.tr(
          <.td(item.id),
          <.td(item.title),
          <.td(
            <.div(bss.pullRight)(
              Button(Button.Props(p.editItem(item), addStyles = Seq(bss.buttonXS)), "Edit"),
              Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.buttonXS)), "Delete")
            )
          )
        )
      }
      <.table(bss.table.table)(
        <.thead(
          <.tr(
            <.th("Id"),
            <.th("Title"),
            <.th()
          )),
        <.tbody(p.items map renderItem)
      )
    }.build

  def apply(items: Seq[EmploymentType], editItem: EmploymentType => Callback, deleteItem: EmploymentType => Callback) =
    component(Props(items, editItem, deleteItem))

}
