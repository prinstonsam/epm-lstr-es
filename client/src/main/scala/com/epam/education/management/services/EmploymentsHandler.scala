package com.epam.education.management.services

import com.epam.education.management.services.EmploymentsHandler.Employments
import diode._
import diode.data.{Empty, Pending, Pot, PotAction}
import dto.Employment

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Handles actions related to employments
  *
  * @param modelRW Reader/Writer to access the model
  */
class EmploymentsHandler[M](modelRW: ModelRW[M, Pot[Employments]]) extends ActionHandler(modelRW) {

  import EmploymentsHandler._

  override def handle = {
    case a: EmploymentAction => handleStrict(a)
  }

  def handleStrict(action: EmploymentAction) = action match {
    case FetchEmploymentsIfEmpty =>
      if (value == Empty) updated(Pending(), Effect.action(UpdateEmployments()))
      else noChange
    case action: UpdateEmployments =>
      val updateF =
        action.effect(AjaxClient.employment.findAll())(Employments)
      action.handleWith(this, updateF)(PotAction.handler())
    case UpdateEmployment(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.employment.update(item).map(_ => UpdateEmployments())))
    case CreateEmployment(item) =>
      // make a local update and inform server
      updated(value.map(_.updated(item)), Effect(AjaxClient.employment.create(item).map(_ => UpdateEmployments())))
    case DeleteEmployment(item) =>
      // make a local update and inform server
      updated(
        value.map(_.remove(item)),
        Effect(AjaxClient.employment.delete(item.id.get).map(_ => UpdateEmployments())))
  }
}

object EmploymentsHandler {

  case class Employments(items: Seq[Employment]) {
    def updated(item: Employment) = {
      items.indexWhere(_.id == item.id) match {
        case -1  => Employments(items :+ item)
        case idx => Employments(items.updated(idx, item))
      }
    }

    def remove(item: Employment) = Employments(items.filterNot(_ == item))
  }

  sealed trait EmploymentAction extends Action

  case object FetchEmploymentsIfEmpty extends EmploymentAction

  case class UpdateEmployments(potResult: Pot[Employments] = Empty)
      extends PotAction[Employments, UpdateEmployments]
      with EmploymentAction {
    override def next(newResult: Pot[Employments]): UpdateEmployments = UpdateEmployments(newResult)
  }

  case class UpdateEmployment(item: Employment) extends EmploymentAction

  case class CreateEmployment(item: Employment) extends EmploymentAction

  case class DeleteEmployment(item: Employment) extends EmploymentAction

}
