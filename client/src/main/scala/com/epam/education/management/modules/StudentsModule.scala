package com.epam.education.management.modules

import java.time.Instant

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components._
import com.epam.education.management.logger.log
import com.epam.education.management.services.SelectedStudentHandler.{SelectStudent, UpdateSelectedStudent}
import com.epam.education.management.services.SelectedStudentHistoryHandler.StudentHistory
import com.epam.education.management.services.StudentTypesHandler.{FetchStudentTypesIfEmpty, StudentTypes}
import com.epam.education.management.services.StudentsHandler._
import com.epam.education.management.services.StudentsModel
import com.epam.education.management.utils.Instants._
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto.{Student, StudentData, StudentType, StudentWithData}
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scala.collection.breakOut
import scalacss.ScalaCssReact._

object StudentsModule {

  case class Props(proxy: ModelProxy[(StudentsModel, Pot[StudentTypes])])

  case class State()

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) = {
      val (StudentsModel(students, _), types) = props.proxy()
      // dispatch a message to refresh the students, which will cause StudentsStore to fetch students from the server
      val updateStudents: Callback = Callback.when(students.isEmpty)(props.proxy.dispatch(UpdateStudents()))
      val updateTypes: Callback = Callback.when(types.isEmpty)(props.proxy.dispatch(FetchStudentTypesIfEmpty))

      updateStudents >> updateTypes
    }

    def editStudent(item: StudentWithData) =
      $.props >>= (_.proxy.dispatch(SelectStudent(Some(item))))

    val createStudent = {
      val item = StudentWithData(Student(None, "", "", ""), StudentData(-1, -1, -1, Instant.now(), None))
      $.props >>= (_.proxy.dispatch(SelectStudent(Some(item))))
    }

    def studentEdited(item: StudentWithData, newVersion: Boolean, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Student editing cancelled")
      } else {
        val action =
          if (item.student.id.getOrElse(-1) > 0) UpdateStudent(item)
          else CreateStudentWithData(item)

        val createVersion =
          if (newVersion) $.props >>= (_.proxy.dispatch(CreateStudentData(item.studentData)))
          else Callback.empty

        val updateStudent = $.props >>= (_.proxy.dispatch(action))

        Callback.log(s"Student edited: $item") >> updateStudent >> createVersion
      }
      // hide the edit dialog, chain callbacks
      cb >> $.props >>= (_.proxy.dispatch(SelectStudent(None)))
    }

    def render(p: Props, s: State) = {
      val (StudentsModel(potStudents, selectedStudent), types) = p.proxy()
      val typesMap: Pot[Map[Int, StudentType]] =
        types.map(_.items.collect {
          case t@StudentType(Some(id), _) => id -> t
        }(breakOut))

      Panel(
        Panel.Props("Students list"),
        <.div(
          Button(Button.Props(createStudent), Icon.plusSquare, " New")),
        potStudents.renderFailed(ex => s"Error loading: $ex"),
        potStudents.renderPending(_ > 500, _ => "Loading..."),
        potStudents.render { students =>
          StudentList(
            students.items,
            types.map(_.items),
            item => editStudent(item),
            item => p.proxy.dispatch(DeleteStudent(item)))
        },
        typesMap.render { studentTypes =>
          // if the dialog is open, add it to the panel
          selectedStudent.item.map { selectedItem =>
            StudentForm(StudentForm.Props(
              p.proxy.zoom(_ => selectedItem),
              selectedStudent.history,
              studentTypes,
              studentEdited))
          } getOrElse Seq.empty[ReactElement]
        }
      )
    }
  }

  val component = ReactComponentB[Props]("Students")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[(StudentsModel, Pot[StudentTypes])]) = component(Props(proxy))
}

object StudentForm {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(
                    proxy: ModelProxy[StudentWithData],
                    history: Pot[StudentHistory],
                    studentTypes: Map[Int, StudentType],
                    submitHandler: (StudentWithData, Boolean, Boolean) => Callback
                  )

  case class State(cancelled: Boolean = true, newVersion: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      $.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback =
    // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(props.proxy(), state.newVersion, state.cancelled)

    def updateField(f: (Student, String) => Student)(e: ReactEventI) = {
      val text = e.target.value
      $.props >>= { p =>
        val studentWithData = p.proxy()
        p.proxy.dispatch(UpdateSelectedStudent(studentWithData.copy(student = f(studentWithData.student, text))))
      }
    }

    def updateStudentData(sd: StudentData) =
      $.props >>= (p => p.proxy.dispatch(UpdateSelectedStudent(p.proxy().copy(studentData = sd))))

    val createNewVersion =
      $.props >>= { p =>
        val swd = p.proxy()
        val studentData = swd.studentData.copy(version = -1, startDate = Instant.now())
        p.proxy.dispatch(UpdateSelectedStudent(swd.copy(studentData = studentData))) >>
          $.modState(_.copy(newVersion = true))
      }

    def render(p: Props, s: State) = {
      val student = p.proxy().student
      val isNew = student.id.getOrElse(-1) <= 0
      log.debug(s"User is ${if (isNew) "adding" else "editing"} a student or two")
      val headerText = if (isNew) "Add new student" else "Edit student"
      Modal(Modal.Props(
        addStyles = if (!isNew) Seq(bss.modal.`modal-wide`) else Seq(),
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(
          if (!s.newVersion) Button(Button.Props(createNewVersion), "New version")
          else EmptyTag,
          Button(Button.Props(submitForm() >> hide, addStyles = Seq(bss.pullRight)), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.row)(
          <.div(if (!isNew) bss.col.`sm-4` else bss.col.`sm-12`)(
            <.div(bss.formGroup,
              <.label(^.`for` := "id", "Id"),
              <.input.text(bss.formControl, ^.id := "id", ^.value := student.id, ^.disabled := true)),
            <.div(bss.formGroup,
              <.label(^.`for` := "firstName", "Given Name"),
              <.input.text(bss.formControl, ^.id := "firstName", ^.value := student.firstName,
                ^.placeholder := "write given name", ^.onChange ==> updateField((s, t) => s.copy(firstName = t)))),
            <.div(bss.formGroup,
              <.label(^.`for` := "lastName", "Family Name"),
              <.input.text(bss.formControl, ^.id := "lastName", ^.value := student.lastName,
                ^.placeholder := "write family name", ^.onChange ==> updateField((s, t) => s.copy(lastName = t)))),
            <.div(bss.formGroup,
              <.label(^.`for` := "email", "Email"),
              <.input.text(bss.formControl, ^.id := "email", ^.value := student.email,
                ^.placeholder := "write email", ^.onChange ==> updateField((s, t) => s.copy(email = t)))),
            if (isNew || s.newVersion) {
              StudentDataElement(StudentDataElement.Props(
                p.proxy().studentData,
                p.studentTypes,
                updateStudentData
              ))
            } else EmptyTag
          ),
          if (!isNew) {
            <.div(bss.col.`sm-8`)(
              Panel(Panel.Props("History"),
                p.history.renderFailed(ex => s"Error loading: $ex"),
                p.history.renderPending(_ > 500, _ => "Loading..."),
                p.history.render { history =>
                  StudentDataList(history.items, Some(p.proxy().studentData), p.studentTypes)
                }
              )
            )
          } else EmptyTag
        )
      )
    }
  }

  val component = ReactComponentB[Props]("StudentForm")
    .initialState(State())
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}



object StudentDataElement {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props(studentData: StudentData, types: Map[Int, StudentType], update: StudentData => Callback)

  type State = Unit

  class Backend($: BackendScope[Props, State]) {

    def updateIntField(f: (StudentData, Int) => StudentData)(e: ReactEventI) = {
      val text = e.target.value
      try {
        val num = text.toInt
        $.props >>= (p => p.update(f(p.studentData, num)))
      } catch {
        case _: NumberFormatException => Callback.warn("Can't parse int: " + text)
      }
    }

    def updateStartDate(e: ReactEventI) = e.target.value.parseDate match {
      case Some(inst) => $.props >>= (p => p.update(p.studentData.copy(startDate = inst)))
      case None => Callback.log("Can't parse startDate Instant: " + e.target.value)
    }

    def updateEndDate(e: ReactEventI) = {
      $.props >>= (p => p.update(p.studentData.copy(endDate = e.target.value.parseDate)))
    }

    def render(p: Props, s: State) = {


      <.div(
        <.div(bss.formGroup,
          <.label(^.`for` := "studentTypeId", "Student type"),
          <.select(bss.formControl, ^.id := "studentTypeId", ^.value := p.studentData.studentTypeId,
            ^.onChange ==> updateIntField((m, v) => m.copy(studentTypeId = v)),
            for ((id, tpe) <- p.types) yield {
              <.option(^.value := id, tpe.title)
            }
          )
        ),
        <.div(bss.formGroup,
          <.label(^.`for` := "startDate", "Start date"),
          <.input.date(bss.formControl, ^.id := "startDate", ^.placeholder := datePattern,
            ^.value := p.studentData.startDate.localDateFormat, ^.onChange ==> updateStartDate)),
        <.div(bss.formGroup,
          <.label(^.`for` := "endDate", "End date"),
          <.input.date(bss.formControl, ^.id := "endDate", ^.placeholder := datePattern,
            ^.value := p.studentData.endDate.map(_.localDateFormat), ^.onChange ==> updateEndDate))
      )
    }
  }

  val component = ReactComponentB[Props]("StudentDataElement")
    .initialState(())
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}