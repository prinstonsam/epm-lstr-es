package com.epam.education.management.modules

import com.epam.education.management.components.Bootstrap.{Button, Modal, Panel}
import com.epam.education.management.components.{FeedbackList, GlobalStyles, Icon}
import com.epam.education.management.logger.log
import com.epam.education.management.services.EmploymentsHandler.{Employments, FetchEmploymentsIfEmpty}
import com.epam.education.management.services.FeedbackHandler._
import com.epam.education.management.services.IterationsHandler.{FetchIterationsIfEmpty, Iterations}
import com.epam.education.management.services.MentorsHandler.{FetchMentorsIfEmpty, Mentors}
import com.epam.education.management.services.StudentsHandler.{FetchStudentsIfEmpty, StudentsWithData}
import diode.data.Pot
import diode.react.ReactPot._
import diode.react._
import dto._
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scala.collection.breakOut
import scalacss.ScalaCssReact._

object FeedbackModule {

  case class Props(
      proxy: ModelProxy[
        Pot[(Feedbacks, Map[Int, Mentor], Map[Int, Iteration], Map[Int, StudentWithData], Map[Int, Employment])]])

  case class State(selectedItem: Option[Feedback] = None, showFeedbackForm: Boolean = false)

  class Backend($ : BackendScope[Props, State]) {
    def mounted(props: Props) =
      // dispatch a message to refresh the mentors, which will cause MentorsStore to fetch mentors from the server
      Callback.when(props.proxy().isEmpty)(
        props.proxy.dispatch(UpdateFeedbacks()) >>
          props.proxy.dispatch(FetchEmploymentsIfEmpty) >>
          props.proxy.dispatch(FetchMentorsIfEmpty) >>
          props.proxy.dispatch(FetchStudentsIfEmpty) >>
          props.proxy.dispatch(FetchIterationsIfEmpty)
      )

    def editFeedback(item: Option[Feedback]) =
      // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showFeedbackForm = true))

    def feedbackEdited(item: Feedback, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Feedback editing cancelled")
      } else {
        val action =
          if (item.id.getOrElse(-1) > 0) UpdateFeedback(item)
          else CreateFeedback(item)
        Callback.log(s"Feedback edited: $item") >>
          $.props >>= (_.proxy.dispatch(action))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showFeedbackForm = false))
    }

    def render(p: Props, s: State) =
      Panel(
        Panel.Props("Feedback list"),
        <.div(Button(Button.Props(editFeedback(None)), Icon.plusSquare, " New")),
        p.proxy().renderFailed(ex => "Error loading"),
        p.proxy().renderPending(_ > 500, _ => "Loading..."),
        p.proxy().render {
          case (feedbacks, mentors, iterations, students, employments) =>
            FeedbackList(
              feedbacks.items,
              mentors,
              iterations,
              students,
              employments,
              item => editFeedback(Some(item)),
              item => p.proxy.dispatch(DeleteFeedback(item)))
        },
        p.proxy().render {
          case (_, mentors, iterations, students, employments) =>
            // if the dialog is open, add it to the panel
            if (s.showFeedbackForm)
              FeedbackForm(
                FeedbackForm.Props(s.selectedItem, mentors, iterations, students, employments, feedbackEdited))
            else // otherwise add an empty placeholder
              Seq.empty[ReactElement]
        }
      )
  }

  // create the React component for feedback management
  val component = ReactComponentB[Props]("Feedbacks")
    .initialState(State()) // initial state from feedback
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[(Feedbacks, Mentors, Iterations, StudentsWithData, Employments)]]) = {
    def mentorsToMap(ms: Seq[Mentor]): Map[Int, Mentor] =
      ms.collect { case m @ Mentor(Some(id), _, _, _, _) => id -> m }(breakOut)
    def iterationsToMap(ms: Seq[Iteration]): Map[Int, Iteration] =
      ms.collect { case m @ Iteration(Some(id), _, _, _, _) => id -> m }(breakOut)
    def studentToMap(ms: Seq[StudentWithData]): Map[Int, StudentWithData] =
      ms.collect { case m @ StudentWithData(s, _) => s.id.get -> m }(breakOut)

    def employmentsToMap(ms: Seq[Employment]): Map[Int, Employment] =
      ms.collect { case e @ Employment(Some(id), _, _, _) => id -> e }(breakOut)
    component(Props(proxy.zoom(x =>
      x.map {
        case (feedbacks, mentors, iterations, students, employments) =>
          (feedbacks,
           mentorsToMap(mentors.items),
           iterationsToMap(iterations.items),
           studentToMap(students.items),
           employmentsToMap(employments.items))
    })))
  }

  object FeedbackForm {
    // shorthand for styles
    import GlobalStyles.{bootstrapStyles => bss}

    case class Props(
        item: Option[Feedback],
        mentors: Map[Int, Mentor],
        iterations: Map[Int, Iteration],
        students: Map[Int, StudentWithData],
        employments: Map[Int, Employment],
        submitHandler: (Feedback, Boolean) => Callback
    )

    case class State(item: Feedback, cancelled: Boolean = true)

    class Backend(t: BackendScope[Props, State]) {
      def submitForm(): Callback = {
        // mark it as NOT cancelled (which is the default)
        t.modState(s => s.copy(cancelled = false))
      }

      def formClosed(state: State, props: Props): Callback =
        // call parent handler with the new item and whether form was OK or cancelled
        props.submitHandler(state.item, state.cancelled)

      def updateField(f: (Feedback, String) => Feedback)(e: ReactEventI) = {
        val text = e.target.value
        t.modState(s => s.copy(item = f(s.item, text)))
      }

      def updateIntField(f: (Feedback, Int) => Feedback)(e: ReactEventI) = {
        val num = e.target.value
        try {
          val intNum = num.toInt
          t.modState(s => s.copy(item = f(s.item, intNum)))
        } catch {
          case _: NumberFormatException => Callback.warn("Incorrect Int: " + num)
        }
      }

      def render(p: Props, s: State) = {
        log.debug(s"User is ${if (s.item.id.getOrElse(-1) <= 0) "adding" else "editing"} a feedback or two")
        val headerText = if (s.item.id.getOrElse(-1) <= 0) "Add new feedback" else "Edit feedback"
        Modal(Modal.Props(
          // header contains a cancel button (X)
          header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
          // footer has the OK button that submits the form before hiding it
          footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
          // this is called after the modal has been hidden (animation is completed)
          closed = formClosed(s, p)),
          <.div(bss.formGroup,
            <.label(^.`for` := "id", "Id"),
            <.input.text(bss.formControl, ^.id := "id", ^.value := s.item.id, ^.disabled := true)),
          <.div(bss.formGroup,
            <.label(^.`for` := "mentor", "Mentors"),
            <.select(bss.formControl, ^.id := "mentor", ^.value := s.item.mentorId.toString,
              ^.onChange ==> updateIntField((m, i) => m.copy(mentorId = i)),
              for ((id, mentor) <- p.mentors) yield {
                <.option(^.value := mentor.id, mentor.firstName + " " + mentor.lastName)
              }
            )
          ),
          <.div(bss.formGroup,
            <.label(^.`for` := "iteration", "Iterations"),
            <.select(bss.formControl, ^.id := "iteration", ^.value := s.item.iterationId.toString,
              ^.onChange ==> updateIntField((it, i) => it.copy(iterationId = i)),
              for {
                (id, iteration) <- p.iterations
              } yield {
                val emp = p.employments.get(iteration.id.get).map(e => e.title).getOrElse("Error!")
                <.option(^.value := iteration.id, emp + ", " + iteration.startDate + ", " + iteration.endDate)
              }
            )
          ),
          <.div(bss.formGroup,
            <.label(^.`for` := "student", "Students"),
            <.select(bss.formControl, ^.id := "student", ^.value := s.item.studentId.toString,
              ^.onChange ==> updateIntField((s, i) => s.copy(studentId = i)),
              for {
                (id, student) <- p.students
              } yield {
                <.option(^.value := student.student.id, student.student.firstName + " " + student.student.lastName)
              }
            )
          ),
          <.div(bss.formGroup,
            <.label(^.`for` := "content", "Content"),
            <.input.text(bss.formControl, ^.id := "content", ^.value := s.item.content,
              ^.placeholder := "write content", ^.onChange ==> updateField((s, t) => s.copy(content = t))))
        )
      }
    }

    val component = ReactComponentB[Props]("FeedbackForm")
      .initialState_P(p =>
        State(p.item.getOrElse(Feedback(mentorId = 0, iterationId = 0, studentId = 0, content = ""))))
      .renderBackend[Backend]
      .build

    def apply(props: Props) = component(props)
  }

}
