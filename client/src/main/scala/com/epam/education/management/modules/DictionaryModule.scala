package com.epam.education.management.modules

import com.epam.education.management.components.GlobalStyles
import com.epam.education.management.services.SMCCircuit
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.prefix_<^._

import scalacss.ScalaCssReact._

object DictionaryModule {
  // shorthand for styles
  import GlobalStyles.{bootstrapStyles => bss}

  case class Props()

  case class State()

  class Backend($ : BackendScope[Props, State]) {

    def render(p: Props, s: State) = {
      val studentTypesWrapper    = SMCCircuit.connect(_.studentTypes)
      val employmentTypesWrapper = SMCCircuit.connect(_.employmentTypes)
      val resultTypesWrapper     = SMCCircuit.connect(_.resultTypes)

      <.div(bss.row)(
        <.div(bss.col.`sm-4`)(studentTypesWrapper(StudentTypesModule(_))),
        <.div(bss.col.`sm-4`)(employmentTypesWrapper(EmploymentTypesModule(_))),
        <.div(bss.col.`sm-4`)(resultTypesWrapper(ResultTypesModule(_)))
      )
    }
  }

  // create the React component for To Do management
  val component = ReactComponentB[Props]("DictionaryModule")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .build

  def apply() = component(Props())
}
