package dao

import dto.Feedback

import scala.concurrent.Future

trait FeedbackDao {

  def insert(row: Feedback): Future[Feedback]

  def update(row: Feedback): Future[Option[Feedback]]

  def getById(id: Int): Future[Option[Feedback]]

  def findAll(): Future[Seq[Feedback]]

  def findByMentorId(id: Int): Future[Seq[Feedback]]

  def findByStudentId(id: Int): Future[Seq[Feedback]]

  def findByStudentIdIterationId(studentId: Int, IterationId: Int): Future[Seq[Feedback]]

  def findByIterationId(id: Int): Future[Seq[Feedback]]

  def countByIterationId(id: Int): Future[Int]

  def deleteById(id: Int): Future[Option[Feedback]]

  def countByStudentId(studentId: Int): Future[Int]
}
