package dao

import java.time.Instant

import dto.StudentWithData

import scala.concurrent.Future

trait StudentWithDataDao {

  def findAll(date: Option[Instant] = None): Future[Seq[StudentWithData]]

  def getByStudentId(studentId: Int, date: Option[Instant]): Future[Option[StudentWithData]]
  def create(studentWithData: StudentWithData) : Future[StudentWithData]
}
