package dao

import dto.StudentData

import scala.concurrent.Future

trait StudentDataDao{

  def findById(studentId: Int): Future[Seq[StudentData]]

  def insert(studentData: StudentData): Future[StudentData]
}