package dao

import scala.concurrent.Future

import dto.Skill

trait SkillDao {

  def insert(skill: Skill): Future[Skill]

  def update(skill: Skill): Future[Option[Skill]]

  def getById(id: Int): Future[Option[Skill]]

  def findAll(): Future[Seq[Skill]]

  def findByStudentId(studentId: Int): Future[Seq[Skill]]

  def findByMentorId(mentorId: Int): Future[Seq[Skill]]

  def deleteById(id: Int): Future[Option[Skill]]
}
