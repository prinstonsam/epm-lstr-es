package dao

import dto.EmploymentType

import scala.concurrent.Future

trait EmploymentTypeDao {

  def insert(employmentType: EmploymentType): Future[EmploymentType]

  def update(employmentType: EmploymentType): Future[Option[EmploymentType]]

  def getById(id: Int): Future[Option[EmploymentType]]

  def findAll(): Future[Seq[EmploymentType]]

  def deleteById(id: Int): Future[Option[EmploymentType]]
}