package dao

import dto.StudentType

import scala.concurrent.Future


trait StudentTypeDao {

  def insert(row: StudentType): Future[StudentType]

  def update(row: StudentType): Future[Option[StudentType]]

  def getById(id: Int): Future[Option[StudentType]]

  def getByTitle(title: String): Future[Seq[StudentType]]

  def findAll(): Future[Seq[StudentType]]

  def deleteById(id: Int): Future[Option[StudentType]]
}
