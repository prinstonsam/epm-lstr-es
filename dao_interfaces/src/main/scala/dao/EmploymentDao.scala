package dao

import dto.Employment

import scala.concurrent.Future

trait EmploymentDao {

  def insert(employment: Employment): Future[Employment]

  def update(employment: Employment): Future[Option[Employment]]

  def getById(id: Int): Future[Option[Employment]]

  def findByTypeId(employmentTypeId: Int): Future[Seq[Employment]]

  def findAll(): Future[Seq[Employment]]

  def deleteById(id: Int): Future[Option[Employment]]
}
