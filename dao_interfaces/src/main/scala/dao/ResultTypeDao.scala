package dao

import dto.ResultType

import scala.concurrent.Future

trait ResultTypeDao {

  def insert(resultType: ResultType): Future[ResultType]

  def update(resultType: ResultType): Future[Option[ResultType]]

  def getById(id: Int): Future[Option[ResultType]]

  def findAll(): Future[Seq[ResultType]]

  def deleteById(id: Int): Future[Option[ResultType]]
}