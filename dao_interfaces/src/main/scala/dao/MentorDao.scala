package dao

import dto.Mentor

import scala.concurrent.Future

trait MentorDao {

  def insert(row: Mentor): Future[Mentor]

  def update(row: Mentor): Future[Option[Mentor]]

  def getById(id: Int): Future[Option[Mentor]]

  def findAll(): Future[Seq[Mentor]]

  def deleteById(id: Int): Future[Option[Mentor]]
}
