package dao

import dto.User

import scala.concurrent.Future

trait UserDao {

  def insert(user: User): Future[User]

  def update(user: User): Future[Option[User]]

  def getById(id: Int): Future[Option[User]]

  def getByEmail(email: String): Future[Option[User]]

  def findAll(): Future[Seq[User]]

  def deleteByID(id: Int): Future[Option[User]]
}
