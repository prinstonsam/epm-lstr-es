package dao

import dto.StudentIteration

import scala.concurrent.Future

trait StudentIterationDao {

  def insert(row: StudentIteration): Future[StudentIteration]

  def update(row: StudentIteration): Future[Option[StudentIteration]]

  def getById(iterationId: Int, studentId: Int): Future[Option[StudentIteration]]

  def findByStudentId(studentId: Int): Future[Seq[StudentIteration]]

  def findByIterationId(iterationId: Int): Future[Seq[StudentIteration]]

  def findAll(): Future[Seq[StudentIteration]]

  def delete(iterationId:Int, studentId:Int): Future[Option[StudentIteration]]

}
