package dao

import java.time.Instant

import dto.Iteration

import scala.concurrent.Future

trait IterationDao {

  def insert(iteration: Iteration): Future[Iteration]

  def update(iteration: Iteration): Future[Option[Iteration]]

  def getById(id: Int): Future[Option[Iteration]]

  def findAll(startDate: Option[Instant] = None, endDate: Option[Instant] = None): Future[Seq[Iteration]]

  def findByEmploymentId(employmentType: Int,
                         startDate: Option[Instant] = None,
                         endDate: Option[Instant] = None): Future[Seq[Iteration]]

  def countByEmploymentId(employmentId: Int,
                          startDate: Option[Instant] = None,
                          endDate: Option[Instant] = None): Future[Int]

  def findByMentorId(mentorId: Int): Future[Seq[Iteration]]

  def findByStudentId(studentId: Int): Future[Seq[Iteration]]

  def deleteById(id: Int): Future[Option[Iteration]]
}
