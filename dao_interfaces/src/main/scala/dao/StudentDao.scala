package dao

import dto.Student

import scala.concurrent.Future

trait StudentDao {

  def insert(row: Student): Future[Student]

  def update(row: Student): Future[Option[Student]]

  def getById(id: Int): Future[Option[Student]]

  def findAll(): Future[Seq[Student]]

  def findByIterationId(iterationId: Int): Future[Seq[Student]]

  def deleteById(id: Int): Future[Option[Student]]

  def countByIterationId(iterationId: Int): Future[Int]
}
