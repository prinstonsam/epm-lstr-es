package dto

case class User(
    id: Option[Int] = None,
    firstName: String,
    lastName: String,
    email: String,
    password: String
)
