package dto

case class Skill(
    id: Option[Int] = None,
    title: String
)
