package dto

case class StudentType(
    id: Option[Int] = None,
    title: String
)
