package dto

case class StudentIteration(
    studentId: Int,
    iterationId: Int,
    resultTypeId: Int
)
