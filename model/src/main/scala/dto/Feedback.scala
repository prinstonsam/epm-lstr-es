package dto

case class Feedback(
    id: Option[Int] = None,
    mentorId: Int,
    iterationId: Int,
    studentId: Int,
    content: String
)
