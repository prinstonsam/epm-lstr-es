package dto

case class EmploymentType(
    id: Option[Int] = None,
    title: String
)
