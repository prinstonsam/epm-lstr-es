package dto

case class StudentSkill(
    studentId: Int,
    skillId: Int
)
