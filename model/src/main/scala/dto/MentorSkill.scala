package dto

case class MentorSkill (
  mentorId: Int,
  skillId: Int
)
