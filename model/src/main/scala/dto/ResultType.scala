package dto

case class ResultType (
    id: Option[Int] = None,
    title: String
)
