package dto.reports

import dto.typed.EmploymentWithType
import dto.ResultType


case class EmploymentReport(
    employmentWithType: EmploymentWithType,
    iterationsCount: Int,
    studentsCount: Int,
    studentResultsMap: Map[ResultType, Int],
    feedbackCount: Int
)
