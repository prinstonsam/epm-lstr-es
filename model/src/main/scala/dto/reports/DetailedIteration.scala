package dto.reports

import dto.Iteration
import dto.typed.EmploymentWithType

case class DetailedIteration(
    iteration: Iteration,
    employmentWithType: EmploymentWithType
)
