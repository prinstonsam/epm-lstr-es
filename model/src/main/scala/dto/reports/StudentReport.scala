package dto.reports

import dto.Skill
import dto.typed.StudentWithType

case class StudentReport(
    studentWithType: StudentWithType,
    iteration: IndexedSeq[DetailedIteration],
    feedbackCount: Int,
    skills: IndexedSeq[Skill]
)
