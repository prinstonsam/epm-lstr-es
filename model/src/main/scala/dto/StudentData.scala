package dto

import java.time.Instant

case class StudentData(
    studentId: Int,
    version: Int = -1,
    studentTypeId: Int,
    startDate: Instant,
    endDate: Option[Instant]
)
