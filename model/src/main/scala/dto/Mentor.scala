package dto

case class Mentor(
    id: Option[Int] = None,
    primarySkills: String,
    firstName: String,
    lastName: String,
    email: String
)
