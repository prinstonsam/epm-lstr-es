package dto.typed

import dto.{Student, StudentType}

case class StudentWithType(
    student: Student,
    studentType: StudentType
)
