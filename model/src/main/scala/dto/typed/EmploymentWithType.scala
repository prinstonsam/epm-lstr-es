package dto.typed

import dto.{Employment, EmploymentType}

case class EmploymentWithType(
    employment: Employment,
    employmentType: EmploymentType
)
