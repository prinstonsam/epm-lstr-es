package dto

case class Employment(
    id: Option[Int] = None,
    title: String,
    duration: Int,
    employmentTypeId: Int
)
