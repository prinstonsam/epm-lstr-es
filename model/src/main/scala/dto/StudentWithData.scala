package dto

case class StudentWithData(student: Student, studentData: StudentData)
