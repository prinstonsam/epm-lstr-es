package dto

case class Student(
    id: Option[Int] = None,
    firstName: String,
    lastName: String,
    email: String,
    isDeleted: Boolean = false
)
