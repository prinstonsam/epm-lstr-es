package dto

import java.time.Instant

case class Iteration(
    id: Option[Int] = None,
    employmentId: Int,
    mentorId: Int,
    startDate: Instant,
    endDate: Instant
)
