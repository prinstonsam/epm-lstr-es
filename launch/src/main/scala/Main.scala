import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.{Config, ConfigFactory}
import org.flywaydb.core.Flyway
import ru.yandex.qatools.embed.postgresql.config.PostgresConfig
import ru.yandex.qatools.embed.postgresql.{PostgresExecutable, PostgresProcess, PostgresStarter}
import slick.jdbc.PostgresProfile.api.Database
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.Directives._

object Main extends App {

  implicit val system     = ActorSystem()
  implicit val mat        = ActorMaterializer()
  implicit val dispatcher = system.dispatcher

  val db = getDb()

  val routes = new MainModule(db).routeModule.routes

  Http().bindAndHandle(routes.routes ~ assets, "127.0.0.1", 8080)

  private def assets: Route =
    path("") {
      getFromResource("static/index.html")
    } ~
    getFromResourceDirectory("static") ~
    getFromResourceDirectory("less") ~
    pathPrefix("main" / "fonts") {
      getFromResourceDirectory("web-modules/main/webjars/lib/font-awesome/fonts")
    }

  def getDb(): Database = {
    val config = ConfigFactory.load().getConfig("launch.db")

    if (config.getBoolean("standalone")) {
      val (db, _) = startEmbededDb()
      db
    } else {
      connectRemote(config.getConfig("remote"))
    }
  }

  def connectRemote(remoteDbConfig: Config): Database = {
    val postgresUrl      = remoteDbConfig.getString("url")
    val postgresUser     = remoteDbConfig.getString("username")
    val postgresPassword = remoteDbConfig.getString("password")

    if (remoteDbConfig.getBoolean("migrate")) {
      val flyway: Flyway = new Flyway
      flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)

      if (remoteDbConfig.getBoolean("cleanBeforeMigrate")) {
        flyway.clean()
      }

      flyway.migrate()
    }

    Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
  }

  def startEmbededDb(): (Database, PostgresProcess) = {
    val runtime: PostgresStarter[PostgresExecutable, PostgresProcess] =
      PostgresStarter.getDefaultInstance
    val postgresUser     = "launchUser"
    val postgresPassword = "launchPwd"
    val config =
      PostgresConfig.defaultWithDbName("launch", postgresUser, postgresPassword)
    val exec: PostgresExecutable = runtime.prepare(config)
    val process                  = exec.start()
    val postgresUrl =
      s"jdbc:postgresql://${config.net.host}:${config.net.port}/${config.storage.dbName}?currentSchema=tests"

    val flyway: Flyway = new Flyway
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    flyway.migrate()

    val db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    (db, process)
  }
}
