import akka.util.Timeout
import com.softwaremill.macwire._
import config.RouteConfig
import routes._

import scala.concurrent.duration._

@Module
class RouteModule(serviceModule: ServiceModule) {

  lazy val routes = wire[Routes]

  lazy val routeConfig: RouteConfig = new RouteConfig {
    override implicit def routeTimeout: Timeout = Timeout(15.seconds)
  }
  lazy val theEmploymentRoutes       = wire[EmploymentRoutes]
  lazy val theEmploymentTypeRoutes   = wire[EmploymentTypeRoutes]
  lazy val theMentorRoutes           = wire[MentorRoutes]
  lazy val theStudentRoutes          = wire[StudentRoutes]
  lazy val theStudentTypeRoutes      = wire[StudentTypeRoutes]
  lazy val theUserRoutes             = wire[UserRoutes]
  lazy val theIterationRoutes        = wire[IterationRoutes]
  lazy val theFeedbackRoutes         = wire[FeedbackRoutes]
  lazy val theStudentIterationRoutes = wire[StudentIterationRoutes]
  lazy val theResultTypeRoutes       = wire[ResultTypeRoutes]
  lazy val theSkillRoutes            = wire[SkillRoutes]
}
