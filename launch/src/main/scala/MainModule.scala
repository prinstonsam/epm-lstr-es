import com.softwaremill.macwire._
import slick.jdbc.PostgresProfile.api.Database

class MainModule(db: Database) {
  lazy val routeModule: RouteModule     = wire[RouteModule]
  lazy val serviceModule: ServiceModule = wire[ServiceModule]
  lazy val daoModule: DaoModule         = wire[DaoModule]
}
