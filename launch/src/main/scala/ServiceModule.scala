import actors.GuardianActor
import akka.typed.AskPattern._
import akka.typed.{ActorRef, ActorSystem, Behavior, Props}
import akka.util.Timeout
import com.softwaremill.macwire._
import service.protocols._

import scala.concurrent.Await
import scala.concurrent.duration.{FiniteDuration, _}

@Module
class ServiceModule(daoModule: DaoModule) {
  import GuardianProtocol._

  val guardian: Behavior[GuardianProtocol] = wireWith(GuardianActor.guardian _)
  val rootService: ActorRef[GuardianProtocol] =
    ActorSystem("guardian", Props(guardian))

  private val awaitDuration: FiniteDuration = 5.seconds

  implicit val timeout = Timeout(5.seconds)

  val employmentService: ActorRef[EmploymentProtocol] =
    Await.result(rootService ? GetEmploymentActor, awaitDuration)

  val theEmploymentTypeService: ActorRef[EmploymentTypeProtocol] =
    Await.result(rootService ? GetEmploymentTypeActor, awaitDuration)

  val theFeedbackService: ActorRef[FeedbackProtocol] =
    Await.result(rootService ? GetFeedbackActor, awaitDuration)

  val theIterationService: ActorRef[IterationProtocol] =
    Await.result(rootService ? GetIterationActor, awaitDuration)

  val theMentorService: ActorRef[MentorProtocol] =
    Await.result(rootService ? GetMentorActor, awaitDuration)

  val theStudentService: ActorRef[StudentProtocol] =
    Await.result(rootService ? GetStudentActor, awaitDuration)

  val theStudentTypeService: ActorRef[StudentTypeProtocol] =
    Await.result(rootService ? GetStudentTypeActor, awaitDuration)

  val theStudentWithDataService: ActorRef[StudentWithDataProtocol] =
    Await.result(rootService ? GetStudentWithDataActor, awaitDuration)

  val theUserService: ActorRef[UserProtocol] =
    Await.result(rootService ? GetUserActor, awaitDuration)

  val theStudentReportService: ActorRef[StudentReportProtocol] =
    Await.result(rootService ? GetStudentReportActor, awaitDuration)

  val theStudentDataService: ActorRef[StudentDataProtocol] =
    Await.result(rootService ? GetStudentDataActor, awaitDuration)

  val theStudentIterationService: ActorRef[StudentIterationProtocol] =
    Await.result(rootService ? GetStudentIterationActor, awaitDuration)

  val theEmploymentReportService: ActorRef[EmploymentReportProtocol] =
    Await.result(rootService ? GetEmploymentReportActor, awaitDuration)

  val theResultTypeService: ActorRef[ResultTypeProtocol] =
    Await.result(rootService ? GetResultTypeActor, awaitDuration)

  val theSkillService: ActorRef[SkillProtocol] =
    Await.result(rootService ? GetSkillActor, awaitDuration)
}
