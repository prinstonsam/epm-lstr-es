import com.softwaremill.macwire._
import dao._
import dao.impl._
import slick.jdbc.PostgresProfile.api.Database

@Module
class DaoModule(db: Database) {

  lazy val employmentDao: EmploymentDao             = wire[EmploymentDaoImpl]
  lazy val employmentTypeDao: EmploymentTypeDao     = wire[EmploymentTypeDaoImpl]
  lazy val feedbackDao: FeedbackDao                 = wire[FeedbackDaoImpl]
  lazy val iterationDao: IterationDao               = wire[IterationDaoImpl]
  lazy val mentorDao: MentorDao                     = wire[MentorDaoImpl]
  lazy val studentDao: StudentDao                   = wire[StudentDaoImpl]
  lazy val studentIterationDao: StudentIterationDao = wire[StudentIterationDaoImpl]
  lazy val studentTypeDao: StudentTypeDao           = wire[StudentTypeDaoImpl]
  lazy val skillDao: SkillDao                       = wire[SkillDaoImpl]
  lazy val userDao: UserDao                         = wire[UserDaoImpl]
  lazy val studentWithDataDao: StudentWithDataDao   = wire[StudentWithDataDaoImpl]
  lazy val resultTypeDao: ResultTypeDao             = wire[ResultTypeDaoImpl]
  lazy val studentDataDao: StudentDataDao           = wire[StudentDataDaoImpl]
}
