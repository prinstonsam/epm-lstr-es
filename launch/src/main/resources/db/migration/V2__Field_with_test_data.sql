INSERT INTO user_table (first_name, last_name, email, password)
VALUES
  ('Maria', 'Smith', 'maria_smith@epam.com', 'qwerty'),
  ('Jack', 'Phelps', 'jack_phelps@epam.com', 'qwerty'),
  ('Christopher', 'Shepherd', 'christopher@epam.com', 'qwerty'),
  ('Kerry', 'Bradley', 'kerry_bradley@epam.com', 'qwerty'),
  ('Jennifer', 'Ryan', 'jennifer_ryan@epam.com', 'qwerty'),
  ('Jack', 'Long', 'jack_long@epam.com', 'qwerty'),
  ('Robert', 'Martin', 'robert_martin@epam.com', 'qwerty'),
  ('Donald', 'Willis', 'donald_willis@epam.com', 'qwerty'),
  ('Ella', 'Parsons', 'ella_parsons@epam.com', 'qwerty'),
  ('Alaina', 'Watkins', 'alaina_watkins@epam.com', 'qwerty'),
  ('Useless', 'User', 'useless_user@epam.com', 'delete');

INSERT INTO mentor (first_name, last_name, email, primary_skills)
VALUES
  ('Lisa', 'Wilson', 'lisa_wilson@epam.com', 'qwerty'),
  ('Sarah', 'Miller', 'sarah_miller@epam.com', 'qwerty'),
  ('John', 'Elliott', 'john_elliott@epam.com', 'qwerty'),
  ('Florence', 'Patten', 'florence_patten@epam.com', 'qwerty'),
  ('Jamie', 'Meyer', 'jamie_meyer@epam.com', 'qwerty'),
  ('Useless', 'Mentor', 'useless_mentor@epam.com', 'delete');

INSERT INTO student_type (title)
VALUES ('type_one'), ('type_two'), ('type_three'),
  ('type_four'), ('type_five'), ('for delete');

INSERT INTO student (first_name, last_name, email)
VALUES
  ('Dessie', 'Garcia', 'dessie_garcia@epam.com'),
  ('Brandy', 'Mitchell', 'brandy_mitchell@epam.com'),
  ('Cleo', 'Richter', 'cleo_ritcher@epam.com'),
  ('Dustin', 'Devine', 'dustin_devine@epam.com'),
  ('Terrence', 'Shuler', 'terrehce_shuler@epam.com'),
  ('Catherine', 'Reed', 'catherine_reed@epam.com'),
  ('Diane', 'Tang', 'diane_tang@epam.com'),
  ('Pearle', 'McGlynn', 'pearle_mcglynn@epam.com'),
  ('Todd', 'Roll', 'todd_roll@epam.com'),
  ('Robert', 'Studdard', 'robert_studdart@epam.com'),
  ('Useless', 'Student', 'useless_student@epam.com');

INSERT INTO student_data (student_id,version,student_type_id,start_date,end_date)
VALUES
  (1, 1, 1, '2016-01-01 12:12:12+03', '2016-02-01 12:12:12+03'),
  (1, 2, 2, '2016-02-01 12:12:12+03', '9999-01-01 12:12:12+03'),
  (2, 1, 1, '2016-01-01 12:12:12+03', '2016-02-01 12:12:12+03'),
  (2, 2, 2, '2016-02-01 12:12:12+03', '9999-01-01 12:12:12+03'),
  (3, 1, 2, '2016-04-01 12:12:12+03', '9999-01-01 12:12:12+03');

INSERT INTO employment_type (title)
VALUES ('Education'), ('Internship'), ('type_three'),
  ('type_four'), ('type_five'), ('for delete');

INSERT INTO employment (title, duration, employment_type_id)
VALUES
  ('Java Fundamentals', 3, 1),
  ('Scala Fundamentals', 2, 1),
  ('JavaScript Fundamentals', 4, 1),
  ('Work In Factory', 5, 2),
  ('Work In Laboratory', 1, 2),
  ('Delete this employment', 1, 1);

INSERT INTO iteration (employment_id, mentor_id, start_date, end_date)
VALUES
  (1, 1, '2016-01-01 12:12:12+03', '2016-04-01 12:12:12+03'),
  (2, 2, '2016-01-01 12:12:12+03', '2016-03-01 12:12:12+03'),
  (3, 3, '2016-01-01 12:12:12+03', '2016-05-01 12:12:12+03'),
  (4, 4, '2016-01-01 12:12:12+03', '2016-06-01 12:12:12+03'),
  (5, 5, '2016-01-01 12:12:12+03', '2016-02-01 12:12:12+03');

INSERT INTO feedback (mentor_id, iteration_id, student_id, content)
VALUES
  (1, 1, 1, 'Review about first member'),
  (2, 2, 2, 'Review about second member'),
  (3, 3, 3, 'Review about third member'),
  (4, 4, 4, 'Review about fourth member'),
  (5, 1, 5, 'Review about fifth member'),
  (1, 2, 1, 'delete this review');

INSERT INTO result_type (title)
VALUES
  ('entered'), ('in progress'), ('graduated');

INSERT INTO student_iteration (student_id, iteration_id, result_type_id)
VALUES
  (1, 1, 1), (2, 2, 1), (3, 3, 1), (4, 4, 1), (5, 1, 1),
  (6, 2, 1), (7, 3, 1), (8, 4, 1), (9, 1, 1), (10, 2, 1);