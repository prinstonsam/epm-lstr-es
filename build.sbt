import Settings.dependencies
import sbt.Keys._
import sbt.Project.projectToRef

lazy val root =
  (project in file("."))
    .settings(Settings.commonSettings)
    .settings(
      name := Settings.baseName
    )
    .aggregate(rest, service, dao, launch, client, serviceInterfaces)

lazy val dao =
  (project in file("dao"))
    .settings(Settings.commonSettings)
    .settings(
      name := Settings.baseName + "-dao",
      dependencies.jvmTest,
      dependencies.mockito,
      dependencies.dbTest,
      dependencies.db,
      dependencies.slf4j
    )
    .dependsOn(modelJVM, daoInterfaces)

lazy val service =
  (project in file("service"))
    .settings(Settings.commonSettings)
    .settings(
      name := Settings.baseName + "-service",
      dependencies.jvmTest,
      dependencies.mockito,
      dependencies.akka,
      dependencies.akkaTyped,
      dependencies.async,
      serviceInterfacesMvn
    )
    .dependsOn(modelJVM, daoInterfaces)

lazy val rest =
  (project in file("rest"))
    .settings(Settings.commonSettings)
    .settings(
      name := Settings.baseName + "-rest",
      dependencies.jvmTest,
      dependencies.akkaHttp,
      dependencies.akkaHttpTest,
      dependencies.akkaTyped,
      serviceInterfacesMvn
    )
    .dependsOn(modelJVM, utilsJVM)

lazy val serviceInterfacesMvn =
  libraryDependencies +=
    Settings.baseOrganization %% (Settings.baseName + "-service_interfaces") % Settings.baseVersion

lazy val run = settingKey[Unit]("run")

lazy val serviceInterfaces =
  (project in file("service_interfaces"))
    .settings(Settings.commonSettings)
    .settings(
      name := Settings.baseName + "-service_interfaces",
      dependencies.akka,
      dependencies.akkaTyped,
      dependencies.macroPlugin
    )
    .dependsOn(modelJVM, macros % "compile-internal")

lazy val daoInterfaces =
  (project in file("dao_interfaces"))
    .settings(Settings.commonSettings)
    .settings(
      name := Settings.baseName + "-dao_interfaces"
    )
    .dependsOn(modelJVM)

lazy val model =
  (crossProject.crossType(CrossType.Pure) in file("model"))
    .settings(Settings.commonSettings: _*)

lazy val modelJVM = model.jvm.settings(name := Settings.baseName + "-model")

lazy val modelJS = model.js.settings(name := Settings.baseName + "-model-js")

lazy val utils =
  (crossProject.crossType(CrossType.Pure) in file("utils"))
    .settings(Settings.commonSettings: _*)
    .settings(
      dependencies.upickle
    )
    .jsSettings(
      dependencies.`scalajs-java-time`
    )

lazy val utilsJVM = utils.jvm.settings(name := Settings.baseName + "-utils")

lazy val utilsJS = utils.js.settings(name := Settings.baseName + "-utils-js")

lazy val launch =
  (project in file("launch"))
    .settings(Settings.commonSettings)
    .settings(
      name := Settings.baseName + "-launch",
      dependencies.macwire,
      dependencies.db,
      dependencies.config,
      dependencies.embededPg,
      dependencies.assets,
      serviceInterfacesMvn,
      commands += ReleaseCmd,
      pipelineStages := Seq(digest, gzip),
      // compress CSS
      LessKeys.compress in Assets := true
    )
    // Include web-static in resources for static serving
    .settings(unmanagedResourceDirectories in Compile += baseDirectory.value / ".." / "web-static")
    // Remove scala.js target from file watch to prevent compilation loop
    .settings(watchSources := watchSources.value.filterNot(_.getPath.contains("target")))
    // Add web-client sources to file watch
    .settings(watchSources <++= (watchSources in client))
    // Make compile depend on Scala.js fast compilation
    .settings(compile <<= (compile in Compile) dependsOn (fastOptJS in Compile in client))
    // Make re-start depend on Scala.js fast compilation
    .settings(reStart <<= reStart dependsOn (fastOptJS in Compile in client))
    // Make assembly depend on Scala.js full optimization
    .settings(assembly <<= assembly dependsOn (fullOptJS in Compile in client))
    //    .settings(unmanagedResourceDirectories in Compile += (WebKeys.public in Assets).value)
    .settings(unmanagedResourceDirectories in Compile += (WebKeys.webTarget in Assets).value)
    .aggregate(clients.map(projectToRef): _*)
    .dependsOn(dao, service, rest)
    .enablePlugins(SbtWeb)

// Command for building a release
lazy val ReleaseCmd = Command.command("release") { state =>
  Seq(
    "set elideOptions in client := Seq(\"-Xelide-below\", \"WARNING\")",
    "client/clean",
    "client/test",
    "server/clean",
    "server/test",
    "server/dist",
    "set elideOptions in client := Seq()"
  ) ::: state
}

// use eliding to drop some debug code in the production build
lazy val elideOptions = settingKey[Seq[String]]("Set limit for elidable functions")

lazy val staticJsTerget = file("web-static/static/target")

// instantiate the JS project for SBT with some additional settings
lazy val client: Project =
(project in file("client"))
  .settings(Settings.commonSettings)
  .settings(
    name := Settings.baseName + "-client",
    dependencies.scalajs,
    dependencies.js,
    dependencies.`scalajs-java-time`,
    // by default we do development build, no eliding
    elideOptions := Seq(),
    scalacOptions ++= elideOptions.value,
    // RuntimeDOM is needed for tests
    jsDependencies += RuntimeDOM % "test",
    // yes, we want to package JS dependencies
    skip in packageJSDependencies := false,
    // use Scala.js provided launcher code to start the client app
    persistLauncher := true,
    persistLauncher in Test := false,
    relativeSourceMaps := true,
    // use uTest framework for tests
    jsDependencies in Test += RuntimeDOM,
    testFrameworks += new TestFramework("utest.runner.Framework")
  )
  .settings(
    // Specifies where to store the outputs of Scala.js compilation.
    Seq(fullOptJS, fastOptJS, packageJSDependencies, packageScalaJSLauncher, packageMinifiedJSDependencies).map(
      task => crossTarget in(Compile, task) := staticJsTerget): _*
  )
  .settings(
    cleanFiles += staticJsTerget
  )
  .enablePlugins(ScalaJSPlugin)
  .dependsOn(modelJS, utilsJS)

// Client projects (just one in this case)
lazy val clients = Seq(client)

lazy val macros =
  (project in file("macros"))
    .settings(Settings.commonSettings)
    .settings(
      name := Settings.baseName + "-macros",
      dependencies.macroPlugin,
      libraryDependencies <+= scalaVersion("org.scala-lang" % "scala-reflect" % _)
    )
