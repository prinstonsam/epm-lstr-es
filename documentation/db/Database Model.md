
user_table
----------
|    Name    |     Type     |         Description         |
|:----------:|:------------:|:---------------------------:|
| id         | INT          | Primary key, auto-increment |
| first_name | VARCHAR(255) | Contains user's first name  |
| last_name  | VARCHAR(255) | Contains user's last name   |
| email      | VARCHAR(255) | Contains user's email       |
| password   | VARCHAR(255) | Contains user's password    |

mentor
------
|      Name      |     Type     |           Description           |
|:--------------:|:------------:|:-------------------------------:|
| id             | INT          | Primary key, auto-increment     |
| first_name     | VARCHAR(255) | Contains mentor's first name    |
| last_name      | VARCHAR(255) | Contains mentor's last name     |
| email          | VARCHAR(255) | Contains mentor's email         |
| primary_skills | VARCHAR(255) | List of mentor's primary skills |

student
-------
|       Name      |     Type     |          Description                   |
|:---------------:|:------------:|:--------------------------------------:|
| id              | INT          | Primary key, auto-increment            |
| first_name      | VARCHAR(255) | Contains student's first name          |
| last_name       | VARCHAR(255) | Contains student's last name           |
| email           | VARCHAR(255) | Contains student's email               |
| student_type_id | INT          | Foreign key references to student_type |

*Constrains for foreign key - on update cascade, on delete restrict, match simple, not null*

student_type
------------
|       Name      |     Type     |          Description          	 |
|:---------------:|:------------:|:---------------------------------:|
| id              | INT          | Primary key, auto-increment       |
| title           | VARCHAR(255) | Contains title for student's type |

employment
----------
|        Name        |     Type     |             Description                     |
|:------------------:|:------------:|:-------------------------------------------:|
| id                 | INT          | Primary key, auto-increment                 |
| title              | VARCHAR(255) | Contains employment's title                 |
| duration           | INT          | Contains employment's duration in months    |
| employment_type_id | INT          | Foreign key references to employment_type   |

*Constrains for foreign key - on update cascade, on delete restrict, match simple, not null*

employment_type
---------------
|       Name      |     Type     |          Description          	    |
|:---------------:|:------------:|:------------------------------------:|
| id              | INT          | Primary key, auto-increment   	    |
| title           | VARCHAR(255) | Contains title for employment's type |

iteration
---------
|      Name     | Type 	|                  Description                  |
|:-------------:|:-----:|:---------------------------------------------:|
| id            | INT  	| Primary key, auto-increment                   |
| employment_id | INT  	| Foreign key references to employment          |
| mentor_id     | INT  	| Foreign key references to mentor              |
| start_date    | DATE 	| Contains a date as date when iteration starts |
| end_date      | DATE 	| Contains a date as date when iteration ends   |

*Constrains for foreign key - on update cascade, on delete restrict, match simple, not null*

feedback
--------
|     Name     |      Type     |              Description             |
|:------------:|:-------------:|:------------------------------------:|
| id           | INT           | Primary key, auto-increment          |
| mentor_id    | INT           | Foreign key references to mentor     |
| iteration_id | INT           | Foreign key references to iteration  |
| student_id   | DATE          | Foreign key references to student    |
| content      | VARCHAR(8000) | Contains mentor's review for student |

*Constrains for foreign key - on update cascade, on delete restrict, match simple, not null*

student_iteration
----------------
|     Name       | Type |               Description               |
|:--------------:|:----:|:---------------------------------------:|
| student_id     | INT  | Foreign key references to student       |
| iteration_id   | INT  | Foreign key references to iteration     |
| result_type_id | INT  | Foreign key references to result's type |

*Constrains for foreign key - on update cascade, on delete restrict, match simple, not null*
