student_type

POST            /studenttype                                        to create a student type
GET             /studenttype                                        to get all student types
GET             /studenttype/{id}                                   to get a specific student type
PUT             /studenttype/{id}                                   to update a specific student type
DELETE          /studenttype/{id}                                   to delete a specific student type

employment_type

POST            /employmenttype                                     to create a employment type
GET             /employmenttype                                     to get all employment types
GET             /employmenttype/{id}                                to get a specific employment type
PUT             /employmenttype/{id}                                to update a specific employment type
DELETE          /employmenttype/{id}                                to delete a specific employment type

user_table

POST            /user                                               to create a user
GET             /user                                               to get all users
GET             /user/{id}                                          to get a specific user
PUT             /user/{id}                                          to update a specific user
DELETE          /user/{id}                                          to delete a specific user

mentor

POST            /mentor                                             to create a mentor
GET             /mentor                                             to get all mentors
GET             /mentor/{id}                                        to get a specific mentor
PUT             /mentor/{id}                                        to update a specific mentor
DELETE          /mentor/{id}                                        to delete a specific mentor

POST            /mentor/{id}/iteration                              to assign specific mentor to iteration
Get             /mentor/{id}/iteration                              to get all mentor's iterations
Get             /mentor/{id}/iteration/{id}                         to get a specific mentor iteration
PUT             /mentor/{id}/iteration/{id}                         to update a specific mentor iteration
DELETE          /mentor/{id}/iteration/{id}                         to unassign specific mentor from iteration

Get             /mentor/{id}/feedback                               to get all mentors's feedbacks
Get             /mentor/{id}/feedback/{id}                          to get a specific mentors's feedback
PUT             /mentor/{id}/feedback/{id}                          to update mentors's feedbacks
DELETE          /mentor/{id}/feedback/{id}                          to delete a specific mentors's feedback

student

POST            /student                                            to create a student
GET             /student                                            to get all students
GET             /student/{id}                                       to get a specific student
PUT             /student/{id}                                       to update a specific student
DELETE          /student/{id}                                       to delete a specific student

POST            /student/{id}/iteration                             to assign specific student to iteration
Get             /student/{id}/iteration                             to get all student's iterations
Get             /student/{id}/iteration/{id}                        to get a specific student's iteration
PUT             /student/{id}/iteration/{id}                        to update a specific student's iteration
DELETE          /student/{id}/iteration/{id}                        to unassign specific student from iteration
Get             /student/{id}/report                                to get report about student's progress

POST            /student/{id}/iteration/{id}/feedback               to create a feedback to specific student on specific iteration

employment

POST            /employment                                         to create a employment
GET             /employment                                         to get all employments
GET             /employment/{id}                                    to get a specific employment
PUT             /employment/{id}                                    to update a specific employment
DELETE          /employment/{id}                                    to delete a specific employment

POST            /employment/{id}/iteration                          to create an iteration
GET             /employment/{id}/iteration                          to get all iterations
GET             /employment/{id}/iteration/{id}                     to get a specific iteration
PUT             /employment/{id}/iteration/{id}                     to update a specific iteration
DELETE          /employment/{id}/iteration/{id}                     to delete a specific iteration

GET             /employment/{id}/iteration/{id}/student             to get all students on specific iteration
DELETE          /employment/{id}/iteration/{id}/student/{id}        to delete a specific student from specific iteration

iteration

POST            /iteration                                          to create an iteration
GET             /iteration                                          to get all iterations
GET             /iteration/{id}                                     to get a specific iteration
PUT             /iteration/{id}                                     to update a specific iteration
DELETE          /iteration/{id}                                     to delete a specific iteration

feedback

POST            /feedback                                           to create a feedback
GET             /feedback                                           to get all feedbacks
GET             /feedback/{id}                                      to get a specific feedback
PUT             /feedback/{id}                                      to update a specific feedback
DELETE          /feedback/{id}                                      to delete a specific feedback
