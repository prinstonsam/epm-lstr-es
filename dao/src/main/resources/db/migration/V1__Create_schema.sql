CREATE TABLE IF NOT EXISTS user_table (
  id         SERIAL NOT NULL PRIMARY KEY,
  first_name VARCHAR(255),
  last_name  VARCHAR(255),
  email      VARCHAR(255),
  password   VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS mentor (
  id             SERIAL NOT NULL PRIMARY KEY,
  first_name     VARCHAR(255),
  last_name      VARCHAR(255),
  email          VARCHAR(255),
  primary_skills VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS student_type (
  id    SERIAL NOT NULL PRIMARY KEY,
  title VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS skill (
  id    SERIAL NOT NULL PRIMARY KEY,
  title VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS student (
  id              SERIAL NOT NULL PRIMARY KEY,
  first_name      VARCHAR(255),
  last_name       VARCHAR(255),
  email           VARCHAR(255),
  is_deleted      BOOLEAN DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS student_data (
  student_id      INT NOT NULL,
  version         INT NOT NULL,
  student_type_id INT NOT NULL,
  start_date      TIMESTAMPTZ,
  end_date        TIMESTAMPTZ,
  PRIMARY KEY (student_id, version),
  FOREIGN KEY (student_type_id) REFERENCES student_type (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (student_id) REFERENCES student (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS employment_type (
  id    SERIAL NOT NULL PRIMARY KEY,
  title VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS employment (
  id                 SERIAL NOT NULL PRIMARY KEY,
  title              VARCHAR(255),
  duration           INT    NOT NULL,
  employment_type_id INT    NOT NULL,
  FOREIGN KEY (employment_type_id) REFERENCES employment_type (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS iteration (
  id            SERIAL NOT NULL PRIMARY KEY,
  employment_id INT    NOT NULL,
  mentor_id     INT    NOT NULL,
  start_date    TIMESTAMPTZ,
  end_date      TIMESTAMPTZ,
  FOREIGN KEY (employment_id) REFERENCES employment (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (mentor_id) REFERENCES mentor (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS result_type (
  id    SERIAL NOT NULL PRIMARY KEY,
  title VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS student_iteration (
  student_id     INT NOT NULL,
  iteration_id   INT NOT NULL,
  result_type_id INT NOT NULL,
  PRIMARY KEY (student_id, iteration_id),
  FOREIGN KEY (student_id) REFERENCES student (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (iteration_id) REFERENCES iteration (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (result_type_id) REFERENCES result_type (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS feedback (
  id           SERIAL NOT NULL PRIMARY KEY,
  mentor_id    INT    NOT NULL,
  iteration_id INT    NOT NULL,
  student_id   INT    NOT NULL,
  content      VARCHAR(8000),
  FOREIGN KEY (mentor_id) REFERENCES mentor (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (iteration_id) REFERENCES iteration (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (student_id) REFERENCES student (id)
  MATCH SIMPLE ON UPDATE RESTRICT ON DELETE RESTRICT
)