package dao.impl

import dao.StudentTypeDao
import dto.StudentType
import slick.jdbc.PostgresProfile.api._
import tables.StudentTypeTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class StudentTypeDaoImpl(db: Database) extends StudentTypeDao {

  val types = TableQuery[StudentTypeTable]

  override def insert(row: StudentType): Future[StudentType] = {
    val query =
      types returning types.map(_.id) into { (stdt, id) =>
        stdt.copy(id = Some(id))
      }
    db.run(query += row)
  }

  override def update(row: StudentType): Future[Option[StudentType]] = {
    val action = for {
      _   <- types.filter(_.id === row.id).update(row)
      sts <- types.filter(_.id === row.id).result
    } yield sts

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[StudentType]] = {
    db.run(types.filter(_.id === id).result.headOption)
  }

  override def getByTitle(title: String): Future[Seq[StudentType]] = {
    db.run(types.filter(_.title === title).result)
  }

  override def deleteById(id: Int): Future[Option[StudentType]] = {
    val action = for {
      tps <- types.filter(_.id === id).forUpdate.result
      _   <- DBIO.seq(tps.map { tp =>
        types
          .filter(_.id === tp.id)
          .delete}
        : _*)
    } yield tps

    db.run(action.transactionally).map(_.headOption)
  }

  override def findAll(): Future[Seq[StudentType]] = db.run(types.result)
}
