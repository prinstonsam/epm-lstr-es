package dao.impl

import java.time.Instant

import tables.CustomProfile.api._
import dao.IterationDao
import dto.Iteration
import slick.lifted.TableQuery
import tables.{IterationTable, StudentIterationTable}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class IterationDaoImpl(db: Database) extends IterationDao {

  val iterations        = TableQuery[IterationTable]
  val studentIterations = TableQuery[StudentIterationTable]

  override def insert(iteration: Iteration): Future[Iteration] = {
    val query =
      iterations returning iterations.map(_.id) into { (itr, id) =>
        itr.copy(id = Some(id))
      }
    db.run(query += iteration)
  }

  override def update(iteration: Iteration): Future[Option[Iteration]] = {
    val action = for {
      _ <- iterations.filter(_.id === iteration.id).update(iteration)
      i <- iterations.filter(_.id === iteration.id).result
    } yield i
    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[Iteration]] =
    db.run(iterations.filter(_.id === id).result.headOption)

  override def findAll(startDate: Option[Instant] = None,
                       endDate: Option[Instant] = None): Future[Seq[Iteration]] = {

    var query: Query[IterationTable, Iteration, Seq] = iterations

    if (startDate.isDefined) {
      query = query.filter(_.startDate >= startDate.get)
    }

    if (endDate.isDefined) {
      query = query.filter(_.endDate < endDate.get)
    }

    db.run(query.result)
  }

  override def findByEmploymentId(employmentId: Int,
                                  startDate: Option[Instant] = None,
                                  endDate: Option[Instant] = None): Future[Seq[Iteration]] = {
    var query = iterations.filter(_.employmentId === employmentId)

    if (startDate.isDefined) {
      query = query.filter(_.startDate >= startDate.get)
    }

    if (endDate.isDefined) {
      query = query.filter(_.startDate < endDate.get)
    }

    db.run(query.result)
  }

  override def countByEmploymentId(employmentId: Int,
                                   startDate: Option[Instant] = None,
                                   endDate: Option[Instant] = None): Future[Int] = {
    var query = iterations.filter(_.employmentId === employmentId)

    if (startDate.isDefined) {
      query = query.filter(_.startDate >= startDate.get)
    }

    if (endDate.isDefined) {
      query = query.filter(_.startDate < endDate.get)
    }

    db.run(query.length.result)
  }

  override def findByMentorId(mentorId: Int): Future[Seq[Iteration]] =
    db.run(iterations.filter(_.mentorId === mentorId).result)

  override def deleteById(id: Int): Future[Option[Iteration]] = {
    val action = for {
      ir <- iterations.filter(_.id === id).forUpdate.result
      _  <- DBIO.seq(ir.map { i =>
        iterations
          .filter(_.id === i.id)
          .delete}
        : _*)
    } yield ir

    db.run(action.transactionally).map(_.headOption)
  }

  override def findByStudentId(studentId: Int): Future[Seq[Iteration]] = {
    val action = for {
      studIter <- studentIterations if studIter.studentId === studentId
      i        <- iterations if i.id === studIter.iterationId
    } yield i

    db.run(action.sortBy(_.id).result)
  }
}
