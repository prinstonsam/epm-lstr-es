package dao.impl

import dao.EmploymentTypeDao
import dto.EmploymentType
import slick.jdbc.PostgresProfile.api._
import tables.EmploymentTypeTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class EmploymentTypeDaoImpl(db: Database) extends EmploymentTypeDao {

  val employmentTypes = TableQuery[EmploymentTypeTable]

  override def insert(employmentType: EmploymentType): Future[EmploymentType] = {
    val query =
      employmentTypes returning employmentTypes.map(_.id) into { (employmentType, id) =>
        employmentType.copy(id = Some(id))
      }
    db.run(query += employmentType)
  }

  override def update(employmentType: EmploymentType): Future[Option[EmploymentType]] = {
    val action = for {
      _  <- employmentTypes.filter(_.id === employmentType.id).update(employmentType)
      et <- employmentTypes.filter(_.id === employmentType.id).result
    } yield et

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[EmploymentType]] =
    db.run(employmentTypes.filter(_.id === id).result.headOption)

  override def findAll(): Future[Seq[EmploymentType]] =
    db.run(employmentTypes.result)

  override def deleteById(id: Int): Future[Option[EmploymentType]] = {
    val action = for {
      etr <- employmentTypes.filter(_.id === id).forUpdate.result
      _   <- DBIO.seq(etr.map { e =>
        employmentTypes
          .filter(_.id === e.id)
          .delete}
        : _*)
    } yield etr

    db.run(action.transactionally).map(_.headOption)
  }
}
