package dao.impl

import dao.SkillDao
import dto.Skill
import slick.jdbc.PostgresProfile.api._
import tables.SkillTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class SkillDaoImpl(db: Database) extends SkillDao {

  val skills = TableQuery[SkillTable]

  override def insert(skill: Skill): Future[Skill] = {
    val query =
      skills returning skills.map(_.id) into { (skl, id) =>
        skl.copy(id = Some(id))
      }
    db.run(query += skill)
  }

  override def update(skill: Skill): Future[Option[Skill]] = {
    val action = for {
      _   <- skills.filter(_.id === skill.id).update(skill)
      skl <- skills.filter(_.id === skill.id).result
    } yield skl

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[Skill]] =
    db.run(skills.filter(_.id === id).result.headOption)

  override def findAll(): Future[Seq[Skill]] =
    db.run(skills.result)

  override def findByStudentId(studentId: Int): Future[Seq[Skill]] = ???

  override def findByMentorId(mentorId: Int): Future[Seq[Skill]] = ???

  override def deleteById(id: Int): Future[Option[Skill]] = {
    val action = for {
      skill <- skills.filter(_.id === id).forUpdate.result
      _     <- DBIO.seq(skill.map { sk =>
        skills
          .filter(_.id === sk.id)
          .delete}
        : _*)
    } yield skill

    db.run(action.transactionally).map(_.headOption)
  }
}
