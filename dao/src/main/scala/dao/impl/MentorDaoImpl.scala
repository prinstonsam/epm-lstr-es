package dao.impl

import dao.MentorDao
import dto.Mentor
import slick.jdbc.PostgresProfile.api._
import tables.MentorTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class MentorDaoImpl(db: Database) extends MentorDao {

  val mentors = TableQuery[MentorTable]

  override def insert(row: Mentor): Future[Mentor] = {
    val query =
      mentors returning mentors.map(_.id) into { (mtr, id) =>
        mtr.copy(id = Some(id))
      }
    db.run(query += row)
  }

  override def update(row: Mentor): Future[Option[Mentor]] = {
    val action = for {
      _   <- mentors.filter(_.id === row.id).update(row)
      sts <- mentors.filter(_.id === row.id).result
    } yield sts

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[Mentor]] = {
    db.run(mentors.filter(_.id === id).result.headOption)
  }

  override def deleteById(id: Int): Future[Option[Mentor]] = {
    val action = for {
      mts <- mentors.filter(_.id === id).forUpdate.result
      _   <- DBIO.seq(mts.map { mt =>
        mentors
          .filter(_.id === mt.id)
          .delete}
        : _*)
    } yield mts

    db.run(action.transactionally).map(_.headOption)
  }

  override def findAll(): Future[Seq[Mentor]] = db.run(mentors.result)
}
