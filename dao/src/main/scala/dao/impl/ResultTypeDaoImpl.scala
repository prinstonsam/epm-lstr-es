package dao.impl

import dao.ResultTypeDao
import dto.ResultType
import slick.jdbc.PostgresProfile.api._
import tables.ResultTypeTable

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class ResultTypeDaoImpl(db: Database) extends ResultTypeDao {

  val resultTypes = TableQuery[ResultTypeTable]

  override def insert(resultType: ResultType): Future[ResultType] = {
    val query =
      resultTypes returning resultTypes.map(_.id) into { (et, id) =>
        et.copy(id = Some(id))
      }
    db.run(query += resultType)
  }

  override def update(resultType: ResultType): Future[Option[ResultType]] = {
    val action = for {
      _  <- resultTypes.filter(_.id === resultType.id).update(resultType)
      et <- resultTypes.filter(_.id === resultType.id).result
    } yield et

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[ResultType]] =
    db.run(resultTypes.filter(_.id === id).result.headOption)

  override def findAll(): Future[Seq[ResultType]] =
    db.run(resultTypes.result)

  override def deleteById(id: Int): Future[Option[ResultType]] = {
    val action = for {
      etr <- resultTypes.filter(_.id === id).forUpdate.result
      _   <- DBIO.seq(etr.map { e =>
        resultTypes
          .filter(_.id === e.id)
          .delete}
        : _*)
    } yield etr

    db.run(action.transactionally).map(_.headOption)
  }
}
