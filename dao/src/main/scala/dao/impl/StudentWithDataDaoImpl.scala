package dao.impl

import java.time.{Instant, ZonedDateTime}

import dao.StudentWithDataDao
import dto.StudentWithData
import tables.CustomProfile.api._
import tables.{StudentDataTable, StudentTable}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class StudentWithDataDaoImpl(db: Database) extends StudentWithDataDao {

  val student     = TableQuery[StudentTable]
  val studentData = TableQuery[StudentDataTable]

  override def findAll(date: Option[Instant] = None): Future[Seq[StudentWithData]] = {
    val dt = date.getOrElse(Instant.now())
    val join =
      for {
        s  <- student
        sd <- studentData
        if s.id === sd.studentId && sd.startDate <= dt && sd.endDate > dt
      } yield (s, sd)
    val sorted = join.sortBy { case (s, _) => s.id }
    db.run(sorted.result).map(_.map(StudentWithData.tupled))
  }

  override def getByStudentId(studentId: Int, date: Option[Instant] = None): Future[Option[StudentWithData]] = {
    val dt = date.getOrElse(ZonedDateTime.now().toInstant)
    val join =
      for {
        s  <- student
        sd <- studentData
        if s.id === sd.studentId && s.id === studentId && sd.startDate <= dt && sd.endDate > dt
      } yield (s, sd)
    db.run(join.result.headOption).map(_.map(StudentWithData.tupled))
  }

  override def create(studentWithData: StudentWithData): Future[StudentWithData] = {
    val studentDao     = new StudentDaoImpl(db)
    val studentDataDao = new StudentDataDaoImpl(db)
    val insert =
      for {
        s  <- studentDao.insertAction(studentWithData.student)
        sd <- studentDataDao.insertAction(studentWithData.studentData.copy(studentId = s.id.getOrElse(0)))
      } yield (s, sd)

    db.run(insert.transactionally).map(StudentWithData.tupled)
  }
}
