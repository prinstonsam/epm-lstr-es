package dao.impl

import dao.UserDao
import dto.User
import slick.jdbc.PostgresProfile.api._
import tables.UserTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class UserDaoImpl(db: Database) extends UserDao {

  val users: TableQuery[UserTable] = TableQuery[UserTable]

  override def insert(user: User): Future[User] = {
    val query =
      users returning users.map(_.id) into { (usr, id) =>
        usr.copy(id = Some(id))
      }
    db.run(query += user)
  }

  override def update(user: User): Future[Option[User]] = {
    val action = for {
      _ <- users.filter(_.id === user.id).update(user)
      u <- users.filter(_.id === user.id).result
    } yield u

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[User]] =
    db.run(users.filter(_.id === id).result.headOption)

  override def getByEmail(email: String): Future[Option[User]] =
    db.run(users.filter(_.email === email).result.headOption)

  override def findAll(): Future[Seq[User]] = db.run(users.result)

  override def deleteByID(id: Int): Future[Option[User]] = {
    val action = for {
      user <- users.filter(_.id === id).forUpdate.result
      _    <- DBIO.seq(user.map { us =>
        users
          .filter(_.id === us.id)
          .delete}
        : _*)
    } yield user

    db.run(action.transactionally).map(_.headOption)
  }
}
