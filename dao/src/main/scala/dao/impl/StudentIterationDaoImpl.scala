package dao.impl

import dao.StudentIterationDao
import dto.StudentIteration
import slick.jdbc.PostgresProfile.api._
import tables.StudentIterationTable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class StudentIterationDaoImpl(db: Database) extends StudentIterationDao {

  val studentIterations: TableQuery[StudentIterationTable] =
    TableQuery[StudentIterationTable]

  override def insert(row: StudentIteration): Future[StudentIteration] = {
    val action = for {
      _ <- studentIterations += row
      stdIter <- studentIterations
        .filter(r => r.studentId === row.studentId && r.iterationId === row.iterationId)
        .result
    } yield stdIter

    db.run(action.transactionally).map(_.head)
  }

  override def update(row: StudentIteration): Future[Option[StudentIteration]] = {
    val action = for {
      _ <- studentIterations
        .filter(r => r.studentId === row.studentId && r.iterationId === row.iterationId)
        .update(row)
      stdIter <- studentIterations
        .filter(r => r.studentId === row.studentId && r.iterationId === row.iterationId)
        .result
    } yield stdIter

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(iterationId: Int, studentId: Int): Future[Option[StudentIteration]] = {
    val query = studentIterations.filter(r => r.studentId === studentId && r.iterationId === iterationId)
    db.run(query.result.headOption)
  }

  override def findByStudentId(studentId: Int): Future[Seq[StudentIteration]] =
    db.run(studentIterations.filter(_.studentId === studentId).result)

  override def findByIterationId(iterationId: Int): Future[Seq[StudentIteration]] =
    db.run(studentIterations.filter(_.iterationId === iterationId).result)

  override def findAll(): Future[Seq[StudentIteration]] =
    db.run(studentIterations.result)

  override def delete(iterationId: Int, studentId: Int): Future[Option[StudentIteration]] = {
    val action = for {
      stdIter <- studentIterations
        .filter(r => r.studentId === studentId && r.iterationId === iterationId)
        .forUpdate
        .result
      _ <- DBIO.seq(stdIter.map { s =>
        studentIterations.filter(r => r.studentId === studentId && r.iterationId === iterationId).delete
      }: _*)
    } yield stdIter

    db.run(action.transactionally).map(_.headOption)
  }

}
