package dao.impl

import dao.EmploymentDao
import dto.Employment
import slick.jdbc.PostgresProfile.api._
import tables.EmploymentTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class EmploymentDaoImpl(db: Database) extends EmploymentDao {

  val employments = TableQuery[EmploymentTable]

  override def insert(employment: Employment): Future[Employment] = {
    val query =
      employments returning employments.map(_.id) into { (empl, id) =>
        empl.copy(id = Some(id))
      }
    db.run(query += employment)
  }

  override def update(employment: Employment): Future[Option[Employment]] = {
    val action = for {
      _   <- employments.filter(_.id === employment.id).update(employment)
      emp <- employments.filter(_.id === employment.id).result
    } yield emp

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[Employment]] =
    db.run(employments.filter(_.id === id).result.headOption)

  override def findByTypeId(employmentTypeId: Int): Future[Seq[Employment]] =
    db.run(employments.filter(_.employmentTypeId === employmentTypeId).result)

  override def findAll(): Future[Seq[Employment]] =
    db.run(employments.result)

  override def deleteById(id: Int): Future[Option[Employment]] = {
    val action = for {
      er <- employments.filter(_.id === id).forUpdate.result
      _  <- DBIO.seq(er.map { e =>
        employments
          .filter(_.id === e.id)
          .delete}
        : _*)
    } yield er

    db.run(action.transactionally).map(_.headOption)
  }
}
