package dao.impl

import dao.StudentDao
import dto.Student
import slick.dbio.Effect.Write
import slick.sql.FixedSqlAction
import tables.CustomProfile.api._
import tables.{StudentIterationTable, StudentTable}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class StudentDaoImpl(db: Database) extends StudentDao {

  val students = TableQuery[StudentTable]
  val studentIterations = TableQuery[StudentIterationTable]

  def insertAction(row: Student): FixedSqlAction[Student, NoStream, Write] = {
    val query =
      students returning students.map(_.id) into { (std, id) =>
        std.copy(id = Some(id))
      }

    query += row
  }

  override def insert(row: Student): Future[Student] = {
    db.run(insertAction(row))
  }

  override def update(row: Student): Future[Option[Student]] = {
    val action = for {
      _ <- students.filter(_.id === row.id).update(row)
      sts <- students.filter(_.id === row.id).result
    } yield sts

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[Student]] = {
    db.run(students.filter(_.id === id).result.headOption)
  }

  override def deleteById(id: Int): Future[Option[Student]] = {
    val action = for {
      _ <- students.filter(_.id === id).map(_.isDeleted).update(true)
      student <- students.filter(_.id === id).result
    } yield student

    db.run(action.transactionally).map(_.headOption)
  }

  override def findAll(): Future[Seq[Student]] = db.run(students.result)

  override def findByIterationId(iterationId: Int): Future[Seq[Student]] = {
    val action = for {
      studIter <- studentIterations if studIter.iterationId === iterationId
      student <- students if student.id === studIter.studentId
    } yield student

    db.run(action.sortBy(_.id).result)
  }

  override def countByIterationId(iterationId: Int): Future[Int] = {
    val action = for {
      studIter <- studentIterations if studIter.iterationId === iterationId
      s <- students if s.id === studIter.studentId
    } yield s

    db.run(action.length.result)
  }
}
