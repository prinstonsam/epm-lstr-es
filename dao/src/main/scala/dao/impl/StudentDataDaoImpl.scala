package dao.impl

import java.time.Instant

import dao.StudentDataDao
import dto.StudentData
import slick.dbio.DBIOAction
import slick.dbio.Effect.{Read, Write}
import tables.CustomProfile.api._
import tables.{StudentDataTable, StudentTable}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class StudentDataDaoImpl(db: Database) extends StudentDataDao {

  val student = TableQuery[StudentTable]
  val studentData = TableQuery[StudentDataTable]

  override def findById(studentId: Int): Future[Seq[StudentData]] = {
    db.run(studentData.filter(_.studentId === studentId).result)
  }

  def insertAction(row: StudentData): DBIOAction[StudentData, NoStream, Read with Write with Write] = {
    require(row.endDate.isEmpty || row.endDate.get.isAfter(row.startDate))

    val latestsQuery = for {
    // TODO check if this statement generates correct sql.
    // Current sql: select ... from (select ... for update) left outer join ...
      (latest, moreLatest) <- studentData.forUpdate joinLeft studentData on { (l, ml) =>
        l.studentId === ml.studentId && l.version < ml.version
      }
      if latest.studentId === row.studentId && moreLatest.isEmpty
    } yield latest

    def updateEndDate(latest: StudentData, endDate: Instant) = {
      studentData
        .filter(r => r.studentId === latest.studentId && r.version === latest.version)
        .map(_.endDate)
        .update(endDate)
    }

    def insertAfterLatest(latest: StudentData) = {
      require(latest.startDate.isBefore(row.startDate), "Inconsistent dates")
      for {
        _ <- updateEndDate(latest, row.startDate)
        rowWithVersion = row.copy(version = latest.version + 1)
        _ <- studentData += rowWithVersion
      } yield rowWithVersion
    }

    def insertNew = {
      val rowWithVersion = row.copy(version = 1)
      for {
        _ <- studentData += rowWithVersion
      } yield rowWithVersion
    }

    for {
      latests <- latestsQuery.result
      rowWithVersion <- latests.headOption.fold(insertNew)(insertAfterLatest)
    } yield rowWithVersion
  }

  override def insert(row: StudentData): Future[StudentData] = {
    //TODO exception typization
    db.run(insertAction(row).transactionally)
  }
}
