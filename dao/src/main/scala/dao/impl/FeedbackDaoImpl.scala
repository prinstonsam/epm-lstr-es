package dao.impl

import dao.FeedbackDao
import dto.Feedback
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import tables.FeedbackTable

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class FeedbackDaoImpl(db: Database) extends FeedbackDao {

  val feedback = TableQuery[FeedbackTable]

  override def insert(row: Feedback): Future[Feedback] = {
    val query =
      feedback returning feedback.map(_.id) into { (fdk, id) =>
        fdk.copy(id = Some(id))
      }
    db.run(query += row)
  }

  override def update(row: Feedback): Future[Option[Feedback]] = {
    val action = for {
      _   <- feedback.filter(_.id === row.id).update(row)
      sts <- feedback.filter(_.id === row.id).result
    } yield sts

    db.run(action.transactionally).map(_.headOption)
  }

  override def getById(id: Int): Future[Option[Feedback]] = {
    db.run(feedback.filter(_.id === id).result.headOption)
  }

  override def findByMentorId(id: Int): Future[Seq[Feedback]] = {
    db.run(feedback.filter(_.mentorId === id).result)
  }

  override def findByStudentId(id: Int): Future[Seq[Feedback]] = {
    db.run(feedback.filter(_.studentId === id).result)
  }

  override def findByStudentIdIterationId(studentId: Int, iterationId: Int): Future[Seq[Feedback]] = {

    var query: Query[FeedbackTable, Feedback, Seq] = feedback

    query = query.filter(_.studentId === studentId)
    query = query.filter(_.iterationId === iterationId)

    db.run(query.result)
  }

  override def findByIterationId(id: Int): Future[Seq[Feedback]] = {
    db.run(feedback.filter(_.iterationId === id).result)
  }

  override def countByIterationId(id: Int): Future[Int] = {
    db.run(feedback.filter(_.iterationId === id).length.result)
  }

  override def deleteById(id: Int): Future[Option[Feedback]] = {
    val action = for {
      fbs <- feedback.filter(_.id === id).forUpdate.result
      _   <- DBIO.seq(fbs.map { fb =>
        feedback
          .filter(_.id === fb.id)
          .delete}
        : _*)
    } yield fbs

    db.run(action.transactionally).map(_.headOption)
  }

  override def findAll(): Future[Seq[Feedback]] = db.run(feedback.result)

  override def countByStudentId(studentId: Int): Future[Int] =
    db.run(feedback.filter(_.studentId === studentId).length.result)
}
