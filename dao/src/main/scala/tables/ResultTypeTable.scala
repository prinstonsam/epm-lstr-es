package tables

import dto.ResultType
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape

class ResultTypeTable(tag: Tag) extends Table[ResultType](tag, "result_type") {

  def id: Rep[Int]       = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title: Rep[String] = column[String]("title")

  override def * : ProvenShape[ResultType] =
    (id.?, title) <> (ResultType.tupled, ResultType.unapply)
}
