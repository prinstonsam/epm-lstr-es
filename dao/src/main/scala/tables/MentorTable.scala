package tables

import dto.Mentor
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape

class MentorTable(tag: Tag) extends Table[Mentor](tag, "mentor") {

  def id: Rep[Int]               = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def firstName: Rep[String]     = column[String]("first_name")
  def lastName: Rep[String]      = column[String]("last_name")
  def email: Rep[String]         = column[String]("email")
  def primarySkills: Rep[String] = column[String]("primary_skills")

  override def * : ProvenShape[Mentor] =
    (id.?, primarySkills, firstName, lastName, email) <> (Mentor.tupled, Mentor.unapply)
}
