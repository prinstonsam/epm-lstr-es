package tables

import dto.Skill
import slick.lifted.ProvenShape
import tables.CustomProfile.api._

class SkillTable(tag: Tag) extends Table[Skill](tag, "skill") {

  def id: Rep[Int]       = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title: Rep[String] = column[String]("title")

  override def * : ProvenShape[Skill] = (id.?, title) <> (Skill.tupled, Skill.unapply)
}
