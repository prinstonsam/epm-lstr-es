package tables

import dto.EmploymentType
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape

class EmploymentTypeTable(tag: Tag) extends Table[EmploymentType](tag, "employment_type") {

  def id: Rep[Int]       = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title: Rep[String] = column[String]("title")

  override def * : ProvenShape[EmploymentType] =
    (id.?, title) <> (EmploymentType.tupled, EmploymentType.unapply)
}
