package tables

import dto.{Iteration, ResultType, Student, StudentIteration}
import tables.CustomProfile.api._
import slick.lifted.{ForeignKeyQuery, PrimaryKey, ProvenShape}

class StudentIterationTable(tag: Tag) extends Table[StudentIteration](tag, "student_iteration") {

  def studentId: Rep[Int]   = column[Int]("student_id")
  def iterationId: Rep[Int] = column[Int]("iteration_id")
  def resultId: Rep[Int]    = column[Int]("result_type_id")

  def primaryKey: PrimaryKey =
    primaryKey("student_iteration_pkey", (studentId, iterationId))

  def fkStudentId: ForeignKeyQuery[StudentTable, Student] =
    foreignKey("student_iteration_member_id_fkey", studentId, TableQuery[StudentTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  def fkIterationId: ForeignKeyQuery[IterationTable, Iteration] =
    foreignKey("student_iteration_iteration_id_fkey", iterationId, TableQuery[IterationTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  def fkResultId: ForeignKeyQuery[ResultTypeTable, ResultType] =
    foreignKey("student_iteration_result_type_id_fkey", iterationId, TableQuery[ResultTypeTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  override def * : ProvenShape[StudentIteration] =
    (studentId, iterationId, resultId) <> (StudentIteration.tupled, StudentIteration.unapply)
}
