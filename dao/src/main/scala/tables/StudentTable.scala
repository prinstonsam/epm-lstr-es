package tables

import dto.Student
import tables.CustomProfile.api._
import slick.lifted.ProvenShape

class StudentTable(tag: Tag) extends Table[Student](tag, "student") {

  def id: Rep[Int]            = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def firstName: Rep[String]  = column[String]("first_name")
  def lastName: Rep[String]   = column[String]("last_name")
  def email: Rep[String]      = column[String]("email")
  def isDeleted: Rep[Boolean] = column[Boolean]("is_deleted")

  override def * : ProvenShape[Student] =
    (id.?, firstName, lastName, email, isDeleted) <> (Student.tupled, Student.unapply)
}
