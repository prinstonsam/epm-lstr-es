package tables

import java.sql.{PreparedStatement, ResultSet}
import java.time.format.{DateTimeFormatter, DateTimeFormatterBuilder}
import java.time.temporal.ChronoField
import java.time.{Instant, ZoneId, ZonedDateTime}

import slick.ast.FieldSymbol
import slick.jdbc.PostgresProfile

import scala.reflect.ClassTag

object CustomProfile extends PostgresProfile {

  class GenericJdbcType[T](val sqlTypeName: String,
                           fnFromString: (String => T),
                           fnToString: (T => String) = ((r: T) => r.toString),
                           val sqlType: Int = java.sql.Types.OTHER,
                           zero: T = null.asInstanceOf[T],
                           override val hasLiteralForm: Boolean = false)
                          (implicit override val classTag: ClassTag[T])
      extends DriverJdbcType[T] {

    override def sqlTypeName(sym: Option[FieldSymbol]): String = sqlTypeName

    override def getValue(r: ResultSet, idx: Int): T = {
      val value = r.getString(idx)
      if (r.wasNull) zero else fnFromString(value)
    }

    override def setValue(v: T, p: PreparedStatement, idx: Int): Unit =
      p.setObject(idx, toStr(v), java.sql.Types.OTHER)

    override def updateValue(v: T, r: ResultSet, idx: Int): Unit =
      r.updateObject(idx, toStr(v), java.sql.Types.OTHER)

    override def valueToSQLLiteral(v: T) =
      if (v == null) "NULL" else s"'${fnToString(v)}'"

    private def toStr(v: T) =
      if (v == null) null else fnToString(v)
  }

  val bpTzDateTimeFormatter =
    new DateTimeFormatterBuilder()
      .append(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
      .optionalStart()
      .appendFraction(ChronoField.NANO_OF_SECOND, 0, 6, true)
      .optionalEnd()
      .appendOffset("+HH:mm", "+00")
      .toFormatter()

  override val api = new API {
    implicit val zonedDateTimeColumnType =
      new GenericJdbcType[ZonedDateTime](
        "timestamptz",
        ZonedDateTime.parse(_, bpTzDateTimeFormatter),
        _.format(bpTzDateTimeFormatter))

    implicit val instantColumnType =
      MappedColumnType.base[Instant, ZonedDateTime](
        ZonedDateTime.ofInstant(_, ZoneId.systemDefault()),
        _.toInstant
      )
  }
}
