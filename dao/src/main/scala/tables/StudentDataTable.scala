package tables

import java.time.Instant
import tables.CustomProfile.api._

import dto._

import slick.lifted.{ForeignKeyQuery, PrimaryKey, ProvenShape}

class StudentDataTable(tag: Tag) extends Table[StudentData](tag, "student_data") {
  def studentId: Rep[Int]     = column[Int]("student_id")
  def version: Rep[Int]       = column[Int]("version")
  def studentTypeId: Rep[Int] = column[Int]("student_type_id")
  def startDate: Rep[Instant] = column[Instant]("start_date")
  def endDate: Rep[Instant]   = column[Instant]("end_date")

  def primaryKey: PrimaryKey = primaryKey("student_iteration_pkey", (studentId, version))

  def fkStudentId: ForeignKeyQuery[StudentTable, Student] =
    foreignKey("student_id_fkey", studentId, TableQuery[StudentTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  def fkStudentTypeId: ForeignKeyQuery[StudentTypeTable, StudentType] =
    foreignKey("student_type_fkey", studentTypeId, TableQuery[StudentTypeTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  override def * : ProvenShape[StudentData] =
    (studentId, version, studentTypeId, startDate, endDate) <>
      (StudentDataVO.tupledSql, StudentDataVO.unapplySql)
}

object StudentDataTable {
  val infiniteDate: Instant = Instant.parse("9999-01-01T00:00:00.00Z")
}

case class StudentDataVO(
    studentId: Int,
    version: Int,
    studentTypeId: Int,
    startDate: Instant,
    endDate: Instant
) {
  def toStudentData: StudentData = {
    StudentData(
      studentId = studentId,
      version = version,
      studentTypeId = studentTypeId,
      startDate = startDate,
      endDate = Some(endDate).filter(_.isBefore(StudentDataTable.infiniteDate))
    )
  }
}

object StudentDataVO {
  val tupledSql = (StudentDataVO.apply _).tupled.andThen(_.toStudentData)
  def unapplySql(studentData: StudentData) =
    studentData match {
      case StudentData(studentId, version, studentTypeId, startDate, endDate) =>
        Some(
          (
            studentId,
            version,
            studentTypeId,
            startDate,
            endDate.getOrElse(StudentDataTable.infiniteDate)
          )
        )
    }
}
