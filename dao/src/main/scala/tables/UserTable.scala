package tables

import dto.User
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape

class UserTable(tag: Tag) extends Table[User](tag, "user_table") {

  def id: Rep[Int]           = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def firstName: Rep[String] = column[String]("first_name")
  def lastName: Rep[String]  = column[String]("last_name")
  def email: Rep[String]     = column[String]("email")
  def password: Rep[String]  = column[String]("password")

  override def * : ProvenShape[User] =
    (id.?, firstName, lastName, email, password) <> (User.tupled, User.unapply)
}
