package tables

import dto.{Employment, EmploymentType}
import slick.jdbc.PostgresProfile.api._
import slick.lifted.{ForeignKeyQuery, ProvenShape}

class EmploymentTable(tag: Tag) extends Table[Employment](tag, "employment") {

  def id: Rep[Int]               = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title: Rep[String]         = column[String]("title")
  def duration: Rep[Int]         = column[Int]("duration")
  def employmentTypeId: Rep[Int] = column[Int]("employment_type_id")

  def fkEmploymentTypeId: ForeignKeyQuery[EmploymentTypeTable, EmploymentType] =
    foreignKey("employment_employment_type_id_fkey", employmentTypeId, TableQuery[EmploymentTypeTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  override def * : ProvenShape[Employment] =
    (id.?, title, duration, employmentTypeId) <> (Employment.tupled, Employment.unapply)
}
