package tables

import dto.StudentType
import slick.jdbc.PostgresProfile.api._
import slick.lifted.ProvenShape

class StudentTypeTable(tag: Tag) extends Table[StudentType](tag, "student_type") {

  def id: Rep[Int]       = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title: Rep[String] = column[String]("title")

  override def * : ProvenShape[StudentType] =
    (id.?, title) <> (StudentType.tupled, StudentType.unapply)
}
