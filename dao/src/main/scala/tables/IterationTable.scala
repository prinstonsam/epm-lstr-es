package tables

import java.time.Instant

import dto.{Employment, Iteration, Mentor}
import slick.lifted.{ForeignKeyQuery, ProvenShape}
import tables.CustomProfile.api._

class IterationTable(tag: Tag) extends Table[Iteration](tag, "iteration") {

  def id: Rep[Int]            = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def employmentId: Rep[Int]  = column[Int]("employment_id")
  def mentorId: Rep[Int]      = column[Int]("mentor_id")
  def startDate: Rep[Instant] = column[Instant]("start_date")
  def endDate: Rep[Instant]   = column[Instant]("end_date")

  def fkMentorId: ForeignKeyQuery[MentorTable, Mentor] =
    foreignKey("iterations_mentor_id_fkey", mentorId, TableQuery[MentorTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  def fkEmploymentId: ForeignKeyQuery[EmploymentTable, Employment] =
    foreignKey("iterations_employment_id_fkey", employmentId, TableQuery[EmploymentTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  override def * : ProvenShape[Iteration] =
    (id.?, employmentId, mentorId, startDate, endDate) <> (Iteration.tupled, Iteration.unapply)
}
