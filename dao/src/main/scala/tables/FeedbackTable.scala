package tables

import dto.{Feedback, Iteration, Mentor, Student}
import slick.jdbc.PostgresProfile.api._
import slick.lifted.{ForeignKeyQuery, ProvenShape}

class FeedbackTable(tag: Tag) extends Table[Feedback](tag, "feedback") {

  def id: Rep[Int]          = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def mentorId: Rep[Int]    = column[Int]("mentor_id")
  def iterationId: Rep[Int] = column[Int]("iteration_id")
  def studentId: Rep[Int]   = column[Int]("student_id")
  def content: Rep[String]  = column[String]("content")

  def fkIterationId: ForeignKeyQuery[IterationTable, Iteration] =
    foreignKey("feedback_iteration_id_fkey", iterationId, TableQuery[IterationTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  def fkMentorId: ForeignKeyQuery[MentorTable, Mentor] =
    foreignKey("feedback_mentor_id_fkey", mentorId, TableQuery[MentorTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  def fkStudentId: ForeignKeyQuery[StudentTable, Student] =
    foreignKey("feedback_student_id_fkey", studentId, TableQuery[StudentTable])(
      _.id,
      onUpdate = ForeignKeyAction.Restrict,
      onDelete = ForeignKeyAction.Restrict)

  override def * : ProvenShape[Feedback] =
    (id.?, mentorId, iterationId, studentId, content) <> (Feedback.tupled, Feedback.unapply)
}
