package dao.impl

import java.time.{ZoneId, ZonedDateTime}

import dto.Iteration
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, DoNotDiscover, FunSpec}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class IterationDaoImplTest
    extends FunSpec
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with WithPostgres
    with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway                 = new Flyway()
  var iterationDao: IterationDaoImpl = _
  var db: Database                   = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    iterationDao = new IterationDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  val iterById1 =
    Iteration(
      Some(1),
      1,
      1,
      ZonedDateTime.of(2016, 1, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant,
      ZonedDateTime.of(2016, 4, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant)

  val iterForInsert =
    Iteration(
      None,
      1,
      1,
      ZonedDateTime.of(2016, 4, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant,
      ZonedDateTime.of(2016, 10, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant)

  val iterInsertRes =
    Iteration(
      Some(6),
      1,
      1,
      ZonedDateTime.of(2016, 4, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant,
      ZonedDateTime.of(2016, 10, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant)

  val iterForUpdate =
    Iteration(
      Some(5),
      2,
      2,
      ZonedDateTime.of(2016, 4, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant,
      ZonedDateTime.of(2016, 10, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant)

  val iterForUpdateErr =
    Iteration(
      Some(-1),
      2,
      2,
      ZonedDateTime.of(2016, 4, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant,
      ZonedDateTime.of(2016, 10, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant)

  val iterForDelete =
    Iteration(
      Some(5),
      5,
      5,
      ZonedDateTime.of(2016, 1, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant,
      ZonedDateTime.of(2016, 2, 1, 12, 12, 12, 0, ZoneId.systemDefault()).toInstant)

  describe("IterationDao") {
    it("should return iteration by id") {
      val getAction = iterationDao.getById(1)
      assert(getAction.futureValue.get == iterById1)
    }

    it("should return iterations by employment_id") {
      val getAction = iterationDao.findByEmploymentId(1)
      assert(getAction.futureValue.head == iterById1)
    }

    it("should return count of iterations by employment_id") {
      val count = iterationDao.countByEmploymentId(1)
      assert(count.futureValue == 1)
    }

    it("should return iterations by mentor_id") {
      val getAction = iterationDao.findByMentorId(1)
      assert(getAction.futureValue.head == iterById1)
    }

    it("should return all iterations") {
      val getAction = iterationDao.findAll()
      assert(getAction.futureValue.length == 5)
    }

    it("should return on getting an empty result") {
      val getAction1 = iterationDao.getById(-1)
      val getAction2 = iterationDao.findByMentorId(-1)
      val getAction3 = iterationDao.findByEmploymentId(-1)
      assert(getAction1.futureValue.isEmpty)
      assert(getAction2.futureValue.isEmpty)
      assert(getAction3.futureValue.isEmpty)
    }

    it("should return iterations by studentId") {
      val getAction = iterationDao.findByStudentId(1)
      assert(getAction.futureValue.length == 1)
    }

    it("should return an empty collection by non-existing studentId") {
      val getAction = iterationDao.findByStudentId(-1)
      assert(getAction.futureValue.isEmpty)
    }

    it("should insert iteration and return it") {
      val insertAction = iterationDao.insert(iterForInsert)
      assert(insertAction.futureValue == iterInsertRes)
    }

    it("should update iteration") {
      val updateAction = iterationDao.update(iterForUpdate)
      assert(updateAction.futureValue.get == iterForUpdate)
    }

    it("should return on update an empty result") {
      val deleteAction = iterationDao.update(iterForUpdateErr)
      assert(deleteAction.futureValue.isEmpty)
    }

    it("should delete iteration by id") {
      val deleteAction = iterationDao.deleteById(5)
      assert(deleteAction.futureValue.get == iterForDelete)
    }

    ignore("should be error on delete used iteration") {
      val deleteAction = iterationDao.deleteById(1)
      deleteAction.futureValue
    }

    it("should return on delete an empty result") {
      val deleteAction = iterationDao.deleteById(-1)
      assert(deleteAction.futureValue.isEmpty)
    }
  }
}
