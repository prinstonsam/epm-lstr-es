package dao.impl

import dto.Feedback
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, DoNotDiscover, FunSuite}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class FeedbackDaoImplTest
    extends FunSuite
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with WithPostgres
    with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway               = new Flyway
  var feedbackDao: FeedbackDaoImpl = _
  var db: Database                 = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    feedbackDao = new FeedbackDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  test("find feedback by id") {
    val value: Option[Feedback] = feedbackDao.getById(1).futureValue
    val feedback                = value.get
    assert(feedback.id == Option(1))
  }

  test("find feedback by mentor id") {
    val value: Seq[Feedback] = feedbackDao.findByMentorId(1).futureValue
    assert(value.length == 2)

  }
  test("find feedback by student id") {
    val value: Seq[Feedback] = feedbackDao.findByStudentId(1).futureValue
    assert(value.length == 2)
  }

  test("find feedback by iteration id") {
    val value: Seq[Feedback] = feedbackDao.findByIterationId(1).futureValue
    assert(value.length == 2)
  }

  test("find count of feedback by iteration id") {
    val value: Int = feedbackDao.countByIterationId(1).futureValue
    assert(value == 2)
  }

  test("insert feedback") {
    val feedback: Feedback = Feedback(Some(-1), 1, 1, 1, "some text")
    val result: Feedback   = feedbackDao.insert(feedback).futureValue
    assert(feedback.copy(id = result.id).equals(result))
  }

  test("delete feedback") {
    feedbackDao.deleteById(1).futureValue
    assert(feedbackDao.getById(1).futureValue.isEmpty)
  }

  test("update feedback") {
    val feedback: Feedback = feedbackDao.getById(1).futureValue.get
    val newInfo: Feedback  = Feedback(feedback.id, 1, 1, 1, "sdjfk")
    feedbackDao.update(newInfo).futureValue
    assert(feedbackDao.getById(1).futureValue.get != feedback)
  }

  test("find all feedback") {
    assert(feedbackDao.findAll.futureValue.length == 6)
  }

  test("trying to get feedback by incorrect id") {
    assert(feedbackDao.getById(-1).futureValue.isEmpty)
  }

  test("trying to delete feedback by incorrect id") {
    assert(feedbackDao.deleteById(-1).futureValue.isEmpty)
  }

  test("trying to update nonexisting feedback") {
    assert(feedbackDao.update(Feedback(Some(-1), 1, 1, 1, "email")).futureValue.isEmpty)
  }

  test("get count of row by Student ID") {
    assert(feedbackDao.countByStudentId(1).futureValue == 2)
  }
}
