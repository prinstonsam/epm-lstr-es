package dao.impl

import dto.User
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterEach, DoNotDiscover, FunSpec}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class UserDaoImplTest extends FunSpec with BeforeAndAfterEach with WithPostgres with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway       = new Flyway()
  var userDao: UserDaoImpl = _
  var db: Database         = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    userDao = new UserDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  val userById1     = User(Some(1), "Maria", "Smith", "maria_smith@epam.com", "qwerty")
  val userForUpdate = User(Some(1), "Maria", "Hagans", "maria_hagans@epam.com", "qwerty")
  val userForError  = User(None, "Maria", "Hagans", "maria_hagans@epam.com", "qwerty")
  val userForInsert = User(None, "John", "Snow", "john_snow@epam.com", "qwerty")
  val userInsertRes = User(Some(12), "John", "Snow", "john_snow@epam.com", "qwerty")
  val userForDelete = User(Some(11), "Useless", "User", "useless_user@epam.com", "delete")

  describe("UserDao") {
    it("should return user by id") {
      val getAction = userDao.getById(1)
      assert(getAction.futureValue.get == userById1)
    }

    it("should return user by email") {
      val getAction = userDao.getByEmail("maria_smith@epam.com")
      assert(getAction.futureValue.get == userById1)
    }

    it("should return all users") {
      val getAction = userDao.findAll()
      assert(getAction.futureValue.length == 11)
    }

    it("should return on receive an empty result") {
      val getAction = userDao.getById(-1)
      assert(getAction.futureValue.isEmpty)
    }

    it("should insert user and return him") {
      val insertAction = userDao.insert(userForInsert)
      assert(insertAction.futureValue == userInsertRes)
    }

    it("should update user") {
      val updateAction = userDao.update(userForUpdate)
      assert(updateAction.futureValue.get == userForUpdate)
    }

    it("should return on update an empty result") {
      val updateAction = userDao.update(userForError)
      assert(updateAction.futureValue.isEmpty)
    }

    it("should delete user by id") {
      val deleteAction = userDao.deleteByID(11)
      assert(deleteAction.futureValue.get == userForDelete)
      assert(userDao.findAll().futureValue.length == 10)
    }

    it("should return on delete an empty result") {
      val deleteAction = userDao.deleteByID(-1)
      assert(deleteAction.futureValue.isEmpty)
    }
  }
}
