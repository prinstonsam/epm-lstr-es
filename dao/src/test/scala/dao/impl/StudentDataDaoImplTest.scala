package dao.impl

import java.time.{Instant, OffsetDateTime}

import dto.StudentData
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest._
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._
@DoNotDiscover
class StudentDataDaoImplTest
    extends FunSpec
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with WithPostgres
    with ScalaFutures {
  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway                             = new Flyway
  var studentDataDao: StudentDataDaoImpl         = _
  var studentWithDataDao: StudentWithDataDaoImpl = _
  var db: Database                               = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    studentDataDao = new StudentDataDaoImpl(db)
    studentWithDataDao = new StudentWithDataDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }
  describe("StudentDataDao") {
    it("should select student data by id") {
      val expected = Seq(
        StudentData(
          2,
          1,
          1,
          OffsetDateTime.parse("2016-01-01T12:12:12+03:00").toInstant,
          Some(OffsetDateTime.parse("2016-02-01T12:12:12+03:00").toInstant)),
        StudentData(
          2,
          2,
          2,
          OffsetDateTime.parse("2016-02-01T12:12:12+03:00").toInstant,
          None)
      )

      assert(studentDataDao.findById(2).futureValue === expected)
    }

    it("should insert studentData") {
      val studentData: StudentData =
        StudentData(1, studentTypeId = 1, startDate = Instant.parse("2016-05-05T00:00:00.00Z"), endDate = None)
      val result = studentDataDao.insert(studentData).futureValue
      assert(result === studentData.copy(version = 3))
    }
  }
}
