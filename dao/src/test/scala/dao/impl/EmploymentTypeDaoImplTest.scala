package dao.impl

import dto.EmploymentType
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, DoNotDiscover, FunSpec}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class EmploymentTypeDaoImplTest
    extends FunSpec
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with WithPostgres
    with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway                           = new Flyway()
  var employmentTypeDao: EmploymentTypeDaoImpl = _
  var db: Database                             = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    employmentTypeDao = new EmploymentTypeDaoImpl(db)
  }

  override def afterAll(): Unit = {
    super.afterAll()
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  val emplTypeById1     = EmploymentType(Some(1), "Education")
  val emplTypeForUpdate = EmploymentType(Some(5), "Updated type")
  val emplTypeForInsert = EmploymentType(None, "Inserted type")
  val emplTypeInsertRes = EmploymentType(Some(7), "Inserted type")
  val emplTypeUpdateErr = EmploymentType(Some(-1), "Updated type")
  val emplTypeDeleteRes = EmploymentType(Some(6), "for delete")

  describe("EmploymentTypeDao") {
    it("should return employment with id=1") {
      val getAction = employmentTypeDao.getById(1)
      assert(getAction.futureValue.get == emplTypeById1)
    }

    it("should return all employments from table") {
      val getAction = employmentTypeDao.findAll()
      assert(getAction.futureValue.length == 6)
    }

    it("should return on receive an empty result") {
      val getAction = employmentTypeDao.getById(-1)
      assert(getAction.futureValue.isEmpty)
    }

    it("should insert a new employment and return it") {
      val insertAction = employmentTypeDao.insert(emplTypeForInsert)
      assert(insertAction.futureValue == emplTypeInsertRes)
      assert(employmentTypeDao.findAll().futureValue.length == 7)
    }

    it("should update employment") {
      val updateAction = employmentTypeDao.update(emplTypeForUpdate)
      assert(updateAction.futureValue.get == emplTypeForUpdate)
    }

    it("should return on update an empty result") {
      val updateAction = employmentTypeDao.update(emplTypeUpdateErr)
      assert(updateAction.futureValue.isEmpty)
    }

    it("should delete employment with id") {
      val deleteAction = employmentTypeDao.deleteById(6)
      assert(deleteAction.futureValue.get == emplTypeDeleteRes)
      assert(employmentTypeDao.findAll().futureValue.length == 5)
    }

    ignore("should throw exception on delete used employment type") {
      employmentTypeDao.deleteById(1).futureValue
      employmentTypeDao.findAll().futureValue.foreach(println)
    }

    it("should return on delete an empty result") {
      val deleteAction = employmentTypeDao.deleteById(-1)
      assert(deleteAction.futureValue.isEmpty)
    }
  }
}
