package dao.impl

import dto.Skill
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, DoNotDiscover, FunSuite}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class SkillDaoImplTest
    extends FunSuite
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with WithPostgres
    with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway                     = new Flyway
  var skillDao: SkillDaoImpl = _
  var db: Database                       = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    skillDao = new SkillDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  test("find skill by id") {
    val value: Option[Skill] = skillDao.getById(1).futureValue
    val skill                = value.get
    assert(skill.id == Option(1))
  }


  test("insert skill") {
    val skill: Skill = Skill(Some(-1), "scala")
    val result: Skill      = skillDao.insert(skill).futureValue
    assert(skill.copy(id = result.id).equals(result))
  }

  test("insert skill with id = None") {
    val skill: Skill = Skill(None, "scala")
    val result: Skill      = skillDao.insert(skill).futureValue
    assert(skill.copy(id = result.id).equals(result))
  }

  test("delete skill") {
    skillDao.deleteById(6).futureValue
    assert(skillDao.getById(6).futureValue.isEmpty)
  }

  test("update skill") {
    val skill: Skill = skillDao.getById(1).futureValue.get
    val newInfo: Skill     = Skill(skill.id, "title")
    skillDao.update(newInfo).futureValue
    assert(!skillDao.getById(1).futureValue.get.equals(skill))
  }

  test("find all skills") {
    assert(skillDao.findAll().futureValue.length == 6)
  }

  test("trying to get skill by incorrect id") {
    assert(skillDao.getById(-1).futureValue.isEmpty)
  }

  test("trying to delete skill by incorrect id") {
    assert(skillDao.deleteById(-1).futureValue.isEmpty)
  }

  test("trying to update nonexisting skill") {
    assert(skillDao.update(Skill(Some(-1), "title")).futureValue.isEmpty)
  }
}
