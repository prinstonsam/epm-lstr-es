package dao.impl

import dto.ResultType
import org.flywaydb.core.Flyway
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

class ResultTypeDaoImplTest
  extends FunSuite
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with WithPostgres
  with ScalaFutures {

    implicit val defaultPatience =
      PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

    var flyway: Flyway                     = new Flyway
    var resultTypeDao: ResultTypeDaoImpl = _
    var db: Database                       = _

    override def beforeAll(): Unit = {
      super.beforeAll()
      flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
      db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
      resultTypeDao = new ResultTypeDaoImpl(db)
    }

    override def beforeEach(): Unit = {
      flyway.migrate()
      super.beforeEach()
    }

    override def afterEach(): Unit = {
      flyway.clean()
      super.afterEach()
    }

    test("find result type by id") {
      val value: Option[ResultType] = resultTypeDao.getById(1).futureValue
      val resultType                = value.get
      assert(resultType.id == Option(1))
    }

    test("insert result type") {
      val resultType: ResultType = ResultType(Some(-1), "graduated")
      val result: ResultType      = resultTypeDao.insert(resultType).futureValue
      assert(resultType.copy(id = result.id).equals(result))
    }

    test("delete result type") {
      resultTypeDao.deleteById(2).futureValue
      assert(resultTypeDao.getById(2).futureValue.isEmpty)
    }

    test("update result type") {
      val resultType: ResultType = resultTypeDao.getById(1).futureValue.get
      val newInfo: ResultType     = ResultType(resultType.id, "inprogress")
      resultTypeDao.update(newInfo).futureValue
      assert(!resultTypeDao.getById(1).futureValue.get.equals(resultType))
    }

    test("find all result types") {
      assert(resultTypeDao.findAll().futureValue.length == 3)
    }

    test("trying to get result type by incorrect id") {
      assert(resultTypeDao.getById(-1).futureValue.isEmpty)
    }

    test("trying to delete result type by incorrect id") {
      assert(resultTypeDao.deleteById(-1).futureValue.isEmpty)
    }

    test("trying to update nonexisting resultType") {
      assert(resultTypeDao.update(ResultType(Some(-1), "title")).futureValue.isEmpty)
    }

}
