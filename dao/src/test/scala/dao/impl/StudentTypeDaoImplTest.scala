package dao.impl

import dto.StudentType
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, DoNotDiscover, FunSuite}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class StudentTypeDaoImplTest
    extends FunSuite
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with WithPostgres
    with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway                     = new Flyway
  var studentTypeDao: StudentTypeDaoImpl = _
  var db: Database                       = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    studentTypeDao = new StudentTypeDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  test("find student type by id") {
    val value: Option[StudentType] = studentTypeDao.getById(1).futureValue
    val studentType                = value.get
    assert(studentType.id == Option(1))
  }

  test("find student type by title") {
    val value: Seq[StudentType] = studentTypeDao.getByTitle("type_one").futureValue
    assert(value.length == 1)
  }

  test("insert student type") {
    val studentType: StudentType = StudentType(Some(-1), "junior")
    val result: StudentType      = studentTypeDao.insert(studentType).futureValue
    assert(studentType.copy(id = result.id).equals(result))
  }

  test("insert student type with id = None") {
    val studentType: StudentType = StudentType(None, "junior")
    val result: StudentType      = studentTypeDao.insert(studentType).futureValue
    assert(studentType.copy(id = result.id).equals(result))
  }

  test("delete student type") {
    studentTypeDao.deleteById(6).futureValue
    assert(studentTypeDao.getById(6).futureValue.isEmpty)
  }

  test("update student type") {
    val studentType: StudentType = studentTypeDao.getById(1).futureValue.get
    val newInfo: StudentType     = StudentType(studentType.id, "title")
    studentTypeDao.update(newInfo).futureValue
    assert(!studentTypeDao.getById(1).futureValue.get.equals(studentType))
  }

  test("find all student types") {
    assert(studentTypeDao.findAll().futureValue.length == 6)
  }

  test("trying to get student type by incorrect id") {
    assert(studentTypeDao.getById(-1).futureValue.isEmpty)
  }

  test("trying to delete student type by incorrect id") {
    assert(studentTypeDao.deleteById(-1).futureValue.isEmpty)
  }

  test("trying to update nonexisting studentType") {
    assert(studentTypeDao.update(StudentType(Some(-1), "title")).futureValue.isEmpty)
  }
}
