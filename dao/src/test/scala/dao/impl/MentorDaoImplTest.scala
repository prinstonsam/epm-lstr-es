package dao.impl

import dto.Mentor
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, DoNotDiscover, FunSuite}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class MentorDaoImplTest
    extends FunSuite
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with WithPostgres
    with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway           = new Flyway
  var mentorDao: MentorDaoImpl = _
  var db: Database             = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    mentorDao = new MentorDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  test("find mentor by id") {
    val value: Option[Mentor] = mentorDao.getById(1).futureValue
    val mentor                = value.get
    assert(mentor.id == Option(1))
  }

  test("insert mentor") {
    val mentor: Mentor = Mentor(Option(-1), "scala", "John", "Smith", "email")
    val result: Mentor = mentorDao.insert(mentor).futureValue
    assert(mentor.copy(id = result.id).equals(result))
  }

  test("delete mentor") {
    mentorDao.deleteById(6).futureValue
    assert(mentorDao.getById(6).futureValue.isEmpty)
  }

  test("update mentor") {
    val mentor: Mentor  = mentorDao.getById(1).futureValue.get
    val newInfo: Mentor = Mentor(mentor.id, "scala", "John", "Smith", "email")
    mentorDao.update(newInfo).futureValue
    assert(mentorDao.getById(1).futureValue.get != mentor)
  }

  test("find all mentors") {
    assert(mentorDao.findAll.futureValue.length == 6)
  }

  test("trying to get mentor by incorrect id") {
    assert(mentorDao.getById(-1).futureValue.isEmpty)
  }

  test("trying to delete mentor by incorrect id") {
    assert(mentorDao.deleteById(-1).futureValue.isEmpty)
  }

  test("trying to update nonexisting mentor") {
    assert(mentorDao.update(Mentor(Some(-1), "scala", "John", "Smith", "email")).futureValue.isEmpty)
  }
}
