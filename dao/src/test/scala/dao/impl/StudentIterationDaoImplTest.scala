package dao.impl

import dto.StudentIteration
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterEach, DoNotDiscover, FunSpec}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class StudentIterationDaoImplTest extends FunSpec with BeforeAndAfterEach with WithPostgres with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway                          = new Flyway
  var studentIterDao: StudentIterationDaoImpl = _
  var db: Database                            = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    studentIterDao = new StudentIterationDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  val stdIterForInsert = StudentIteration(1, 2, 1)
  val stdIter          = StudentIteration(1, 1, 1)
  val stdIterForDelete = StudentIteration(10, 2, 1)
  val stdIterForUpdate = StudentIteration(10, 2, 1)
  val stdIterUpdError  = StudentIteration(-1, -1, 1)

  describe("StudentIterationDao") {

    it("should return all student iterations") {
      val getAction = studentIterDao.findAll()
      assert(getAction.futureValue.length == 10)
    }

    it("should return student iteration by primary key") {
      val getAction = studentIterDao.getById(stdIter.iterationId, stdIter.studentId)
      assert(getAction.futureValue.get == stdIter)
    }

    it("should return on receiving empty result") {
      val getAction = studentIterDao.getById(-1, -1)
      assert(getAction.futureValue.isEmpty)
    }

    it("should return all student iterations with studentId") {
      val getAction = studentIterDao.findByIterationId(1)
      assert(getAction.futureValue.length == 3)
    }

    it("should return empty collection for non-existing studentId") {
      val getAction = studentIterDao.findByStudentId(-1)
      assert(getAction.futureValue.isEmpty)
    }

    it("should return all student iterations with iterationId") {
      val getAction = studentIterDao.findByIterationId(1)
      assert(getAction.futureValue.length == 3)
    }

    it("should return empty collection for non-existing iterationId") {
      val getAction = studentIterDao.findByIterationId(-1)
      assert(getAction.futureValue.isEmpty)
    }

    it("should insert a new student iteration") {
      val insertAction = studentIterDao.insert(stdIterForInsert)
      assert(insertAction.futureValue == stdIterForInsert)
    }

    it("should update student iteration") {
      val updateAction = studentIterDao.update(stdIterForUpdate)
      assert(updateAction.futureValue.get == stdIterForUpdate)
    }

    it("should return on update empty result") {
      val updateAction = studentIterDao.update(stdIterUpdError)
      assert(updateAction.futureValue.isEmpty)
    }

    it("should delete student iteration") {
      val deleteAction = studentIterDao.delete(stdIterForDelete.iterationId, stdIterForDelete.studentId)
      assert(deleteAction.futureValue.get == stdIterForDelete)
    }

    it("should return on delete empty result") {
      val deleteAction = studentIterDao.delete(-1, -1)
      assert(deleteAction.futureValue.isEmpty)
    }
  }
}
