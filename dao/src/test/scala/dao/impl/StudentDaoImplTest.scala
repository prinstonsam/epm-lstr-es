package dao.impl

import dto.Student
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest._
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class StudentDaoImplTest
    extends FunSpec
    with BeforeAndAfterAll
    with BeforeAndAfterEach
    with WithPostgres
    with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway             = new Flyway
  var studentDao: StudentDaoImpl = _
  var db: Database               = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    studentDao = new StudentDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  describe("StudentDao") {
    it("find student by id") {
      val value: Option[Student] = studentDao.getById(1).futureValue
      val student                = value.get
      assert(student.id == Option(1))
    }

    it("insert student") {
      val student: Student = Student(Option(1), "John", "Smith", "email")
      val result: Student  = studentDao.insert(student).futureValue
      assert(student.copy(id = result.id).equals(result))
    }

    it("insert student without id") {
      val student: Student = Student(None, "John", "Smith", "email")
      val result: Student  = studentDao.insert(student).futureValue
      assert(student.copy(id = result.id) === result)
      assert(result.id.nonEmpty)
    }

    it("delete student") {
      studentDao.deleteById(11).futureValue
      assert(studentDao.getById(11).futureValue.get.isDeleted === true)
    }

    it("update student") {
      val student: Student = studentDao.getById(1).futureValue.get
      val newInfo: Student = Student(student.id, "John", "Smith", "email")
      studentDao.update(newInfo).futureValue
      assert(studentDao.getById(1).futureValue.get != student)
    }

    it("find all students") {
      assert(studentDao.findAll().futureValue.length == 11)
    }

    it("should return collection by Iteration Id") {
      val getAction = studentDao.findByIterationId(1)
      assert(getAction.futureValue.length == 3)
    }

    it("should return length of collection by Iteration Id") {
      val getAction = studentDao.countByIterationId(1)
      assert(getAction.futureValue == 3)
    }

    it("should return empty collection by non-existing Iteration Id") {
      val getAction = studentDao.findByIterationId(-1)
      assert(getAction.futureValue.isEmpty)
    }

    it("trying to get student by incorrect id") {
      assert(studentDao.getById(-1).futureValue.isEmpty)
    }

    it("trying to delete student by incorrect id") {
      assert(studentDao.deleteById(-1).futureValue.isEmpty)
    }

    it("trying to update non-existing student") {
      assert(studentDao.update(Student(Some(-1), "Non", "Existing", "email")).futureValue.isEmpty)
    }
  }
}
