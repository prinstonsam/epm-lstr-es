package dao.impl

import java.time.Instant

import dto.{Student, StudentData, StudentWithData}
import org.flywaydb.core.Flyway
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._
@DoNotDiscover
class StudentWithDataDaoImplTest extends FunSpec
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with WithPostgres
  with ScalaFutures  {
  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway             = new Flyway
  var studentDao: StudentDaoImpl = _
  var studentDataDao: StudentDataDaoImpl = _
  var studentWithDataDao: StudentWithDataDaoImpl = _
  var db: Database               = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    studentDao = new StudentDaoImpl(db)
    studentDataDao = new StudentDataDaoImpl(db)
    studentWithDataDao = new StudentWithDataDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }
  describe("StudentWithDataDao") {
    it("find all students") {
      assert(studentWithDataDao.findAll().futureValue.length == 3)
    }
    it("find all students by date") {
      assert(studentWithDataDao.findAll(Option(Instant.parse("2015-01-01T09:12:12Z"))).futureValue.isEmpty)
    }
    it("get student by id") {
      val value: Option[StudentWithData] = studentWithDataDao.getByStudentId(1).futureValue
      val student = value.get
      assert(student.studentData == StudentData(1, 2, 2, Instant.parse("2016-02-01T09:12:12Z"), None))
    }
    it("get student by id with specified date") {
      val value: Option[StudentWithData] = studentWithDataDao.getByStudentId(1,
        Option(Instant.parse("2016-01-01T09:12:12Z"))).futureValue
      val student = value.get
      assert(student.studentData == StudentData(1, 1, 1,
        Instant.parse("2016-01-01T09:12:12Z"),
        Option(Instant.parse("2016-02-01T09:12:12Z"))))
    }
    it("trying to get student by incorrect id") {
      assert(studentWithDataDao.getByStudentId(-1).futureValue.isEmpty)
    }
    it("trying to get student by incorrect date") {
      assert(studentWithDataDao.getByStudentId(1,Option(Instant.parse("2015-02-01T09:12:12Z"))).futureValue.isEmpty)
    }
    it("should create student") {
      val student = Student(None, "firstName1", "lastName1", "email1")
      val studentData =
        StudentData(
          -1,
          studentTypeId = 1,
          startDate = Instant.ofEpochSecond(1472805209L, 874000000L),
          endDate = Some(Instant.ofEpochSecond(4632843600L, 0L)))

      val result = studentWithDataDao.create(StudentWithData(student, studentData)).futureValue
      val id = result.student.id.get
      assert(result === StudentWithData(student.copy(id = Some(id)), studentData.copy(studentId = id, version = 1)))
    }
  }
}
