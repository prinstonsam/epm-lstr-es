package dao.impl

import org.scalatest.{BeforeAndAfterAll, Suites}
import posgresql.WithPostgres

/** Tests runner for tests that requires PostgreSql server.
  *
  * @author Simon Popugaev
  */
class AllTests
    extends Suites(
      new EmploymentDaoImplTest,
      new EmploymentTypeDaoImplTest,
      new FeedbackDaoImplTest,
      new IterationDaoImplTest,
      new MentorDaoImplTest,
      new StudentDaoImplTest,
      new StudentIterationDaoImplTest,
      new StudentTypeDaoImplTest,
      new UserDaoImplTest,
      new ResultTypeDaoImplTest,
      new StudentDataDaoImplTest,
      new StudentWithDataDaoImplTest)
    with BeforeAndAfterAll
    with WithPostgres {

  override def beforeAll(): Unit = {
    super.beforeAll()

    nestedSuites.foreach {
      case suite: WithPostgres =>
        suite.startedExternally = true
        suite.config = config
        suite.postgresUrl = postgresUrl
        suite.postgresDbName = postgresDbName
        suite.postgresUser = postgresUser
        suite.postgresPassword = postgresPassword
    }
  }

}
