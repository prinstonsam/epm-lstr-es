package dao.impl

import dto.Employment
import org.flywaydb.core.Flyway
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, DoNotDiscover, FunSpec}
import posgresql.WithPostgres
import slick.jdbc.PostgresProfile.api._

@DoNotDiscover // See AllTests class
class EmploymentDaoImplTest
    extends FunSpec
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with WithPostgres
    with ScalaFutures {

  implicit val defaultPatience =
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(50, Millis))

  var flyway: Flyway                   = new Flyway
  var employmentDao: EmploymentDaoImpl = _
  var db: Database                     = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    flyway.setDataSource(postgresUrl, postgresUser, postgresPassword)
    db = Database.forURL(postgresUrl, user = postgresUser, password = postgresPassword)
    employmentDao = new EmploymentDaoImpl(db)
  }

  override def beforeEach(): Unit = {
    flyway.migrate()
    super.beforeEach()
  }

  override def afterEach(): Unit = {
    flyway.clean()
    super.afterEach()
  }

  val emplById1       = Employment(Some(1), "Java Fundamentals", 3, 1)
  val emplForInsert   = Employment(None, "Employment", 10, 1)
  val emplInsertRes   = Employment(Some(7), "Employment", 10, 1)
  val emplForUpdate   = Employment(Some(1), "Scala Advanced", 3, 1)
  val emplForUpdError = Employment(None, "Scala Basics", 10, 1)
  val emplForDelete   = Employment(Some(6), "Delete this employment", 1, 1)

  describe("EmploymentDao") {
    it("should return employment by id") {
      val getAction = employmentDao.getById(1)
      assert(getAction.futureValue.get == emplById1)
    }

    it("should return all employments") {
      val getAction = employmentDao.findAll()
      assert(getAction.futureValue.length == 6)
    }

    it("should return on receive an empty result") {
      val getAction = employmentDao.getById(-1)
      assert(getAction.futureValue.isEmpty)
    }

    it("should insert a new employment and return it") {
      val insertAction = employmentDao.insert(emplForInsert)
      assert(insertAction.futureValue == emplInsertRes)
      assert(employmentDao.findAll().futureValue.length == 7)
    }

    it("should update employment") {
      val updateAction = employmentDao.update(emplForUpdate)
      assert(updateAction.futureValue.get == emplForUpdate)
    }

    it("should return on update an empty result") {
      val updateAction = employmentDao.update(emplForUpdError)
      assert(updateAction.futureValue.isEmpty)
    }

    it("should delete employment by id") {
      val deleteAction = employmentDao.deleteById(6)
      assert(deleteAction.futureValue.get == emplForDelete)
      assert(employmentDao.findAll().futureValue.length == 5)
    }

    it("should return on delete an empty result") {
      val deleteAction = employmentDao.deleteById(-1)
      assert(deleteAction.futureValue.isEmpty)
    }
  }
}
