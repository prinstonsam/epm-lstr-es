package posgresql

import org.scalatest.{BeforeAndAfterAll, Suite}
import ru.yandex.qatools.embed.postgresql.config.PostgresConfig
import ru.yandex.qatools.embed.postgresql.{PostgresExecutable, PostgresProcess, PostgresStarter}

trait WithPostgres extends BeforeAndAfterAll { this: Suite =>

  var startedExternally        = false
  var process: PostgresProcess = _
  var config: PostgresConfig   = _
  var postgresUrl: String      = _
  var postgresDbName: String   = "postgres_test"
  var postgresUser: String     = "testUser"
  var postgresPassword: String = "pwd"

  private def startPostgres() = {
    val runtime: PostgresStarter[PostgresExecutable, PostgresProcess] =
      PostgresStarter.getDefaultInstance
    config = PostgresConfig.defaultWithDbName(postgresDbName, postgresUser, postgresPassword)
    val exec: PostgresExecutable = runtime.prepare(config)
    process = exec.start()
    postgresUrl =
      s"jdbc:postgresql://${config.net.host}:${config.net.port}/${config.storage.dbName}?currentSchema=tests"
  }

  private def stopPostgres() = {
    process.stop()
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
    if (!startedExternally) {
      startPostgres()
    }
  }

  override def afterAll() = {
    if (!startedExternally) {
      stopPostgres()
    }
    super.afterAll()
  }
}
