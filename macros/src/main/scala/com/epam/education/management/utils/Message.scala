package com.epam.education.management.utils

import scala.annotation.{StaticAnnotation, compileTimeOnly}
import scala.language.experimental.macros
import scala.reflect.macros.whitebox

/**
  * Add method `m (params*)(lastParam)` method co companion object of class `class ClassName(params*, param)`
  **/
@compileTimeOnly("enable macro paradise to expand macro annotations")
class message extends StaticAnnotation {
  def macroTransform(annottees: Any*): Any = macro message.impl
}

object message {

  def impl(c: whitebox.Context)(annottees: c.Expr[Any]*): c.Expr[Any] = {
    import c.universe._

    val inputs: List[Tree] = annottees.map(_.tree)(collection.breakOut)

    inputs match {
      case (target @ q"""${cMods: Modifiers} class ${cName: TypeName}[..$tparams] $ctorMods(
            ..${params: List[ValDef]}, ${lastParam: ValDef}
          ) extends { ..$cearlydefns } with ..$parents { $self => ..$stats }""") :: tail =>
        val companion =
          tail match {
            case (md @ ModuleDef(_, mName, _)) :: Nil
              if cName.decodedName.toString == mName.decodedName.toString => md
            case Nil =>
              val flags =
                Seq(Flag.PRIVATE, Flag.PROTECTED, Flag.LOCAL)
                  .filter(cMods.hasFlag)
                  .foldLeft(NoFlags)(_ | _)
              val mods = Modifiers(flags, cMods.privateWithin, Nil)

              q"$mods object ${TermName(cName.toString)} {}"
            case _ => c.abort(c.enclosingPosition, "Expected a companion object")
          }

        val q"""$oMods object $oName
                extends { ..$oEarlydefns } with ..$oParents { $oSelf => ..${oBody: List[Tree]} }""" = companion

        val args = (params :+ lastParam).map(_.name)
        val bodyWithM = q"def m(..$params)($lastParam) = new $cName(..$args)" :: oBody

        val companionWithM =
          q"""$oMods object $oName
                extends { ..$oEarlydefns } with ..$oParents { $oSelf => ..$bodyWithM }"""

        val res = target :: companionWithM :: Nil

        c.Expr(q"..$res")

      case _ => c.abort(c.enclosingPosition, "Class with a single parameters group expected")
    }

  }
}