package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL._
import dao.StudentTypeDao
import service.protocols.StudentTypeProtocol
import service.protocols.StudentTypeProtocol._

object StudentTypeActor {
  def behavior(studentTypeDao: StudentTypeDao): Behavior[StudentTypeProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(studentType, sender) => studentTypeDao.insert(studentType).foreach(sender ! _)
        case Update(studentType, sender) => studentTypeDao.update(studentType).foreach(sender ! _)
        case GetById(id, sender)         => studentTypeDao.getById(id).foreach(sender ! _)
        case FindAll(sender)             => studentTypeDao.findAll().foreach(sender ! _)
        case Delete(id, sender)          => studentTypeDao.deleteById(id).foreach(sender ! _)
      }
    }
}
