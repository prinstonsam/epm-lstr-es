package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL.{Static, _}
import dao.FeedbackDao
import service.protocols.FeedbackProtocol
import service.protocols.FeedbackProtocol._

object FeedbackActor {

  def behavior(feedbackDao: FeedbackDao): Behavior[FeedbackProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(feedback, sender)                => feedbackDao.insert(feedback).foreach(sender ! _)
        case Update(feedback, sender)                => feedbackDao.update(feedback).foreach(sender ! _)
        case GetById(id, sender)                     => feedbackDao.getById(id).foreach(sender ! _)
        case FindAll(sender)                         => feedbackDao.findAll().foreach(sender ! _)
        case FindByMentorId(mentorId, sender)        => feedbackDao.findByMentorId(mentorId).foreach(sender ! _)
        case FindByStudentId(studentId, sender)      => feedbackDao.findByStudentId(studentId).foreach(sender ! _)
        case FindByIterationId(iterationId, sender)  => feedbackDao.findByIterationId(iterationId).foreach(sender ! _)
        case Delete(id, sender)                      => feedbackDao.deleteById(id).foreach(sender ! _)
        case CountByStudentId(studentId, sender)     => feedbackDao.countByStudentId(studentId).foreach(sender ! _)
        case CountByIterationId(iterationId, sender) => feedbackDao.countByIterationId(iterationId).foreach(sender ! _)
        case FindByStudentIdIterationId(studentId, iterationId, sender) =>
          feedbackDao.findByStudentIdIterationId(studentId, iterationId).foreach(sender ! _)
      }
    }
}
