package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL.{Static, _}
import dao.IterationDao
import service.protocols.IterationProtocol
import service.protocols.IterationProtocol._

object IterationActor {

  def behavior(iterationDao: IterationDao): Behavior[IterationProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(iteration, sender)           => iterationDao.insert(iteration).foreach(sender ! _)
        case Update(iteration, sender)           => iterationDao.update(iteration).foreach(sender ! _)
        case GetById(id, sender)                 => iterationDao.getById(id).foreach(sender ! _)
        case FindAll(startDate, endDate, sender) => iterationDao.findAll(startDate, endDate).foreach(sender ! _)
        case FindByEmploymentId(emplId, startDate, endDate, sender) =>
          iterationDao.findByEmploymentId(emplId, startDate, endDate).foreach(sender ! _)
        case FindByMentorId(mentorId, sender)   => iterationDao.findByMentorId(mentorId).foreach(sender ! _)
        case FindByStudentId(studentId, sender) => iterationDao.findByStudentId(studentId).foreach(sender ! _)
        case CountByEmploymentId(employmentId, startDate, endDate, sender) =>
          iterationDao.countByEmploymentId(employmentId, startDate, endDate).foreach(sender ! _)
        case Delete(id, sender) => iterationDao.deleteById(id).foreach(sender ! _)
      }
    }
}
