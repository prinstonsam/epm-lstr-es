package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL._
import dao.UserDao
import service.protocols.UserProtocol
import service.protocols.UserProtocol._

object UserActor {

  def behavior(userDao: UserDao): Behavior[UserProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(user, sender)      => userDao.insert(user).foreach(sender ! _)
        case Update(user, sender)      => userDao.update(user).foreach(sender ! _)
        case GetById(id, sender)       => userDao.getById(id).foreach(sender ! _)
        case GetByEmail(email, sender) => userDao.getByEmail(email).foreach(sender ! _)
        case FindAll(sender)           => userDao.findAll().foreach(sender ! _)
        case Delete(id, sender)        => userDao.deleteByID(id).foreach(sender ! _)
      }
    }
}
