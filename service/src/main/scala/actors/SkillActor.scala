package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL._
import dao.SkillDao
import service.protocols.SkillProtocol
import service.protocols.SkillProtocol._

object SkillActor {

  def behavior(skillDao: SkillDao): Behavior[SkillProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(skill, sender)              => skillDao.insert(skill).foreach(sender ! _)
        case Update(skill, sender)              => skillDao.update(skill).foreach(sender ! _)
        case GetById(id, sender)                => skillDao.getById(id).foreach(sender ! _)
        case FindAll(sender)                    => skillDao.findAll().foreach(sender ! _)
        case FindByStudentId(studentId, sender) => skillDao.findByStudentId(studentId).foreach(sender ! _)
        case FindByMentorId(mentorId, sender)   => skillDao.findByMentorId(mentorId).foreach(sender ! _)
        case Delete(id, sender)                 => skillDao.deleteById(id).foreach(sender ! _)
      }
    }
}
