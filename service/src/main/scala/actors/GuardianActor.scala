package actors

import akka.typed.ScalaDSL._
import akka.typed.{ActorRef, Behavior, PreStart, Props}
import dao._
import service.protocols.GuardianProtocol._
import service.protocols._

object GuardianActor {

  private def startedGuardian(state: State): Behavior[GuardianProtocol] =
    Static {
      case GetEmploymentActor(sender)       => sender ! state.employmentActor
      case GetEmploymentTypeActor(sender)   => sender ! state.employmentTypeActor
      case GetFeedbackActor(sender)         => sender ! state.feedbackActor
      case GetIterationActor(sender)        => sender ! state.iterationActor
      case GetStudentIterationActor(sender) => sender ! state.studentIterationActor
      case GetMentorActor(sender)           => sender ! state.mentorActor
      case GetStudentReportActor(sender)    => sender ! state.reportActor
      case GetSkillActor(sender)            => sender ! state.skillActor
      case GetStudentActor(sender)          => sender ! state.studentActor
      case GetStudentWithDataActor(sender)  => sender ! state.studentWithDataActor
      case GetStudentTypeActor(sender)      => sender ! state.studentTypeActor
      case GetUserActor(sender)             => sender ! state.userActor
      case GetResultTypeActor(sender)       => sender ! state.resultTypeActor
      case GetEmploymentReportActor(sender) => sender ! state.employmentReportActor
      case GetStudentDataActor(sender)      => sender ! state.studentDataActor
    }

  def guardian(
      employmentDao: EmploymentDao,
      employmentTypeDao: EmploymentTypeDao,
      studentDao: StudentDao,
      studentTypeDao: StudentTypeDao,
      studentIterationDao: StudentIterationDao,
      feedbackDao: FeedbackDao,
      mentorDao: MentorDao,
      iterationDao: IterationDao,
      userDao: UserDao,
      skillDao: SkillDao,
      resultTypeDao: ResultTypeDao,
      studentDataDao: StudentDataDao,
      studentWithDataDao: StudentWithDataDao
  ): Behavior[GuardianProtocol] = Full {
    case Sig(ctx, PreStart) =>
      val employmentActor = ctx.spawn(Props(EmploymentActor.behavior(employmentDao)), "employmentActor")
      val employmentTypeActor =
        ctx.spawn(Props(EmploymentTypeActor.behavior(employmentTypeDao)), "employmentTypeActor")
      val feedbackActor  = ctx.spawn(Props(FeedbackActor.behavior(feedbackDao)), "feedbackActor")
      val iterationActor = ctx.spawn(Props(IterationActor.behavior(iterationDao)), "iterationActor")
      val mentorActor    = ctx.spawn(Props(MentorActor.behavior(mentorDao)), "mentorActor")
      val skillActor     = ctx.spawn(Props(SkillActor.behavior(skillDao)), "skillActor")
      val studentActor   = ctx.spawn(Props(StudentActor.behavior(studentDao)), "studentActor")
      val studentWithDataActor =
        ctx.spawn(Props(StudentWithDataActor.behavior(studentWithDataDao)), "studentWithTypeActor")
      val studentTypeActor = ctx.spawn(Props(StudentTypeActor.behavior(studentTypeDao)), "studentTypeActor")
      val userActor        = ctx.spawn(Props(UserActor.behavior(userDao)), "userActor")
      val resultTypeActor  = ctx.spawn(Props(ResultTypeActor.behavior(resultTypeDao)), "resultTypeActor")
      val studentIterationActor =
        ctx.spawn(Props(StudentIterationActor.behavior(studentIterationDao)), "studentIterationActor")
      val studentDataActor = ctx.spawn(Props(StudentDataActor.behavior(studentDataDao)), "studentDataActor")
      val employmentReportActor =
        ctx.spawn(
          Props(
            EmploymentReportActor.behavior(
              employmentActor,
              employmentTypeActor,
              iterationActor,
              studentActor,
              studentIterationActor,
              feedbackActor,
              resultTypeActor
            )),
          "employmentReportActor")

      val reportActor = ctx.spawn(
        Props(
          StudentReportActor.behavior(
            employmentActor,
            employmentTypeActor,
            feedbackActor,
            iterationActor,
            skillActor,
            studentWithDataDao,
            studentActor,
            studentTypeActor)),
        "reportActor")

      startedGuardian(
        State(
          employmentActor,
          employmentTypeActor,
          feedbackActor,
          iterationActor,
          studentIterationActor,
          mentorActor,
          reportActor,
          skillActor,
          studentActor,
          studentTypeActor,
          studentWithDataActor,
          userActor,
          resultTypeActor,
          employmentReportActor,
          studentDataActor
        ))
  }

  case class State(
      employmentActor: ActorRef[EmploymentProtocol],
      employmentTypeActor: ActorRef[EmploymentTypeProtocol],
      feedbackActor: ActorRef[FeedbackProtocol],
      iterationActor: ActorRef[IterationProtocol],
      studentIterationActor: ActorRef[StudentIterationProtocol],
      mentorActor: ActorRef[MentorProtocol],
      reportActor: ActorRef[StudentReportProtocol],
      skillActor: ActorRef[SkillProtocol],
      studentActor: ActorRef[StudentProtocol],
      studentTypeActor: ActorRef[StudentTypeProtocol],
      studentWithDataActor: ActorRef[StudentWithDataProtocol],
      userActor: ActorRef[UserProtocol],
      resultTypeActor: ActorRef[ResultTypeProtocol],
      employmentReportActor: ActorRef[EmploymentReportProtocol],
      studentDataActor: ActorRef[StudentDataProtocol]
  )
}
