package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL.{Static, _}
import dao.StudentWithDataDao
import service.protocols.StudentWithDataProtocol
import service.protocols.StudentWithDataProtocol._

object StudentWithDataActor {

  def behavior(studentWithDataDao: StudentWithDataDao): Behavior[StudentWithDataProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case GetByStudentId(studentId, date, sender) =>
          studentWithDataDao.getByStudentId(studentId, date).foreach(sender ! _)
        case FindAll(date, sender)           => studentWithDataDao.findAll(date).foreach(sender ! _)
        case Create(studentWithData, sender) => studentWithDataDao.create(studentWithData).foreach(sender ! _)
      }
    }
}
