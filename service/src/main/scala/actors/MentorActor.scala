package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL._
import dao.MentorDao
import service.protocols.MentorProtocol
import service.protocols.MentorProtocol._

object MentorActor {

  def behavior(mentorDao: MentorDao): Behavior[MentorProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(mentor, sender) => mentorDao.insert(mentor).foreach(sender ! _)
        case Update(mentor, sender) => mentorDao.update(mentor).foreach(sender ! _)
        case GetById(id, sender)    => mentorDao.getById(id).foreach(sender ! _)
        case FindAll(sender)        => mentorDao.findAll().foreach(sender ! _)
        case Delete(id, sender)     => mentorDao.deleteById(id).foreach(sender ! _)
      }
    }
}
