package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL._
import dao.ResultTypeDao
import service.protocols.ResultTypeProtocol
import service.protocols.ResultTypeProtocol._

object ResultTypeActor {
  def behavior(resultTypeDao: ResultTypeDao): Behavior[ResultTypeProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(resultType, sender) => resultTypeDao.insert(resultType).foreach(sender ! _)
        case Update(resultType, sender) => resultTypeDao.update(resultType).foreach(sender ! _)
        case GetById(id, sender)        => resultTypeDao.getById(id).foreach(sender ! _)
        case FindAll(sender)            => resultTypeDao.findAll().foreach(sender ! _)
        case Delete(id, sender)         => resultTypeDao.deleteById(id).foreach(sender ! _)
      }
    }
}
