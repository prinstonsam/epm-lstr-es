package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL._
import dao.StudentDao
import service.protocols.StudentProtocol
import service.protocols.StudentProtocol._

object StudentActor {

  def behavior(studentDao: StudentDao): Behavior[StudentProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(student, sender)            => studentDao.insert(student).foreach(sender ! _)
        case Update(student, sender)            => studentDao.update(student).foreach(sender ! _)
        case GetById(id, sender)                => studentDao.getById(id).foreach(sender ! _)
        case FindAll(sender)                    => studentDao.findAll().foreach(sender ! _)
        case FindByIterationId(iterId, sender)  => studentDao.findByIterationId(iterId).foreach(sender ! _)
        case CountByIterationId(iterId, sender) => studentDao.countByIterationId(iterId).foreach(sender ! _)
        case Delete(id, sender)                 => studentDao.deleteById(id).foreach(sender ! _)
      }
    }
}
