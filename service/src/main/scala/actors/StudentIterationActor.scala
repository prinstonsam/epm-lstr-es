package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL._
import dao.StudentIterationDao
import service.protocols.StudentIterationProtocol
import service.protocols.StudentIterationProtocol._

object StudentIterationActor {

  def behavior(studentIterationDao: StudentIterationDao): Behavior[StudentIterationProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(studentIteration, sender)  => studentIterationDao.insert(studentIteration).foreach(sender ! _)
        case Update(studentIteration, sender)  => studentIterationDao.update(studentIteration).foreach(sender ! _)
        case GetById(iterId, studId, sender)   => studentIterationDao.getById(iterId, studId).foreach(sender ! _)
        case FindAll(sender)                   => studentIterationDao.findAll().foreach(sender ! _)
        case FindByIterationId(iterId, sender) => studentIterationDao.findByIterationId(iterId).foreach(sender ! _)
        case FindByStudentId(studId, sender)   => studentIterationDao.findByStudentId(studId).foreach(sender ! _)
        case Delete(iterId, studId, sender)    => studentIterationDao.delete(iterId, studId).foreach(sender ! _)
      }
    }
}
