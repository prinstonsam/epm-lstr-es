package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL._
import dao.EmploymentDao
import service.protocols.EmploymentProtocol
import service.protocols.EmploymentProtocol._

object EmploymentActor {

  def behavior(employmentDao: EmploymentDao): Behavior[EmploymentProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(employment, sender)             => employmentDao.insert(employment).foreach(sender ! _)
        case Update(employment, sender)             => employmentDao.update(employment).foreach(sender ! _)
        case GetById(id, sender)                    => employmentDao.getById(id).foreach(sender ! _)
        case FindByTypeId(employmentTypeId, sender) => employmentDao.findByTypeId(employmentTypeId).foreach(sender ! _)
        case FindAll(sender)                        => employmentDao.findAll().foreach(sender ! _)
        case Delete(id, sender)                     => employmentDao.deleteById(id).foreach(sender ! _)
      }
    }
}
