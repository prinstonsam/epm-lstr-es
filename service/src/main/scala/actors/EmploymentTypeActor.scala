package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL.{Static, _}
import dao.EmploymentTypeDao
import service.protocols.EmploymentTypeProtocol
import service.protocols.EmploymentTypeProtocol._

object EmploymentTypeActor {

  def behavior(employmentTypeDao: EmploymentTypeDao): Behavior[EmploymentTypeProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case Create(employmentType, sender) => employmentTypeDao.insert(employmentType).foreach(sender ! _)
        case Update(employmentType, sender) => employmentTypeDao.update(employmentType).foreach(sender ! _)
        case GetById(id, sender)            => employmentTypeDao.getById(id).foreach(sender ! _)
        case FindAll(sender)                => employmentTypeDao.findAll().foreach(sender ! _)
        case Delete(id, sender)             => employmentTypeDao.deleteById(id).foreach(sender ! _)
      }
    }
}
