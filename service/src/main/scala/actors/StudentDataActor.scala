package actors

import akka.typed.Behavior
import akka.typed.ScalaDSL.{Static, _}
import dao.StudentDataDao
import service.protocols.StudentDataProtocol
import service.protocols.StudentDataProtocol._

object StudentDataActor {
  def behavior(studentDataDao: StudentDataDao): Behavior[StudentDataProtocol] =
    ContextAware { ctx =>
      import ctx.executionContext
      Static {
        case FindById(studentId, sender) => studentDataDao.findById(studentId).foreach(sender ! _)
        case Create(studentData, sender) => studentDataDao.insert(studentData).foreach(sender ! _)
      }
    }
}
