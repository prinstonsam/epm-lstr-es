package actors

import java.time.Instant

import akka.typed.AskPattern._
import akka.typed.ScalaDSL._
import akka.typed.{ActorRef, Behavior}
import akka.util.Timeout
import dto.{Employment, Iteration}
import dto.reports.EmploymentReport
import dto.typed.EmploymentWithType
import service.protocols._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

object EmploymentReportActor {

  implicit val timeout = Timeout(5.seconds)

  def behavior(
      employmentActor: ActorRef[EmploymentProtocol],
      employmentTypeActor: ActorRef[EmploymentTypeProtocol],
      iterationActor: ActorRef[IterationProtocol],
      studentActor: ActorRef[StudentProtocol],
      studentIterationActor: ActorRef[StudentIterationProtocol],
      feedbackActor: ActorRef[FeedbackProtocol],
      resultTypeActor: ActorRef[ResultTypeProtocol]
  ): Behavior[EmploymentReportProtocol] =
    ContextAware { ctx =>
      Static {
        case EmploymentReportProtocol.GetEmploymentReport(employmentId, startDate, endDate, sender) =>
          getEmploymentReport(
            employmentId,
            startDate,
            endDate,
            employmentActor,
            employmentTypeActor,
            iterationActor,
            studentActor,
            studentIterationActor,
            feedbackActor,
            resultTypeActor
          ).foreach(sender ! _)
      }
    }

  private def getEmploymentReport(
      employmentId: Int,
      startDate: Option[Instant],
      endDate: Option[Instant],
      employmentActor: ActorRef[EmploymentProtocol],
      employmentTypeActor: ActorRef[EmploymentTypeProtocol],
      iterationActor: ActorRef[IterationProtocol],
      studentActor: ActorRef[StudentProtocol],
      studentIterationActor: ActorRef[StudentIterationProtocol],
      feedbackActor: ActorRef[FeedbackProtocol],
      resultTypeActor: ActorRef[ResultTypeProtocol]
  ): Future[Option[EmploymentReport]] = {

    def getEmploymentWithType(employment: Employment) =
      employmentTypeActor ? EmploymentTypeProtocol.GetById.m(employment.employmentTypeId) map {
        case None => None
        case Some(employmentType) =>
          Some(EmploymentWithType(employment, employmentType))
      }

    val eventualMaybeEmploymentWithType =
      employmentActor ? EmploymentProtocol.GetById.m(employmentId) flatMap {
        case None             => Future.successful(None)
        case Some(employment) => getEmploymentWithType(employment)
      }

    val eventualIterationCount =
      iterationActor ? IterationProtocol.CountByEmploymentId.m(employmentId, startDate, endDate)

    val eventualIterations: Future[Seq[Iteration]] =
      iterationActor ? IterationProtocol.FindByEmploymentId.m(employmentId, startDate, endDate)

    val eventualStudentCount =
      eventualIterations flatMap { iterations =>
        Future.sequence {
          iterations.map { iteration =>
            studentActor ? StudentProtocol.CountByIterationId.m(iteration.id.get)
          }
        }.map(_.sum)
      }

    val eventualStudentIterations =
      eventualIterations
        .flatMap(iterations =>
          Future.sequence {
            iterations.map { iteration =>
              studentIterationActor ? StudentIterationProtocol.FindByIterationId.m(iteration.id.get)
            }
        })
        .map(_.flatten)

    val eventualStudentResultIdToCount =
      eventualStudentIterations.map { studentIterations =>
        studentIterations
          .groupBy(s => s.resultTypeId)
          .map { case (k, v) =>
            k -> v.length
          }
      }

    val eventualStudentResultMap =
      eventualStudentResultIdToCount.flatMap { idToCount =>
        val eventualPairs =
          idToCount.map { case (k, v) =>
            resultTypeActor ? ResultTypeProtocol.GetById.m(k) map {
              case None             => None
              case Some(resultType) => Some(resultType -> v)
            }
          }.toIndexedSeq
        Future.sequence(eventualPairs).map(_.flatten.toMap)
      }

    val eventualFeedbackCount =
      eventualIterations flatMap { iterations =>
        Future.sequence {
          iterations.map { iteration =>
            feedbackActor ? FeedbackProtocol.CountByIterationId.m(iteration.id.get)
          }
        }.map(_.sum)
      }

    eventualMaybeEmploymentWithType.flatMap {
      case None => Future.successful(None)
      case Some(employmentWithType) =>
        for {
          iterationCount   <- eventualIterationCount
          studentCount     <- eventualStudentCount
          studentResultMap <- eventualStudentResultMap
          feedbackCount    <- eventualFeedbackCount
        } yield
          Some(EmploymentReport(employmentWithType, iterationCount, studentCount, studentResultMap, feedbackCount))
    }
  }
}
