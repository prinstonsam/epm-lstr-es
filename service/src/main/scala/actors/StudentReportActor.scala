package actors

import java.time.Instant

import akka.typed.AskPattern._
import akka.typed.ScalaDSL._
import akka.typed.{ActorRef, Behavior}
import akka.util.Timeout
import dao._
import dto._
import dto.reports.{DetailedIteration, StudentReport}
import dto.typed.{EmploymentWithType, StudentWithType}
import service.protocols.StudentReportProtocol._
import service.protocols._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

object StudentReportActor {

  implicit val timeout = Timeout(5.seconds)

  def behavior(
      employmentActor: ActorRef[EmploymentProtocol],
      employmentTypeActor: ActorRef[EmploymentTypeProtocol],
      feedbackActor: ActorRef[FeedbackProtocol],
      iterationActor: ActorRef[IterationProtocol],
      skillActor: ActorRef[SkillProtocol],
      studentWithDataDao: StudentWithDataDao,
      studentActor: ActorRef[StudentProtocol],
      studentTypeActor: ActorRef[StudentTypeProtocol]
  ): Behavior[StudentReportProtocol] =
    ContextAware { ctx =>
      Static {
        case GetStudentReport(studentId, startDate, endDate, sender) =>
          getStudentReport(
            studentId,
            startDate,
            endDate,
            employmentActor,
            employmentTypeActor,
            feedbackActor,
            iterationActor,
            skillActor,
            studentWithDataDao,
            studentActor,
            studentTypeActor
          ).foreach(sender ! _)

      }
    }

  private def getStudentReport(
      studentId: Int,
      startDate: Option[Instant],
      endDate: Option[Instant],
      employmentActor: ActorRef[EmploymentProtocol],
      employmentTypeActor: ActorRef[EmploymentTypeProtocol],
      feedbackActor: ActorRef[FeedbackProtocol],
      iterationActor: ActorRef[IterationProtocol],
      skillActor: ActorRef[SkillProtocol],
      studentWithDataDao: StudentWithDataDao,
      studentActor: ActorRef[StudentProtocol],
      studentTypeActor: ActorRef[StudentTypeProtocol]
  ): Future[Option[StudentReport]] = {

    val updatedStartDate = startDate match {
      case None       => Instant.MIN
      case Some(date) => date
    }

    val updatedEndDate = endDate match {
      case None       => Instant.MAX
      case Some(date) => date
    }

    val studentTypeId: Future[Option[Int]] = {
      studentWithDataDao.getByStudentId(studentId, None).map {
        case None                  => None
        case Some(studentWithData) => Some(studentWithData.studentData.studentTypeId)
      }
    }

    val studentType = studentTypeId.flatMap {
      case None     => Future.successful(None)
      case Some(id) => studentTypeActor ? StudentTypeProtocol.GetById.m(id)
    }

    def getStudentWithType(student: Student): Future[Option[StudentWithType]] =
      studentType.map {
        case None          => None
        case Some(stdType) => Some(StudentWithType(student, stdType))
      }

    def getEmploymentWithType(employment: Employment): Future[Option[EmploymentWithType]] =
      employmentTypeActor ? EmploymentTypeProtocol.GetById.m(employment.employmentTypeId) map {
        case None                 => None
        case Some(employmentType) => Some(EmploymentWithType(employment, employmentType))
      }

    def getEmploymentByIteration(iteration: Iteration): Future[Option[EmploymentWithType]] =
      employmentActor ? EmploymentProtocol.GetById.m(iteration.employmentId) flatMap {
        case None             => Future.successful(None)
        case Some(employment) => getEmploymentWithType(employment)
      }

    val eventualMaybeStudentWithType: Future[Option[StudentWithType]] =
      studentActor ? StudentProtocol.GetById.m(studentId) flatMap {
        case None          => Future.successful(None)
        case Some(student) => getStudentWithType(student)
      }

    val eventualFeedbackCount: Future[Int] =
      feedbackActor ? (FeedbackProtocol.CountByStudentId(studentId, _))

    val eventualSkills: Future[IndexedSeq[Skill]] =
      skillActor ? SkillProtocol.FindByStudentId.m(studentId) map (_.toIndexedSeq)

    val eventualDetailedIterations: Future[IndexedSeq[DetailedIteration]] =
      iterationActor ? IterationProtocol.FindByStudentId.m(studentId) flatMap { iterations =>
        Future.sequence {
          iterations.map { iteration =>
            getEmploymentByIteration(iteration)
            //TODO using method get() on employmentWithType not recommended
            .map {
              case None => None
              case Some(employmentWithType) =>
                Some(DetailedIteration(iteration, employmentWithType))
            }
          }.toIndexedSeq
        }.map(_.flatten)
      }

    eventualMaybeStudentWithType.flatMap {
      case None => Future.successful(None)
      case Some(studentWithType) =>
        for {
          detailedIterations <- eventualDetailedIterations
          feedbackCount      <- eventualFeedbackCount
          skills             <- eventualSkills
        } yield Some(StudentReport(studentWithType, detailedIterations, feedbackCount, skills))
    }
  }
}
