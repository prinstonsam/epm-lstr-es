package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.EmploymentTypeDao
import dto.EmploymentType
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.EmploymentTypeProtocol
import service.protocols.EmploymentTypeProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class EmploymentTypeActorTest
    extends WordSpec
    with Matchers
    with BeforeAndAfterAll
    with ScalaFutures
    with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val employmentTypeDao                           = mock[EmploymentTypeDao]
  var system: ActorSystem[EmploymentTypeProtocol] = _

  def employmentTypeActor: ActorRef[EmploymentTypeProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(EmploymentTypeActor.behavior(employmentTypeDao = employmentTypeDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  "EmploymentType Service" when {
    val employmentType = EmploymentType(Some(1), "Employment1")
    "find all - return list of employmentTypes" in {
      when(employmentTypeDao.findAll()).thenReturn(Future(Seq(employmentType)))
      val eventualEmploymentTypes: Future[Seq[EmploymentType]] = employmentTypeActor ? FindAll
      eventualEmploymentTypes.futureValue shouldEqual Seq(employmentType)
    }

    "find all - return empty collection" in {
      when(employmentTypeDao.findAll()).thenReturn(Future(Seq()))
      val eventualEmploymentTypes: Future[Seq[EmploymentType]] = employmentTypeActor ? FindAll
      eventualEmploymentTypes.futureValue shouldEqual Seq()
    }

    "get EmploymentType by ID - return value" in {
      when(employmentTypeDao.getById(1)).thenReturn(Future(Option(employmentType)))
      val eventualEmploymentTypes = employmentTypeActor.?[Option[EmploymentType]](GetById(1, _))
      eventualEmploymentTypes.futureValue shouldEqual Some(employmentType)
    }

    "get EmploymentType by ID - return None" in {
      when(employmentTypeDao.getById(1)).thenReturn(Future(None))
      val eventualEmploymentTypes = employmentTypeActor.?[Option[EmploymentType]](GetById(1, _))
      eventualEmploymentTypes.futureValue shouldEqual None
    }
    "create EmploymentType" in {
      when(employmentTypeDao.insert(employmentType)).thenReturn(Future(employmentType))
      val eventualEmploymentTypes = employmentTypeActor.?[EmploymentType](Create(employmentType, _))
      eventualEmploymentTypes.futureValue shouldEqual employmentType
    }

    "update EmploymentType - return value" in {
      when(employmentTypeDao.update(employmentType)).thenReturn(Future(Option(employmentType)))
      val eventualEmploymentTypes = employmentTypeActor.?[Option[EmploymentType]](Update(employmentType, _))
      eventualEmploymentTypes.futureValue shouldEqual Some(employmentType)
    }

    "update EmploymentType - return None" in {
      when(employmentTypeDao.update(employmentType)).thenReturn(Future(None))
      val eventualEmploymentTypes = employmentTypeActor.?[Option[EmploymentType]](Update(employmentType, _))
      eventualEmploymentTypes.futureValue shouldEqual None
    }

    "delete EmploymentType - return value" in {
      when(employmentTypeDao.deleteById(1)).thenReturn(Future(Option(employmentType)))
      val eventualEmploymentTypes = employmentTypeActor.?[Option[EmploymentType]](Delete(1, _))
      eventualEmploymentTypes.futureValue shouldEqual Some(employmentType)
    }

    "delete EmploymentType - return None" in {
      when(employmentTypeDao.deleteById(1)).thenReturn(Future(None))
      val eventualEmploymentTypes = employmentTypeActor.?[Option[EmploymentType]](Delete(1, _))
      eventualEmploymentTypes.futureValue shouldEqual None
    }
  }
}
