package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.FeedbackDao
import dto.Feedback
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.FeedbackProtocol
import service.protocols.FeedbackProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class FeedbackActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val feedbackDao                           = mock[FeedbackDao]
  var system: ActorSystem[FeedbackProtocol] = _

  def feedbackActor: ActorRef[FeedbackProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(FeedbackActor.behavior(feedbackDao = feedbackDao)))
  }

  override def afterAll(): Unit = {
    val _ = system.terminate()
  }

  "Feedback Service" when {
    val feedback = Feedback(Some(1), 1, 1, 1, "Feedback about first student")
    "find all - return list of feedbacks" in {
      when(feedbackDao.findAll()).thenReturn(Future(Seq(feedback)))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor ? FindAll
      eventualFeedbacks.futureValue shouldEqual Seq(feedback)

    }

    "find all - return empty collection" in {
      when(feedbackDao.findAll()).thenReturn(Future(Seq()))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor ? FindAll
      eventualFeedbacks.futureValue shouldEqual Seq()
    }

    "get Feedback by ID - return value" in {
      when(feedbackDao.getById(1)).thenReturn(Future(Option(feedback)))
      val eventualFeedbacks = feedbackActor.?[Option[Feedback]](GetById(1, _))
      eventualFeedbacks.futureValue shouldEqual Some(feedback)
    }

    "get Feedback by ID - return None" in {
      when(feedbackDao.getById(1)).thenReturn(Future(None))
      val eventualFeedbacks = feedbackActor.?[Option[Feedback]](GetById(1, _))
      eventualFeedbacks.futureValue shouldEqual None
    }

    "find Feedback by Mentor ID - return value" in {
      when(feedbackDao.findByMentorId(1)).thenReturn(Future(Seq(feedback)))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor.?(FindByMentorId(1, _))
      eventualFeedbacks.futureValue shouldEqual Seq(feedback)
    }

    "find Feedback by Mentor ID - return empty collection" in {
      when(feedbackDao.findByMentorId(-1)).thenReturn(Future(Seq()))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor.?(FindByMentorId(-1, _))
      eventualFeedbacks.futureValue shouldEqual Seq()
    }

    "find Feedback by Student ID - return value" in {
      when(feedbackDao.findByStudentId(1)).thenReturn(Future(Seq(feedback)))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor.?(FindByStudentId(1, _))
      eventualFeedbacks.futureValue shouldEqual Seq(feedback)
    }

    "find Feedback by Student ID - return empty collection" in {
      when(feedbackDao.findByStudentId(-1)).thenReturn(Future(Seq()))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor.?(FindByStudentId(-1, _))
      eventualFeedbacks.futureValue shouldEqual Seq()
    }

    "find Feedback by Iteration ID - return value" in {
      when(feedbackDao.findByIterationId(1)).thenReturn(Future(Seq(feedback)))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor.?(FindByIterationId(1, _))
      eventualFeedbacks.futureValue shouldEqual Seq(feedback)
    }

    "find Feedback by Iteration ID - return empty collection" in {
      when(feedbackDao.findByIterationId(-1)).thenReturn(Future(Seq()))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor.?(FindByIterationId(-1, _))
      eventualFeedbacks.futureValue shouldEqual Seq()
    }

    "find Feedback by Student ID & Iteration ID - return value" in {
      when(feedbackDao.findByStudentIdIterationId(1, 1)).thenReturn(Future(Seq(feedback)))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor.?(FindByStudentIdIterationId(1, 1, _))
      eventualFeedbacks.futureValue shouldEqual Seq(feedback)
    }

    "find Feedback by Student ID & Iteration ID - return empty collection" in {
      when(feedbackDao.findByStudentIdIterationId(-1, 1)).thenReturn(Future(Seq()))
      val eventualFeedbacks: Future[Seq[Feedback]] = feedbackActor.?(FindByStudentIdIterationId(-1, 1, _))
      eventualFeedbacks.futureValue shouldEqual Seq()
    }

    "insert Feedback" in {
      when(feedbackDao.insert(feedback)).thenReturn(Future(feedback))
      val eventualFeedbacks = feedbackActor.?[Feedback](Create(feedback, _))
      eventualFeedbacks.futureValue shouldEqual feedback
    }

    "update Feedback - return value" in {
      when(feedbackDao.update(feedback)).thenReturn(Future(Option(feedback)))
      val eventualFeedbacks = feedbackActor.?[Option[Feedback]](Update(feedback, _))
      eventualFeedbacks.futureValue shouldEqual Some(feedback)
    }

    "update Feedback - return None" in {
      when(feedbackDao.update(feedback)).thenReturn(Future(None))
      val eventualFeedbacks = feedbackActor.?[Option[Feedback]](Update(feedback, _))
      eventualFeedbacks.futureValue shouldEqual None
    }

    "delete Feedback - return value" in {
      when(feedbackDao.deleteById(1)).thenReturn(Future(Option(feedback)))
      val eventualFeedbacks = feedbackActor.?[Option[Feedback]](Delete(1, _))
      eventualFeedbacks.futureValue shouldEqual Some(feedback)
    }

    "delete Feedback - return None" in {
      when(feedbackDao.deleteById(1)).thenReturn(Future(None))
      val eventualFeedbacks = feedbackActor.?[Option[Feedback]](Delete(1, _))
      eventualFeedbacks.futureValue shouldEqual None
    }
  }
}
