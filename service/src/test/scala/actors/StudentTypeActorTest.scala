package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.StudentTypeDao
import dto.StudentType
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.StudentTypeProtocol
import service.protocols.StudentTypeProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class StudentTypeActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val studentTypeDao                           = mock[StudentTypeDao]
  var system: ActorSystem[StudentTypeProtocol] = _

  def studentTypeActor: ActorRef[StudentTypeProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(StudentTypeActor.behavior(studentTypeDao = studentTypeDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  "orm.StudentType Service" when {
    val studentType = StudentType(Some(1), "title")
    "get all - return list of studentTypes" in {
      when(studentTypeDao.findAll()).thenReturn(Future(Seq(studentType)))
      val eventualStudentTypes: Future[Seq[StudentType]] = studentTypeActor ? FindAll
      eventualStudentTypes.futureValue shouldEqual Seq(studentType)

    }

    "get all - return empty list" in {
      when(studentTypeDao.findAll()).thenReturn(Future(Seq()))
      val eventualStudentTypes: Future[Seq[StudentType]] = studentTypeActor ? FindAll
      eventualStudentTypes.futureValue shouldEqual Seq()
    }

    "get orm.StudentType by ID - return value" in {
      when(studentTypeDao.getById(1)).thenReturn(Future(Option(studentType)))
      val eventualStudentTypes = studentTypeActor.?[Option[StudentType]](GetById(1, _))
      eventualStudentTypes.futureValue shouldEqual Some(studentType)
    }

    "get orm.StudentType by ID - return None" in {
      when(studentTypeDao.getById(1)).thenReturn(Future(None))
      val eventualStudentTypes = studentTypeActor.?[Option[StudentType]](GetById(1, _))
      eventualStudentTypes.futureValue shouldEqual None
    }
    "create orm.StudentType" in {
      when(studentTypeDao.insert(studentType)).thenReturn(Future(studentType))
      val eventualStudentTypes = studentTypeActor.?[StudentType](Create(studentType, _))
      eventualStudentTypes.futureValue shouldEqual studentType
    }

    "update orm.StudentType - return value" in {
      when(studentTypeDao.update(studentType)).thenReturn(Future(Option(studentType)))
      val eventualStudentTypes = studentTypeActor.?[Option[StudentType]](Update(studentType, _))
      eventualStudentTypes.futureValue shouldEqual Some(studentType)
    }

    "update orm.StudentType - return None" in {
      when(studentTypeDao.update(studentType)).thenReturn(Future(None))
      val eventualStudentTypes = studentTypeActor.?[Option[StudentType]](Update(studentType, _))
      eventualStudentTypes.futureValue shouldEqual None
    }

    "delete orm.StudentType - return value" in {
      when(studentTypeDao.deleteById(1)).thenReturn(Future(Option(studentType)))
      val eventualStudentTypes = studentTypeActor.?[Option[StudentType]](Delete(1, _))
      eventualStudentTypes.futureValue shouldEqual Some(studentType)
    }

    "delete orm.StudentType - return None" in {
      when(studentTypeDao.deleteById(1)).thenReturn(Future(None))
      val eventualStudentTypes = studentTypeActor.?[Option[StudentType]](Delete(1, _))
      eventualStudentTypes.futureValue shouldEqual None
    }
  }

}
