package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.MentorDao
import dto.Mentor
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.MentorProtocol
import service.protocols.MentorProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class MentorActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val mentorDao = mock[MentorDao]
  var system: ActorSystem[MentorProtocol] = _

  def mentorActor: ActorRef[MentorProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(MentorActor.behavior(mentorDao = mentorDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  "orm.Mentor Service" when {
    val mentor = Mentor(Some(1), "firstName", "lastName", "email", "scala")
    "get all - return list of mentors" in {
      when(mentorDao.findAll()).thenReturn(Future(Seq(mentor)))
      val eventualMentors: Future[Seq[Mentor]] = mentorActor ? FindAll
      eventualMentors.futureValue shouldEqual Seq(mentor)

    }

    "get all - return empty list" in {
      when(mentorDao.findAll()).thenReturn(Future(Seq()))
      val eventualMentors: Future[Seq[Mentor]] = mentorActor ? FindAll
      eventualMentors.futureValue shouldEqual Seq()
    }

    "get orm.Mentor by ID - return value" in {
      when(mentorDao.getById(1)).thenReturn(Future(Option(mentor)))
      val eventualMentors = mentorActor.?[Option[Mentor]](GetById(1, _))
      eventualMentors.futureValue shouldEqual Some(mentor)
    }

    "get orm.Mentor by ID - return None" in {
      when(mentorDao.getById(1)).thenReturn(Future(None))
      val eventualMentors = mentorActor.?[Option[Mentor]](GetById(1, _))
      eventualMentors.futureValue shouldEqual None
    }
    "create orm.Mentor" in {
      when(mentorDao.insert(mentor)).thenReturn(Future(mentor))
      val eventualMentors = mentorActor.?[Mentor](Create(mentor, _))
      eventualMentors.futureValue shouldEqual mentor
    }

    "update orm.Mentor - return value" in {
      when(mentorDao.update(mentor)).thenReturn(Future(Option(mentor)))
      val eventualMentors = mentorActor.?[Option[Mentor]](Update(mentor, _))
      eventualMentors.futureValue shouldEqual Some(mentor)
    }

    "update orm.Mentor - return None" in {
      when(mentorDao.update(mentor)).thenReturn(Future(None))
      val eventualMentors = mentorActor.?[Option[Mentor]](Update(mentor, _))
      eventualMentors.futureValue shouldEqual None
    }

    "delete orm.Mentor - return value" in {
      when(mentorDao.deleteById(1)).thenReturn(Future(Option(mentor)))
      val eventualMentors = mentorActor.?[Option[Mentor]](Delete(1, _))
      eventualMentors.futureValue shouldEqual Some(mentor)
    }

    "delete orm.Mentor - return None" in {
      when(mentorDao.deleteById(1)).thenReturn(Future(None))
      val eventualMentors = mentorActor.?[Option[Mentor]](Delete(1, _))
      eventualMentors.futureValue shouldEqual None
    }
  }

}
