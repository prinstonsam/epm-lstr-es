package actors

import java.time.Instant

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.StudentWithDataDao
import dto.{Student, StudentData, StudentWithData}
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.StudentWithDataProtocol
import service.protocols.StudentWithDataProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class StudentWithDataActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {
  val dt = Instant.now()
  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val studentWithDataDao                           = mock[StudentWithDataDao]
  var system: ActorSystem[StudentWithDataProtocol] = _

  def studentWithDataActor: ActorRef[StudentWithDataProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(StudentWithDataActor.behavior(studentWithDataDao = studentWithDataDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  "StudentWithData Service" when {
    val studentWithData = StudentWithData(Student(Some(1), "firstName", "lastName", "email", false),
      StudentData(1, 1, 1, Instant.now, None))

    "find all - return list of studentWithDatas" in {
      when(studentWithDataDao.findAll(None)).thenReturn(Future(Seq(studentWithData)))
      val eventualStudentWithDatas = studentWithDataActor.?[Seq[StudentWithData]](FindAll(None, _))
      eventualStudentWithDatas.futureValue shouldEqual Seq(studentWithData)

    }

    "find all - return list of studentWithDatas by date" in {
      when(studentWithDataDao.findAll(Some(dt))).thenReturn(Future(Seq(studentWithData)))
      val eventualStudentWithDatas = studentWithDataActor.?[Seq[StudentWithData]](FindAll(Some(dt), _))
      eventualStudentWithDatas.futureValue shouldEqual Seq(studentWithData)
    }

    "get StudentWithData by None" in {
      when(studentWithDataDao.getByStudentId(1, None)).thenReturn(Future(Option(studentWithData)))
      val eventualStudentWithDatas = studentWithDataActor.?[Option[StudentWithData]](GetByStudentId(1, None, _))
      eventualStudentWithDatas.futureValue shouldEqual Some(studentWithData)
    }

    "get StudentWithData by date" in {
      when(studentWithDataDao.getByStudentId(1, Some(dt))).thenReturn(Future(Option(studentWithData)))
      val eventualStudentWithDatas =
        studentWithDataActor.?[Option[StudentWithData]](GetByStudentId(1, Some(dt), _))
      eventualStudentWithDatas.futureValue shouldEqual Some(studentWithData)
    }

    "get StudentWithData by ID - return None" in {
      when(studentWithDataDao.getByStudentId(666, None)).thenReturn(Future(None))
      val eventualStudentWithDatas = studentWithDataActor.?[Option[StudentWithData]](GetByStudentId(666, None, _))
      eventualStudentWithDatas.futureValue shouldEqual None
    }

    "create StudentWithData" in {
      when(studentWithDataDao.create(studentWithData)).thenReturn(Future(studentWithData))
      val eventualStudentWithDatas = studentWithDataActor.?[StudentWithData](Create(studentWithData, _))
      eventualStudentWithDatas.futureValue shouldEqual studentWithData
    }
  }
}
