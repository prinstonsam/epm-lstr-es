package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.{StudentDao, StudentIterationDao}
import dto.Student
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.StudentProtocol
import service.protocols.StudentProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class StudentActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val studentDao                           = mock[StudentDao]
  val studentIterationDao                  = mock[StudentIterationDao]
  var system: ActorSystem[StudentProtocol] = _

  def studentActor: ActorRef[StudentProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(StudentActor.behavior(studentDao = studentDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  "Student Service" when {
    val student = Student(Some(1), "firstName", "lastName", "email", false)

    "find all - return list of students" in {
      when(studentDao.findAll()).thenReturn(Future(Seq(student)))
      val eventualStudents: Future[Seq[Student]] = studentActor ? FindAll
      eventualStudents.futureValue shouldEqual Seq(student)

    }

    "find all - return empty list" in {
      when(studentDao.findAll()).thenReturn(Future(Seq()))
      val eventualStudents: Future[Seq[Student]] = studentActor ? FindAll
      eventualStudents.futureValue shouldEqual Seq()
    }

    "get Student by ID - return value" in {
      when(studentDao.getById(1)).thenReturn(Future(Option(student)))
      val eventualStudents = studentActor.?[Option[Student]](GetById(1, _))
      eventualStudents.futureValue shouldEqual Some(student)
    }

    "get Student by ID - return None" in {
      when(studentDao.getById(1)).thenReturn(Future(None))
      val eventualStudents = studentActor.?[Option[Student]](GetById(1, _))
      eventualStudents.futureValue shouldEqual None
    }

    "find Students by Iteration ID - return value" in {
      when(studentDao.findByIterationId(1)).thenReturn(Future(Seq(student)))
      val eventualStudents: Future[Seq[Student]] = studentActor.?(FindByIterationId(1, _))
      eventualStudents.futureValue shouldEqual Seq(student)
    }

    "find Students by Iteration ID - return empty collection" in {
      when(studentDao.findByIterationId(-1)).thenReturn(Future(Seq()))
      val eventualStudents: Future[Seq[Student]] = studentActor.?(FindByIterationId(-1, _))
      eventualStudents.futureValue shouldEqual Seq()
    }

    "create Student" in {
      when(studentDao.insert(student)).thenReturn(Future(student))
      val eventualStudents = studentActor.?[Student](Create(student, _))
      eventualStudents.futureValue shouldEqual student
    }

    "update Student - return value" in {
      when(studentDao.update(student)).thenReturn(Future(Option(student)))
      val eventualStudents = studentActor.?[Option[Student]](Update(student, _))
      eventualStudents.futureValue shouldEqual Some(student)
    }

    "update Student - return None" in {
      when(studentDao.update(student)).thenReturn(Future(None))
      val eventualStudents = studentActor.?[Option[Student]](Update(student, _))
      eventualStudents.futureValue shouldEqual None
    }

    "delete Student - return value" in {
      when(studentDao.deleteById(1)).thenReturn(Future(Option(student)))
      val eventualStudents = studentActor.?[Option[Student]](Delete(1, _))
      eventualStudents.futureValue shouldEqual Some(student)
    }

    "delete Student - return None" in {
      when(studentDao.deleteById(1)).thenReturn(Future(None))
      val eventualStudents = studentActor.?[Option[Student]](Delete(1, _))
      eventualStudents.futureValue shouldEqual None
    }
  }
}
