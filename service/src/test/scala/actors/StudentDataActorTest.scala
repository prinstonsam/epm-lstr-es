package actors

import java.time.Instant

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.StudentDataDao
import dto.StudentData
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.StudentDataProtocol
import service.protocols.StudentDataProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class StudentDataActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience = PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val studentDataDao                           = mock[StudentDataDao]
  var system: ActorSystem[StudentDataProtocol] = _

  def studentDataActor: ActorRef[StudentDataProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(StudentDataActor.behavior(studentDataDao = studentDataDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  val date = Instant.now()
  "StudentData Service" when {
    val studentData = StudentData(1, 1, 1, date, None)

    "create StudentData" in {
      when(studentDataDao.insert(studentData)).thenReturn(Future(studentData))
      val eventualStudentDatas = studentDataActor.?[StudentData](Create(studentData, _))
      eventualStudentDatas.futureValue shouldEqual studentData
    }
  }
}
