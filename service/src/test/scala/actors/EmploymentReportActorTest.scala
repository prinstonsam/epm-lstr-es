package actors

import java.time.Instant

import akka.typed._
import akka.util.Timeout
import akka.typed.AskPattern._
import dao._
import dto._
import dto.reports.EmploymentReport
import dto.typed.EmploymentWithType
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class EmploymentReportActorTest
    extends WordSpec
    with Matchers
    with BeforeAndAfterAll
    with ScalaFutures
    with MockitoSugar {

  import GuardianProtocol._

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val employmentDao       = mock[EmploymentDao]
  val employmentTypeDao   = mock[EmploymentTypeDao]
  val feedbackDao         = mock[FeedbackDao]
  val mentorDao           = mock[MentorDao]
  val iterationDao        = mock[IterationDao]
  val studentIterationDao = mock[StudentIterationDao]
  val studentDao          = mock[StudentDao]
  val studentTypeDao      = mock[StudentTypeDao]
  val userDao             = mock[UserDao]
  val resultTypeDao       = mock[ResultTypeDao]
  val studentWithDataDao  = mock[StudentWithDataDao]
  val skillDao            = mock[SkillDao]
  val studentDataDao      = mock[StudentDataDao]

  var system: ActorSystem[GuardianProtocol] = _

  def employmentReportActor: ActorRef[EmploymentReportProtocol] =
    (system ? GuardianProtocol.GetEmploymentReportActor).futureValue

  val guardian: Behavior[GuardianProtocol] = GuardianActor.guardian(
    employmentDao,
    employmentTypeDao,
    studentDao,
    studentTypeDao,
    studentIterationDao,
    feedbackDao,
    mentorDao,
    iterationDao,
    userDao,
    skillDao,
    resultTypeDao,
    studentDataDao,
    studentWithDataDao)

  val rootService: ActorRef[GuardianProtocol] =
    ActorSystem("guardian", Props(guardian))

  implicit val timeout = Timeout(5.seconds)

  private val awaitDuration: FiniteDuration = 5.seconds

  val theEmploymentReportService: ActorRef[EmploymentReportProtocol] =
    Await.result(rootService ? GetEmploymentReportActor, awaitDuration)

  override def beforeAll(): Unit = {
    super.beforeAll()
    system = ActorSystem("EmploymentReportActorTest", Props(guardian))
  }

  override def afterAll(): Unit = {
    val _ = system.terminate()
    super.afterAll()
  }

  val startDate = Instant.parse("2016-01-01T00:00:00.00Z")
  val endDate   = Instant.parse("2016-09-01T00:00:00.00Z")

  val startDate1 = Instant.parse("2016-01-01T00:00:00.00Z")
  val startDate2 = Instant.parse("2016-01-01T00:00:00.00Z")
  val startDate3 = Instant.parse("2016-01-01T00:00:00.00Z")
  val startDate4 = Instant.parse("2016-01-01T00:00:00.00Z")
  val startDate5 = Instant.parse("2016-01-01T00:00:00.00Z")

  val endDate1 = Instant.parse("2016-04-01T00:00:00.00Z")
  val endDate2 = Instant.parse("2016-05-01T00:00:00.00Z")
  val endDate3 = Instant.parse("2016-06-01T00:00:00.00Z")
  val endDate4 = Instant.parse("2016-08-01T00:00:00.00Z")
  val endDate5 = Instant.parse("2016-09-01T00:00:00.00Z")

  val employmentType = EmploymentType(Some(1), "Proj")
  val employment     = Employment(Some(1), "Project", 100, 1)

  val iteration1 = Iteration(Some(1), 1, 1, startDate1, endDate1)
  val iteration2 = Iteration(Some(2), 2, 1, startDate2, endDate2)
  val iteration3 = Iteration(Some(3), 3, 1, startDate3, endDate3)
  val iteration4 = Iteration(Some(4), 1, 1, startDate4, endDate4)
  val iteration5 = Iteration(Some(5), 1, 1, startDate5, endDate5)

  val iterations = Seq(iteration1, iteration2, iteration3, iteration4, iteration5)

  val studentIteration11 = StudentIteration(1, 1, 1)
  val studentIteration12 = StudentIteration(2, 1, 2)
  val studentIteration13 = StudentIteration(3, 1, 3)
  val studentIteration1  = Seq(studentIteration11, studentIteration12, studentIteration13)

  val studentIteration21 = StudentIteration(1, 2, 1)
  val studentIteration22 = StudentIteration(2, 2, 1)
  val studentIteration23 = StudentIteration(3, 2, 3)
  val studentIteration2  = Seq(studentIteration21, studentIteration22, studentIteration23)

  val studentIteration31 = StudentIteration(1, 3, 1)
  val studentIteration32 = StudentIteration(2, 3, 1)
  val studentIteration33 = StudentIteration(3, 3, 1)
  val studentIteration3  = Seq(studentIteration31, studentIteration32, studentIteration33)

  val studentIteration41 = StudentIteration(1, 4, 2)
  val studentIteration42 = StudentIteration(2, 4, 3)
  val studentIteration43 = StudentIteration(3, 4, 3)
  val studentIteration4  = Seq(studentIteration41, studentIteration42, studentIteration43)

  val studentIteration51 = StudentIteration(1, 5, 3)
  val studentIteration52 = StudentIteration(2, 5, 2)
  val studentIteration53 = StudentIteration(3, 5, 4)
  val studentIteration5  = Seq(studentIteration51, studentIteration52, studentIteration53)

  val resultType1 = ResultType(Some(1), "first")
  val resultType2 = ResultType(Some(2), "second")
  val resultType3 = ResultType(Some(3), "third")
  val resultType4 = ResultType(Some(4), "fourth")

  val employmentWithType = EmploymentWithType(employment, employmentType)
  val iterationCount     = 5
  val studentCount       = 15
  val studentResultMap   = Map(resultType1 -> 6, resultType2 -> 3, resultType3 -> 5, resultType4 -> 1)
  val feedbackCount      = 150

  val employmentReport =
    EmploymentReport(employmentWithType, iterationCount, studentCount, studentResultMap, feedbackCount)

  "EmploymentReport Service" when {
    "returns a report about employment" in {
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType)))
      when(iterationDao.findByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(iterations))

      //iteration count
      when(iterationDao.countByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(5))

      //student count
      when(studentDao.countByIterationId(1)).thenReturn(Future(1))
      when(studentDao.countByIterationId(2)).thenReturn(Future(2))
      when(studentDao.countByIterationId(3)).thenReturn(Future(3))
      when(studentDao.countByIterationId(4)).thenReturn(Future(4))
      when(studentDao.countByIterationId(5)).thenReturn(Future(5))

      //feedback count
      when(feedbackDao.countByIterationId(1)).thenReturn(Future(10))
      when(feedbackDao.countByIterationId(2)).thenReturn(Future(20))
      when(feedbackDao.countByIterationId(3)).thenReturn(Future(30))
      when(feedbackDao.countByIterationId(4)).thenReturn(Future(40))
      when(feedbackDao.countByIterationId(5)).thenReturn(Future(50))

      //result map
      when(iterationDao.findByEmploymentId(1)).thenReturn(Future(iterations))
      when(studentIterationDao.findByIterationId(1)).thenReturn(Future(studentIteration1))
      when(studentIterationDao.findByIterationId(2)).thenReturn(Future(studentIteration2))
      when(studentIterationDao.findByIterationId(3)).thenReturn(Future(studentIteration3))
      when(studentIterationDao.findByIterationId(4)).thenReturn(Future(studentIteration4))
      when(studentIterationDao.findByIterationId(5)).thenReturn(Future(studentIteration5))
      when(resultTypeDao.getById(1)).thenReturn(Future(Some(resultType1)))
      when(resultTypeDao.getById(2)).thenReturn(Future(Some(resultType2)))
      when(resultTypeDao.getById(3)).thenReturn(Future(Some(resultType3)))
      when(resultTypeDao.getById(4)).thenReturn(Future(Some(resultType4)))

      val eventualEmploymentReport =
        theEmploymentReportService ?
          EmploymentReportProtocol.GetEmploymentReport.m(employment.employmentTypeId, Some(startDate), Some(endDate))
      eventualEmploymentReport.futureValue shouldEqual Some(employmentReport)
    }

    "return None if Employment does not exist" in {
      when(employmentDao.getById(1)).thenReturn(Future(None))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType)))
      when(iterationDao.findByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(iterations))

      //iteration count
      when(iterationDao.countByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(5))

      //student count
      when(studentDao.countByIterationId(1)).thenReturn(Future(1))
      when(studentDao.countByIterationId(2)).thenReturn(Future(2))
      when(studentDao.countByIterationId(3)).thenReturn(Future(3))
      when(studentDao.countByIterationId(4)).thenReturn(Future(4))
      when(studentDao.countByIterationId(5)).thenReturn(Future(5))

      //feedback count
      when(feedbackDao.countByIterationId(1)).thenReturn(Future(10))
      when(feedbackDao.countByIterationId(2)).thenReturn(Future(20))
      when(feedbackDao.countByIterationId(3)).thenReturn(Future(30))
      when(feedbackDao.countByIterationId(4)).thenReturn(Future(40))
      when(feedbackDao.countByIterationId(5)).thenReturn(Future(50))

      //result map
      when(iterationDao.findByEmploymentId(1)).thenReturn(Future(iterations))
      when(studentIterationDao.findByIterationId(1)).thenReturn(Future(studentIteration1))
      when(studentIterationDao.findByIterationId(2)).thenReturn(Future(studentIteration2))
      when(studentIterationDao.findByIterationId(3)).thenReturn(Future(studentIteration3))
      when(studentIterationDao.findByIterationId(4)).thenReturn(Future(studentIteration4))
      when(studentIterationDao.findByIterationId(5)).thenReturn(Future(studentIteration5))
      when(resultTypeDao.getById(1)).thenReturn(Future(Some(resultType1)))
      when(resultTypeDao.getById(2)).thenReturn(Future(Some(resultType2)))
      when(resultTypeDao.getById(3)).thenReturn(Future(Some(resultType3)))
      when(resultTypeDao.getById(4)).thenReturn(Future(Some(resultType4)))

      val eventualEmploymentReport =
        theEmploymentReportService ?
          EmploymentReportProtocol.GetEmploymentReport.m(employment.employmentTypeId, Some(startDate), Some(endDate))
      eventualEmploymentReport.futureValue shouldEqual None
    }

    "return None if EmploymentType does not exist" in {
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(None))
      when(iterationDao.findByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(iterations))

      //iteration count
      when(iterationDao.countByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(5))

      //student count
      when(studentDao.countByIterationId(1)).thenReturn(Future(1))
      when(studentDao.countByIterationId(2)).thenReturn(Future(2))
      when(studentDao.countByIterationId(3)).thenReturn(Future(3))
      when(studentDao.countByIterationId(4)).thenReturn(Future(4))
      when(studentDao.countByIterationId(5)).thenReturn(Future(5))

      //feedback count
      when(feedbackDao.countByIterationId(1)).thenReturn(Future(10))
      when(feedbackDao.countByIterationId(2)).thenReturn(Future(20))
      when(feedbackDao.countByIterationId(3)).thenReturn(Future(30))
      when(feedbackDao.countByIterationId(4)).thenReturn(Future(40))
      when(feedbackDao.countByIterationId(5)).thenReturn(Future(50))

      //result map
      when(iterationDao.findByEmploymentId(1)).thenReturn(Future(iterations))
      when(studentIterationDao.findByIterationId(1)).thenReturn(Future(studentIteration1))
      when(studentIterationDao.findByIterationId(2)).thenReturn(Future(studentIteration2))
      when(studentIterationDao.findByIterationId(3)).thenReturn(Future(studentIteration3))
      when(studentIterationDao.findByIterationId(4)).thenReturn(Future(studentIteration4))
      when(studentIterationDao.findByIterationId(5)).thenReturn(Future(studentIteration5))
      when(resultTypeDao.getById(1)).thenReturn(Future(Some(resultType1)))
      when(resultTypeDao.getById(2)).thenReturn(Future(Some(resultType2)))
      when(resultTypeDao.getById(3)).thenReturn(Future(Some(resultType3)))
      when(resultTypeDao.getById(4)).thenReturn(Future(Some(resultType4)))

      val eventualEmploymentReport =
        theEmploymentReportService ?
          EmploymentReportProtocol.GetEmploymentReport.m(employment.employmentTypeId, Some(startDate), Some(endDate))
      eventualEmploymentReport.futureValue shouldEqual None
    }

    "return emplty report if Seq(Iteration) does not exist" in {
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType)))
      when(iterationDao.findByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(Seq()))

      //iteration count
      when(iterationDao.countByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(0))

      //student count
      when(studentDao.countByIterationId(1)).thenReturn(Future(1))
      when(studentDao.countByIterationId(2)).thenReturn(Future(2))
      when(studentDao.countByIterationId(3)).thenReturn(Future(3))
      when(studentDao.countByIterationId(4)).thenReturn(Future(4))
      when(studentDao.countByIterationId(5)).thenReturn(Future(5))

      //feedback count
      when(feedbackDao.countByIterationId(1)).thenReturn(Future(10))
      when(feedbackDao.countByIterationId(2)).thenReturn(Future(20))
      when(feedbackDao.countByIterationId(3)).thenReturn(Future(30))
      when(feedbackDao.countByIterationId(4)).thenReturn(Future(40))
      when(feedbackDao.countByIterationId(5)).thenReturn(Future(50))

      //result map
      when(iterationDao.findByEmploymentId(1)).thenReturn(Future(iterations))
      when(studentIterationDao.findByIterationId(1)).thenReturn(Future(studentIteration1))
      when(studentIterationDao.findByIterationId(2)).thenReturn(Future(studentIteration2))
      when(studentIterationDao.findByIterationId(3)).thenReturn(Future(studentIteration3))
      when(studentIterationDao.findByIterationId(4)).thenReturn(Future(studentIteration4))
      when(studentIterationDao.findByIterationId(5)).thenReturn(Future(studentIteration5))
      when(resultTypeDao.getById(1)).thenReturn(Future(None))
      when(resultTypeDao.getById(2)).thenReturn(Future(None))
      when(resultTypeDao.getById(3)).thenReturn(Future(None))
      when(resultTypeDao.getById(4)).thenReturn(Future(None))

      val eventualEmploymentReport =
        theEmploymentReportService ?
          EmploymentReportProtocol.GetEmploymentReport.m(employment.employmentTypeId, Some(startDate), Some(endDate))

      val employmentReportWithoutIteration =
        EmploymentReport(employmentWithType, 0, 0, Map(), 0)
      eventualEmploymentReport.futureValue shouldEqual Some(employmentReportWithoutIteration)
    }

    "return report without ResultType's that does not exist" in {
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType)))
      when(iterationDao.findByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(iterations))

      //iteration count
      when(iterationDao.countByEmploymentId(1, Some(startDate), Some(endDate))).thenReturn(Future(5))

      //student count
      when(studentDao.countByIterationId(1)).thenReturn(Future(1))
      when(studentDao.countByIterationId(2)).thenReturn(Future(2))
      when(studentDao.countByIterationId(3)).thenReturn(Future(3))
      when(studentDao.countByIterationId(4)).thenReturn(Future(4))
      when(studentDao.countByIterationId(5)).thenReturn(Future(5))

      //feedback count
      when(feedbackDao.countByIterationId(1)).thenReturn(Future(10))
      when(feedbackDao.countByIterationId(2)).thenReturn(Future(20))
      when(feedbackDao.countByIterationId(3)).thenReturn(Future(30))
      when(feedbackDao.countByIterationId(4)).thenReturn(Future(40))
      when(feedbackDao.countByIterationId(5)).thenReturn(Future(50))

      //result map
      when(iterationDao.findByEmploymentId(1)).thenReturn(Future(iterations))
      when(studentIterationDao.findByIterationId(1)).thenReturn(Future(studentIteration1))
      when(studentIterationDao.findByIterationId(2)).thenReturn(Future(studentIteration2))
      when(studentIterationDao.findByIterationId(3)).thenReturn(Future(studentIteration3))
      when(studentIterationDao.findByIterationId(4)).thenReturn(Future(studentIteration4))
      when(studentIterationDao.findByIterationId(5)).thenReturn(Future(studentIteration5))
      when(resultTypeDao.getById(1)).thenReturn(Future(None))
      when(resultTypeDao.getById(2)).thenReturn(Future(None))
      when(resultTypeDao.getById(3)).thenReturn(Future(Some(resultType3)))
      when(resultTypeDao.getById(4)).thenReturn(Future(Some(resultType4)))

      val eventualEmploymentReport =
        theEmploymentReportService ?
          EmploymentReportProtocol.GetEmploymentReport.m(employment.employmentTypeId, Some(startDate), Some(endDate))

      val resultTypeMap: Map[ResultType, Int] = Map(resultType3 -> 5, resultType4 -> 1)
      val employmentReportWithoutResultType =
        EmploymentReport(employmentWithType, 5, 15, resultTypeMap, 150)
      eventualEmploymentReport.futureValue shouldEqual Some(employmentReportWithoutResultType)
    }
  }
}
