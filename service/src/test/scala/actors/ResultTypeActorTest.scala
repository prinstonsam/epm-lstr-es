package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.ResultTypeDao
import dto.ResultType
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.ResultTypeProtocol
import service.protocols.ResultTypeProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class ResultTypeActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))
  implicit val timeout = Timeout(5.seconds)

  val resultTypeDao = mock[ResultTypeDao]

  var system: ActorSystem[ResultTypeProtocol] = _

  def resultTypeActor: ActorRef[ResultTypeProtocol] = system

  override def beforeAll(): Unit = {
    system = ActorSystem("MySystem", Props(ResultTypeActor.behavior(resultTypeDao = resultTypeDao)))
  }

  override def afterAll(): Unit = {
    val _ = system.terminate()
  }

  "ResultType Service" when {
    val resultType = ResultType(Some(1), "title")
    "get all - return list of resultTypes" in {
      when(resultTypeDao.findAll()).thenReturn(Future(Seq(resultType)))
      val eventualResultTypes: Future[Seq[ResultType]] = resultTypeActor ? FindAll
      eventualResultTypes.futureValue shouldEqual Seq(resultType)

    }

    "get all - return empty list" in {
      when(resultTypeDao.findAll()).thenReturn(Future(Seq()))
      val eventualResultTypes: Future[Seq[ResultType]] = resultTypeActor ? FindAll
      eventualResultTypes.futureValue shouldEqual Seq()
    }

    "get ResultType by ID - return value" in {
      when(resultTypeDao.getById(1)).thenReturn(Future(Option(resultType)))
      val eventualResultTypes = resultTypeActor.?[Option[ResultType]](GetById(1, _))
      eventualResultTypes.futureValue shouldEqual Some(resultType)
    }

    "get ResultType by ID - return None" in {
      when(resultTypeDao.getById(1)).thenReturn(Future(None))
      val eventualResultTypes = resultTypeActor.?[Option[ResultType]](GetById(1, _))
      eventualResultTypes.futureValue shouldEqual None
    }
    "create ResultType" in {
      when(resultTypeDao.insert(resultType)).thenReturn(Future(resultType))
      val eventualResultTypes = resultTypeActor.?[ResultType](Create(resultType, _))
      eventualResultTypes.futureValue shouldEqual resultType
    }

    "update ResultType - return value" in {
      when(resultTypeDao.update(resultType)).thenReturn(Future(Option(resultType)))
      val eventualResultTypes = resultTypeActor.?[Option[ResultType]](Update(resultType, _))
      eventualResultTypes.futureValue shouldEqual Some(resultType)
    }

    "update ResultType - return None" in {
      when(resultTypeDao.update(resultType)).thenReturn(Future(None))
      val eventualResultTypes = resultTypeActor.?[Option[ResultType]](Update(resultType, _))
      eventualResultTypes.futureValue shouldEqual None
    }

    "delete ResultType - return value" in {
      when(resultTypeDao.deleteById(1)).thenReturn(Future(Option(resultType)))
      val eventualResultTypes = resultTypeActor.?[Option[ResultType]](Delete(1, _))
      eventualResultTypes.futureValue shouldEqual Some(resultType)
    }

    "delete ResultType - return None" in {
      when(resultTypeDao.deleteById(1)).thenReturn(Future(None))
      val eventualResultTypes = resultTypeActor.?[Option[ResultType]](Delete(1, _))
      eventualResultTypes.futureValue shouldEqual None
    }
  }

}
