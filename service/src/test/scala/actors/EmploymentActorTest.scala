package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.EmploymentDao
import dto.Employment
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.EmploymentProtocol
import service.protocols.EmploymentProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class EmploymentActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val employmentDao                           = mock[EmploymentDao]
  var system: ActorSystem[EmploymentProtocol] = _

  def employmentActor: ActorRef[EmploymentProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(EmploymentActor.behavior(employmentDao = employmentDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  "Employment Service" when {
    val employment = Employment(Some(1), "first_employment", 1, 1)
    "find all - return list of employments" in {
      when(employmentDao.findAll()).thenReturn(Future(Seq(employment)))
      val eventualEmployments: Future[Seq[Employment]] = employmentActor ? FindAll
      eventualEmployments.futureValue shouldEqual Seq(employment)

    }

    "find all - return empty list" in {
      when(employmentDao.findAll()).thenReturn(Future(Seq()))
      val eventualEmployments: Future[Seq[Employment]] = employmentActor ? FindAll
      eventualEmployments.futureValue shouldEqual Seq()
    }

    "get Employment by ID - return value" in {
      when(employmentDao.getById(1)).thenReturn(Future(Option(employment)))
      val eventualEmployments = employmentActor.?[Option[Employment]](GetById(1, _))
      eventualEmployments.futureValue shouldEqual Some(employment)
    }

    "get Employment by ID - return None" in {
      when(employmentDao.getById(1)).thenReturn(Future(None))
      val eventualEmployments = employmentActor.?[Option[Employment]](GetById(1, _))
      eventualEmployments.futureValue shouldEqual None
    }

    "find Employment by type ID - return Value" in {
      when(employmentDao.findByTypeId(1)).thenReturn(Future(Seq(employment)))
      val eventualEmployments: Future[Seq[Employment]] = employmentActor.?(FindByTypeId(1, _))
      eventualEmployments.futureValue shouldEqual Seq(employment)
    }

    "find Employment by type ID - return empty collection" in {
      when(employmentDao.findByTypeId(-1)).thenReturn(Future(Seq()))
      val eventualEmployments: Future[Seq[Employment]] = employmentActor.?(FindByTypeId(-1, _))
      eventualEmployments.futureValue shouldEqual Seq()
    }

    "insert Employment" in {
      when(employmentDao.insert(employment)).thenReturn(Future(employment))
      val eventualEmployments = employmentActor ? Create.m(employment)
      eventualEmployments.futureValue shouldEqual employment
    }

    "update Employment - return value" in {
      when(employmentDao.update(employment)).thenReturn(Future(Option(employment)))
      val eventualEmployments = employmentActor.?[Option[Employment]](Update(employment, _))
      eventualEmployments.futureValue shouldEqual Some(employment)
    }

    "update Employment - return None" in {
      when(employmentDao.update(employment)).thenReturn(Future(None))
      val eventualEmployments = employmentActor.?[Option[Employment]](Update(employment, _))
      eventualEmployments.futureValue shouldEqual None
    }

    "delete Employment - return value" in {
      when(employmentDao.deleteById(1)).thenReturn(Future(Option(employment)))
      val eventualEmployments = employmentActor.?[Option[Employment]](Delete(1, _))
      eventualEmployments.futureValue shouldEqual Some(employment)
    }

    "delete Employment - return None" in {
      when(employmentDao.deleteById(1)).thenReturn(Future(None))
      val eventualEmployments = employmentActor.?[Option[Employment]](Delete(1, _))
      eventualEmployments.futureValue shouldEqual None
    }
  }
}
