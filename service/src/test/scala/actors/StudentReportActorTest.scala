package actors

import java.time._

import akka.typed.AskPattern._
import akka.typed.{ActorRef, ActorSystem, Behavior, Props}
import akka.util.Timeout
import dao._
import dto._
import dto.reports.{DetailedIteration, StudentReport}
import dto.typed.{EmploymentWithType, StudentWithType}
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.GuardianProtocol._
import service.protocols.{GuardianProtocol, StudentReportProtocol}
import service.protocols.StudentReportProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class StudentReportActorTest
    extends WordSpec
    with Matchers
    with BeforeAndAfterAll
    with ScalaFutures
    with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val skillDao            = mock[SkillDao]
  val employmentDao       = mock[EmploymentDao]
  val employmentTypeDao   = mock[EmploymentTypeDao]
  val feedbackDao         = mock[FeedbackDao]
  val mentorDao           = mock[MentorDao]
  val iterationDao        = mock[IterationDao]
  val studentIterationDao = mock[StudentIterationDao]
  val studentDao          = mock[StudentDao]
  val studentWithDataDao  = mock[StudentWithDataDao]
  val studentDataDao      = mock[StudentDataDao]
  val studentTypeDao      = mock[StudentTypeDao]
  val userDao             = mock[UserDao]
  val resultTypeDao       = mock[ResultTypeDao]

  var system: ActorSystem[GuardianProtocol] = _

  def studentReportActor: ActorRef[StudentReportProtocol] =
    (system ? GuardianProtocol.GetStudentReportActor).futureValue

  val guardian: Behavior[GuardianProtocol] = GuardianActor.guardian(
    employmentDao,
    employmentTypeDao,
    studentDao,
    studentTypeDao,
    studentIterationDao,
    feedbackDao,
    mentorDao,
    iterationDao,
    userDao,
    skillDao,
    resultTypeDao,
    studentDataDao,
    studentWithDataDao)

  val rootService: ActorRef[GuardianProtocol] =
    ActorSystem("guardian", Props(guardian))

  implicit val timeout = Timeout(5.seconds)

  private val awaitDuration: FiniteDuration = 5.seconds

  val theStudentReportService: ActorRef[StudentReportProtocol] =
    (rootService ? GetStudentReportActor).futureValue

  override def beforeAll() = {
    super.beforeAll()
    system = ActorSystem("StudentReportActorTest", Props(guardian))

  }

  override def afterAll() = {
    val _ = system.terminate()
    super.afterAll()
  }

  val startDate1 = Instant.parse("2016-01-01T00:00:00.00Z")

  val startDate2 = Instant.parse("2016-01-01T00:00:00.00Z")
  val startDate3 = Instant.parse("2016-01-01T00:00:00.00Z")
  val startDate4 = Instant.parse("2016-01-01T00:00:00.00Z")
  val startDate5 = Instant.parse("2016-01-01T00:00:00.00Z")
  val endDate1   = Instant.parse("2016-04-01T00:00:00.00Z")

  val endDate2 = Instant.parse("2016-05-01T00:00:00.00Z")
  val endDate3 = Instant.parse("2016-06-01T00:00:00.00Z")
  val endDate4 = Instant.parse("2016-08-01T00:00:00.00Z")
  val endDate5 = Instant.parse("2016-09-01T00:00:00.00Z")

  val studentId       = 1
  val student         = Student(Some(1), "John", "Snow", "john_snow@epam.com")
  val studentType     = StudentType(Some(1), "student")
  val studentData     = StudentData(studentId, 1, 1, startDate1, Some(endDate5))
  val studentWithData = StudentWithData(student, studentData)

  val iteration1 = Iteration(Some(1), 1, 1, startDate1, endDate1)
  val iteration2 = Iteration(Some(2), 2, 1, startDate2, endDate2)
  val iteration3 = Iteration(Some(3), 3, 1, startDate3, endDate3)

  val employment1 = Employment(Some(1), "Java Basics", 3, 1)
  val employment2 = Employment(Some(2), "Scala Basic", 3, 1)
  val employment3 = Employment(Some(3), "Factory Internship", 3, 2)

  val employmentType1 = EmploymentType(Some(1), "Education")
  val employmentType2 = EmploymentType(Some(2), "Factory")

  val studentIteration1 = StudentIteration(1, 1, 5)
  val studentIteration2 = StudentIteration(1, 2, 4)
  val studentIteration3 = StudentIteration(1, 3, 3)
  val studentIteration4 = StudentIteration(1, 4, 2)
  val studentIteration5 = StudentIteration(1, 5, 1)

  val feedbackCount = 4

  val skill1 = Skill(Some(1), "Java")
  val skill2 = Skill(Some(2), "Scala")
  val skill3 = Skill(Some(3), "English")

  val iterations  = Seq(iteration1, iteration2, iteration3)
  val employments = Seq(employment1, employment2, employment3)
  val studentIterations =
    Seq(studentIteration1, studentIteration2, studentIteration3, studentIteration4, studentIteration5)
  val skills = Seq(skill1, skill2, skill3)

  val studentWithType     = StudentWithType(student, studentType)
  val employmentWithType1 = EmploymentWithType(employment1, employmentType1)
  val employmentWithType2 = EmploymentWithType(employment2, employmentType1)
  val employmentWithType3 = EmploymentWithType(employment3, employmentType2)

  val detailedIteration1 = DetailedIteration(iteration1, employmentWithType1)
  val detailedIteration2 = DetailedIteration(iteration2, employmentWithType2)
  val detailedIteration3 = DetailedIteration(iteration3, employmentWithType3)

  val detailedIterations = IndexedSeq(detailedIteration1, detailedIteration2, detailedIteration3)

  val studentReport = StudentReport(studentWithType, detailedIterations, feedbackCount, skills.toIndexedSeq)

  "StudentReport Service" when {
    "returns a report about student" in {
      when(studentDao.getById(studentId)).thenReturn(Future(Option(student)))
      when(studentTypeDao.getById(studentData.studentTypeId)).thenReturn(Future(Option(studentType)))
      when(skillDao.findByStudentId(studentId)).thenReturn(Future(skills))
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment1)))
      when(employmentDao.getById(2)).thenReturn(Future(Some(employment2)))
      when(employmentDao.getById(3)).thenReturn(Future(Some(employment3)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType1)))
      when(employmentTypeDao.getById(2)).thenReturn(Future(Some(employmentType2)))
      when(iterationDao.findByStudentId(studentId)).thenReturn(Future(iterations))
      when(studentWithDataDao.getByStudentId(studentId, None)).thenReturn(Future(Option(studentWithData)))
      when(feedbackDao.countByStudentId(studentId)).thenReturn(Future(feedbackCount))

      val eventualStudentReport: Future[Option[StudentReport]] = studentReportActor ? (GetStudentReport(
          studentId,
          None,
          None,
          _))
      eventualStudentReport.futureValue shouldEqual Some(studentReport)
    }

    "doesn't return a report about student - no student in DB" in {
      when(studentDao.getById(studentId)).thenReturn(Future(None))
      when(studentTypeDao.getById(studentData.studentTypeId)).thenReturn(Future(Option(studentType)))
      when(skillDao.findByStudentId(studentId)).thenReturn(Future(skills))
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment1)))
      when(employmentDao.getById(2)).thenReturn(Future(Some(employment2)))
      when(employmentDao.getById(3)).thenReturn(Future(Some(employment3)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType1)))
      when(employmentTypeDao.getById(2)).thenReturn(Future(Some(employmentType2)))
      when(iterationDao.findByStudentId(studentId)).thenReturn(Future(iterations))
      when(studentWithDataDao.getByStudentId(studentId, None)).thenReturn(Future(Option(studentWithData)))
      when(feedbackDao.countByStudentId(studentId)).thenReturn(Future(feedbackCount))

      val eventualStudentReport: Future[Option[StudentReport]] = studentReportActor ? (GetStudentReport(
          studentId,
          None,
          None,
          _))
      eventualStudentReport.futureValue shouldEqual None
    }

    "doesn't return a report about student - no studentType in DB" in {
      when(studentDao.getById(studentId)).thenReturn(Future(Option(student)))
      when(studentTypeDao.getById(studentData.studentTypeId)).thenReturn(Future(None))
      when(skillDao.findByStudentId(studentId)).thenReturn(Future(skills))
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment1)))
      when(employmentDao.getById(2)).thenReturn(Future(Some(employment2)))
      when(employmentDao.getById(3)).thenReturn(Future(Some(employment3)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType1)))
      when(employmentTypeDao.getById(2)).thenReturn(Future(Some(employmentType2)))
      when(iterationDao.findByStudentId(studentId)).thenReturn(Future(iterations))
      when(studentWithDataDao.getByStudentId(studentId, None)).thenReturn(Future(Option(studentWithData)))
      when(feedbackDao.countByStudentId(studentId)).thenReturn(Future(feedbackCount))

      val eventualStudentReport: Future[Option[StudentReport]] = studentReportActor ? (GetStudentReport(
          studentId,
          None,
          None,
          _))
      eventualStudentReport.futureValue shouldEqual None
    }

    "doesn't return a report about student - no employmentType in DB" in {
      when(studentDao.getById(studentId)).thenReturn(Future(Option(student)))
      when(studentTypeDao.getById(studentData.studentTypeId)).thenReturn(Future(Option(studentType)))
      when(skillDao.findByStudentId(studentId)).thenReturn(Future(skills))
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment1)))
      when(employmentDao.getById(2)).thenReturn(Future(Some(employment2)))
      when(employmentDao.getById(3)).thenReturn(Future(Some(employment3)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(None))
      when(employmentTypeDao.getById(2)).thenReturn(Future(None))
      when(iterationDao.findByStudentId(studentId)).thenReturn(Future(iterations))
      when(studentWithDataDao.getByStudentId(studentId, None)).thenReturn(Future(Option(studentWithData)))
      when(feedbackDao.countByStudentId(studentId)).thenReturn(Future(feedbackCount))

      val eventualStudentReport: Future[Option[StudentReport]] = studentReportActor ? (GetStudentReport(
          studentId,
          None,
          None,
          _))
      val studentReport = StudentReport(studentWithType, IndexedSeq(), feedbackCount, skills.toIndexedSeq)
      eventualStudentReport.futureValue shouldEqual Some(studentReport)
    }

    "doesn't return a report about student - no employment in DB" in {
      when(studentDao.getById(studentId)).thenReturn(Future(Option(student)))
      when(studentTypeDao.getById(studentData.studentTypeId)).thenReturn(Future(Option(studentType)))
      when(skillDao.findByStudentId(studentId)).thenReturn(Future(skills))
      when(employmentDao.getById(1)).thenReturn(Future(None))
      when(employmentDao.getById(2)).thenReturn(Future(None))
      when(employmentDao.getById(3)).thenReturn(Future(None))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType1)))
      when(employmentTypeDao.getById(2)).thenReturn(Future(Some(employmentType2)))
      when(iterationDao.findByStudentId(studentId)).thenReturn(Future(iterations))
      when(studentWithDataDao.getByStudentId(studentId, None)).thenReturn(Future(Option(studentWithData)))
      when(feedbackDao.countByStudentId(studentId)).thenReturn(Future(feedbackCount))

      val eventualStudentReport: Future[Option[StudentReport]] = studentReportActor ? (GetStudentReport(
          studentId,
          None,
          None,
          _))
      val studentReport = StudentReport(studentWithType, IndexedSeq(), feedbackCount, skills.toIndexedSeq)
      eventualStudentReport.futureValue shouldEqual Some(studentReport)
    }

    "doesn't return a report about student - no empty iteration seq in DB" in {
      when(studentDao.getById(studentId)).thenReturn(Future(Option(student)))
      when(studentTypeDao.getById(studentData.studentTypeId)).thenReturn(Future(Option(studentType)))
      when(skillDao.findByStudentId(studentId)).thenReturn(Future(skills))
      when(employmentDao.getById(1)).thenReturn(Future(Some(employment1)))
      when(employmentDao.getById(2)).thenReturn(Future(Some(employment2)))
      when(employmentDao.getById(3)).thenReturn(Future(Some(employment3)))
      when(employmentTypeDao.getById(1)).thenReturn(Future(Some(employmentType1)))
      when(employmentTypeDao.getById(2)).thenReturn(Future(Some(employmentType2)))
      when(iterationDao.findByStudentId(studentId)).thenReturn(Future(Seq()))
      when(studentWithDataDao.getByStudentId(studentId, None)).thenReturn(Future(Option(studentWithData)))
      when(feedbackDao.countByStudentId(studentId)).thenReturn(Future(feedbackCount))

      val eventualStudentReport: Future[Option[StudentReport]] = studentReportActor ? (GetStudentReport(
          studentId,
          None,
          None,
          _))
      val studentReport = StudentReport(studentWithType, IndexedSeq(), feedbackCount, skills.toIndexedSeq)
      eventualStudentReport.futureValue shouldEqual Some(studentReport)
    }
  }
}
