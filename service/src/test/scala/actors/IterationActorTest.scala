package actors

import java.time.ZonedDateTime

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.IterationDao
import dto.Iteration
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.IterationProtocol
import service.protocols.IterationProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class IterationActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val iterationDao                           = mock[IterationDao]
  var system: ActorSystem[IterationProtocol] = _

  def iterationActor: ActorRef[IterationProtocol] = system
  implicit val timeout                            = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(IterationActor.behavior(iterationDao = iterationDao)))
  }

  override def afterAll(): Unit = {
    val _ = system.terminate()
  }

  "Iteration Service" when {
    val iteration = Iteration(Some(1), 1, 1, ZonedDateTime.now().toInstant, ZonedDateTime.now().plusYears(1).toInstant)
    "find all - return list of iterations" in {
      when(iterationDao.findAll(None, None)).thenReturn(Future(Seq(iteration)))
      val eventualIterations: Future[Seq[Iteration]] = iterationActor ? FindAll.m(None, None)
      eventualIterations.futureValue shouldEqual List(iteration)

    }

    "find all - return empty list" in {
      when(iterationDao.findAll(None, None)).thenReturn(Future(Seq()))
      val eventualIterations: Future[Seq[Iteration]] = iterationActor ? FindAll.m(None, None)
      eventualIterations.futureValue shouldEqual Seq()
    }

    "get Iteration by ID - return value" in {
      when(iterationDao.getById(1)).thenReturn(Future(Some(iteration)))
      val eventualIterations = iterationActor.?[Option[Iteration]](GetById(1, _))
      eventualIterations.futureValue shouldEqual Some(iteration)
    }

    "get Iteration by ID - return None" in {
      when(iterationDao.getById(1)).thenReturn(Future(None))
      val eventualIterations = iterationActor.?[Option[Iteration]](GetById(1, _))
      eventualIterations.futureValue shouldEqual None
    }

    val startDate = ZonedDateTime.now().minusYears(1).toInstant
    val endDate   = ZonedDateTime.now().plusYears(1).toInstant

    "find Iteration by Employment ID - return value" in {
      when(iterationDao.findByEmploymentId(1, Option(startDate), Option(endDate))).thenReturn(Future(Seq(iteration)))
      val eventualIterations: Future[Seq[Iteration]] =
        iterationActor.?(FindByEmploymentId(1, Option(startDate), Option(endDate), _))
      eventualIterations.futureValue shouldEqual Seq(iteration)
    }

    "find Iteration by Employment ID - return empty collection" in {
      when(iterationDao.findByEmploymentId(-1, Option(startDate), Option(endDate))).thenReturn(Future(Seq()))
      val eventualIterations: Future[Seq[Iteration]] =
        iterationActor.?(FindByEmploymentId(-1, Option(startDate), Option(endDate), _))
      eventualIterations.futureValue shouldEqual Seq()
    }

    "find Iteration by Mentor ID - return value" in {
      when(iterationDao.findByMentorId(1)).thenReturn(Future(Seq(iteration)))
      val eventualIterations: Future[Seq[Iteration]] = iterationActor.?(FindByMentorId(1, _))
      eventualIterations.futureValue shouldEqual Seq(iteration)
    }

    "find Iteration by Mentor ID - return empty collection" in {
      when(iterationDao.findByMentorId(-1)).thenReturn(Future(Seq()))
      val eventualIterations: Future[Seq[Iteration]] = iterationActor.?(FindByMentorId(-1, _))
      eventualIterations.futureValue shouldEqual Seq()
    }

    "find Iteration by Student ID - return value" in {
      when(iterationDao.findByStudentId(1)).thenReturn(Future(Seq(iteration)))
      val eventualIterations: Future[Seq[Iteration]] = iterationActor.?(FindByStudentId(1, _))
      eventualIterations.futureValue shouldEqual Seq(iteration)
    }

    "find Iteration by Student ID - return empty collection" in {
      when(iterationDao.findByStudentId(-1)).thenReturn(Future(Seq()))
      val eventualIterations: Future[Seq[Iteration]] = iterationActor.?(FindByStudentId(-1, _))
      eventualIterations.futureValue shouldEqual Seq()
    }

    "create Iteration" in {
      when(iterationDao.insert(iteration)).thenReturn(Future(iteration))
      val eventualIterations = iterationActor.?[Iteration](Create(iteration, _))
      eventualIterations.futureValue shouldEqual iteration
    }

    "update Iteration - return value" in {
      when(iterationDao.update(iteration)).thenReturn(Future(Some(iteration)))
      val eventualIterations = iterationActor.?[Option[Iteration]](Update(iteration, _))
      eventualIterations.futureValue shouldEqual Some(iteration)
    }

    "update Iteration - return None" in {
      when(iterationDao.update(iteration)).thenReturn(Future(None))
      val eventualIterations = iterationActor.?[Option[Iteration]](Update(iteration, _))
      eventualIterations.futureValue shouldEqual None
    }

    "delete Iteration - return value" in {
      when(iterationDao.deleteById(1)).thenReturn(Future(Some(iteration)))
      val eventualIterations = iterationActor.?[Option[Iteration]](Delete(1, _))
      eventualIterations.futureValue shouldEqual Some(iteration)
    }

    "delete Iteration - return None" in {
      when(iterationDao.deleteById(1)).thenReturn(Future(None))
      val eventualIterations = iterationActor.?[Option[Iteration]](Delete(1, _))
      eventualIterations.futureValue shouldEqual None
    }
  }

}
