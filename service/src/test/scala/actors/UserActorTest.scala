package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.UserDao
import dto.User
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.UserProtocol
import service.protocols.UserProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class UserActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val userDao                           = mock[UserDao]
  var system: ActorSystem[UserProtocol] = _

  def userActor: ActorRef[UserProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(UserActor.behavior(userDao = userDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  "User Service" when {
    val user = User(Some(1), "firstName", "lastName", "email@epam.com", "qwerty")
    "find all - return list of users" in {
      when(userDao.findAll()).thenReturn(Future(Seq(user)))
      val eventualUsers: Future[Seq[User]] = userActor ? FindAll
      eventualUsers.futureValue shouldEqual Seq(user)

    }

    "find all - return empty list" in {
      when(userDao.findAll()).thenReturn(Future(Seq()))
      val eventualUsers: Future[Seq[User]] = userActor ? FindAll
      eventualUsers.futureValue shouldEqual Seq()
    }

    "get User by ID - return value" in {
      when(userDao.getById(1)).thenReturn(Future(Some(user)))
      val eventualUsers = userActor.?[Option[User]](GetById(1, _))
      eventualUsers.futureValue shouldEqual Some(user)
    }

    "get User by ID - return None" in {
      when(userDao.getById(1)).thenReturn(Future(None))
      val eventualUsers = userActor.?[Option[User]](GetById(1, _))
      eventualUsers.futureValue shouldEqual None
    }

    "get User by email - return value" in {
      when(userDao.getByEmail("email@epam.com")).thenReturn(Future(Some(user)))
      val eventualUsers = userActor.?[Option[User]](GetByEmail("email@epam.com", _))
      eventualUsers.futureValue shouldEqual Some(user)
    }

    "get User by email - return None" in {
      when(userDao.getByEmail("email@epam.com")).thenReturn(Future(None))
      val eventualUsers = userActor.?[Option[User]](GetByEmail("email@epam.com", _))
      eventualUsers.futureValue shouldEqual None
    }

    "create User" in {
      when(userDao.insert(user)).thenReturn(Future(user))
      val eventualUsers = userActor.?[User](Create(user, _))
      eventualUsers.futureValue shouldEqual user
    }

    "update User - return value" in {
      when(userDao.update(user)).thenReturn(Future(Some(user)))
      val eventualUsers = userActor.?[Option[User]](Update(user, _))
      eventualUsers.futureValue shouldEqual Some(user)
    }

    "update User - return None" in {
      when(userDao.update(user)).thenReturn(Future(None))
      val eventualUsers = userActor.?[Option[User]](Update(user, _))
      eventualUsers.futureValue shouldEqual None
    }

    "delete User - return value" in {
      when(userDao.deleteByID(1)).thenReturn(Future(Some(user)))
      val eventualUsers = userActor.?[Option[User]](Delete(1, _))
      eventualUsers.futureValue shouldEqual Some(user)
    }

    "delete User - return None" in {
      when(userDao.deleteByID(1)).thenReturn(Future(None))
      val eventualUsers = userActor.?[Option[User]](Delete(1, _))
      eventualUsers.futureValue shouldEqual None
    }
  }
}
