package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.StudentIterationDao
import dto.StudentIteration
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.StudentIterationProtocol
import service.protocols.StudentIterationProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class StudentIterationTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val studentIterationDao                           = mock[StudentIterationDao]
  var system: ActorSystem[StudentIterationProtocol] = _

  def studentIterationActor: ActorRef[StudentIterationProtocol] = system
  implicit val timeout                                          = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(StudentIterationActor.behavior(studentIterationDao = studentIterationDao)))
  }

  override def afterAll(): Unit = {
    val _ = system.terminate()
  }

  "StudentIteration Service" when {
    val studentIteration = StudentIteration(1, 1, 1)

    "find all - return list of studentIterations" in {
      when(studentIterationDao.findAll()).thenReturn(Future(Seq(studentIteration)))

      val eventualIterations: Future[Seq[StudentIteration]] = studentIterationActor ? FindAll

      eventualIterations.futureValue shouldEqual List(studentIteration)

    }

    "find all - return empty list" in {
      when(studentIterationDao.findAll()).thenReturn(Future(Seq()))

      val eventualIterations: Future[Seq[StudentIteration]] = studentIterationActor ? FindAll

      eventualIterations.futureValue shouldEqual Seq()
    }

    "get StudentIteration by ID - return value" in {
      when(studentIterationDao.getById(studentIteration.iterationId, studentIteration.studentId))
        .thenReturn(Future(Some(studentIteration)))

      val eventualIterations = studentIterationActor.?[Option[StudentIteration]](
        GetById(studentIteration.iterationId, studentIteration.studentId, _))

      eventualIterations.futureValue shouldEqual Some(studentIteration)
    }

    "get StudentIteration by ID - return None" in {
      when(studentIterationDao.getById(studentIteration.iterationId, studentIteration.studentId))
        .thenReturn(Future(None))

      val eventualIterations = studentIterationActor.?[Option[StudentIteration]](
        GetById(studentIteration.iterationId, studentIteration.studentId, _))

      eventualIterations.futureValue shouldEqual None
    }

    "find StudentIteration by Iteration ID - return value" in {
      when(studentIterationDao.findByIterationId(1)).thenReturn(Future(Seq(studentIteration)))

      val eventualIterations: Future[Seq[StudentIteration]] =
        studentIterationActor.?(FindByIterationId(1, _))

      eventualIterations.futureValue shouldEqual Seq(studentIteration)
    }

    "find StudentIteration by Iteration ID - return empty collection" in {
      when(studentIterationDao.findByIterationId(-1)).thenReturn(Future(Seq()))

      val eventualIterations: Future[Seq[StudentIteration]] =
        studentIterationActor.?(FindByIterationId(-1, _))

      eventualIterations.futureValue shouldEqual Seq()
    }

    "find StudentIteration by Student ID - return value" in {
      when(studentIterationDao.findByStudentId(1)).thenReturn(Future(Seq(studentIteration)))

      val eventualIterations: Future[Seq[StudentIteration]] = studentIterationActor.?(FindByStudentId(1, _))

      eventualIterations.futureValue shouldEqual Seq(studentIteration)
    }

    "find StudentIteration by Student ID - return empty collection" in {
      when(studentIterationDao.findByStudentId(-1)).thenReturn(Future(Seq()))

      val eventualIterations: Future[Seq[StudentIteration]] = studentIterationActor.?(FindByStudentId(-1, _))

      eventualIterations.futureValue shouldEqual Seq()
    }

    "create Iteration" in {
      when(studentIterationDao.insert(studentIteration)).thenReturn(Future(studentIteration))

      val eventualIterations = studentIterationActor.?[StudentIteration](Create(studentIteration, _))

      eventualIterations.futureValue shouldEqual studentIteration
    }

    "update Iteration - return value" in {
      when(studentIterationDao.update(studentIteration)).thenReturn(Future(Some(studentIteration)))

      val eventualIterations = studentIterationActor.?[Option[StudentIteration]](Update(studentIteration, _))

      eventualIterations.futureValue shouldEqual Some(studentIteration)
    }

    "update Iteration - return None" in {
      when(studentIterationDao.update(studentIteration)).thenReturn(Future(None))

      val eventualIterations = studentIterationActor.?[Option[StudentIteration]](Update(studentIteration, _))

      eventualIterations.futureValue shouldEqual None
    }

    "delete Iteration - return value" in {
      when(studentIterationDao.delete(studentIteration.iterationId, studentIteration.studentId))
        .thenReturn(Future(Some(studentIteration)))

      val eventualIterations = studentIterationActor.?[Option[StudentIteration]](
        Delete(studentIteration.iterationId, studentIteration.studentId, _))

      eventualIterations.futureValue shouldEqual Some(studentIteration)
    }

    "delete Iteration - return None" in {
      when(studentIterationDao.delete(studentIteration.iterationId, studentIteration.studentId))
        .thenReturn(Future(None))

      val eventualIterations = studentIterationActor.?[Option[StudentIteration]](
        Delete(studentIteration.iterationId, studentIteration.studentId, _))

      eventualIterations.futureValue shouldEqual None
    }
  }
}
