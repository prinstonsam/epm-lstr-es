package actors

import akka.typed.AskPattern._
import akka.typed._
import akka.util.Timeout
import dao.SkillDao
import dto.Skill
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import service.protocols.SkillProtocol
import service.protocols.SkillProtocol._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class SkillActorTest extends WordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with MockitoSugar {

  implicit val defaultPatience =
    PatienceConfig(Span(5, Seconds), Span(50, Millis))

  val skillDao                           = mock[SkillDao]
  var system: ActorSystem[SkillProtocol] = _

  def skillActor: ActorRef[SkillProtocol] = system

  implicit val timeout = Timeout(5.seconds)

  override def beforeAll() = {
    system = ActorSystem("MySystem", Props(SkillActor.behavior(skillDao = skillDao)))
  }

  override def afterAll() = {
    val _ = system.terminate()
  }

  "orm.Skill Service" when {
    val skill = Skill(Some(1), "title")
    "get all - return list of skills" in {
      when(skillDao.findAll()).thenReturn(Future(Seq(skill)))
      val eventualSkills: Future[Seq[Skill]] = skillActor ? FindAll
      eventualSkills.futureValue shouldEqual Seq(skill)

    }

    "get all - return empty list" in {
      when(skillDao.findAll()).thenReturn(Future(Seq()))
      val eventualSkills: Future[Seq[Skill]] = skillActor ? FindAll
      eventualSkills.futureValue shouldEqual Seq()
    }

    "get orm.Skill by ID - return value" in {
      when(skillDao.getById(1)).thenReturn(Future(Option(skill)))
      val eventualSkills = skillActor.?[Option[Skill]](GetById(1, _))
      eventualSkills.futureValue shouldEqual Some(skill)
    }

    "get orm.Skill by ID - return None" in {
      when(skillDao.getById(1)).thenReturn(Future(None))
      val eventualSkills = skillActor.?[Option[Skill]](GetById(1, _))
      eventualSkills.futureValue shouldEqual None
    }
    "create orm.Skill" in {
      when(skillDao.insert(skill)).thenReturn(Future(skill))
      val eventualSkills = skillActor.?[Skill](Create(skill, _))
      eventualSkills.futureValue shouldEqual skill
    }

    "update orm.Skill - return value" in {
      when(skillDao.update(skill)).thenReturn(Future(Option(skill)))
      val eventualSkills = skillActor.?[Option[Skill]](Update(skill, _))
      eventualSkills.futureValue shouldEqual Some(skill)
    }

    "update orm.Skill - return None" in {
      when(skillDao.update(skill)).thenReturn(Future(None))
      val eventualSkills = skillActor.?[Option[Skill]](Update(skill, _))
      eventualSkills.futureValue shouldEqual None
    }

    "delete orm.Skill - return value" in {
      when(skillDao.deleteById(1)).thenReturn(Future(Option(skill)))
      val eventualSkills = skillActor.?[Option[Skill]](Delete(1, _))
      eventualSkills.futureValue shouldEqual Some(skill)
    }

    "delete orm.Skill - return None" in {
      when(skillDao.deleteById(1)).thenReturn(Future(None))
      val eventualSkills = skillActor.?[Option[Skill]](Delete(1, _))
      eventualSkills.futureValue shouldEqual None
    }
  }

}
