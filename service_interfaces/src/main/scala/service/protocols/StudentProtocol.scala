package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.Student

sealed trait StudentProtocol

object StudentProtocol {

  @message case class Create(student: Student, sender: ActorRef[Student]) extends StudentProtocol

  @message case class Update(student: Student, sender: ActorRef[Option[Student]]) extends StudentProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[Student]]) extends StudentProtocol

  @message case class FindByIterationId(iterationId: Int, sender: ActorRef[Seq[Student]]) extends StudentProtocol

  @message case class CountByIterationId(iterationId: Int, sender: ActorRef[Int]) extends StudentProtocol

  case class FindAll(sender: ActorRef[Seq[Student]]) extends StudentProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[Student]]) extends StudentProtocol

}
