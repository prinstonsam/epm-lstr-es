package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.Mentor

sealed trait MentorProtocol

object MentorProtocol {

  @message case class Create(mentor: Mentor, sender: ActorRef[Mentor]) extends MentorProtocol

  @message case class Update(mentor: Mentor, sender: ActorRef[Option[Mentor]]) extends MentorProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[Mentor]]) extends MentorProtocol

  case class FindAll(sender: ActorRef[Seq[Mentor]]) extends MentorProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[Mentor]]) extends MentorProtocol
}
