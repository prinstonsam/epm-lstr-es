package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.User

sealed trait UserProtocol

object UserProtocol {

  @message case class Create(user: User, sender: ActorRef[User]) extends UserProtocol

  @message case class Update(user: User, sender: ActorRef[Option[User]]) extends UserProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[User]]) extends UserProtocol

  @message case class GetByEmail(email: String, sender: ActorRef[Option[User]]) extends UserProtocol

  case class FindAll(sender: ActorRef[Seq[User]]) extends UserProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[User]]) extends UserProtocol

}
