package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.EmploymentType

sealed trait EmploymentTypeProtocol

object EmploymentTypeProtocol {

  @message case class Create(employmentType: EmploymentType, sender: ActorRef[EmploymentType]) extends EmploymentTypeProtocol

  @message case class Update(employmentType: EmploymentType, sender: ActorRef[Option[EmploymentType]])
      extends EmploymentTypeProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[EmploymentType]]) extends EmploymentTypeProtocol

  case class FindAll(sender: ActorRef[Seq[EmploymentType]]) extends EmploymentTypeProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[EmploymentType]]) extends EmploymentTypeProtocol

}
