package service.protocols

import java.time.Instant

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.reports.EmploymentReport

sealed trait EmploymentReportProtocol

object EmploymentReportProtocol {

  @message case class GetEmploymentReport(
      employmentId: Int,
      startDate: Option[Instant] = None,
      endDate: Option[Instant] = None,
      sender: ActorRef[Option[EmploymentReport]]
  ) extends EmploymentReportProtocol
}
