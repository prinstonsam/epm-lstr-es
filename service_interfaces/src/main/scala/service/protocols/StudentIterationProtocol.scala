package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.StudentIteration

sealed trait StudentIterationProtocol

object StudentIterationProtocol {

  @message case class Create(studentIteration: StudentIteration, sender: ActorRef[StudentIteration])
      extends StudentIterationProtocol

  @message case class Update(studentIteration: StudentIteration, sender: ActorRef[Option[StudentIteration]])
      extends StudentIterationProtocol

  @message case class GetById(iterationId: Int, studentId: Int, sender: ActorRef[Option[StudentIteration]])
      extends StudentIterationProtocol

  case class FindAll(sender: ActorRef[Seq[StudentIteration]]) extends StudentIterationProtocol

  @message case class FindByIterationId(iterationId: Int, sender: ActorRef[Seq[StudentIteration]])
      extends StudentIterationProtocol

  @message case class FindByStudentId(studentId: Int, sender: ActorRef[Seq[StudentIteration]])
    extends StudentIterationProtocol

  @message case class Delete(iterationId: Int, studentId: Int, sender: ActorRef[Option[StudentIteration]])
      extends StudentIterationProtocol
}
