package service.protocols

import akka.typed.ActorRef

sealed trait GuardianProtocol

object GuardianProtocol {

  case class GetEmploymentActor(sender: ActorRef[ActorRef[EmploymentProtocol]]) extends GuardianProtocol

  case class GetEmploymentTypeActor(sender: ActorRef[ActorRef[EmploymentTypeProtocol]]) extends GuardianProtocol

  case class GetFeedbackActor(sender: ActorRef[ActorRef[FeedbackProtocol]]) extends GuardianProtocol

  case class GetIterationActor(sender: ActorRef[ActorRef[IterationProtocol]]) extends GuardianProtocol

  case class GetStudentIterationActor(sender: ActorRef[ActorRef[StudentIterationProtocol]]) extends GuardianProtocol

  case class GetMentorActor(sender: ActorRef[ActorRef[MentorProtocol]]) extends GuardianProtocol

  case class GetSkillActor(sender: ActorRef[ActorRef[SkillProtocol]]) extends GuardianProtocol

  case class GetStudentActor(sender: ActorRef[ActorRef[StudentProtocol]]) extends GuardianProtocol

  case class GetStudentTypeActor(sender: ActorRef[ActorRef[StudentTypeProtocol]]) extends GuardianProtocol

  case class GetStudentReportActor(sender: ActorRef[ActorRef[StudentReportProtocol]]) extends GuardianProtocol

  case class GetStudentWithDataActor(sender: ActorRef[ActorRef[StudentWithDataProtocol]]) extends GuardianProtocol

  case class GetUserActor(sender: ActorRef[ActorRef[UserProtocol]]) extends GuardianProtocol

  case class GetResultTypeActor(sender: ActorRef[ActorRef[ResultTypeProtocol]]) extends GuardianProtocol

  case class GetEmploymentReportActor(sender: ActorRef[ActorRef[EmploymentReportProtocol]]) extends GuardianProtocol

  case class GetStudentDataActor(sender: ActorRef[ActorRef[StudentDataProtocol]]) extends GuardianProtocol
}
