package service.protocols

import java.time.Instant

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.Iteration

sealed trait IterationProtocol

object IterationProtocol {

  @message case class Create(iteration: Iteration, sender: ActorRef[Iteration]) extends IterationProtocol

  @message case class Update(iteration: Iteration, sender: ActorRef[Option[Iteration]]) extends IterationProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[Iteration]]) extends IterationProtocol

  @message case class FindAll(
      startDate: Option[Instant] = None,
      endDate: Option[Instant] = None,
      sender: ActorRef[Seq[Iteration]]
  ) extends IterationProtocol

  @message case class FindByEmploymentId(
      employmentId: Int,
      startDate: Option[Instant] = None,
      endDate: Option[Instant] = None,
      sender: ActorRef[Seq[Iteration]]
  ) extends IterationProtocol

  @message case class FindByMentorId(mentorId: Int, sender: ActorRef[Seq[Iteration]]) extends IterationProtocol

  @message case class FindByStudentId(studentId: Int, sender: ActorRef[Seq[Iteration]]) extends IterationProtocol

  @message case class CountByEmploymentId(
      employmentId: Int,
      startDate: Option[Instant] = None,
      endDate: Option[Instant] = None,
      sender: ActorRef[Int]
  ) extends IterationProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[Iteration]]) extends IterationProtocol
}
