package service.protocols

import java.time.Instant

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.reports.StudentReport

sealed trait StudentReportProtocol

object StudentReportProtocol {

  @message case class GetStudentReport(
      studentId: Int,
      startDate: Option[Instant],
      endDate: Option[Instant],
      sender: ActorRef[Option[StudentReport]]
  ) extends StudentReportProtocol
}
