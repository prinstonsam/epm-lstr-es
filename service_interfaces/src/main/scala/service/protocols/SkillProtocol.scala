package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.Skill

sealed trait SkillProtocol

object SkillProtocol {

  @message case class Create(skill: Skill, sender: ActorRef[Skill]) extends SkillProtocol

  @message case class Update(skill: Skill, sender: ActorRef[Option[Skill]]) extends SkillProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[Skill]]) extends SkillProtocol

  case class FindAll(sender: ActorRef[Seq[Skill]]) extends SkillProtocol

  @message case class FindByStudentId(studentId: Int, sender: ActorRef[Seq[Skill]]) extends SkillProtocol

  @message case class FindByMentorId(mentorId: Int, sender: ActorRef[Seq[Skill]]) extends SkillProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[Skill]]) extends SkillProtocol

}
