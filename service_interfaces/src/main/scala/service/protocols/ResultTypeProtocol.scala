package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.ResultType

sealed trait ResultTypeProtocol

object ResultTypeProtocol {

  @message case class Create(resultType: ResultType, sender: ActorRef[ResultType]) extends ResultTypeProtocol

  @message case class Update(studentType: ResultType, sender: ActorRef[Option[ResultType]]) extends ResultTypeProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[ResultType]]) extends ResultTypeProtocol

  case class FindAll(sender: ActorRef[Seq[ResultType]]) extends ResultTypeProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[ResultType]]) extends ResultTypeProtocol

}
