package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.StudentData

trait StudentDataProtocol

object StudentDataProtocol {

  @message case class FindById(studentId: Int, sender: ActorRef[Seq[StudentData]]) extends StudentDataProtocol

  @message case class Create(studentData: StudentData, sender: ActorRef[StudentData]) extends StudentDataProtocol
}
