package service.protocols

import akka.typed.ActorRef
import dto.Employment
import com.epam.education.management.utils.message

sealed trait EmploymentProtocol

object EmploymentProtocol {

  @message case class Create(employment: Employment, sender: ActorRef[Employment]) extends EmploymentProtocol

  @message case class Update(employment: Employment, sender: ActorRef[Option[Employment]]) extends EmploymentProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[Employment]]) extends EmploymentProtocol

  @message case class FindByTypeId(employmentTypeId: Int, sender: ActorRef[Seq[Employment]]) extends EmploymentProtocol

  case class FindAll(sender: ActorRef[Seq[Employment]]) extends EmploymentProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[Employment]]) extends EmploymentProtocol
}
