package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.Feedback

sealed trait FeedbackProtocol

object FeedbackProtocol {

  @message case class Create(feedback: Feedback, sender: ActorRef[Feedback]) extends FeedbackProtocol

  @message case class Update(feedback: Feedback, sender: ActorRef[Option[Feedback]]) extends FeedbackProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[Feedback]]) extends FeedbackProtocol

  case class FindAll(sender: ActorRef[Seq[Feedback]]) extends FeedbackProtocol

  @message case class FindByMentorId(mentorId: Int, sender: ActorRef[Seq[Feedback]]) extends FeedbackProtocol

  @message case class FindByStudentId(studentId: Int, sender: ActorRef[Seq[Feedback]]) extends FeedbackProtocol

  @message case class FindByStudentIdIterationId(studentId: Int, iterationId: Int, sender: ActorRef[Seq[Feedback]])
      extends FeedbackProtocol

  @message case class FindByIterationId(iterationId: Int, sender: ActorRef[Seq[Feedback]]) extends FeedbackProtocol

  @message case class CountByIterationId(iterationId: Int, sender: ActorRef[Int]) extends FeedbackProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[Feedback]]) extends FeedbackProtocol

  @message case class CountByStudentId(studentId: Int, sender: ActorRef[Int]) extends FeedbackProtocol

}
