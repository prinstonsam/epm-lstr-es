package service.protocols

import java.time.Instant

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.StudentWithData

trait StudentWithDataProtocol

object StudentWithDataProtocol {

  @message case class GetByStudentId(
      studentId: Int,
      date: Option[Instant] = None,
      sender: ActorRef[Option[StudentWithData]]
  ) extends StudentWithDataProtocol

  @message case class FindAll(date: Option[Instant] = None, sender: ActorRef[Seq[StudentWithData]])
      extends StudentWithDataProtocol

  @message case class Create(studentWithData: StudentWithData, sender: ActorRef[StudentWithData])
      extends StudentWithDataProtocol
}
