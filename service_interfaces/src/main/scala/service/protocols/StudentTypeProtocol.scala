package service.protocols

import akka.typed.ActorRef
import com.epam.education.management.utils.message
import dto.StudentType

sealed trait StudentTypeProtocol

object StudentTypeProtocol {

  @message case class Create(studentType: StudentType, sender: ActorRef[StudentType]) extends StudentTypeProtocol

  @message case class Update(studentType: StudentType, sender: ActorRef[Option[StudentType]]) extends StudentTypeProtocol

  @message case class GetById(id: Int, sender: ActorRef[Option[StudentType]]) extends StudentTypeProtocol

  case class FindAll(sender: ActorRef[Seq[StudentType]]) extends StudentTypeProtocol

  @message case class Delete(id: Int, sender: ActorRef[Option[StudentType]]) extends StudentTypeProtocol

}
